function applytopup_call = acid_applytopup_call()


% source images
applytopup_sources         = cfg_files;
applytopup_sources.tag     = 'applytopup_sources';
applytopup_sources.name    = 'Images (--imain)';
applytopup_sources.help    = {'Select source images (4D NIfTI).'};
applytopup_sources.filter  = 'image';
applytopup_sources.ufilter = '.*';
applytopup_sources.num     = [0 2];


% topup images
applytopup_topup         = cfg_files;
applytopup_topup.tag     = 'applytopup_topup';
applytopup_topup.name    = 'Topup images (--topup)';
applytopup_topup.help    = {'Select topup images (4D NIfTI).'};
applytopup_topup.filter  = 'image';
applytopup_topup.ufilter = '.*';
applytopup_topup.num     = [0 1];

% % datain
% applytopup_inindex         = cfg_files;
% applytopup_inindex.tag     = 'applytopup_inindex';
% applytopup_inindex.name    = 'Data input file (--inindex)';
% applytopup_inindex.help    = {'Select the inindex txt file.'};
% applytopup_inindex.filter  = 'txt';
% applytopup_inindex.ufilter = '.*';
% applytopup_inindex.num     = [0 1];


applytopup_inindex         = cfg_entry;
applytopup_inindex.tag     = 'applytopup_inindex';
applytopup_inindex.name    = '--inindex';
applytopup_inindex.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
applytopup_inindex.strtype = 'e';
applytopup_inindex.num     = [1 Inf];

% datain
applytopup_datain         = cfg_files;
applytopup_datain.tag     = 'applytopup_datain';
applytopup_datain.name    = 'Data input file (--datain)';
applytopup_datain.help    = {'Select the datain txt file.'};
applytopup_datain.filter  = 'txt';
applytopup_datain.ufilter = '.*';
applytopup_datain.num     = [0 1];



% % config
% applytopup_config         = cfg_files;
% applytopup_config.tag     = 'applytopup_config';
% applytopup_config.name    = 'config file (--config)';
% applytopup_config.help    = {'Select the config txt file.'};
% applytopup_config.filter  = 'txt';
% applytopup_config.ufilter = '.*';
% applytopup_config.num     = [0 1];


% % output directory
% fsl_dir         = cfg_files;
% fsl_dir.tag     = 'fsl_dir';
% fsl_dir.name    = 'FSL directory';
% fsl_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% fsl_dir.filter  = 'dir';
% fsl_dir.ufilter = '.*';
% fsl_dir.num     = [0 1];
% % fsl_dir.val     = {{''}};


% % output directory
% applytopup_input_dir         = cfg_files;
% applytopup_input_dir.tag     = 'applytopup_input_dir';
% applytopup_input_dir.name    = 'Input data directory';
% applytopup_input_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% applytopup_input_dir.filter  = 'dir';
% applytopup_input_dir.ufilter = '.*';
% applytopup_input_dir.num     = [0 1];
% applytopup_input_dir.val     = {{''}};


applytopup_method        = cfg_menu;
applytopup_method.tag    = 'applytopup_method';
applytopup_method.name   = '--method';
applytopup_method.help   = {''};
applytopup_method.labels = {
    'jac'
    'lsr'
    }';
applytopup_method.values = {0 1};
applytopup_method.val    = {0};





% file name
terminal_call         = cfg_entry;
terminal_call.tag     = 'terminal_call';
terminal_call.name    = 'Additonal applytopup input parameter';
terminal_call.help    = {''};
terminal_call.strtype = 's';
terminal_call.val     = {''};



%% 
applytopup_call         = cfg_exbranch;
applytopup_call.tag     = 'applytopup_call';
applytopup_call.name    = 'applytopup';
applytopup_call.val     = { applytopup_sources, applytopup_datain, applytopup_inindex, applytopup_topup, applytopup_method, terminal_call};
applytopup_call.help    = { '' };
applytopup_call.prog = @local_applytopup;
applytopup_call.vout = @vout_applytopup;


end

function out = local_applytopup(job)



terminal_call = char(job.terminal_call);

[input_dir,input_name,~] = spm_fileparts(char(job.applytopup_sources(1)));


V = acid_load_4Dimage(char(job.applytopup_sources));

keyword = 'applytopup';

p_out = acid_bids(input_dir,input_name,keyword,1);

fname = acid_bids_filename(V,keyword,'_dwi','');

output_name = [p_out filesep fname(1:end-4)];

cd(input_dir)

imain = char(job.applytopup_sources);

if job.applytopup_method == 0
    dummy_method = 'jac';
else
    dummy_method = 'lsr';
end

inindex = job.applytopup_inindex;
inindex = strjoin(string(inindex), ',');
inindex = char(inindex);

dummy_topup = char(job.applytopup_topup);


%--imain=my_hifi_b0_brain_mask --mask=my_hifi_b0_brain_mask --acqp=datain.txt --config=config.txt  --out=applytopup_corrected_data
command = ['applytopup' ' --imain=' imain(1,1:end-2) ',' imain(2,1:end-2) ' --datain=' char(job.applytopup_datain) ' --inindex=' inindex ' --topup=' char(dummy_topup(1:end-6)) ' --out=' output_name ' --method=' dummy_method ' '  terminal_call];

[status,cmdout] = system(command);


disp(status)
disp(cmdout)


command_fields = strsplit(terminal_call);

out_config = find(contains(command_fields, '--out'));

if ~isempty(out_config) 
    extracted_value = command_fields{out_config};
else
    extracted_value = '';
    warning('Parameter "--out" not found!');
end

applytopup_output = extracted_value(7:end);

unzip_file = [applytopup_output '.nii.gz'];

unzip_command = ['gzip -dk -f ' unzip_file];

[status,cmdout] = system(unzip_command);

disp(status)
disp(cmdout)

out.rsource{1} = [input_dir filesep applytopup_output '.nii,1'];


end

function dep = vout_applytopup(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'applytopup processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

end