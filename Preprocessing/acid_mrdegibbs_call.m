function mrdegibbs_call = acid_mrdegibbs_call()


% source images
mrdegibbs_sources         = cfg_files;
mrdegibbs_sources.tag     = 'mrdegibbs_sources';
mrdegibbs_sources.name    = 'Images (--in)';
mrdegibbs_sources.help    = {'Select source images (4D NIfTI).'};
mrdegibbs_sources.filter  = 'any';
mrdegibbs_sources.ufilter = 'nii';
mrdegibbs_sources.num     = [0 1];



% % datain
% mrdegibbs_datain         = cfg_files;
% mrdegibbs_datain.tag     = 'mrdegibbs_datain';
% mrdegibbs_datain.name    = 'Data input file (--datain)';
% mrdegibbs_datain.help    = {'Select the datain txt file.'};
% mrdegibbs_datain.filter  = 'txt';
% mrdegibbs_datain.ufilter = '.*';
% mrdegibbs_datain.num     = [0 1];
% 
% 
% 
% % config
% mrdegibbs_config         = cfg_files;
% mrdegibbs_config.tag     = 'mrdegibbs_config';
% mrdegibbs_config.name    = 'config file (--config)';
% mrdegibbs_config.help    = {'Select the config txt file.'};
% mrdegibbs_config.filter  = 'txt';
% mrdegibbs_config.ufilter = '.*';
% mrdegibbs_config.num     = [0 1];


% % output directory
% fsl_dir         = cfg_files;
% fsl_dir.tag     = 'fsl_dir';
% fsl_dir.name    = 'FSL directory';
% fsl_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% fsl_dir.filter  = 'dir';
% fsl_dir.ufilter = '.*';
% fsl_dir.num     = [0 1];
% % fsl_dir.val     = {{''}};


% % output directory
% mrdegibbs_input_dir         = cfg_files;
% mrdegibbs_input_dir.tag     = 'mrdegibbs_input_dir';
% mrdegibbs_input_dir.name    = 'Input data directory';
% mrdegibbs_input_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% mrdegibbs_input_dir.filter  = 'dir';
% mrdegibbs_input_dir.ufilter = '.*';
% mrdegibbs_input_dir.num     = [0 1];
% mrdegibbs_input_dir.val     = {{''}};



% file name
terminal_call         = cfg_entry;
terminal_call.tag     = 'terminal_call';
terminal_call.name    = 'Additonal mrdegibbs input parameter';
terminal_call.help    = {''};
terminal_call.strtype = 's';
terminal_call.val     = {''};



%% satrtup
mrdegibbs_call         = cfg_exbranch;
mrdegibbs_call.tag     = 'mrdegibbs_call';
mrdegibbs_call.name    = 'mrdegibbs';
mrdegibbs_call.val     = { mrdegibbs_sources, terminal_call};
mrdegibbs_call.help    = { '' };
mrdegibbs_call.prog = @local_mrdegibbs;
mrdegibbs_call.vout = @vout_mrdegibbs;


end

function out = local_mrdegibbs(job)



terminal_call = char(job.terminal_call);

[input_dir,input_name,~] = spm_fileparts(char(job.mrdegibbs_sources));


V = acid_prepare_and_load_4Dimage(char(job.mrdegibbs_sources));

keyword = 'mrdegibbs';

p_out = acid_bids(input_dir,input_name,keyword,1);

fname = acid_bids_filename(V,keyword,'_dwi','');

output_name = [p_out filesep fname(1:end-4) '.nii'];

cd(input_dir)

imain = char(job.mrdegibbs_sources);

%--imain=my_hifi_b0_brain_mask --mask=my_hifi_b0_brain_mask --acqp=datain.txt --config=config.txt  --out=mrdegibbs_corrected_data
command = ['mrdegibbs ' imain ' ' output_name ' ' terminal_call];

[status,cmdout] = system(command);


disp(status)
disp(cmdout)


command_fields = strsplit(terminal_call);

out_config = find(contains(command_fields, '--out'));

if ~isempty(out_config) 
    extracted_value = command_fields{out_config};
else
    extracted_value = '';
    warning('Parameter "--out" not found!');
end

mrdegibbs_output = extracted_value(7:end);

unzip_file = [mrdegibbs_output '.nii.gz'];

unzip_command = ['gzip -dk -f ' unzip_file];

[status,cmdout] = system(unzip_command);

disp(status)
disp(cmdout)

out.rsource{1} = [output_name ',1'];


end

function dep = vout_mrdegibbs(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'mrdegibbs processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

end