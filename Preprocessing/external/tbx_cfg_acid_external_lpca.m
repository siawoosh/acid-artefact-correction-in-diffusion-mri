function lpca_call = tbx_cfg_acid_external_lpca




% source images
lpca_sources         = cfg_files;
lpca_sources.tag     = 'lpca_sources';
lpca_sources.name    = 'Input images';
lpca_sources.help    = {'Select source images (4D NIfTI).'};
lpca_sources.filter  = 'any';
lpca_sources.ufilter = 'nii';
lpca_sources.num     = [0 Inf];


% number of workers for parallel programming (npools)
npool         = cfg_entry;
npool.tag     = 'npool';
npool.name    = 'Number of workers (parallel programming)';
npool.help    = {'Specify number of workers for parallel computation. A value of 1 indicates that no parallel computing is used.'};
npool.strtype = 'e';
npool.num     = [1 1];
npool.val     = {1};


%% satrtup
lpca_call         = cfg_exbranch;
lpca_call.tag     = 'lpca_call';
lpca_call.name    = 'LPCA';
lpca_call.val     = {lpca_sources, npool};
lpca_call.help    = { '' };
lpca_call.prog = @local_lpca;
lpca_call.vout = @vout_lpca;
end

function out = local_lpca(job)
           
        files = char(job.lpca_sources);

        [input_dir,input_name,~] = spm_fileparts(files(1,:));


        dependency_output = acid_LPCA_call(input_name,input_dir,job,job.npool);

        out.rsource{1} = [dependency_output];


end


function dep = vout_lpca(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'LPCA processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end