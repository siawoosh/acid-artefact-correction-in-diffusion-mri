function [dummy_4D, fname_out, bvals, bvecs] = acid_poas(bvecs, bvals, kstar, kappa0_type, lambda, sigma, ncoils, P, PMSK, dummy_4D, rounding_tolerance, dummy_automated_kappa)

% Implementation of msPOAS
% Becker et al. (2014) NeuroImage 95, 90?105
% http://dx.doi.org/10.1016/j.neuroimage.2014.03.053

% Original implementation in R-package dti
% J. Polzehl August 2013



[V,P_uncomp,dummy_compress] = acid_prepare_and_load_4Dimage(P);


% This is to check whether the data are 3D or 4D
% V = acid_load_4Dimage(P);

% define output directory
keyword = 'msPOAS';
[path,fname,~] = spm_fileparts(V(1).fname);

p_out = acid_bids(path,fname,keyword,1);


diary([p_out filesep 'logfile_' keyword '.txt'])

time1 = tic;
disp('Start initializations');

% defaults
% TODO: check, whether these go to the defaults file
level      = 10;   % JP: threshold for S0 images to define head mask, needs to be adjusted
tolgrad    = 0.01; % tolerance for normalizing gradient directions
interp_def = -7;   % SM: interpolation kernel for reslicing the data

% Look up table for Gauss approximation of non-central chi
% columns NCP (1), ExpV (2), SD (3) and Var (4)
load 'acid_poas_sofmchitable.mat' sofmchitable;
if ncoils > 32
    warning('No lookup table for ncoils larger then 32! set ncoils to 1');
    ncoils = 1;
end
sofmchitable = sofmchitable(:, ncoils, :);

% Determine indices of diffusion and non-diffusion weighted images
% JP: slightly different b-values should be identified,
%     currently this needs to be done manually when
%     specifying the b-values
% TODO: find a better way

% load in 4D image
% V = acid_load_4Dimage(P);

% check bvals/bvecs input
if isempty(bvals)
    error('No b-values have been provided. Check for correct input.')
elseif size(bvals,2)~=size(V,1)
    bvals = bvals';
    if size(bvals,2)~=size(V,1)
        error('The number of source images and b-value entries has to be the same.');
    end
end 

if isempty(bvecs)
    error('No b-vectors have been provided. Check for correct input.')
elseif size(bvecs,2)~=size(V,1)
    bvecs = bvecs';
    if size(bvecs,2)~=size(V,1)
        error('The number of source images and b-vector entries has to be the same');
    end
end

bvals = round(bvals/rounding_tolerance)*rounding_tolerance;

b0     = min(bvals);
MSK_b0 = find(bvals == b0);
MSK_b  = find(bvals > b0);

if size(MSK_b0,2) > (size(bvals,2)/2)
    warning('More than 50% of your b-values were set to zero! Plesase check whether you used um/ms as units and did not adjust the rounding_tolerance in the defaults!');
end

[b_unique,~,~] = unique(bvals(MSK_b));

n = histc(bvals(MSK_b),b_unique);

disp(['Measured shells:                    ' num2str(b_unique)]);
disp(['Number of b-vectors for each shell: ' num2str(n)]);

if min(n) < 3
    error('One diffusion shell contains less then three diffusion directions!')
end

% Spatial dimension of the volumes
ddim = V(MSK_b0(1)).dim;

%% sigma
if isa(sigma,'char')

    sigma_struct = spm_vol(sigma);

    sigma = acid_read_vols(sigma_struct(1), V(MSK_b0(1)), interp_def);

    sigma_displayed = mean(sigma,'all');

    disp(['Used sigma (mean): ' num2str(sigma_displayed)]);
else

    sigma_displayed = sigma;
    sigma = ones(ddim).*sigma;
    disp(['Used sigma: ' num2str(sigma_displayed)]);
end


%% kappa
if dummy_automated_kappa == 1
    % number_gradients = size(bvecs,2);
    mean_n = mean(n);
    kappa0 = kappa0_type * (acos(1-5/mean_n) + acos(1-10/mean_n)) / 2 ;
else
    kappa0 = kappa0_type;
end


disp(['Used kappa: ' num2str(kappa0)]);
disp(['Used lambda: ' num2str(lambda)]);

if kappa0 <= 0
    error('kappa should not be <= 0!');
end

if kappa0 >= 1
    error('kappa should not be >= 1!');
end


%% Perform smoothing


% Sanity check whether for each volume there is a b-value and a
% diffusion gradient vector
if (size(bvals,2) ~= size(bvecs,2))
    error('Number of b-values and diffusion gradient vectors do not match!');
end
if (size(bvals,2) ~= size(V,1))
    error('Number of b-values and volumes do not match!');
end

% Number of non-diffusion weighted images
ns0 = length(MSK_b0);

% A0 is an 4D-array for the non-diffusion weigthed images
% SM: Note that the b0 images are resampled to the first b0 image,
%     ensuring that previous mat-files are encountered before averaging
%     all b0 images.
A0 = zeros([ddim ns0]);
kk = 1;
for i = MSK_b0
    for p = 1:ddim(3)
        M = spm_matrix([0 0 -p 0 0 0 1 1 1]);
        M1 = inv(M * inv(V(MSK_b0(1)).mat) * V(i).mat); % TODO: resolve inv()
        A0(:, :, p, kk)  = spm_slice_vol(V(i), M1, ddim(1:2), interp_def);
    end
    kk = kk + 1;
end
A0  = single(A0);

% Determine mean non-diffusion weigthed image
% scaled with the noise parameter sigma
s0factor = sqrt(ns0); % JP: needed for rescaling of estimated s0
s0       = sum(A0./sigma, 4) / s0factor; % JP: this keeps the variance the same as for A0./sigma
ws0      = 1/ns0;
clear A0; % no longer needed

% si is an 4D-array for the diffusion weigthed images
% SM: All DW images are resliced with respect to the first image
si = zeros([ddim length(MSK_b)]);
kk = 1;
for i = MSK_b
    for p = 1:ddim(3)
        M = spm_matrix([0 0 -p 0 0 0 1 1 1]);
        M1 = inv(M * inv(V(MSK_b0(1)).mat) * V(i).mat);  % TODO: resolve inv()
        si(:, :, p, kk) = spm_slice_vol(V(i), M1, ddim(1:2), interp_def);
    end
    kk = kk + 1;
end
si = single(si);

% scaled with the noise parameter sigma
si = si ./ sigma;

% Normalize gradients
% TODO: check
normgrad = sqrt(sum(bvecs .* bvecs, 1)); %JP: need norm not norm^2
if(~isempty(find(normgrad ~= 1)))
    if(~isempty(find((normgrad-1) > tolgrad))) % SM: if norm of gradients is within the tolerance message won't be visible
        warning('Diffusion gradients have been manually normalised!');
    end
    MSKgrad = find(normgrad > 0);
    bvecs(:, MSKgrad) = bsxfun(@rdivide, bvecs(:, MSKgrad), normgrad(MSKgrad));
end
clear normgrad;

% Find all diffusion gradient vectors
grad = bvecs(:, MSK_b);
n3g = acid_poas_getnext3g(bvecs, bvals);
%    nshell = n3g{1}+1;
% JP n3g is a list (cell structure) with 5 components
%    n3g{1} = nbv;  number of DW shells (without b0)
%    n3g{2} = bv;   bvals
%    n3g{3} = ubv;  unique bvals
%    n3g{4} = ind;  index array of corners for triangles containg the
%                   gradient indices (0 - (ngrad-1)
%    n3g{5} = w;    weights for spherical interpolation

% Read brain mask
% SM: interpolation to the first b0 image - using the spm-mat files
% TODO: Improve handling of mask if not given! Change default?
if exist('PMSK', 'var') && ~isempty(PMSK)
    VMSK = spm_vol(PMSK);
    if(size(VMSK, 1) > 1)
        warning('Currently, only the first mask image is used!');
    end
    mask = acid_read_vols(VMSK(1), V(MSK_b0(1)), interp_def);
    mask = mask > 0;
else
    mask = [];
end
if(isempty(mask))
    mask = s0 > (level/mean(sigma,'all')*s0factor);% JP: *s0factor to scale back to range of s0
end
dmask = zeros(size(mask), 'single');
dmask(mask) = 1;

% Initializations
ni0  = ones(size(s0), 'single');
ni   = ones(size(si), 'single');
vxg  = sqrt(sum(V(1).mat(1:3, 1:3).^2)); % [yvoxel/xvoxel zvoxel/xvoxel]
vext = [vxg(2)/vxg(1) vxg(3)/vxg(1)];
gradstats = acid_poas_getkappasmsh3(grad, n3g{1}, n3g{2}, n3g{3});
[hseqi, nw] = acid_poas_gethseqfullse3msh(kstar, gradstats, kappa0, vext);
nind = int16(nw * 1.25);
zth  = ones(size(si), 'single');
zth0 = ones(size(s0), 'single');
if lambda < 1d10
    kinit = 0;
else
    disp('Performing non-adaptive smoothing for last step only');
    kinit = kstar;
end

% etime1 = toc(time1);

% T = toc(time1)/60;
% 
% T = duration(minutes(T),'format','hh:mm:ss');
% 
% 
% disp(['End initializations: Elapsed time ' char(T) '.']);
disp(['Starting first of ' num2str(kstar) ' iterations']);

% Perform POAS iteration
spm_progress_bar('Init', kstar, 'Running msPOAS', 'iteration');
for k = kinit:kstar
    time2 = tic;
    hakt = hseqi(:,k+1);
    [msth, msni, msth0, msni0] = acid_poas_interpolatesphere0(zth, zth0, ni, ni0, n3g, mask);
    clear zth zth0; % save memory, will be recomputed before next read
    [parind, parw, parnind] = acid_poas_lkfullse3msh(hakt, kappa0./hakt, gradstats, vext, nind);
    hakt0 = mean(hakt);
    [parind0, parw0, parnind0] = acid_c_poas_lkfulls0(hakt0, vext, nind); % mex
    parind0 = parind0(:, 1:parnind0);
    parw0 = parw0(1:parnind0);
    vmsth = acid_c_poas_linterpol(msth, sofmchitable, 2, 4)/2;
    vmsth = reshape(vmsth,size(msth));
    vmsth0 = acid_c_poas_linterpol(msth0, sofmchitable, 2, 4)/2;
    vmsth0 = reshape(vmsth0, size(msth0));
    [nni, zth, nni0, zth0] = acid_c_poas_adsmse3ms(si, s0, msth, msni, msth0, msni0, vmsth, vmsth0, dmask, lambda, ws0, parind, parw, parnind, parind0, parw0, parnind0);
    clear msth msni msth0 msni0 vmsth vmsth0; % save memory, will be recomputed before next read
    ni = max(ni, nni);
    ni0 = max(ni0, nni0);
    clear nni nni0; % save memory, will be recomputed before next read

    % etime2 = toc(time2);

    T = toc(time2)/60;

    T = duration(minutes(T),'format','hh:mm:ss.SSS');

    disp(['Iteration ' num2str(k) ' completed in ' char(T) '.']);

    spm_progress_bar('Set', k);
end

% etime1 = toc(time1);

T = toc(time1)/60;

T = duration(minutes(T),'format','hh:mm:ss');

disp(['End of iterations: Elapsed time ' char(T) '.']);
spm_progress_bar('Clear');

disp('Now collecting and writing results');

% rescale th0 image with sigma/s0factor to get correct scale
zth0 = zth0./s0factor.*sigma ;

% Rescale results to original range
zth = zth.*sigma;
if ~dummy_4D
    for i = 1:numel(MSK_b)
        acid_write_vol(zth(:,:,:,i), V(MSK_b(i)), p_out, keyword, '_dwi');
    end
    
    % b0 image
    for i = 1:numel(MSK_b0)
        acid_write_vol(zth0, V(MSK_b0(i)), p_out, [keyword '-b0'], '_dwi');
        V4D = [];
    end
else
    zth_out = zeros(ddim(1,1), ddim(1,2), ddim(1,3), size(V,1));
    % zth = cat(4,zth0,zth);
    for i = 1:numel(MSK_b0)
        j = MSK_b0(1,i);
        zth_out(:,:,:,j) = zth0;    
    end

    for i = 1:numel(MSK_b)
        j = MSK_b(1,i);
        zth_out(:,:,:,j) = zth(:,:,:,i);    
    end
    % TODO: do we need a special POAS function here?
    V4D = acid_write_vol(zth_out, V(MSK_b0(1)), p_out, keyword, '_dwi');
end

% save json file
acid_save_json(V(1), p_out, keyword);

% % generate and save new bvals and bvecs files
% bvals_new = [bvals(1, MSK_b0(1)) bvals(1, find(bvals(1, :) > b0))];
% bvecs_new = cat(2, bvecs(:, MSK_b0(1)), bvecs(:, find(bvals(1, :) > b0)));

fname = acid_bids_filename(V(1),keyword,'_dwi','.bval');
fname = [p_out filesep fname];
dlmwrite(fname, bvals);

fname = acid_bids_filename(V(1),keyword,'_dwi','.bvec');
fname = [p_out filesep fname];
dlmwrite(fname, bvecs);

[fname_out] = acid_compress_and_delete_4Dimage(dummy_compress, V4D(1,:).fname, P_uncomp);

etime1 = toc(time1);

T = toc(time1)/60;

T = duration(minutes(T),'format','hh:mm:ss');

disp(['Finished msPOAS: Total elapsed time ' char(T) '.']);

diary off
end