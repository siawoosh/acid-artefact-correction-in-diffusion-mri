function poas = tbx_cfg_acid_poas(in_vols, mask, bvals_bvecs)

kstar         = cfg_entry;
kstar.tag     = 'kstar';
kstar.name    = 'k star';
kstar.help    = {'Number of iterations. Typically 10 to 20. Defines maximum size of neighborhood for msPOAS.'};
kstar.strtype = 'e';
kstar.num     = [1 1];
kstar.val     = {1e1};

kappa_scaling         = cfg_entry;
kappa_scaling.tag     = 'kappa_scaling';
kappa_scaling.name    = 'Automatically determined: Scaling factor for the automatically determined kappa';
kappa_scaling.help    = {'Scaling factor for the automated determined angular resolution. Change with care! (kappa = estimated_kappa * scaling_factor)'};
kappa_scaling.strtype = 'e';
kappa_scaling.num     = [1 1];
kappa_scaling.val     = {1};

kappa         = cfg_entry;
kappa.tag     = 'kappa';
kappa.name    = 'Manual input: kappa';
kappa.help    = {'Change with care!'};
kappa.strtype = 'e';
kappa.num     = [1 1];
kappa.val     = {0.8};

kappa_type        = cfg_choice;
kappa_type.tag    = 'kappa_type';
kappa_type.name   = 'Choose kappa input type';
kappa_type.help   = {''};
kappa_type.values = {kappa_scaling, kappa};
kappa_type.val    = {kappa_scaling};

lambda         = cfg_entry;
lambda.tag     = 'lambda';
lambda.name    = 'lambda';
lambda.help    = {'Adpatation parameter'};
lambda.strtype = 'e';
lambda.num     = [1 1];
lambda.val    = {10};

sigma_file         = cfg_files;
sigma_file.tag     = 'sigma_file';
sigma_file.name    = 'File (*.txt / *.mat)';
sigma_file.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_file.filter  = 'any';
sigma_file.ufilter = '.*txt';
sigma_file.num     = [0 1];

sigma_exp         = cfg_entry;
sigma_exp.tag     = 'sigma_exp';
sigma_exp.name    = 'Expression/Dependency';
sigma_exp.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_exp.strtype = 'e';
sigma_exp.num     = [Inf Inf];

sigma_map         = cfg_files;
sigma_map.tag     = 'sigma_map';
sigma_map.name    = 'Noise map (*.nii)';
sigma_map.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_map.filter  = 'any';
sigma_map.ufilter = 'nii';
sigma_map.num     = [0 1];

sigma_type        = cfg_choice;
sigma_type.tag    = 'sigma_type';
sigma_type.name   = 'sigma (noise estimate)';
sigma_type.help   = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_type.values = {sigma_exp, sigma_map, sigma_file};
sigma_type.val    = {sigma_exp};

ncoils         = cfg_entry;
ncoils.tag     = 'ncoils';
ncoils.name    = 'ncoils';
ncoils.help    = {'Number of receiver coils. Effective number can be less!'};
ncoils.strtype = 'e';
ncoils.num     = [1 1];
ncoils.val     = {1e0};

poas         = cfg_exbranch;
poas.tag     = 'poas';
poas.name    = 'msPOAS';
poas.val     = {in_vols mask bvals_bvecs kstar sigma_type lambda kappa_type ncoils};
poas.help    = {
    'Changeable defaults: '
    '- kstar: Number of iterations. Typically 10 to 20. Defines maximum size of neighborhood for msPOAS.'
    '- kappa: Angular resolution. Change with care!'
    '- lambda: Adpatation parameter'
    '- sigma: Needs to be provided by background estimator or similar. SD parameter of noise distribution.'
    '- ncoils: Number of receiver coils. Effective number can be less!'
    };
poas.prog = @local_poas;
poas.vout = @vout_poas;

% POAS
poas_choice        = cfg_choice;
poas_choice.tag    = 'poas_choice';
poas_choice.name   = 'Choose POAS options';

poas_choice.help   = {
    'Sigma estimation'
    'POAS'
    }';
poas_choice.values = {poas};


    function out = local_poas(job)

        % read in bvals and bvecs
        if isfield(job.bvals_bvecs,'bvals_bvecs_file')

            job.poas_files = cell2mat(job.bvals_bvecs.bvals_bvecs_file);

            [~,~,e] = fileparts(job.poas_files(1,:));
            if ~isempty(job.poas_files(1,:))
                switch lower(e)
                    case '.mat'

                        if size(cell2mat(struct2cell(load(job.poas_files(1,:)))),1) == 1 || size(cell2mat(struct2cell(load(job.poas_files(1,:)))),2) == 1
                            job.poas_bvals = cell2mat(struct2cell(load(job.poas_files(1,:))));
                            job.poas_bvectors = cell2mat(struct2cell(load(job.poas_files(2,:))));
                        elseif size(cell2mat(struct2cell(load(job.poas_files(1,:)))),1) == 4 || size(cell2mat(struct2cell(load(job.poas_files(1,:)))),2) == 4
                            bval_bvec_dummy = cell2mat(struct2cell(load(job.poas_files(1,:))));
                            job.poas_bvectors = bval_bvec_dummy(2:4,:);
                            job.poas_bvals = bval_bvec_dummy(1,:);
                        else
                            error('Unexpected file extension!')
                        end

                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise

                        if size(dlmread(job.poas_files(1,:)),1) == 1 || size(dlmread(job.poas_files(1,:)),2) == 1
                            job.poas_bvals = dlmread(job.poas_files(1,:));
                            job.poas_bvectors = dlmread(job.poas_files(2,:));
                        elseif size(dlmread(job.poas_files(1,:)),1) == 3 || size(dlmread(job.poas_files(1,:)),2) == 3
                            job.poas_bvectors = dlmread(job.poas_files(1,:));
                            job.poas_bvals = dlmread(job.poas_files(2,:));
                        else
                            error('Unexpected file extension!')
                        end
                end
            else
                job.poas_bvals = '';
                job.poas_bvectors = '';
            end

        elseif isfield(job.bvals_bvecs,'bvals_bvecs_exp_type')




            if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvals_exp')

                if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp)
                    job.poas_bvals = job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp;
                else
                    job.poas_bvals = '';
                end
            end

            if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvecs_exp')
                if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp)
                    job.poas_bvectors = job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp;
                else
                    job.poas_bvectors = '';
                end
            end
        end

        rounding_tolerance = acid_get_defaults('bval.rounding_tolerance');

        if isfield(job.kappa_type,'kappa_scaling')
            job.kappa = job.kappa_type.kappa_scaling;
            dummy_automated_kappa = 1;
        else
            job.kappa = job.kappa_type.kappa;
            dummy_automated_kappa = 0;
        end

        % read in sigma
        if isfield(job.sigma_type, 'sigma_exp')
            sigma = job.sigma_type.sigma_exp;
        elseif isfield(job.sigma_type, 'sigma_file')
            if ~isempty(job.sigma_type.sigma_file{1})
                sigma = job.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        elseif isfield(job.sigma_type, 'sigma_map')
            sigma = job.sigma_type.sigma_map{1};
        end

        [~, fname_out, bval, bvec] = acid_poas(job.poas_bvectors, job.poas_bvals, job.kstar, job.kappa, job.lambda, sigma, job.ncoils, char(job.in_vols), char(job.mask), 1, rounding_tolerance, dummy_automated_kappa);

        out.rsource = {fname_out(1,:)};
        out.bval = bval;
        out.bvec = bvec;
    end

    function dep = vout_poas(~)
        dep(1)            = cfg_dep;
        dep(1).sname      = 'Smoothed images';
        dep(1).src_output = substruct('.','rsource');
        dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
        dep(2)            = cfg_dep;
        dep(2).sname      = 'b-values';
        dep(2).src_output = substruct('.','bval');
        dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
        dep(3)            = cfg_dep;
        dep(3).sname      = 'b-vectors';
        dep(3).src_output = substruct('.','bvec');
        dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});
    end
end