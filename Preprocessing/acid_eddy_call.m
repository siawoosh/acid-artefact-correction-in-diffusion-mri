function eddy_call = acid_eddy_call(bvals_bvecs)


% source images
eddy_sources         = cfg_files;
eddy_sources.tag     = 'eddy_sources';
eddy_sources.name    = 'Images (--imain)';
eddy_sources.help    = {'Select source images (4D NIfTI).'};
eddy_sources.filter  = 'image';
eddy_sources.ufilter = '.*';
eddy_sources.num     = [0 1];

% optional topup
eddy_topup         = cfg_files;
eddy_topup.tag     = 'eddy_topup';
eddy_topup.name    = 'Optional topup input: (--topup)';
eddy_topup.help    = {'Select topup images (4D NIfTI).'};
eddy_topup.filter  = 'image';
eddy_topup.ufilter = '.*';
eddy_topup.num     = [0 1];

%% b values / b vectors
bvals_bvecs_file         = cfg_files;
bvals_bvecs_file.tag     = 'bvals_bvecs_file';
bvals_bvecs_file.name    = 'Select b-values and b-vectors files (*.mat|txt|bval|bvec)';
bvals_bvecs_file.help    = {'Select two files specifying the b-values (.bval) and b-vectors (.bvec) of the N dMRI volumes.'
    'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
bvals_bvecs_file.filter  = 'any';
bvals_bvecs_file.ufilter = 'mat|txt|bval|bvec';
bvals_bvecs_file.num     = [0 2];

% mask
eddy_mask         = cfg_files;
eddy_mask.tag     = 'eddy_mask';
eddy_mask.name    = 'Mask (binary) (--mask)';
eddy_mask.help    = {'Select binary mask. Regions outside the mask will be ignored when obtaining the registration parameters.'};
eddy_mask.filter  = 'image';
eddy_mask.ufilter = '.*';
eddy_mask.num     = [0 1];


% acqparams
eddy_acqparams         = cfg_files;
eddy_acqparams.tag     = 'eddy_acqparams';
eddy_acqparams.name    = 'Acqparams file (--acqp)';
eddy_acqparams.help    = {'Select the acqparams txt file.'};
eddy_acqparams.filter  = 'txt';
eddy_acqparams.ufilter = '.*';
eddy_acqparams.num     = [0 1];



% index
eddy_index         = cfg_files;
eddy_index.tag     = 'eddy_index';
eddy_index.name    = 'Index file (--index)';
eddy_index.help    = {'Select the index txt file.'};
eddy_index.filter  = 'txt';
eddy_index.ufilter = '.*';
eddy_index.num     = [0 1];


% % output directory
% fsl_dir         = cfg_files;
% fsl_dir.tag     = 'fsl_dir';
% fsl_dir.name    = 'FSL directory';
% fsl_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% fsl_dir.filter  = 'dir';
% fsl_dir.ufilter = '.*';
% fsl_dir.num     = [0 1];
% % fsl_dir.val     = {{''}};


% % output directory
% eddy_input_dir         = cfg_files;
% eddy_input_dir.tag     = 'eddy_input_dir';
% eddy_input_dir.name    = 'Input data directory';
% eddy_input_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% eddy_input_dir.filter  = 'dir';
% eddy_input_dir.ufilter = '.*';
% eddy_input_dir.num     = [0 1];
% eddy_input_dir.val     = {{''}};



% file name
terminal_call         = cfg_entry;
terminal_call.tag     = 'terminal_call';
terminal_call.name    = 'Additonal eddy input parameter';
terminal_call.help    = {''};
terminal_call.strtype = 's';
terminal_call.val     = {''};



%% satrtup
eddy_call         = cfg_exbranch;
eddy_call.tag     = 'eddy_call';
eddy_call.name    = 'eddy';
eddy_call.val     = { eddy_sources, bvals_bvecs_file, eddy_topup, eddy_mask, eddy_acqparams, eddy_index, terminal_call};
eddy_call.help    = { '' };
eddy_call.prog = @local_eddy;
eddy_call.vout = @vout_eddy;


end

function out = local_eddy(job)


% read in bvals and bvecs
if isfield(job,'bvals_bvecs_file')

    job.files1 = job.bvals_bvecs_file{1,1};
    job.files2 = job.bvals_bvecs_file{2,1};
    [~,~,e] = fileparts(job.files1);

    if ~isempty(job.files1)
        switch lower(e)
            case '.mat'
                if size(cell2mat(struct2cell(load(job.files1))),1) == 1 && size(cell2mat(struct2cell(load(job.files2))),1) == 3
                    job.bvals = cell2mat(struct2cell(load(job.files1)));
                    job.bvecs = cell2mat(struct2cell(load(job.files2)));
                elseif size(cell2mat(struct2cell(load(job.files1))),1) == 3 && size(cell2mat(struct2cell(load(job.files2))),1) == 1
                    job.bvecs = cell2mat(struct2cell(load(job.files1)));
                    job.bvals = cell2mat(struct2cell(load(job.files2)));
                else
                    error('Unexpected file extension!')
                end

            case '.nii'
                error('Unexpected file extension!')
            otherwise
                if size(dlmread(job.files1),1) == 1 && size(dlmread(job.files2),1) == 3
                    job.bvals = dlmread(job.files1);
                    job.bvecs = dlmread(job.files2);
                elseif size(dlmread(job.files1),1) == 3 && size(dlmread(job.files2),1) == 1
                    job.bvecs = dlmread(job.files1);
                    job.bvals = dlmread(job.files2);
                else
                    error('Unexpected file extension!')
                end
        end
    else
        job.bvals = '';
        job.bvecs = '';
    end

elseif isfield(job,'bvals_bvecs_exp_type')

    if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvals_exp')
        if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp)
            job.bvals = job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp;
        else
            job.bvals = '';
        end
    end

    if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvecs_exp')
        if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp)
            job.bvecs = job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp;
        else
            job.bvecs = '';
        end
    end
end




terminal_call = char(job.terminal_call);

if ~isempty(char(job.eddy_topup))
    % eddy_topup = char(job.eddy_topup);
    [topup_dir,topup_name,~] = spm_fileparts(char(job.eddy_topup));

    eddy_topup_call = [' --topup=' topup_dir filesep topup_name(1:end-10)];

    % eddy_topup_call = [' --topup=' eddy_topup(1:end-6)];

else
    eddy_topup_call = '';
end





[input_dir,input_name,~] = spm_fileparts(char(job.eddy_sources));


V = acid_load_4Dimage(char(job.eddy_sources));

keyword = 'eddy';

p_out = acid_bids(input_dir,input_name,keyword,1);

fname = acid_bids_filename(V,keyword,'_dwi','');

output_name = [p_out filesep fname(1:end-4)];

bvals_name = [p_out filesep fname(1:end-8) '_bval.txt'];
bvecs_name = [p_out filesep fname(1:end-8) '_bvec.txt'];


dlmwrite(bvals_name, job.bvals,'delimiter',' ');
dlmwrite(bvecs_name, job.bvecs,'delimiter',' ');

cd(input_dir)

imain = char(job.eddy_sources);
mask  = char(job.eddy_mask);


%--imain=my_hifi_b0_brain_mask --mask=my_hifi_b0_brain_mask --acqp=acqparams.txt --index=index.txt  --out=eddy_corrected_data
command = ['eddy' ' --imain=' imain(1:end-2) ' --bvecs=' bvecs_name ' --bvals=' bvals_name eddy_topup_call ' --mask=' mask(1:end-2) ' --acqp=' char(job.eddy_acqparams)  ' --index=' char(job.eddy_index) ' --out=' output_name ' ' terminal_call];

[status,cmdout] = system(command);


disp(status)
disp(cmdout)


% command_fields = strsplit(terminal_call);
% 
% out_index = find(contains(command_fields, '--out'));
% 
% if ~isempty(out_index) 
%     extracted_value = command_fields{out_index};
% else
%     extracted_value = '';
%     warning('Parameter "--out" not found!');
% end
% 
% eddy_output = extracted_value(7:end);

eddy_output = output_name;

unzip_file = [eddy_output '.nii.gz'];

unzip_command = ['gzip -dk -f ' unzip_file];

[status,cmdout] = system(unzip_command);

disp(status)
disp(cmdout)

out.rsource{1} = [eddy_output '.nii,1'];

out.bvals = job.bvals;
out.bvecs = job.bvecs;


end

function dep = vout_eddy(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'Eddy processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(2)            = cfg_dep;
dep(2).sname      = 'b-values';
dep(2).src_output = substruct('.','bvals');
dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
dep(3)            = cfg_dep;
dep(3).sname      = 'b-vectors';
dep(3).src_output = substruct('.','bvecs');
dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});
end