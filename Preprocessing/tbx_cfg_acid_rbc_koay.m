function powrice = tbx_cfg_acid_rbc_koay(in_vols)

% number of channels
n_channel         = cfg_entry;
n_channel.tag     = 'n_channel';
n_channel.name    = 'Effective number of channels';
n_channel.help    = {'The effective number of channels scales with the parallel imaging factor, i.e., it is >= 2 for PAT=2.'};
n_channel.strtype = 'e';
n_channel.num     = [1 1];
n_channel.val     = {1e0};

% images
noise_map         = cfg_files;
noise_map.tag     = 'noise_map';
noise_map.name    = 'Noise map';
noise_map.help    = {'Select the noise map.'};
noise_map.filter  = 'image';
noise_map.ufilter = '.*';
noise_map.num     = [0 1];

%% call local resample function
powrice      = cfg_exbranch;
powrice.tag  = 'powrice';
powrice.name = 'Koays rician bias correction';
powrice.val  = {in_vols noise_map};
powrice.help = {
    'Rician bias correction on raw data using the M2 method described in Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531).'
    };
powrice.prog = @local_rbc;
powrice.vout = @vout_rbc;

end

function out = local_rbc(job)  

    opt = jm_debias_opt();

    input_images = char(job.in_vols);
    noise_map = char(job.noise_map);

    if strcmp(noise_map(end-1:end),',1')
        noise_map = noise_map(1:end-2);
    end

    if strcmp(input_images(end-1:end),',1')
        input_images = input_images(1:end-2);
    end

   fn_debias = jm_debias(input_images, noise_map, opt);

    out.files = {fn_debias};
end

function dep = vout_rbc(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Rician bias corrected';
    dep(1).src_output = substruct('.','files');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end