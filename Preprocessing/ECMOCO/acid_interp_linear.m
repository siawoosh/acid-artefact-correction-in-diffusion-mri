function fit1 = acid_interp_linear(y, x, steps, resMax)

% =========================================================================
% The script perform linear interpolation. The second slice up to the one
% before last slice is interpolated.
%
% Inputs:
%  y      - y datapoints
%  x      - x datapoints
%  steps  - step size on the x axis
%  resMax - equals to maxValueOfx - max(x)

% Outputs:
%  fit1   - interpolated datapoints on the y axis
%
% Created by S.Mohammadi 07/02/2012
% =========================================================================

xx     = x(1):steps:x(length(x))+resMax;
dsteps = round(size(xx,2)/size(x,2));
fit1   = zeros(1,size(xx,2));

for i = 1:size(x,2)-1
    
    % using linear function: f(x) = m*x+b
    m = (y(i+1)-y(i))/(x(i+1)-x(i));
    b = y(i) - m*x(i);
    fit1(dsteps*(i-1)+1) = y(i);
    for j=1:dsteps-1
        fit1(dsteps*(i-1)+j+1) = m*xx(dsteps*(i-1)+j+1)+b;
    end
end

fit1(dsteps*(size(x,2)-1)+1) = y(size(x,2));

for i = 1:round(resMax/steps)
    fit1(dsteps*(size(x,2)-1)+1+i) = m*xx(dsteps*(size(x,2)-1)+1+i)+b;
end

end