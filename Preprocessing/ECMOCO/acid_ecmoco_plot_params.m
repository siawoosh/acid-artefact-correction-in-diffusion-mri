function acid_ecmoco_plot_params(params, V, dof, dummy_type, p_out)

% =========================================================================
% This script creates diagnostic plots for ecmoco by plotting the estimated
% transformation parameters.
%
% Inputs:
%  params      - transformation paramaters
%  V           - header for extracting filename
%  dof         - degrees of freedom
%  p_out       - output folder
%  dummy_type  - options for registration type
%                (0: volume-wise, 1: slice-wise, 2: combined registration)
%
% Outputs: matlab figure(s) containing the transformation parameters
% 
% Created by G.David
% Adapted by B.Fricke
% =========================================================================

if dummy_type==0 || dummy_type==2
    % for VW registration, display two figures, one for MO parameters and one for EC parameters.
    if isempty(find(dof(1:6)>0,1))
        warning('Motion parameters were not estimated.')
    else
        plot_motion_params(params, V, dof, p_out);
    end
        
    if(dof(2)==1 || dof(8)==1 || dof(10)==1 || dof(11)==1)
        plot_ec_params(params, V, dof, p_out);
    else
        warning('EC parameters were not estimated.')
    end
    
end
    
if dummy_type==1 || dummy_type==2
    % for SW registration, display one figure for both MO and EC parameters. Display
    % only estimated parameters.
    plot_all_parameters(params, V, dof, p_out);
end
end

%% FUNCTION FOR PLOTTING MOTION PARAMETERS
function plot_motion_params(xx, V, dof, p_out)
      
    % Translation     
    figure
    subplot(2,1,1)
    ylabel('translation [mm]','FontSize',12)
    hold on
    
    if dof(1)==1
        subplot(2,1,1); plot((xx(:,1)),'r.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlex = 'x (red)';
        hold on;
    else
        titlex = 'x (not est.)';
    end
    
    if dof(2)==1
        subplot(2,1,1); plot((xx(:,2)),'g.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titley = 'y (green)';
        hold on;
    else
        titley = 'y (not est.)';
    end
    
    if dof(3)==1
        subplot(2,1,1); plot((xx(:,3)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlez = 'z (blue)';
    else
        titlez = 'z (not est.)';
    end
    
    Title = ['Translation in ' titlex ', ' titley ', ' titlez];
    h = title(Title); set(h,'FontSize',12); set(gca,'FontSize',12);
    
    % Rotation
    subplot(2,1,2)
    ylabel('rotation [rad]','FontSize',12)
    hold on
    
    if dof(4)==1
        subplot(2,1,2); plot((xx(:,4)),'r.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlex = 'x (red)';
        hold on;
    else
        titlex = 'x (not est.)';
    end
    
    if dof(5)==1
        subplot(2,1,2); plot((xx(:,5)),'g.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titley = 'y (green)';
        hold on;
    else
        titley = 'y (not est.)';
    end
    
    if dof(6)==1
        subplot(2,1,2); plot((xx(:,6)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlez = 'z (blue)';
    else
        titlez = 'z (not est.)';
    end
    
    Title = ['Rotation along ' titlex ', ' titley ', ' titlez];
    h = title(Title); set(h,'FontSize',12); set(gca,'FontSize',12);

    fname = acid_bids_filename(V(1),'ECMOCO-params-vw-mo','','.fig');
    fname = [p_out filesep fname];
    saveas(gcf,fname,'fig'); 
end

%% FUNCTION FOR PLOTTING EC PARAMETERS
function plot_ec_params(xx, V, dof, p_out)
   
    figure
    % x-y shear: Gx
    subplot(2,2,1);
    hold on
    if dof(10)==1
        plot((xx(:,10)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title1 = 'x-y shear: G_x';
    else
        Title1 = sprintf('x-y shear: G_x (not est.)');
    end
    h = title(Title1); set(h,'FontSize',12); set(gca,'FontSize',12);
    
    % y scaling: Gy
    subplot(2,2,2);
    hold on
    if dof(8)==1
        plot((xx(:,8)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title2 = 'y scaling: G_y';
    else
        Title2 = sprintf('y scaling: G_y (not est.)');
    end
    h = title(Title2); set(h,'FontSize',12); set(gca,'FontSize',12);
    
    % y-z shear: Gz
    subplot(2,2,3);
    hold on
    if dof(11)==1
        plot(xx(:,11),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title3 = 'y-z shear: G_z';
    else
        Title3 = sprintf('y-z shear: G_z (not est.)');
    end
    h = title(Title3); set(h,'FontSize',12); set(gca,'FontSize',12);    
    
    % y translation
    subplot(2,2,4);
    hold on
    if dof(2)==1
        subplot(2,2,4); plot(xx(:,2),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);
        Title4 = 'y translation: B_0';
    else
        Title4 = sprintf('y translation: B_0 (not est.)');
    end
    h = title(Title4); set(h,'FontSize',12); set(gca,'FontSize',12);    
    
    fname = acid_bids_filename(V(1),'ECMOCO-params-vw-ec','','.fig');
    fname = [p_out filesep fname];
    saveas(gcf,fname,'fig'); 
end

%% FUNCTION FOR PLOTTING ALL PARAMETERS [for SW registration]
function plot_all_parameters(xx, V, dof, p_out)

    figure
    MSK = find(dof(13:24)>0);
    nm = numel(MSK);
    
    if nm > 1        
        % specify colors for plots
        colors(1,:) = [1 1 0]; % yellow
        colors(2,:) = [1 0 1]; % magenta
        colors(3,:) = [0 1 1]; % cyan
        colors(4,:) = [1 0 0]; % red
        colors(5,:) = [0 1 0]; % green
        colors(6,:) = [0 0 1]; % blue
        colors(7,:) = [0 0 0]; % black
        if size(xx,3) > 8 % if more slices, generate random colors for them
            colors(8:size(xx,3)-1,:) = rand(size(xx,3)-8,3);
        end
    
        % specify titles for plots
        titles = cell(12,1);
        titles{1,1}  = 'Translation in x (mm)';
        titles{2,1}  = 'Translation in y (mm)';
        titles{3,1}  = 'Translation in z (mm)';
        titles{4,1}  = 'Rotation along x';
        titles{5,1}  = 'Rotation along y';
        titles{6,1}  = 'Rotation along z';
        titles{7,1}  = 'Scaling in x';
        titles{8,1}  = 'Scaling in y';
        titles{9,1}  = 'Scaling in z';
        titles{10,1} = 'Shearing in x-y';
        titles{11,1} = 'Shearing in y-z';
        titles{12,1} = 'Shearing in x-z';
        
        % looping through DOFs
        for i = 1:nm 

            subplot(2,ceil(nm/2),i); % creates a subplot for each DOF

            if i == nm % plot with legend
                for kk = 2:size(xx,3)
                    plot(xx(:,MSK(i),kk),'color',colors(kk-1,:),'LineWidth',0.05,'DisplayName',sprintf('Slice %d',kk-1)); xlim([0 size(xx,1)+1]);
                    h_l = legend('-DynamicLegend');
                    set(h_l,'FontSize',10);
                    set(h_l,'Location','east');
                    hold all
                end
                title(titles{MSK(i),1}, 'FontSize', 20);
                set(gca,'FontSize',20);
            else % plot without legend
                for kk = 2:size(xx,3)
                    plot(xx(:,MSK(i),kk),'color',colors(kk-1,:),'LineWidth',0.05); xlim([0 size(xx,1)+1]);
                    hold all
                end
                title(titles{MSK(i),1}, 'FontSize', 20);
                set(gca,'FontSize',20);
            end
        end
    end
        
    fname = acid_bids_filename(V(1),'ECMOCO-params-sw','','.fig');
    fname = [p_out filesep fname];
    saveas(gcf,fname,'fig');
end