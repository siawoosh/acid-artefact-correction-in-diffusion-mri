function V = acid_ecmoco_reslice(reg, params, j, dm, interp, dummy_type)

% =========================================================================
% The script reslices a single image using the given transformation
% parameters. Note that the all source images have to be given in the
% argument (mireg), and the image to be resliced is specified by the
% argument j.
%
% Inputs:
%   reg        - structure containing the headers of the source and template image(s)
%   params     - transformation parameters
%   j          - index of source image to be resliced
%   dm         - dimension of resliced image
%   interp     - type of interpolation
%   dummy_type - options for registration type (0: volume-wise , 1: slice-wise)
%
% Outputs:
%   V - header(s) of the resliced image(s) [struct array] 
% =========================================================================

% global phase_encoding;

% select image to reslice
VS = reg.VS(j);

% initialize
V = zeros(dm);

if dummy_type==0
    iM = inv(acid_spm_matrix(params));
    for p = 1:dm(3) % looping over slices
        M = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(reg.VT.mat)*iM*VS.mat);
        tmp = spm_slice_vol(VS,M,dm(1:2),interp);
        V(:,:,p) = tmp;
    end
else
    for p = 1:dm(3) % looping over slices
        iM = inv(acid_spm_matrix(params(:,p)));
        M = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(reg.VT.mat)*iM*VS.mat);
        tmp = spm_slice_vol(VS,M,dm(1:2),interp);
        V(:,:,p) = tmp;
    end    
end
end