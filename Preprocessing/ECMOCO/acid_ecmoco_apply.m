function [I_out, V_out] = acid_ecmoco_apply(V, VT, params, interp, p_out, keyword, suffix, dummy_type, dummy_write, dummy_par)

% =========================================================================
% Inputs:
%   V           - header(s) of the source image(s)
%   VT          - header of the target image
%   params      - transformation parameters to be applied
%   bvecs       - b-vectors of the source images
%   interp      - type of interpolation
%   p_out       - output folder
%   keyword     - keyword for the _desc field of the output filename
%   suffix      - suffix for the BIDS compatible filename
%   dummy_type  - options for segistration type
%                 (0: volume-wise, 1: slice-wise, 2: combined registration)
%   dummy_write - options for writing out resliced image(s)
%   dummy_par   - options for parallel computing
%
% Outputs:
%   I_out       - 3D or 4D image matrix containing the resliced image(s)
%   V_out       - Header of the output images
%   bvecs_rot   - Rotated b-vectors of the source images
%
% =========================================================================

% transformation parameters
if size(params,2)~=size(V,1)
    error('The size of the specified registration parameters is not compatible with the number of images!');
elseif size(params,1) ~= 12
    error('Incorrect registration parameters: The first dimension of the specified registration matrix has to be 12.');
end

% reshape for script acid_ecmoco_reslice.m
if dummy_type==0
    params = params';
else
    params = permute(params,[2 1 3]);
end

%% WRITING RESLICED IMAGES

% create a structure containing headers of source and target images
reg    = struct('VT',[],'VS',[]);
reg.VT = VT;
reg.VS = V;
dm     = reg.VT.dim;

% looping through volumes
parfor (k = 1:size(params,1), dummy_par)
    if dummy_type == 0
        I_out(:,:,:,k) = acid_ecmoco_reslice(reg, params(k,:,1), k, dm, interp, dummy_type);
    else
        I_out(:,:,:,k) = acid_ecmoco_reslice(reg,squeeze(params(k,:,2:end)), k, dm, interp, dummy_type);
    end  
end

if dummy_write
    
    % write resliced image
    Vtmp = reg.VT;
    Vtmp.fname = reg.VS(1).fname;
    V_out = acid_write_vol(I_out, Vtmp, p_out, keyword, suffix);
    
    % save json file
    acid_save_json(V(1), p_out, keyword);
    
    % save bvals and bvecs files
    acid_save_bvals_bvecs(V(1), p_out, keyword);
    
end
end