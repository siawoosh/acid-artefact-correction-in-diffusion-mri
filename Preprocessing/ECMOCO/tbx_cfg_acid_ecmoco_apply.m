function ecmoco_write = tbx_cfg_acid_ecmoco_apply

% source images
ecmoco_write_sources         = cfg_files;
ecmoco_write_sources.tag     = 'ecmoco_write_sources';
ecmoco_write_sources.name    = 'Source images';
ecmoco_write_sources.help    = {'Select source images (4D NIfTI). These images will be registered to the target image.'};
ecmoco_write_sources.filter  = 'image';
ecmoco_write_sources.ufilter = '.*';
ecmoco_write_sources.num     = [0 Inf];

% registration parameters
ecmoco_write_params         = cfg_files;
ecmoco_write_params.tag     = 'ecmoco_write_params';
ecmoco_write_params.name    = 'Registration parameters';
ecmoco_write_params.help    = {'Select a mat file containing the registration parameters'};
ecmoco_write_params.filter  = 'mat';
ecmoco_write_params.ufilter = '.*';
ecmoco_write_params.num     = [0 Inf];

% b vectors
ecmoco_bvecs_file         = cfg_files;
ecmoco_bvecs_file.tag     = 'ecmoco_bvecs_file';
ecmoco_bvecs_file.name    = 'File (*.mat|txt|bvec)';
ecmoco_bvecs_file.help    = {'Select a file specifying the b-vectors (bvecs) of the source images.'
    'The file should contain a horizontal array, where b-vectors should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
ecmoco_bvecs_file.filter  = 'any';
ecmoco_bvecs_file.ufilter = 'mat|txt|bvec';
ecmoco_bvecs_file.num     = [0 1];
% ecmoco_bvecs_file.val     = {{''}};

ecmoco_bvecs_exp         = cfg_entry;
ecmoco_bvecs_exp.tag     = 'ecmoco_bvecs_exp';
ecmoco_bvecs_exp.name    = 'Expression/Dependency';
ecmoco_bvecs_exp.help    = {'Specify the b-vectors (bvecs) of the source images.'
    'The expression should contain a horizontal array, where b-vectors should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
ecmoco_bvecs_exp.strtype = 'e';
ecmoco_bvecs_exp.num     = [Inf Inf];
%ecmoco_bvecs_exp.val     = {''};

ecmoco_bvecs_type        = cfg_choice;
ecmoco_bvecs_type.tag    = 'ecmoco_bvecs_type';
ecmoco_bvecs_type.name   = 'b-vectors';
ecmoco_bvecs_type.help   = {''};
ecmoco_bvecs_type.values = {ecmoco_bvecs_exp, ecmoco_bvecs_file};
ecmoco_bvecs_type.val    = {ecmoco_bvecs_exp};

% target
ecmoco_write_target         = cfg_files;
ecmoco_write_target.tag     = 'ecmoco_write_target';
ecmoco_write_target.name    = 'Target image';
ecmoco_write_target.help    = {'Select target image. After applying the registration parameters, the source images will be resliced on this image'};
ecmoco_write_target.filter  = 'image';
ecmoco_write_target.ufilter = '.*';
ecmoco_write_target.num     = [1 1];

% exbranch
ecmoco_write      = cfg_exbranch;
ecmoco_write.tag  = 'ecmoco_write';
ecmoco_write.name = 'ECMOCO: apply existing';
ecmoco_write.val  = {ecmoco_write_sources, ecmoco_write_params, ecmoco_bvecs_type, ecmoco_write_target};
ecmoco_write.help = {
                    'Reslices images using the transformation parameters stored in the matfiles produced by ECMOCO (with or without variable targets). It allows to specify a target image to which the images will be resliced and a dimension, along which the EC distortions were be corrected.'
                    };
ecmoco_write.prog = @local_ecmoco_apply;
ecmoco_write.vout = @vout_ecmoco_apply;

end

function out = local_ecmoco_apply(job)


    % read in bvecs
    if isfield(job.ecmoco_bvecs_type,'ecmoco_bvecs_file')
        job.ecmoco_bvecs = job.ecmoco_bvecs_type.ecmoco_bvecs_file;
        job.ecmoco_bvecs = cell2mat(job.ecmoco_bvecs);
        [~,~,e] = fileparts(job.ecmoco_bvecs);
        if ~isempty(job.ecmoco_bvecs_type.ecmoco_bvecs_file{1})
            switch lower(e)
                case '.mat'
                    job.ecmoco_bvecs = cell2mat(struct2cell(load(job.ecmoco_bvecs)));
                case '.nii'
                    error('Unexpected file extension!')
                otherwise
                    job.ecmoco_bvecs = dlmread(job.ecmoco_bvecs);
            end
        else
            job.ecmoco_bvecs = '';
        end
    elseif isfield(job.ecmoco_bvecs_type,'ecmoco_bvecs_exp')
        if ~isempty(job.ecmoco_bvecs_type.ecmoco_bvecs_exp)
            job.ecmoco_bvecs = job.ecmoco_bvecs_type.ecmoco_bvecs_exp;
        else
            job.ecmoco_bvecs = '';
        end
    end
    
    % input parameters
    params = load(char(job.ecmoco_write_params));
    interp = acid_get_defaults('ecmo.interpol_reslice');
    if size(params.params,3) > 1
        dummy_type = 1;
    else
        dummy_type = 0;
    end
    dummy_write = 1;
    dummy_par   = 0;
    
    % load in 4D image
    V  = acid_load_4Dimage(char(job.ecmoco_write_sources));
    VT = spm_vol(char(job.ecmoco_write_target));
    
    % define output directory
    keyword = 'ECMOCO';
    [path,fname,~] = spm_fileparts(V(1).fname);

    p_out = acid_bids(path,fname,keyword,1);
    
    % running the script
    [~,bvecs_rot] = acid_ecmoco_apply(V, VT, params.params,job.ecmoco_bvecs, interp, p_out, keyword, '_dwi', dummy_type, dummy_write, dummy_par);

    % get saved ECMOCO corrected images
    fname_out = [p_out filesep fname(1:end-4) '-' keyword '_dwi.nii'];
    
    for i = 1:size(job.ecmoco_write_sources,1)
        out.rfiles{i,:} = [fname_out ',' num2str(i)];
    end
    out.rfile1 = out.rfiles(1);
    out.bvecs  = bvecs_rot;
end

function dep = vout_ecmoco_apply(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Corrected images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'First corrected image';
    dep(2).src_output = substruct('.','rfile1');
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'Rotated b-vectors';
    dep(3).src_output = substruct('.','bvecs');
    dep(3).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
end