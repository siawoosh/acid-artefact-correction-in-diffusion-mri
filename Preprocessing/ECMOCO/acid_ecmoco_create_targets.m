function VT = acid_ecmoco_create_targets(V, bvals, bvecs, I_mask, P_bias, p_out, ph_dir, dummy_write)

% =========================================================================
% Generating registration templates (target images) by calculating
% the median of the b0 shell and the mean of each diffusion-weighted shell.
%
% Inputs:
%   V           - headers of dMRI dataset
%   bvals       - b values
%   I_mask      - 3D binary mask to apply on the image before optimization.
%                 Voxels outside the mask are ignored in the optimization process.
%   P_bias      - filename of bias field image
%   p_out       - output directory
%   ph_dir      - phase-encoding direction (1: x, 2: y, 3: z direction)
%   dummy_write - options for writing out target image(s)
%
% Outputs:
%   VT  - header(s) of the target image(s)
%
% Created by S.Mohammadi 13.05.2015
% Adapted by G.David
% =========================================================================
      
% get dimension
dm = V(1).dim;

% load in dMRI data
for k = 1:length(V)     
    I(:,:,:,k) = acid_read_vols(V(k),V(1),1);
end

if isempty(I_mask)
    I_mask = ones(dm);
end

% get unique b values
bvals = round(bvals/100)*100;
[bvals_unique,~,~] = unique(bvals);
    
% default flags for registration
% remove non-rigid body dofs
flags = struct('sep',[4 2],'params',[0 0 0  0 0 0  1 1 1  0 0 0], ...
 'cost_fun','nmi','fwhm',[7 7],...
 'tol',[0.01 0.01 0.01  0.005 0.005 0.005  0.005 0.005 0.005  0.005 0.005 0.005], ...
 'graphics',1, 'dof', [1 1 1 1 1 1 0 0 0 0 0 0], 'perc', 0.8,'AMSK',ones(dm),'reshold',-4,'smk',[1 1 1]);

for i = 1:size(bvals_unique,2) % loop through each b value
    
    % load in each volume within the shell
    I_msk_shell = I(:,:,:,bvals==bvals_unique(i));
    
    % create median image for b0 shell and mean image(s) for dw shell(s)
    if i==1
        IT = median(I_msk_shell,4);
    else
        IT = mean(I_msk_shell,4);
    end
        
    % save target image
    VT(i,1) = acid_write_vol(IT, V(1), p_out, ['ECMOCO-target-b' num2str(bvals_unique(i))]);
        
    % apply bias field
    if ~isempty(P_bias)
        I_bias = acid_read_vols(spm_vol(P_bias),V(1),1);
    end
    
    % obtain transformation parameters to the first image in the series
    % Note: should we do it for dw shells if interpolation is applied?
    % V_msk_shell = V(bvals==bvals_unique(i));
    if ~isempty(P_bias)
        flags.Abias = I_bias;
        params = acid_spm_coreg_freeze_bias(V(1),VT(i,1),flags,0,V_mask,ph_dir);
    else 
        params = acid_spm_coreg(I(:,:,:,1), I, flags, 0, I_mask, V(1), ph_dir);
    end

    % reslice on the first image in the series
    % this overwrites the target image in the output folder
    acid_ecmoco_apply(VT(i,1), V(1), params', 0, p_out, '', '', 0, dummy_write, 0);
       
end 
end