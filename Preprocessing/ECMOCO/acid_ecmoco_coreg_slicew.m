function xk = acid_ecmoco_coreg_slicew(IS, IT, flags, thr_hist, I_mask, V_ref, z_avg, ph_dir)

% =========================================================================
% This script obtains slice-wise (2D) linear transformation parameters (xk)
% between volumes IS and IT using parameters specified in flags.
%
% Inputs:
%   IS       - source image matrix
%   IT       - target image matrix
%   flags    - parameters for registration
%   thr_hist - lower threshold for histogram: values below this threshold
%              are ignored in the optimization process
%   I_mask   - 3D binary mask to apply on the image before optimization.
%              Voxels outside the mask are ignored in the optimization process.
%   V_ref    - reference header to get the dimension and resolution
%   z_avg    - sliding-window averaging across the z direction (slices).
%              z_avg specifies the size of the sliding window
%              (number of slices to average)
%   ph_dir   - phase-encoding direction (1: x, 2: y, 3: z direction)
%
% Outputs:
%   xk    - estimated transformation parameters
% =========================================================================

dm = V_ref.dim;
xk = repmat([0 0 0 0 0 0 1 1 1 0 0 0]', 1, dm(3));
flags_tmp = flags;

for z = 1:dm(3)

    % only do registration is slice is not empty
    IS_tmp = IS(:,:,z); IS_tmp = IS_tmp(:); IS_tmp(isnan(IS_tmp)) = [];
    IT_tmp = IT(:,:,z); IT_tmp = IT_tmp(:); IT_tmp(isnan(IT_tmp)) = [];
    I_mask_tmp = I_mask(:,:,z); I_mask_tmp = I_mask_tmp(:); I_mask_tmp(isnan(I_mask_tmp)) = [];
       
    if ~sum(IS_tmp(:)) || ~sum(IT_tmp(:)) || ~sum(I_mask_tmp(:))
        %warning('Skipping this slice because the source, template, or the mask contains zeros only!')
    else

        % build up mask
        I_mask_tmp = repmat(I_mask(:,:,z),1,1,dm(3));
        
        % boundary slices
        if z==1 || z==dm(3)
            IS_tmp = repmat(IS(:,:,z),1,1,dm(3));
            IT_tmp = repmat(IT(:,:,z),1,1,dm(3));

        % inner slices
        else
            if z_avg < 2
                IS_tmp = repmat(IS(:,:,z),1,1,dm(3));
                IT_tmp = repmat(IT(:,:,z),1,1,dm(3));
            else
                zsmooth_indx = (z_avg - 1)/2;
                
                if round(zsmooth_indx) == zsmooth_indx           
                    zsmooth_indx_minus = zsmooth_indx;
                    zsmooth_indx_plus  = zsmooth_indx;
                else
                    zsmooth_indx_minus = round(zsmooth_indx) - 1;
                    zsmooth_indx_plus  = round(zsmooth_indx);         
                end
                
                if zsmooth_indx_minus < 1, zsmooth_indx_minus = 1; end
                if zsmooth_indx_plus  > dm(3), zsmooth_indx_plus = dm(3); end
                    
                IS_tmp = repmat(median(IS(:,:,z-zsmooth_indx_minus:z+zsmooth_indx_plus),z_avg,'omitnan'),1,1,dm(3));
                IT_tmp = repmat(IT(:,:,z),1,1,dm(3));
            end
        end
        
        % modify flags
        flags_tmp.params = flags.params(z,:);
        
        % determine linear registration parameters to match VF with VG
        xk(:,z) = acid_spm_coreg(IT_tmp, IS_tmp, flags_tmp, thr_hist, I_mask_tmp, V_ref, ph_dir);
    end
end
end