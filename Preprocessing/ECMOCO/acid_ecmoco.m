function [VT,bvecs_rot,fname_out] = acid_ecmoco(P, dummy_type, dof_vw, dof_sw, bvals, bvecs, dummy_init_vw, excluded, P_mask, P_bias, dummy_write, dummy_plot, ...
    thr_hist, sep, interp, phdir, zsmooth_idx, npool, dummy_target, P_target, rounding_tol)

% =========================================================================
% The script performs eddy current and motion correction on dMRI images by
% means of registration.
%
% Inputs:
%   P             - filename(s) of source image(s)
%   dummy_type    - options for registration type
%                   (0: volume-wise, 1:slice-wise, 2: combined)
%   dof_vw        - degrees of freedom for volume-wise (3D) registation
%   dof_sw        - degrees of freedom for slice-wise (2D) registration
%   bvals         - b-values of the source images
%   bvecs         - b-vectors of the source images
%   dummy_init_vw - options for interpolation mode for volume-wise registration (0: OFF, 1: ON)
%   excluded      - list of excluded volumes
%   P_mask        - filename of the binary mask
%   P_bias        - filename of bias field map
%   dummy_write   - options for writing out the resliced images (0: OFF, 1: ON)
%   dummy_plot    - options for displaying transformation parameters (0: OFF, 1: ON)
%   thr_hist      - histogram threshold, voxel intensities below this are
%                   ignored during the optimizaiton process
%   sep           - resampling parameter for optimization
%   interp        - type of interpolation
%   phdir         - phase-encoding direction (1: x, 2: y, 3: z)
%   zsmooth_idx   - size of the smoothing filter along z (in voxels)
%   npool         - number of cores to utilize during parallel computing
%   dummy_target  - options for target selection mode
%                   (0: multi-target mode, 1: single-target mode)
%   P_target      - target image (for dummy_target=1)
%   outdir        - output directory
%   rounding_tol  - rounding tolerance for the b values (bvals)
%
% Outputs:
%   VT            - header(s) of the target volume(s)
%   fname_out     - output filename of the ECMOCO corrected data
%
% written: S. Mohammadi (02/02/2013)
% adapted: G. David, B. Fricke
% =========================================================================

global IN_dof %VFmat InterpLength VGmat VFdim phase_encoding

tic
% defaults
dummy_init_sw = 0;

[V,P_uncomp,dummy_compress] = acid_prepare_and_load_4Dimage(P);


% load in 4D image
% V = acid_load_4Dimage(P);
dm = [V(1).dim, size(V,1)];

% check bvals input
if isempty(bvals)
    bvals = 1E5*ones(1,size(V,1));
end
if size(bvals,2)~=size(V,1)
    bvals = bvals';
    if size(bvals,2)~=size(V,1)
        error('The number of source images and b-value entries has to be the same.');
    end
end

% set up DOFs
switch dummy_type
    case 0, dof = [dof_vw, zeros(1,12)];
    case 1, dof = [zeros(1,12), dof_sw(1:2), 0, dof_sw(3), 0, 0, dof_sw(4:5), 0, dof_sw(6:7), 0];
    case 2, dof = [dof_vw, dof_sw(1:2), 0, dof_sw(3), 0, 0, dof_sw(4:5), 0, dof_sw(6:7), 0];
end
IN_dof = dof;

% indices of included (non-excluded) volumes
i = 1;
if ~isempty(excluded{1})

    [~, ~, e]=fileparts(excluded{1});
    if strcmp(e,'.mat')
        excluded = load(excluded{1});
    elseif strcmp(e,'.txt')
        excluded = dlmread(excluded{1});
    end
    if size(excluded,2)==1
        excluded=excluded';
    end
    if size(excluded,1)>1
        excluded = excluded.idx(1,:)';
    end
    included = zeros(size(P,1)-length(excluded),1);
    for k = 1:size(P,1)
        if ~ismember(k,excluded)
            included(i,1) = k;
            i = i+1;
        end
    end
else
    excluded = [];
    included = (1:size(P,1))';
end

% Load in mask
if ~isempty(P_mask)
    I_mask = acid_read_vols(spm_vol(P_mask),V(1),1);
else
    I_mask = ones(dm(1:3));
end

% Load in bias field map
% if ~isempty(P_bias)
%     I_bias = acid_read_vols(spm_vol(P_bias),V(1),1);
% end

% define output directory
keyword = 'ECMOCO';
[path,fname,~] = spm_fileparts(V(1).fname);

p_out = acid_bids(path,fname,keyword,1);


% Start logging
diary([p_out filesep 'logfile_' keyword '.txt'])

if dummy_type == 2
    fprintf(['\nUsed degrees of freedom for volume-wise: [' num2str(dof_vw) ']\n'])
    fprintf(['\nUsed degrees of freedom for slice-wise:  [' num2str(dof_sw) ']\n'])
elseif dummy_type == 1
    fprintf(['\nUsed degrees of freedom for slice-wise: [' num2str(dof_sw) ']\n'])
elseif dummy_type == 0
    fprintf(['\nUsed degrees of freedom for volume-wise: [' num2str(dof_vw) ']\n'])
end

rotation_matrix = abs(round(normalize(V(1).mat(1:3,1:3),'norm'),2));

[~,x_indice] = max(rotation_matrix(1,:));
[~,y_indice] = max(rotation_matrix(2,:));
[~,z_indice] = max(rotation_matrix(3,:));

if phdir == 1
    fprintf('\nPhase-encoding direction: x\n')
    if x_indice ~= 1
        if y_indice == 1
            phdir = 2;
        elseif z_indice == 1
            phdir = 3;
        end
    end
elseif phdir == 2
    fprintf('\nPhase-encoding direction: y\n')
    if y_indice ~= 2
        if x_indice == 2
            phdir = 1;
        elseif z_indice == 2
            phdir = 3;
        end
    end
elseif phdir == 3
    fprintf('\nPhase-encoding direction: z\n')
    if z_indice ~= 3
        if x_indice == 3
            phdir = 1;
        elseif y_indice == 3
            phdir = 2;
        end
    end
end

if phdir == 1
    I_mask = permute(I_mask,[2 1 3]);
elseif phdir == 2
    % --- %
elseif phdir == 3
    I_mask = permute(I_mask,[1 3 2]);
end



%% GENERATE TARGETS

fprintf('\nCreating target images...\n')
if dummy_target
    VT = spm_vol(P_target);
    bvals = 9999;
    dummy_init_vw = 0;
else
    if min(bvals) < 1E5
        VT = acid_ecmoco_create_targets(V, bvals, bvecs, I_mask, P_bias, p_out, phdir, dummy_write);
    else
        VT = V(1);
        bvals = 9999;
    end
end

if iscell(VT)
    VT = cell2mat(VT);
end

fprintf('Done.\n');

%% CREATE REGISTRATION STRUCTURE

% define flags for registration
% flags - a structure containing the following elements:
%          sep      - optimisation sampling steps (mm)
%                     default: [4 2]
%          params   - starting estimates (6 elements)
%                     default: [0 0 0  0 0 0]
%          cost_fun - cost function string:
%                      'mi'  - Mutual Information
%                      'nmi' - Normalised Mutual Information
%                      'ecc' - Entropy Correlation Coefficient
%                      'ncc' - Normalised Cross Correlation
%                      default: 'nmi'
%          tol      - tolerences for accuracy of each param
%                     default: [0.02 0.02 0.02 0.001 0.001 0.001]
%          fwhm     - smoothing to apply to 256x256 joint histogram
%                     default: [7 7]
%          dof   - degrees of freedom

% default flags
flags = struct('sep',sep,'params',[0 0 0  0 0 0  1 1 1  0 0 0], ...
    'cost_fun','nmi','fwhm',[7 7],...
    'tol',[0.01 0.01 0.01  0.005 0.005 0.005  0.005 0.005 0.005  0.005 0.005 0.005], ...
    'graphics',1,'dof',dof,'perc',0.8,'I_mask',ones(dm(1:3)),'interp',interp,'smk',[1 1 1]);

% flags for b0 volumes (only rigid-body DOFs)
flags0 = flags;
flags0.dof([7:12,19:24]) = 0;

% create structure containing the source and target images
% VT - header(s) of the target image(s)
% VS - header(s) of the source image(s)
reg = struct('VT',[],'VS',[]);
reg.VT = VT;
reg.VS = V;


%% PREPARE ESTIMATION OF REGISTRATION PARAMETERS

% round b values to 100's
% return b values without repetition
if size(bvals,1) == 1
    bvals = bvals';
end
bvals_round = round(bvals/rounding_tol)*rounding_tol;
[bvals_unique,~,~] = unique(bvals_round,'rows');

% load in source image(s)
IS = zeros(dm);
for n = 1:dm(4)
    IS(:,:,:,n) = acid_read_vols(reg.VS(n),reg.VT(1),1);
end

% load in target image(s)
IT = zeros([dm(1:3), numel(bvals_unique)]);
for n = 1:numel(bvals_unique)
    IT(:,:,:,n) = acid_read_vols(reg.VT(n),V(1),0);
end

% initializing transformation and initialization parameters
if dummy_type==0
    params = zeros(12,dm(4),1);
    params(7:9,:,1) = 1;
    initparams = params;
else
    params = zeros(12,dm(4),dm(3)+1);
    params(7:9,:,:) = 1;
    initparams = params;
end

% idx: indices of b0 volumes followed by indices of dw volumes (shells in increasing order)
% idx_b0: indices of b0 volumes
% idx_dw: indices of dw volumes
idx = []; idx_b0 = []; idx_dw = [];
for n = 1:numel(bvals_unique)
    idx_tmp = find(bvals_round==bvals_unique(n));
    idx = cat(1,idx,idx_tmp);
    if n==1
        idx_b0 = idx_tmp;
    else
        idx_dw = cat(1,idx_dw,idx_tmp);
    end
end

% idx2: same as idx but excluding the specified volumes
% idx2_b0: same as idx_b0 but excluding the specified volumes
% idx2_dw: same as idx_dw but excluding the specified volumes
idx2 = []; idx2_b0 = []; idx2_dw = [];
i = 1;
for n = 1:numel(idx)
    if ~ismember(idx(n),excluded)
        idx2(i,1) = idx(n);
        i = i+1;
    end
end

if ~isempty(idx_b0)
    i = 1;
    for n = 1:numel(idx_b0)
        if ~ismember(idx_b0(n),excluded)
            idx2_b0(i,1) = idx_b0(n);
            i = i+1;
        end
    end
else
    idx2_b0 = [];
end

if ~isempty(idx_dw)
    i = 1;
    for n = 1:numel(idx_dw)
        if ~ismember(idx_dw(n),excluded)
            idx2_dw(i,1) = idx_dw(n);
            i = i+1;
        end
    end
else
    idx2_dw = [];
end


npool = acid_multicore(npool);

% if npool>1
%     p = gcp('nocreate');
%     if p.NumWorkers ~= npool
%         % delete(gcp('nocreate'));
%         if exist('parpool')>0
%             poolobj = parpool('local',npool);
%             dummy_matlabpool = false;
%         elseif exist('matlab')>0
%             parpool('open',npool);
%             dummy_matlabpool = true;
%         else
%             npool = 1;
%             warning('No parallel processing possible on this matlab version! Proceed without parallel processing.')
%         end
%     end
% end

if npool>1
    dummy_par = npool;
else
    dummy_par = 0;
end

%% VOLUME-WISE (3D) REGISTRATION

if dummy_type==0 || dummy_type==2

    fprintf('\nVolume-wise (3D) registration:\n')

    % ==================================================================
    % ======================= Volume-wise: b0 volumes ==================
    % ==================================================================

    % create registration flags
    % NOTE: for b0 volumes, rigid-body (6 DOF) registration is performed
    flagstmp = flags0;
    flagstmp.dof = flags0.dof(1:12);


    if numel(idx2_b0) < 2
        dummy_init_vw = 0;
        warning('At least two b0 volumes are needed for the initialization mode. Initialization mode is deactivated.')
    end

    if dummy_init_vw % no parallel computing
        disp('Running registration for b0 volumes...')

        if numel(idx2_b0) < 2
            error('At least two b0 volumes are needed if the initialization mode is used.')
        end

        for jj = 1:numel(idx2_b0)

            j = idx2_b0(jj);
            [~,b] = min(abs(bvals_round(j) - bvals_unique));

            % initialize transformation parameters with the values from the
            % previous b0 volume
            if j > idx2(1)
                flagstmp.params = params(:,idx2_b0(jj-1))';
            end

            % obtain transformation parameters
            if isempty(P_bias)
                xk = acid_spm_coreg(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
            else
                xk = acid_spm_coreg_bias(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
            end
            params(:,j,1) = xk;
        end

        % create initialization values (interpolated from the b0 voluems) for the dwi volumes
        % NOTE: only rigid-body parameters are interpolated
        if dummy_init_vw
            for i = 1:6
                initparams(i,:,1) = interp1(idx2_b0', params(i,idx2_b0,1), 1:numel(bvals), 'linear', params(i,idx2_b0(end),1));
            end
        end

    else % parallel computing

        tmp_matrix = zeros(12, length(idx2_b0));
        f = (numel(idx2_b0) / npool);

        if round(f) ~= f
            f = ceil(f);
            f = f + 1;
        end
        disp('Running registration for b0 volumes... (see progress bar)')
        spm_progress_bar('Init', 100, 'Running volume-wise registration for b0 volumes', 'Progress [%]');

        for k = 1:f
            spm_progress_bar('Set', (k-1)/f*100);
            start_counter = (1 + npool * (k-1));
            end_counter = (npool * k);

            while end_counter > numel(idx2_b0)
                end_counter = end_counter - 1;
            end

            parfor (jj = start_counter : end_counter, dummy_par)
                j = idx2_b0(jj);
                [~,b] = min(abs(bvals_round(j) - bvals_unique));

                % obtain transformation parameters
                if isempty(P_bias)
                    xk = acid_spm_coreg(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
                else
                    xk = acid_spm_coreg_bias(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
                end
                tmp_matrix(:,jj,:) = xk;
            end
        end

        params(:,idx2_b0,1) = tmp_matrix;
        spm_progress_bar('Set', 100);

    end

    disp('Done.')

    % ==================================================================
    % ======================= Volume-wise: dw volumes ==================
    % ==================================================================

    tmp_matrix = zeros(12, length(idx2_dw));
    f = (numel(idx2_dw) / npool);

    if round(f) ~= f
        f = ceil(f);
        f = f + 1;
    end
    disp('Running registration for dw volumes... (see progress bar)')
    spm_progress_bar('Init', 100, 'Running volume-wise registration for dw volumes', 'Progress [%]');

    for k = 1:f
        spm_progress_bar('Set', (k-1)/f*100);
        start_counter = (1 + npool * (k-1));
        end_counter = (npool * k);

        while end_counter > numel(idx2_dw)
            end_counter = end_counter - 1;
        end

        parfor (jj = start_counter : end_counter, dummy_par)
            j = idx2_dw(jj);
            [~,b] = min(abs(bvals_round(j) - bvals_unique));

            % create registration flags
            flagstmp = flags;
            flagstmp.dof = flags.dof(1:12);

            % initialize transformation parameters with the interpolated values
            if dummy_init_vw
                flagstmp.params = initparams(:,j)';
            end

            % obtain transformation parameters
            if isempty(P_bias)
                xk = acid_spm_coreg(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
            else
                xk = acid_spm_coreg_bias(IT(:,:,:,b),IS(:,:,:,j),flagstmp,thr_hist,I_mask,reg.VT(b),phdir);
            end
            tmp_matrix(:,jj,:) = xk;
        end
    end

    spm_progress_bar('Set', 100);
    disp('Done.')
    params(:,idx2_dw,1) = tmp_matrix;

end




% reslice if the data have undergone volume-wise registration
if dummy_type == 0 || dummy_type == 2
    if numel(excluded) > 0
        fprintf('\nReplace registration parameters for the excluded volumes:\n')
        for k = 1:numel(excluded)

            % i: index of volume whose reg. parameters will be copied
            % if the first volume is excluded
            if excluded(k)<included(1)
                i = included(1);
                while ismember(i,excluded)
                    i = i+1;
                end
            else
                i = excluded(k)-1;
                while ismember(i,excluded)
                    i = i-1;
                end
            end
            if ~dummy_type
                params(:,excluded(k)) = params(:,i);
            else
                params(:,excluded(k),:) = params(:,i,:);
            end

        end
        disp('Done.')
    end

    [I, V_vw] = acid_ecmoco_apply(V, V(1), params(:,:,1), flags.interp, p_out, 'ECMOCO', '_dwi', 0, dummy_write, dummy_par);
    
end


%% SLICE-WISE (2D) REGISTRATION

if dummy_type==1 || dummy_type==2
    fprintf('\nSlice-wise (2D) registration:\n')
    % ==================================================================
    % ======================= Slice-wise: b0 volumes ===================
    % ==================================================================

    % create registration flags
    % NOTE: for b0 volumes, rigid-body (6 DOF) registration is performed
    flagstmp = flags0;
    flagstmp.dof = flags0.dof(13:end);
    flagstmp.params = squeeze(initparams(:,idx2(1),2:end))';

    if dummy_init_sw % no parallel computing
        disp('Running registration for b0 volumes... ');
        for jj = 1:numel(idx2_b0)
            j = idx2_b0(jj);
            [~,b] = min(abs(bvals_round(j) - bvals_unique));

            % initialize transformation parameters with the values from the
            % previous b0 volume
            if j > idx2(1)
                flagstmp.params = squeeze(params(:,idx2_b0(jj-1),2:end))';
            end

            % obtain transformation parameters
            xk = acid_ecmoco_coreg_slicew(IS(:,:,:,j), IT(:,:,:,b), flagstmp, thr_hist, I_mask, reg.VT(b), zsmooth_idx, phdir);
            params(:,j,2:end) = xk;
        end

        % create initialization values (interpolated from the b0 voluems) for the dwi volumes
        % NOTE: only rigid-body parameters are interpolated
        if dummy_init_sw
            for k = 1:6
                for s = 1:dm(3)
                    initparams(k,:,s) = interp1(idx2_b0', params(k,idx2_b0,s), 1:numel(bvals), 'linear', params(k,idx2_b0(end),s));
                end
            end
        end

    else % parallel computing

        tmp_matrix = repmat([0 0 0  0 0 0  1 1 1  0 0 0]', 1, length(idx2_b0), dm(3));
        f = (numel(idx2_b0) / npool);

        if round(f) ~= f
            f = ceil(f);
            f = f + 1;
        end

        disp('Running registration for b0 volumes... (see progress bar)')
        spm_progress_bar('Init', 100, 'Running slice-wise registration for b0 volumes', 'Progress [%]');

        for k = 1:f
            spm_progress_bar('Set', (k-1)/f*100);
            start_counter = (1 + npool * (k-1));
            end_counter = (npool * k);

            while end_counter > numel(idx2_b0)
                end_counter = end_counter - 1;
            end

            parfor (jj = start_counter : end_counter, dummy_par)
                j = idx2_b0(jj);
                [~,b] = min(abs(bvals_round(j) - bvals_unique));

                % obtain transformation parameters
                xk = acid_ecmoco_coreg_slicew(IS(:,:,:,j), IT(:,:,:,b), flagstmp, thr_hist, I_mask, reg.VT(b), zsmooth_idx, phdir);
                tmp_matrix(:,jj,:) = xk;
            end
        end

        params(:,idx2_b0,2:end) = tmp_matrix;
        spm_progress_bar('Set', 100);

    end

    disp('Done.')

    % ==================================================================
    % ======================= Slice-wise: dw volumes ===================
    % ==================================================================

    tmp_matrix = repmat([0 0 0  0 0 0  1 1 1  0 0 0]', 1, length(idx2_dw), dm(3));
    f = (numel(idx2_dw) / npool);

    if round(f) ~= f
        f = ceil(f);
        f = f + 1;
    end
    disp('Running registration for dw volumes... (see progress bar)')
    spm_progress_bar('Init', 100, 'Running slice-wise registration for dw volumes', 'Progress [%]');

    for k = 1:f
        spm_progress_bar('Set', (k-1)/f*100);
        start_counter = (1 + npool * (k-1));
        end_counter = (npool * k);

        while end_counter > numel(idx2_dw)
            end_counter = end_counter - 1;
        end

        parfor (jj = start_counter : end_counter, dummy_par)
            j = idx2_dw(jj);
            [~,b] = min(abs(bvals_round(j) - bvals_unique));

            % create registration flags
            flagstmp = flags;
            flagstmp.dof = flags.dof(13:end);
            flagstmp.params = squeeze(initparams(:,idx2(1),2:end))';

            % initialize transformation parameters with the interpolated values
            if dummy_init_sw
                flagstmp.params = squeeze(initparams(:,j,2:end))';
            else
                flagstmp.params = squeeze(initparams(:,idx2(1),2:end))';
            end

            % obtain transformation parameters
            xk = acid_ecmoco_coreg_slicew(IS(:,:,:,j), IT(:,:,:,b), flagstmp, thr_hist, I_mask, reg.VT(b), zsmooth_idx, phdir);
            tmp_matrix(:,jj,:) = xk;
        end
    end

    spm_progress_bar('Set', 100);
    disp('Done.')
    params(:,idx2_dw,2:end) = tmp_matrix;
end

%% replace registration parameters for the excluded volumes
if numel(excluded) > 0
  
    fprintf('\nReplace registration parameters for the excluded volumes:\n')

    for k = 1:numel(excluded)

        % i: index of volume whose reg. parameters will be copied
        % if the first volume is excluded
        if excluded(k)<included(1)
            i = included(1);
            while ismember(i,excluded)
                i = i+1;
            end
        else
            i = excluded(k)-1;
            while ismember(i,excluded)
                i = i-1;
            end
        end
        if ~dummy_type
            params(:,excluded(k)) = params(:,i);
        else
            params(:,excluded(k),:) = params(:,i,:);
        end

    end
    disp('Done.')
end

%% save transformation parameters
fprintf('\nSaving transformation parameters...\n')
fname_params = acid_bids_filename(V(1),[keyword '-params'],'','.mat');
fname_params = [p_out filesep fname_params];
save(fname_params,'params')
disp('Done.')

%% plot transformation parameters
if dummy_plot
    if dummy_type
        acid_ecmoco_plot_params(permute(params,[2 1 3]), V(1), IN_dof, dummy_type, p_out)
    else
        acid_ecmoco_plot_params(params', V(1), IN_dof, dummy_type, p_out)
    end
end

%% RESLICE IMAGES (based on the obtained transformation parameters)
if dummy_write

    fprintf('\nReslicing images...\n')

    % % write corrected images
    if dummy_type == 1
        [I, V_vw] = acid_ecmoco_apply(V, V(1), params, flags.interp, p_out, 'ECMOCO', '_dwi', dummy_type, dummy_write, dummy_par);
    elseif dummy_type == 2
        [I, V_vw] = acid_ecmoco_apply(V_vw, V(1), params, flags.interp, p_out, '', '_dwi', dummy_type, dummy_write, dummy_par);
    end

    % save json file
    acid_save_json(V(1), p_out, keyword);

    % save bvals and bvecs files
    acid_save_bvals_bvecs(V(1), p_out, keyword);

    % compute and save rotate bvecs
    if ~isempty(bvecs)
        bvecs_rot = zeros(size(bvecs));

        for i = 1:size(params,2)
            R = spm_matrix([0 0 0 -params(4,i,1) -params(5,i,1) -params(6,i,1) 1 1 1 0 0 0]);
            bvecs_rot(:,i) = R(1:3,1:3) * bvecs(:,i);
        end

        fname = acid_bids_filename(V(1),[keyword '-rotated'],'_dwi','.bvec');
        fname = [p_out filesep fname];
        dlmwrite(fname, bvecs_rot);
    else
        bvecs_rot = '';
    end

    % write out mean corrected images within each shell
    if min(bvals) < 1E5
        Vtmp = V(1);
        for n = 1:numel(bvals_unique)
            b_msk = bvals_round==bvals_unique(n);
            MeanShell = mean(I(:,:,:,b_msk),4);
            Vtmp.fname = [fname_params(1:end-11) '-mean-b' num2str(bvals_unique(n)) '.nii'];
            Vtmp.dim = size(MeanShell);
            Vtmp.n = [1,1];
            Vtmp.descrip = '3D image';
            spm_write_vol(Vtmp,MeanShell);
        end
    end
end

% % close parallel computing
% if npool>1
%     if dummy_matlabpool
%         parpool close;
%     else
%         delete(poolobj);
%     end
% end

if dummy_target
    VT = Vtmp;
end

[fname_out] = acid_compress_and_delete_4Dimage(dummy_compress, V_vw(1,:).fname, P_uncomp);

fprintf('Done.\n')
T = toc/60;

T = duration(minutes(T),'format','hh:mm:ss');
disp(['The total time for ' keyword ' was: ' char(T) '.']);
diary off



end