function ecmoco = tbx_cfg_acid_ecmoco(bvals_bvecs)

% source images
ecmoco_sources         = cfg_files;
ecmoco_sources.tag     = 'ecmoco_sources';
ecmoco_sources.name    = 'Source images';
ecmoco_sources.help    = {'Select source images (4D NIfTI). These images will be registered to the target image(s).'};
ecmoco_sources.filter  = 'any';
ecmoco_sources.ufilter = 'nii';
ecmoco_sources.num     = [0 Inf];

% degrees of freedom for volume-wise registration
ecmoco_dof_vw_file         = cfg_files;
ecmoco_dof_vw_file.tag     = 'ecmoco_dof_vw_file';
ecmoco_dof_vw_file.name    = 'File (*.mat|txt)';
ecmoco_dof_vw_file.help    = {'Select a file containing the degrees of freedom for volume-wise registration.'
    'Note: The file should contain a 1 x 12 vector.'};
ecmoco_dof_vw_file.filter  = 'any';
ecmoco_dof_vw_file.ufilter = 'mat|txt';
ecmoco_dof_vw_file.num     = [1 1];
ecmoco_dof_vw_file.val     = {{''}};

ecmoco_dof_vw_exp         = cfg_menu;
ecmoco_dof_vw_exp.tag     = 'ecmoco_dof_vw_exp';
ecmoco_dof_vw_exp.name    = 'Choose predefined option';
ecmoco_dof_vw_exp.help    = {
    'Rigid body: [1 1 1 1 1 1 0 0 0 0 0 0]'
    'Rigid body and eddy current distortions: [1 1 1 1 1 1 0 1 0 1 1 0]'
    'Distortions in a spherical phantom: [0 1 0 0 0 0 0 1 0 1 1 0]'
    'Recommended for spinal cord: [1 1 0 0 0 0 0 1 0 0 0 0]'};
ecmoco_dof_vw_exp.labels = {'Rigid body', 'Rigid body and eddy current distortions', 'Distortions in a spherical phantom', 'Recommended for spinal cord'};
ecmoco_dof_vw_exp.values  = {[1 1 1 1 1 1 0 0 0 0 0 0] [1 1 1 1 1 1 0 1 0 1 1 0] [0 1 0 0 0 0 0 1 0 1 1 0] [1 1 0 0 0 0 0 1 0 0 0 0]};
ecmoco_dof_vw_exp.val     = {[1 1 1 1 1 1 0 1 0 1 1 0]};

ecmoco_dof_vw_manual_exp         = cfg_entry;
ecmoco_dof_vw_manual_exp.tag     = 'ecmoco_dof_vw_manual';
ecmoco_dof_vw_manual_exp.name    = 'Manual expression';
ecmoco_dof_vw_manual_exp.help    = {'Specify the degrees of freedom (DOFs) of the registration. Input is a 1x12 binary array, '
    'The input vector must have 12 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco_dof_vw_manual_exp.strtype = 'e';
ecmoco_dof_vw_manual_exp.num     = [1 12];
ecmoco_dof_vw_manual_exp.val     = {[0 0 0 0 0 0 0 0 0 0 0 0]};

ecmoco_dof_vw_type        = cfg_choice;
ecmoco_dof_vw_type.tag    = 'ecmoco_dof_vw_type';
ecmoco_dof_vw_type.name   = 'Select degrees of freedom input type for volume-wise (3D) registration';
ecmoco_dof_vw_type.help   = {''};
ecmoco_dof_vw_type.values = {ecmoco_dof_vw_file, ecmoco_dof_vw_exp, ecmoco_dof_vw_manual_exp};
ecmoco_dof_vw_type.val    = {ecmoco_dof_vw_exp};

% degrees of freedom for slice-wise registration
ecmoco_dof_sw_file         = cfg_files;
ecmoco_dof_sw_file.tag     = 'ecmoco_dof_sw_file';
ecmoco_dof_sw_file.name    = 'File (*.mat|txt)';
ecmoco_dof_sw_file.help    = {'Select a file containing the degrees of freedom for slice-wise registration.'
    'Note: The file should contain a 1 x 7 vector.'};
ecmoco_dof_sw_file.filter  = 'any';
ecmoco_dof_sw_file.ufilter = 'mat|txt';
ecmoco_dof_sw_file.num     = [0 1];
ecmoco_dof_sw_file.val     = {{''}};

ecmoco_dof_sw_exp         = cfg_menu;
ecmoco_dof_sw_exp.tag     = 'ecmoco_dof_sw_exp';
ecmoco_dof_sw_exp.name    = 'Choose predefined option';
ecmoco_dof_sw_exp.help    = {
    'a) Rigid body: [1 1 1 0 0 0 0]'
    'b) Rigid body and eddy current distortions: [1 1 1 0 1 1 1]'
    'c) Recommended for spinal cord: [1 1 0 0 1 0 0]]'};
ecmoco_dof_sw_exp.labels = {'Rigid body', 'Rigod body and eddy current distortions', 'Recommended for spinal cord'};
ecmoco_dof_sw_exp.values  = {[1 1 1 0 0 0 0] [1 1 1 0 1 1 1] [1 1 0 0 1 0 0]};
ecmoco_dof_sw_exp.val     = {[1 1 0 0 1 0 0]};

ecmoco_dof_sw_manual_exp         = cfg_entry;
ecmoco_dof_sw_manual_exp.tag     = 'ecmoco_dof_sw_manual';
ecmoco_dof_sw_manual_exp.name    = 'Manual expression';
ecmoco_dof_sw_manual_exp.help    = {'Specify the degrees of freedom (DOFs) of the registration. You can choose between 7 affine parameters.The 7 binary entries represent (1-2) translation along x,y; (3) rotation around z; (4-5) scaling along x,y; (6-7) shearing along x and y.'
    'Note that the input vector for this parameter must have 7 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco_dof_sw_manual_exp.strtype = 'e';
ecmoco_dof_sw_manual_exp.num     = [1 7];
ecmoco_dof_sw_manual_exp.val     = {[0 0 0 0 0 0 0]};

ecmoco_dof_sw_type        = cfg_choice;
ecmoco_dof_sw_type.tag    = 'ecmoco_dof_sw_type';
ecmoco_dof_sw_type.name   = 'Select degrees of freedom input type for slice-wise (2D) registration';
ecmoco_dof_sw_type.help   = {'Specify the degrees of freedom (DOFs) of the registration. You can choose between 7 affine parameters. The 7 binary entries represent (1-2) translation along x,y; (3) rotation around z; (4-5) scaling along x,y; (6-7) shearing along x and y.' 
    'Note that the input vector for this parameter must have 7 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco_dof_sw_type.values = {ecmoco_dof_sw_file, ecmoco_dof_sw_exp, ecmoco_dof_sw_manual_exp};
ecmoco_dof_sw_type.val    = {ecmoco_dof_sw_exp};

ecmoco_branch_vw         = cfg_branch;
ecmoco_branch_vw.tag     = 'ecmoco_branch_vw';
ecmoco_branch_vw.name    = 'Volume-wise (3D)';
% bvals_bvecs_exp_type.help    = {'Specify the b-values (bval) and b-vectors of the N dMRI volumes.'
%     'Note: The expression should contain a 4 x N vector, where b-values should appear in the same order as the source images were entered (b-values: 1 x N, b-vectors: (2-4) x N). b-values are expected in units of s/mm^2.'};
ecmoco_branch_vw.val  = {ecmoco_dof_vw_type};

ecmoco_branch_sw         = cfg_branch;
ecmoco_branch_sw.tag     = 'ecmoco_branch_sw';
ecmoco_branch_sw.name    = 'Slice-wise (2D)';
% bvals_bvecs_exp_type.help    = {'Specify the b-values (bval) and b-vectors of the N dMRI volumes.'
%     'Note: The expression should contain a 4 x N vector, where b-values should appear in the same order as the source images were entered (b-values: 1 x N, b-vectors: (2-4) x N). b-values are expected in units of s/mm^2.'};
ecmoco_branch_sw.val  = {ecmoco_dof_sw_type};

ecmoco_branch_vw_sw        = cfg_branch;
ecmoco_branch_vw_sw.tag     = 'ecmoco_branch_vw_sw';
ecmoco_branch_vw_sw.name    = 'Volume-wise (3D) + Slice-wise (2D)';
% bvals_bvecs_exp_type.help    = {'Specify the b-values (bval) and b-vectors of the N dMRI volumes.'
%     'Note: The expression should contain a 4 x N vector, where b-values should appear in the same order as the source images were entered (b-values: 1 x N, b-vectors: (2-4) x N). b-values are expected in units of s/mm^2.'};
ecmoco_branch_vw_sw.val  = {ecmoco_dof_vw_type, ecmoco_dof_sw_type};

% registration type
ecmoco_dummy_type         = cfg_choice;
ecmoco_dummy_type.tag     = 'ecmoco_dummy_type';
ecmoco_dummy_type.name    = 'Select registration type';
ecmoco_dummy_type.help    = {'Specify type of registration: volume-wise (3D), slice-wise (2D), or their combination.'
    'We recommend to use volume-wise registration for the brain, and volume-wise + slice-wise for the spinal cord.',};
% ecmoco_dummy_type.labels = {'Volume-wise (3D)','Slice-wise (2D)', 'Volume-wise (3D) + Slice-wise (2D)'};
ecmoco_dummy_type.values = {ecmoco_branch_vw, ecmoco_branch_sw, ecmoco_branch_vw_sw};
ecmoco_dummy_type.val    = {ecmoco_branch_vw};

% target mode
ecmoco_dummy_single_or_multi_target      = cfg_menu;
ecmoco_dummy_single_or_multi_target.tag  = 'ecmoco_dummy_single_or_multi_target';
ecmoco_dummy_single_or_multi_target.name = 'Target mode (single or multi target)';
ecmoco_dummy_single_or_multi_target.help = {'Single target:'
    '"EC_MO_correction" is a modification of the "spm_coreg" algorithm written by John Ashburner (http://www.fil.ion.ucl.ac.uk/spm) for dealing with EC distortions and motion correction of diffusion weighted images based on a linear eddy current model described in Mohammadi at al. (2010).'
    'Please cite Mohammadi et al., MRM 2010 when using this code.'
    'Because this toolbox uses the optimization function of spm_coreg, it is modality independent, i.e. in principal any low-b-value image or high-b-value image can be used as target, provided the SNR of the DTI dataset is high enough. It is recommend to use either a low-b-value image or the mean of the high-b-value images as target.'
    'Multi target:'
    'This method uses for each diffusion shell a specific target, which resamples the average contrast and SNR of the images of each shell.'
    'It aims at compensating for a potential reduction of the performance of the standard eddy current and motion correction method if the SNR is very low.'
    };
ecmoco_dummy_single_or_multi_target.labels = {
    'Single target'
    'Multi target'
    }';
ecmoco_dummy_single_or_multi_target.values    = {true false};
ecmoco_dummy_single_or_multi_target.val       = {false};

% target image
ecmoco_target         = cfg_files;
ecmoco_target.tag     = 'ecmoco_target';
ecmoco_target.name    = 'Target image (only for target mode: single target)';
ecmoco_target.help    = {'Select one reference image for single target mode. All images will be registered to this image. Recommended is using either the low-b-value image or the mean of the high-b-value images as target.'};
ecmoco_target.filter  = 'image';
ecmoco_target.ufilter = '.*';
ecmoco_target.num     = [0 1];
ecmoco_target.val     = {{''}};

% % initialization mode
% ecmoco_dummy_init      = cfg_menu;
% ecmoco_dummy_init.tag  = 'ecmoco_dummy_init';
% ecmoco_dummy_init.name = 'Initialization mode';
% ecmoco_dummy_init.help = {'If ON, the iteration for the transformation parameters (TP) is initialized by the TPs of the b0 volumes.'
%     'It is the recommended option if the b0 and higher b-shells are acquired interspersed.'
%     };
% ecmoco_dummy_init.labels = {
%     'ON'
%     'OFF'
%     }';
% ecmoco_dummy_init.values    = {true false};
% ecmoco_dummy_init.val       = {true};

% excluded
ecmoco_excluded         = cfg_files;
ecmoco_excluded.tag     = 'ecmoco_excluded';
ecmoco_excluded.name    = 'Excluded volumes';
ecmoco_excluded.help    = {'Indices of volumes which will be ignored in the registration.'};
ecmoco_excluded.filter  = 'mat|txt';
ecmoco_excluded.ufilter = '.*';
ecmoco_excluded.num     = [0 1];
ecmoco_excluded.val     = {{''}};

% mask
ecmoco_mask         = cfg_files;
ecmoco_mask.tag     = 'ecmoco_mask';
ecmoco_mask.name    = 'Mask (binary)';
ecmoco_mask.help    = {'Select binary mask. Regions outside the mask will be ignored when obtaining the registration parameters.'};
ecmoco_mask.filter  = 'image';
ecmoco_mask.ufilter = '.*';
ecmoco_mask.num     = [0 1];
ecmoco_mask.val     = {{''}};

% % zsmooth
% ecmoco_zsmooth      = cfg_menu;
% ecmoco_zsmooth.tag  = 'ecmoco_zsmooth';
% ecmoco_zsmooth.name = 'z-averaging';
% ecmoco_zsmooth.help = {'Activate averaging in z-direction (only for slice-wise registration).',};
% ecmoco_zsmooth.labels = {
%     'ON'
%     'OFF'
%     }';
% ecmoco_zsmooth.values = {1 0};
% ecmoco_zsmooth.val    = {0};

% number of workers for parallel programming (npools)
npool         = cfg_entry;
npool.tag     = 'npool';
npool.name    = 'Number of workers (parallel programming)';
npool.help    = {'Specify number of workers for parallel computation. A value of 1 indicates that no parallel computing is used. A value above 1 requires the Parallel Computing Toolbox.'};
npool.strtype = 'e';
npool.num     = [1 1];
npool.val     = {1};

% write resliced images
ecmoco_dummy_write        = cfg_menu;
ecmoco_dummy_write.tag    = 'ecmoco_dummy_write';
ecmoco_dummy_write.name   = 'Choose write option';
ecmoco_dummy_write.help   = {'Choose whether you want to reslice and output the registered images. (default: ON)'};
ecmoco_dummy_write.labels = {
    'ON'
    'OFF'
    }';
ecmoco_dummy_write.values = {1 0};
ecmoco_dummy_write.val    = {1};

% phase-encoding direction
ecmoco_phdir      = cfg_menu;
ecmoco_phdir.tag  = 'ecmoco_phdir';
ecmoco_phdir.name = 'Phase-encoding direction';
ecmoco_phdir.help = {''
'Specify the phase-encoding direction.'
''};
ecmoco_phdir.labels = {'x','y','z'};
ecmoco_phdir.values = {1 2 3};
ecmoco_phdir.val    = {2};

% exbranch
ecmoco      = cfg_exbranch;
ecmoco.tag  = 'ecmoco';
ecmoco.name = 'ECMOCO: create new';
ecmoco.val  = {ecmoco_sources, ecmoco_dummy_single_or_multi_target, ecmoco_target, bvals_bvecs, ecmoco_dummy_type, ecmoco_mask, ecmoco_excluded, ecmoco_phdir, npool};
ecmoco.help = {
    'Multi Targe Eddy Current and Motion Correction.'
    'This method uses for each diffusion shell a specific target, which resamples the average contrast and SNR of the images of each shell.'
    'It aims at compensating for a potential reduction of the performance of the standard eddy current and motion correction method if the SNR is very low.'
    };
ecmoco.prog = @local_ecmoco;
ecmoco.vout = @vout_ecmoco;

end

function out = local_ecmoco(job)

    ecmoco_interpol    = acid_get_defaults('ecmo.interpol_reslice');
    ecmoco_plot        = acid_get_defaults('ecmo.plot');
    ecmoco_dummy_write = acid_get_defaults('ecmo.write');
    ecmoco_sep         = acid_get_defaults('ecmo.separation');
    rounding_tolerance = acid_get_defaults('bval.rounding_tolerance');
    ecmoco_dummy_init  = acid_get_defaults('ecmo.initialization');
    ecmoco_zsmooth_slc_number = acid_get_defaults('ecmo.zsmooth');

    %% VW
    if isfield(job.ecmoco_dummy_type,'ecmoco_branch_vw')

        ecmoco_dummy_type = 0;

        if isfield(job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type,'ecmoco_dof_vw_file')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_file = job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type.ecmoco_dof_vw_file;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type,'ecmoco_dof_vw_exp')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_exp = job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type.ecmoco_dof_vw_exp;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type,'ecmoco_dof_vw_manual')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_manual = job.ecmoco_dummy_type.ecmoco_branch_vw.ecmoco_dof_vw_type.ecmoco_dof_vw_manual;
        end
    end

    %% SW
    if isfield(job.ecmoco_dummy_type,'ecmoco_branch_sw')

        ecmoco_dummy_type = 1;

        if isfield(job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_file')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_file = job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_file;     
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_exp')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_exp = job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_exp;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_manual')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_manual = job.ecmoco_dummy_type.ecmoco_branch_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_manual;
        end
    end

    %% VW + SW (VW)
    if isfield(job.ecmoco_dummy_type,'ecmoco_branch_vw_sw')

        ecmoco_dummy_type = 2;

        if isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type,'ecmoco_dof_vw_file')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_file = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type.ecmoco_dof_vw_file;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type,'ecmoco_dof_vw_exp')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_exp = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type.ecmoco_dof_vw_exp;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type,'ecmoco_dof_vw_manual')
            job.ecmoco_dof_vw_type.ecmoco_dof_vw_manual = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type.ecmoco_dof_vw_manual;
        end

        if isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_file')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_file = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_file;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_exp')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_exp = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_exp;
        elseif isfield(job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type,'ecmoco_dof_sw_manual')
            job.ecmoco_dof_sw_type.ecmoco_dof_sw_manual = job.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_manual;
        end
    end

    % read in dof_vw
    if isfield(job,'ecmoco_dof_vw_type')

        if isfield(job.ecmoco_dof_vw_type,'ecmoco_dof_vw_file')
            job.ecmoco_dof_vw = job.ecmoco_dof_vw_type.ecmoco_dof_vw_file;
            job.ecmoco_dof_vw = cell2mat(job.ecmoco_dof_vw);
            [~,~,e] = fileparts(job.ecmoco_dof_vw);
            if ~isempty(job.ecmoco_dof_vw_type.ecmoco_dof_vw_file{1})
                switch lower(e)
                    case '.mat'
                        job.ecmoco_dof_vw = cell2mat(struct2cell(load(job.ecmoco_dof_vw)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        job.ecmoco_dof_vw = dlmread(job.ecmoco_dof_vw);
                end
            else
                job.ecmoco_dof_vw = '';
            end
        elseif isfield(job.ecmoco_dof_vw_type,'ecmoco_dof_vw_exp')
            if ~isempty(job.ecmoco_dof_vw_type.ecmoco_dof_vw_exp)
                job.ecmoco_dof_vw = job.ecmoco_dof_vw_type.ecmoco_dof_vw_exp;
            else
                job.ecmoco_dof_vw = '';
            end
        elseif isfield(job.ecmoco_dof_vw_type,'ecmoco_dof_vw_manual')
            if ~isempty(job.ecmoco_dof_vw_type.ecmoco_dof_vw_manual)
                job.ecmoco_dof_vw = job.ecmoco_dof_vw_type.ecmoco_dof_vw_manual;
            else
                job.ecmoco_dof_vw = '';
            end
        end

    else
        job.ecmoco_dof_vw = '';
    end

    if isfield(job,'ecmoco_dof_sw_type')
        % read in dof_sw
        if isfield(job.ecmoco_dof_sw_type,'ecmoco_dof_sw_file')
            job.ecmoco_dof_sw = job.ecmoco_dof_sw_type.ecmoco_dof_sw_file;
            job.ecmoco_dof_sw = cell2mat(job.ecmoco_dof_sw);
            [~,~,e] = fileparts(job.ecmoco_dof_sw);
            if ~isempty(job.ecmoco_dof_sw_type.ecmoco_dof_sw_file{1})
                switch lower(e)
                    case '.mat'
                        job.ecmoco_dof_sw = cell2mat(struct2cell(load(job.ecmoco_dof_sw)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        job.ecmoco_dof_sw = dlmread(job.ecmoco_dof_sw);
                end
            else
                job.ecmoco_dof_sw = '';
            end
        elseif isfield(job.ecmoco_dof_sw_type,'ecmoco_dof_sw_exp')
            if ~isempty(job.ecmoco_dof_sw_type.ecmoco_dof_sw_exp)
                job.ecmoco_dof_sw = job.ecmoco_dof_sw_type.ecmoco_dof_sw_exp;
            else
                job.ecmoco_dof_sw = '';
            end
        elseif isfield(job.ecmoco_dof_sw_type,'ecmoco_dof_sw_manual')
            if ~isempty(job.ecmoco_dof_sw_type.ecmoco_dof_sw_manual)
                job.ecmoco_dof_sw = job.ecmoco_dof_sw_type.ecmoco_dof_sw_manual;
            else
                job.ecmoco_dof_sw = '';
            end
        end
    else
        job.ecmoco_dof_sw = '';
    end

    if ecmoco_dummy_type==0
        job.ecmoco_dof = job.ecmoco_dof_vw;
    elseif ecmoco_dummy_type==1
        job.ecmoco_dof = job.ecmoco_dof_sw;
    end

    ecmoco_thr_hist = 0; % no thresholding for now

    % read in bvals and bvecs
    if isfield(job.bvals_bvecs,'bvals_bvecs_file')

        job.files1 = job.bvals_bvecs.bvals_bvecs_file{1,1};
        job.files2 = job.bvals_bvecs.bvals_bvecs_file{2,1};
        [~,~,e] = fileparts(job.files1);

        if ~isempty(job.files1)
            switch lower(e)
                case '.mat'
                    if size(cell2mat(struct2cell(load(job.files1))),1) == 1 && size(cell2mat(struct2cell(load(job.files2))),1) == 3
                        job.bvals = cell2mat(struct2cell(load(job.files1)));
                        job.bvecs = cell2mat(struct2cell(load(job.files2)));
                    elseif size(cell2mat(struct2cell(load(job.files1))),1) == 3 && size(cell2mat(struct2cell(load(job.files2))),1) == 1
                        job.bvecs = cell2mat(struct2cell(load(job.files1)));
                        job.bvals = cell2mat(struct2cell(load(job.files2)));
                    else
                        error('Unexpected file extension!')
                    end

                case '.nii'
                    error('Unexpected file extension!')
                otherwise
                    if size(dlmread(job.files1),1) == 1 && size(dlmread(job.files2),1) == 3
                        job.bvals = dlmread(job.files1);
                        job.bvecs = dlmread(job.files2);
                    elseif size(dlmread(job.files1),1) == 3 && size(dlmread(job.files2),1) == 1
                        job.bvecs = dlmread(job.files1);
                        job.bvals = dlmread(job.files2);
                    else
                        error('Unexpected file extension!')
                    end
            end
        else
            job.bvals = '';
            job.bvecs = '';
        end

    elseif isfield(job.bvals_bvecs,'bvals_bvecs_exp_type')

        if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvals_exp')
            if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp)
                job.bvals = job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp;
            else
                job.bvals = '';
            end
        end

        if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvecs_exp')
            if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp)
                job.bvecs = job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp;
            else
                job.bvecs = '';
            end
        end
    end

    [VG,bvecs_rot,fname_out] = acid_ecmoco(char(job.ecmoco_sources), ecmoco_dummy_type, job.ecmoco_dof_vw, job.ecmoco_dof_sw, job.bvals, job.bvecs, ...
        ecmoco_dummy_init, job.ecmoco_excluded, char(job.ecmoco_mask), '', ecmoco_dummy_write, ecmoco_plot, ...
        ecmoco_thr_hist, ecmoco_sep, ecmoco_interpol, job.ecmoco_phdir, ecmoco_zsmooth_slc_number, job.npool, ...
        job.ecmoco_dummy_single_or_multi_target, job.ecmoco_target, rounding_tolerance);

    % get basename for the saved files
    keyword = 'ECMOCO';
    k = strfind(VG(1).fname,keyword);
    fname_base = VG(1).fname(1:k(end)+length(keyword)-1);
    
    % get saved ECMOCO corrected images
    % fname_out = [fname_base '_dwi.nii'];
    for i = 1:size(job.ecmoco_sources,1)   
        out.rfiles{i,:} = [fname_out ',' num2str(i)]; 
    end
    out.rfile1 = out.rfiles(1);

    % get saved target images
    VG = VG(:);
    for i = 1:size(VG,1)
        out.VGfiles(i,1) = {VG(i).fname};
    end

    % get saved mean b0 image
    out.b0 = {[fname_base '-mean-b0.nii']};
    
    % get ECMOCO parameters
    out.matfile = {[fname_base '-params.mat']};
    
    % get bvalues
    out.bval = job.bvals;

    % get bvalues
    out.bvecs = bvecs_rot;
end

function dep = vout_ecmoco(job)
    kk = 1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Corrected images';
    dep(kk).src_output = substruct('.','rfiles');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;

    if job.ecmoco_dummy_single_or_multi_target == 0
        dep(kk)            = cfg_dep;
        dep(kk).sname      = 'First corrected image';
        dep(kk).src_output = substruct('.','rfile1');
        dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
        kk = kk+1;
        dep(kk)            = cfg_dep;
        dep(kk).sname      = 'Target image(s)';
        dep(kk).src_output = substruct('.','VGfiles');
        dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
        kk = kk+1;
        dep(kk)            = cfg_dep;
        dep(kk).sname      = 'Average b0 image (corrected)';
        dep(kk).src_output = substruct('.','b0');
        dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
        kk = kk+1;
    end

    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'b-values';
    dep(kk).src_output = substruct('.','bval');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Rotated b-vectors';
    dep(kk).src_output = substruct('.','bvecs');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Registration parameters';
    dep(kk).src_output = substruct('.','matfile');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
end