function topup_call = acid_topup_call()


% source images
topup_sources         = cfg_files;
topup_sources.tag     = 'topup_sources';
topup_sources.name    = 'Images (--imain)';
topup_sources.help    = {'Select source images (4D NIfTI).'};
topup_sources.filter  = 'image';
topup_sources.ufilter = '.*';
topup_sources.num     = [0 1];



% datain
topup_datain         = cfg_files;
topup_datain.tag     = 'topup_datain';
topup_datain.name    = 'Data input file (--datain)';
topup_datain.help    = {'Select the datain txt file.'};
topup_datain.filter  = 'txt';
topup_datain.ufilter = '.*';
topup_datain.num     = [0 1];



% config
topup_config         = cfg_files;
topup_config.tag     = 'topup_config';
topup_config.name    = 'config file (--config)';
topup_config.help    = {'Select the config txt file.'};
topup_config.filter  = 'txt';
topup_config.ufilter = '.*';
topup_config.num     = [0 1];


% % output directory
% fsl_dir         = cfg_files;
% fsl_dir.tag     = 'fsl_dir';
% fsl_dir.name    = 'FSL directory';
% fsl_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% fsl_dir.filter  = 'dir';
% fsl_dir.ufilter = '.*';
% fsl_dir.num     = [0 1];
% % fsl_dir.val     = {{''}};


% % output directory
% topup_input_dir         = cfg_files;
% topup_input_dir.tag     = 'topup_input_dir';
% topup_input_dir.name    = 'Input data directory';
% topup_input_dir.help    = {'Select the output directory. If unspecified, the output will be saved in the derivatives folder.'};
% topup_input_dir.filter  = 'dir';
% topup_input_dir.ufilter = '.*';
% topup_input_dir.num     = [0 1];
% topup_input_dir.val     = {{''}};



% file name
terminal_call         = cfg_entry;
terminal_call.tag     = 'terminal_call';
terminal_call.name    = 'Additonal topup input parameter';
terminal_call.help    = {''};
terminal_call.strtype = 's';
terminal_call.val     = {''};



%% satrtup
topup_call         = cfg_exbranch;
topup_call.tag     = 'topup_call';
topup_call.name    = 'topup';
topup_call.val     = { topup_sources, topup_datain, topup_config, terminal_call};
topup_call.help    = { '' };
topup_call.prog = @local_topup;
topup_call.vout = @vout_topup;


end

function out = local_topup(job)



terminal_call = char(job.terminal_call);

[input_dir,input_name,~] = spm_fileparts(char(job.topup_sources));


V = acid_load_4Dimage(char(job.topup_sources));

keyword = 'topup';

p_out = acid_bids(input_dir,input_name,keyword,1);

fname = acid_bids_filename(V,keyword,'_dwi','');

output_name = [p_out filesep fname(1:end-4)];

cd(input_dir)

imain = char(job.topup_sources);

%--imain=my_hifi_b0_brain_mask --mask=my_hifi_b0_brain_mask --acqp=datain.txt --config=config.txt  --out=topup_corrected_data
command = ['topup' ' --imain=' imain(1:end-2) ' --datain=' char(job.topup_datain)  ' --config=' char(job.topup_config) ' --out=' output_name ' ' terminal_call];

[status,cmdout] = system(command);


disp(status)
disp(cmdout)


command_fields = strsplit(terminal_call);

out_config = find(contains(command_fields, '--out'));

if ~isempty(out_config) 
    extracted_value = command_fields{out_config};
else
    extracted_value = '';
    warning('Parameter "--out" not found!');
end

topup_output = [output_name '_fieldcoef'];

unzip_file = [topup_output '.nii.gz'];

unzip_command = ['gzip -dk -f ' unzip_file];

% [status,cmdout] = system(unzip_command);

disp(status)
disp(cmdout)

out.rsource{1,:} = [topup_output '.nii,1'];


end

function dep = vout_topup(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'topup processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

end