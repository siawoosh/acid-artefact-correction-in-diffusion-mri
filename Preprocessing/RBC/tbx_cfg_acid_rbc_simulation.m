function rbc_simulation = tbx_cfg_acid_rbc_simulation


% noise estimate (sigma)
SNR         = cfg_entry;
SNR.tag     = 'SNR';
SNR.name    = 'SNRs';
SNR.help    = {'Type in the SNRs.'};
SNR.strtype = 'e';
SNR.num     = [0 Inf];
SNR.val     = {[15,16]};

% noise estimate (sigma)
number_of_noise_realizations         = cfg_entry;
number_of_noise_realizations.tag     = 'number_of_noise_realizations';
number_of_noise_realizations.name    = 'Number of noise realizations';
number_of_noise_realizations.help    = {'Type in a scalar for the noise estimate.'};
number_of_noise_realizations.strtype = 'e';
number_of_noise_realizations.num     = [0 1];
number_of_noise_realizations.val     = {[2500]};


voxel_inx      = cfg_menu;
voxel_inx.tag  = 'voxel_inx';
voxel_inx.name = 'Voxel';
voxel_inx.help = {''
'Specify the RBC correction type'
''};
voxel_inx.labels = {'cb_voxel_1', 'cb_voxel_2', 'cb_voxel_3', ...
'ct_voxel_1', 'ct_voxel_2', 'ct_voxel_3', ...
'or_voxel_1', 'or_voxel_2', 'or_voxel_3', ...
'slf_voxel_1', 'slf_voxel_2', 'slf_voxel_3', ...
'mc_voxel_1', 'mc_voxel_2', 'mc_voxel_3', ...
'vc_voxel_1', 'vc_voxel_2', 'vc_voxel_3', ...
'th_voxel_1', 'th_voxel_2', 'th_voxel_3', ...
'fc_voxel_1', 'fc_voxel_2', 'fc_voxel_3'};%'HA', 'MA','LA'};


voxel_inx.values = {1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24}; % 25 26 27};
voxel_inx.val    = {1};


%% call local resample function
rbc_simulation      = cfg_exbranch;
rbc_simulation.tag  = 'rbc_simulation';
rbc_simulation.name = 'Rician bias simulation';
rbc_simulation.val  = {voxel_inx SNR number_of_noise_realizations };
rbc_simulation.help = {
    'Rician bias correction on raw data using the M2 method described in Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531).'
    };
rbc_simulation.prog = @local_rbc_simulation;

end

function local_rbc_simulation(job)  

    acid_rbc_simulation(job.SNR, job.number_of_noise_realizations,job.voxel_inx);

end
