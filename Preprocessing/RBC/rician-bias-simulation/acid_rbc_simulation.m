function acid_rbc_simulation(simulated_SNRs,number_of_noise_realizations,voxel_inx)

path_of_script = fileparts(mfilename('fullpath'));
addpath(genpath([path_of_script filesep 'Functions']));

AxTM = {'RW','AW', 'MW','RD','AD'};

simulate_data(path_of_script,number_of_noise_realizations,simulated_SNRs,voxel_inx);

results = struct;


voxel_folder = {'cb_voxel_1', 'cb_voxel_2', 'cb_voxel_3', ...
'ct_voxel_1', 'ct_voxel_2', 'ct_voxel_3', ...
'or_voxel_1', 'or_voxel_2', 'or_voxel_3', ...
'slf_voxel_1', 'slf_voxel_2', 'slf_voxel_3', ...
'mc_voxel_1', 'mc_voxel_2', 'mc_voxel_3', ...
'vc_voxel_1', 'vc_voxel_2', 'vc_voxel_3', ...
'th_voxel_1', 'th_voxel_2', 'th_voxel_3', ...
'fc_voxel_1', 'fc_voxel_2', 'fc_voxel_3', ...
'HA', 'MA','LA'};


voxel_name   = voxel_folder{voxel_inx};
voxel_folder = voxel_folder(voxel_inx);

create_directories_results(path_of_script, voxel_folder, simulated_SNRs)

fit_voxels(path_of_script,simulated_SNRs,voxel_folder)


acid_rbc_simulation_plot(path_of_script,simulated_SNRs,voxel_name,voxel_inx)

cd(path_of_script)

try rmdir([path_of_script filesep 'simulation_data' filesep 'simulation_of_paper_data'],'s')
catch
end

try rmdir([path_of_script filesep 'Results_And_Figures' filesep 'Fit_Results'],'s')
catch
end

end
