% List of open inputs
% 3D to 4D File Conversion: 3D Volumes - cfg_files
% Image Calculator: Input Images - cfg_files
% Image Calculator: Expression - cfg_entry
nrun = X; % enter the number of runs here
jobfile = {'/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/Preprocessing/RBC/axisymmetric_dki_with_rician_bias_correction_simulation_study/SPM_Batches/RBC-module-batch_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(3, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % 3D to 4D File Conversion: 3D Volumes - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % Image Calculator: Input Images - cfg_files
    inputs{3, crun} = MATLAB_CODE_TO_FILL_INPUT; % Image Calculator: Expression - cfg_entry
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
