% List of open inputs
% Diffusion Kurtosis Imaging (DKI): Input images - cfg_files
% Diffusion Kurtosis Imaging (DKI): b-values - cfg_entry
% Diffusion Kurtosis Imaging (DKI): b-vectors - cfg_entry
% Diffusion Kurtosis Imaging (DKI): Expression/Dependency - cfg_entry
nrun = X; % enter the number of runs here
jobfile = {'/Users/fricke/Documents/Matlab_Stuff/simulation-repo/axisymmetric_dki_with_rician_bias_correction_simulation_study-main/SPM_Batches/StandardDKI_RBC_new_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(4, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % Diffusion Kurtosis Imaging (DKI): Input images - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % Diffusion Kurtosis Imaging (DKI): b-values - cfg_entry
    inputs{3, crun} = MATLAB_CODE_TO_FILL_INPUT; % Diffusion Kurtosis Imaging (DKI): b-vectors - cfg_entry
    inputs{4, crun} = MATLAB_CODE_TO_FILL_INPUT; % Diffusion Kurtosis Imaging (DKI): Expression/Dependency - cfg_entry
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
