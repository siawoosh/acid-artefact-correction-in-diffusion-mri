%-----------------------------------------------------------------------
% Job saved on 07-Jul-2023 11:43:54 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_DKI = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.in_vols = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_nlls = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_RBC.RBC_ON.dummy_RBC_ON = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type.sigma_exp = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_RBC.RBC_ON.n_coils = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_RBC.RBC_ON.map_noise = {''};
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.dummy_write_W = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.dummy_algo.nlls.n_workers = 1;
matlabbatch{1}.spm.tools.dti.fit_choice.dki_fit.mask = {''};
