% List of open inputs
% Rician bias correction: Input images - cfg_files
% Rician bias correction: Noise estimate - cfg_entry
% Rician bias correction: RBC correction type - cfg_menu
nrun = X; % enter the number of runs here
jobfile = {'/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/Preprocessing/RBC/rician-bias-simulation/SPM_Batches/RBC-module-batch_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(3, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % Rician bias correction: Input images - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % Rician bias correction: Noise estimate - cfg_entry
    inputs{3, crun} = MATLAB_CODE_TO_FILL_INPUT; % Rician bias correction: RBC correction type - cfg_menu
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
