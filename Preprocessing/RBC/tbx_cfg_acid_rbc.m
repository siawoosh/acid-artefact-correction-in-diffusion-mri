function powrice = tbx_cfg_acid_rbc(in_vols, p_out)

% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

% noise estimate (sigma)
% sigma         = cfg_entry;
% sigma.tag     = 'sigma';
% sigma.name    = 'Noise estimate';
% sigma.help    = {'Type in a scalar for the noise estimate.'};
% sigma.strtype = 'e';
% sigma.num     = [0 1];
% sigma.val     = {[1]};


sigma_file         = cfg_files;
sigma_file.tag     = 'sigma_file';
sigma_file.name    = 'File (*.txt / *.mat)';
sigma_file.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_file.filter  = 'any';
sigma_file.ufilter = '.*txt';
sigma_file.num     = [0 1];

sigma_exp         = cfg_entry;
sigma_exp.tag     = 'sigma_exp';
sigma_exp.name    = 'Expression/Dependency';
sigma_exp.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_exp.strtype = 'e';
sigma_exp.num     = [Inf Inf];

sigma_map         = cfg_files;
sigma_map.tag     = 'sigma_map';
sigma_map.name    = 'Noise map (*.nii)';
sigma_map.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_map.filter  = 'any';
sigma_map.ufilter = 'nii';
sigma_map.num     = [0 1];

sigma_type        = cfg_choice;
sigma_type.tag    = 'sigma_type';
sigma_type.name   = 'sigma (noise estimate)';
sigma_type.help   = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_type.values = {sigma_exp, sigma_map, sigma_file};
sigma_type.val    = {sigma_exp};



% phase-encoding direction
dummy_cond      = cfg_menu;
dummy_cond.tag  = 'dummy_cond';
dummy_cond.name = 'RBC correction type';
dummy_cond.help = {''
'Specify the RBC correction type'
''};
dummy_cond.labels = {'RBC Koay Ades-Aron','RBC Koay'};
dummy_cond.values = {1 2};
dummy_cond.val    = {1};


% % number of channels
% n_channel         = cfg_entry;
% n_channel.tag     = 'n_channel';
% n_channel.name    = 'Effective number of channels';
% n_channel.help    = {'The effective number of channels scales with the parallel imaging factor, i.e., it is >= 2 for PAT=2.'};
% n_channel.strtype = 'e';
% n_channel.num     = [1 1];
% n_channel.val     = {1e0};

%% call local resample function
powrice      = cfg_exbranch;
powrice.tag  = 'powrice';
powrice.name = 'Rician bias correction';
powrice.val  = {in_vols sigma_type dummy_cond};
powrice.help = {
    'Rician bias correction on raw data using the M2 method described in Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531).'
    };
powrice.prog = @local_rbc;
powrice.vout = @vout_rbc;

end

function out = local_rbc(job)  

    if ~isfield(job,'dummy_cond')
        job.dummy_cond = 1;
    end

 % read in sigma
        if isfield(job.sigma_type, 'sigma_exp')
            sigma = job.sigma_type.sigma_exp;
        elseif isfield(job.sigma_type, 'sigma_file')
            if ~isempty(job.sigma_type.sigma_file{1})
                sigma = job.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        elseif isfield(job.sigma_type, 'sigma_map')
            sigma = job.sigma_type.sigma_map{1};
        end




    fname_out = acid_rbc(char(job.in_vols), sigma, 1, job.dummy_cond);
    out.files = {[fname_out ',1']};
end

function dep = vout_rbc(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Rician bias corrected';
    dep(1).src_output = substruct('.','files');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end