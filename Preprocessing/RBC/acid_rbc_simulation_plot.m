function acid_rbc_simulation_plot(path_of_script,SNRlist,Voxel,voxel_inx)


DKI_Variables ={'RD','AD','RW','AW','MW'};

for i = 1:5
        if i == 1
            figure('DefaultAxesFontSize',25);
            subplot(1,5,i)
            hold on
            set(gcf, 'Position',  [0, 100, 2634, 1024])
            set(gca,'units','centimeters')
            pos = get(gca,'Position');
            ti = get(gca,'TightInset');
        else
            subplot(1,5,i)
            hold on
            set(gcf, 'Position',  [0, 100, 2634, 1024])
            set(gca,'units','centimeters')
            pos = get(gca,'Position');
            ti = get(gca,'TightInset');
        end
    for k = 1:size(SNRlist,2)

        SNR = SNRlist(:,k);

        load([path_of_script filesep 'Datasets' filesep 'GroundTruth' filesep 'GT_Table.mat']);

        y = table2array(GT_Table(voxel_inx,:));


        data1 = [ path_of_script '/Results_And_Figures/Fit_Results/voxels/' Voxel '/simulated_SNR_' num2str(SNR) '/standardDKI/DKI-NLLS/SNR_' num2str(SNR) '_simulation_standard_DKI_001_desc-DKI-NLLS-' DKI_Variables{i} '_map.nii']; % no RBC DKI
        data2 = [ path_of_script '/Results_And_Figures/Fit_Results/voxels/' Voxel '/simulated_SNR_' num2str(SNR) '/standardDKI_RBC/DKI-NLLS-RBC/SNR_' num2str(SNR) '_simulation_standard_DKI_001_desc-DKI-NLLS-RBC-' DKI_Variables{i} '_map.nii']; % DKI-RBC
        data3 = [ path_of_script '/Results_And_Figures/Fit_Results/voxels/' Voxel '/simulated_SNR_' num2str(SNR) '/RBC-module-old/DKI-NLLS/SNR_' num2str(SNR) '_simulation_standard_DKI_001_desc-RBC-DKI-NLLS-' DKI_Variables{i} '_map.nii']; % RBC old
        data4 = [ path_of_script '/Results_And_Figures/Fit_Results/voxels/' Voxel '/simulated_SNR_' num2str(SNR) '/RBC-module-full/DKI-NLLS/SNR_' num2str(SNR) '_simulation_standard_DKI_001_desc-RBC-DKI-NLLS-' DKI_Variables{i} '_map.nii']; % full new
        data5 = [ path_of_script '/Results_And_Figures/Fit_Results/voxels/' Voxel '/simulated_SNR_' num2str(SNR) '/RBC-module-koaya/DKI-NLLS/4D_db_desc-DKI-NLLS-' DKI_Variables{i} '_map.nii']; % koaya


        
        
        struct1 = spm_vol(data1);
        struct2 = spm_vol(data2);
        struct3 = spm_vol(data3);
        struct4 = spm_vol(data4);
        struct5 = spm_vol(data5);

        
        dm1 = [struct1(1).dim, size(struct1,1)];
        dm2 = [struct2(1).dim, size(struct2,1)];
        dm3 = [struct3(1).dim, size(struct3,1)];
        dm4 = [struct4(1).dim, size(struct4,1)];
        dm5 = [struct5(1).dim, size(struct5,1)];

        matrix1 = zeros(dm1);
        for n = 1:dm1(4)
        matrix1(:,:,:,n) = acid_read_vols(struct1(n),struct1(1),1);
        end
        
        matrix2 = zeros(dm2);
        for n = 1:dm2(4)
        matrix2(:,:,:,n) = acid_read_vols(struct2(n),struct2(1),1);
        end
        
        matrix3 = zeros(dm3);
        for n = 1:dm3(4)
        matrix3(:,:,:,n) = acid_read_vols(struct3(n),struct3(1),1);
        end
        
        matrix4 = zeros(dm4);
        for n = 1:dm4(4)
        matrix4(:,:,:,n) = acid_read_vols(struct4(n),struct4(1),1);
        end
        
        matrix5 = zeros(dm5);
        for n = 1:dm5(4)
        matrix5(:,:,:,n) = acid_read_vols(struct5(n),struct5(1),1);
        end 


        matrix_all = [reshape(matrix1(2,:,2), [], 1), reshape(matrix2(2,:,2), [], 1), reshape(matrix3(2,:,2), [], 1), reshape(matrix4(2,:,2), [], 1), reshape(matrix5(2,:,2), [], 1)];

        for j=1:5
             mean_all(:,j) = median(matrix_all(:,j));
             std_all(:,j)  = std(matrix_all(:,j));     
        end
        

        % mean_complete_1(k) = mean_all(:,1);
        % mean_complete_2(k) = mean_all(:,2);

        if i == 1 || i == 2
            mean_complete_1(k) = mean_all(:,1) * 1000;
            mean_complete_2(k) = mean_all(:,2) * 1000;
            mean_complete_3(k) = mean_all(:,3) * 1000;
            mean_complete_4(k) = mean_all(:,4) * 1000;
            mean_complete_5(k) = mean_all(:,5) * 1000;
        else
            mean_complete_1(k) = mean_all(:,1);
            mean_complete_2(k) = mean_all(:,2);
            mean_complete_3(k) = mean_all(:,3);
            mean_complete_4(k) = mean_all(:,4);
            mean_complete_5(k) = mean_all(:,5);
        end

    end
        title([DKI_Variables{i} ' Voxel: ' Voxel ', L=1'])
        line([(SNRlist(1,1)-5) (SNRlist(1,end)+5)], [y(i) y(i)], 'Color', '#A2142F', 'LineStyle', '--'); hold on;
        plot(SNRlist,(y(i) + (y(i)-mean_complete_1)),"ro", 'LineStyle', '-'); hold on;
        plot(SNRlist,(y(i) - (y(i)-mean_complete_1)),"ro", 'LineStyle', '-');
        plot(SNRlist,mean_complete_2,"g+", 'LineStyle', '--'); hold on;
        plot(SNRlist,mean_complete_3,"b*", 'LineStyle', '--'); hold on;
        plot(SNRlist,mean_complete_4,"cx", 'LineStyle', '--'); hold on;
        plot(SNRlist,mean_complete_5,"m-", 'LineStyle', '--'); hold on;
        
        legend('Ground Truth','No RBC','No RBC','DKI-RBC', 'RBC Koay Ades-Aron','RBC Koay', 'RBC Koaya external')

        xlim([(SNRlist(1,1)-5) (SNRlist(1,end)+5)])
        xlabel('SNR')
        ylabel('Median (no unit)')
end
