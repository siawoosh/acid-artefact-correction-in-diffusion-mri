function fname_out = acid_rbc(P, sigma, N, dummy_cond)

% =========================================================================
%  The script performs denoising on images with non-central Chi
%  distributed noise. Denoising consists of 3 steps:
%  1 - estimation of noise underlying gaussian standard deviation from
%      noise data (noRF acquisition)
%  2 - denoising assuming gaussian distribution (BM4D algorithm)
%  3 - correction taking into account effective non-central Chi distribution
%  4 - mapping to gaussian distribution (optional)
%
%  Step 3 is important for low SNR data and will therefore impact the
%  results of DKI fitting with high b-values.
%
%  FORMAT acid_rician_bc(encha, noise_est, P)
%
%  Inputs:
%    P      - filenames of the input dMRI data
%    sigma  - noise estimate
%    N      - effective number of channels
%    p_out  - output folder; if unspecified, the output are saved in the
%             derivatives folder
%
%  Evelyne Balteau - Cyclotron Research Centre - Liege/Belgium May 2012
%  The Rician bias correction on raw data using the M2 method described in
%  Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531) was
%  implemented to ACID toolbox by S. Mohammadi 10/10/2019
% =========================================================================
tic
% load in 4D image


[V,P_uncomp,dummy_compress] = acid_prepare_and_load_4Dimage(P);


VG = V(1);
n  = size(V,1);

% define output directory
keyword = 'RBC';
[path,fname,~] = spm_fileparts(VG.fname);

p_out = acid_bids(path,fname,keyword,1);
diary([p_out filesep 'logfile_' keyword '.txt'])


% Spatial dimension of the volumes
ddim = VG.dim;

%% sigma
if isa(sigma,'char')

    sigma_struct = spm_vol(sigma);

    sigma = acid_read_vols(sigma_struct(1), VG, -7);

    sigma_displayed = mean(sigma,'all');
    disp(['Used sigma (mean): ' num2str(sigma_displayed)]);
else

    sigma_displayed = sigma;
    sigma = ones(ddim).*sigma;
    disp(['Used sigma: ' num2str(sigma_displayed)]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% remove rice bias in all images - sigma is assumed to be the same in all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for cim = 1:n

    [~,nam] = fileparts(V(cim).fname);
    disp(['Processing ' nam ' (image #' num2str(cim) '/' num2str(size(P,1)) ')']);

    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    % load image
    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Y = acid_read_vols(V(cim),VG,1);

    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    % calculate lookup table for noise correction
    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %       [An, Mn] = eb_An_Mn_LUT(sig, max(Y(:))*1.01, N);
    %
    %         %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %         % calculate noise-free signal based on <m> estimate and LUT
    %         %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %         etaY = Y;
    %         minMn = sig*sqrt(pi/2)*fact(2*N-1,2)/(2^(N-1)*fact(N-1,1));
    %         etaY(Y<minMn) = minMn;
    %         etaY = interp1(Mn,An,etaY(:),'linear');
    %         etaY = reshape(etaY, V(cim).dim);
    %         powY    =etaY;
    %         powY(powY<=0)       = 1;
    %        powY = powY+20;

    %     %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %     % calculate noise-free signal based on power image subtraction
    %     %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    % SNR classification and handling based on https://doi.org/10.1016/j.jmr.2006.01.016

    SNR = Y./sigma;

    % Separate the SNR into three distinct regions:
    %   a. SNR < 1.913 which can not be corrected and will subsequently be set
    %      to 0
    %   b. SNR > 1.913 but < 10 which will be corrected using the look-up
    %      tables in jm_debias_ref
    %   c. SNR > 10 or SNR = NaN which will not be corrected

    ind_a = SNR < 1.913;

    ind_c = (SNR > 10) | isnan(SNR);


    powY = sqrt(Y.^2-N.*sigma.^2); % case b

    if dummy_cond == 2
        powY(ind_a)  = 0; % case a
        powY(ind_c)  = Y(ind_c); % case c
    end
    powY(imag(powY)>0)  = 0;    

    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    % save images as nii
    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    Ni.dat(:,:,:,cim) = single(powY);
    spm_progress_bar('Set',cim);

    disp(['Rician bias corrected 4D images (# ' num2str(cim) ') written'])
end

% save denoised images
V_out = VG;
V_out.descrip = '4d array of Rician corrected images';
V4D = acid_write_vol(Ni.dat, V_out, p_out, keyword, 'same', '', '', 1);
spm_progress_bar('Init',n,V_out.descrip,'volumeses completed');
spm_progress_bar('Clear');

% save json file
acid_save_json(V(1), p_out, keyword);
    
% save bvals and bvecs files
acid_save_bvals_bvecs(V(1), p_out, keyword);


[fname_out] = acid_compress_and_delete_4Dimage(dummy_compress, V4D(1,:).fname, P_uncomp);


T = toc/60;

T = duration(minutes(T),'format','hh:mm:ss');
disp(['The total time for ' keyword ' was: ' char(T) '.']);
diary off
end