function mppca_call = acid_mppca_call


% source images
mppca_sources         = cfg_files;
mppca_sources.tag     = 'mppca_sources';
mppca_sources.name    = 'Input images';
mppca_sources.help    = {'Select source images (4D NIfTI).'};
mppca_sources.filter  = 'any';
mppca_sources.ufilter = 'nii';
mppca_sources.num     = [0 Inf];


%% satrtup
mppca_call         = cfg_exbranch;
mppca_call.tag     = 'mppca_call';
mppca_call.name    = 'MP-PCA';
mppca_call.val     = {mppca_sources};
mppca_call.help    = { '' };
mppca_call.prog = @local_mppca;
mppca_call.vout = @vout_mppca;


end

function out = local_mppca(job)


files = char(job.mppca_sources);

[input_dir,input_name,~] = spm_fileparts(files(1,:));


% [input_dir,input_name,~] = spm_fileparts(char(job.mppca_sources));


V = acid_load_4Dimage(char(job.mppca_sources));

keyword = 'MPPCA';

p_out = acid_bids(input_dir,input_name,keyword,1);

fname = acid_bids_filename(V,keyword,'_dwi','');

output_name = [p_out filesep fname(1:end-4) '.nii'];

noise_map_name = [p_out filesep fname(1:end-8) '-noise_map.nii'];

cd(input_dir)

imain = char(job.mppca_sources);

% imain = imain(1:end-2);


%--imain=my_hifi_b0_brain_mask --mask=my_hifi_b0_brain_mask --acqp=acqparams.txt --index=index.txt  --out=mppca_corrected_data
command = ['dwidenoise' ' ' imain ' ' output_name ' ' '-noise' ' ' noise_map_name];

[status,cmdout] = system(command);


disp(status)
disp(cmdout)


out.rsource{1} = [output_name];

end


function dep = vout_mppca(~)
dep(1)            = cfg_dep;
dep(1).sname      = 'MPPCA processed images';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end