function [V,P_uncomp,dummy_compress] = acid_prepare_and_load_4Dimage(P)

[z,u,e,~] = spm_fileparts(P(1,:));

switch lower(e)
    case '.nii'
        V = acid_load_4Dimage(P);
        P_uncomp = P;
        dummy_compress = 0;
    case '.gz'
        P_comp = [z filesep u e];
        P_cell = acid_gunzip(P_comp);
        P = P_cell{1};
        P_uncomp = [z filesep u];
        V = acid_load_4Dimage(P_uncomp);
        dummy_compress = 1;
end