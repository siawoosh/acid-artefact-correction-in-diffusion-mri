function [fname_out] = acid_compress_and_delete_4Dimage(dummy_compress, fname_in, P_uncomp)



if dummy_compress == 0

    fname_out = fname_in;

elseif dummy_compress == 1
    fname_compressed = acid_gzip(fname_in);
    
    fname_out = fname_compressed{1};


    to_delete = convertCharsToStrings(P_uncomp);
    
    delete(to_delete)
    delete(fname_in)
end