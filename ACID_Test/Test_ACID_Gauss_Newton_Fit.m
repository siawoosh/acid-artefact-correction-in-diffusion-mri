function Test_ACID_Gauss_Newton_Fit(fit_data,check_output)

% This script checks whether the ACID Gauss Newton output is
% the same as the one computed with the ACID version of the beta_branch
% branch on 06/11/2020 with Matlab version 2019b

load bvalues.mat % b values
bvalues0 = bvalues;

[filepath,~,~] = fileparts(mfilename('fullpath'));

folder_signals = [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end

p_out = [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep 'derivatives'];

if fit_data
    for which_fit = 1:4
        
        [filepath,~,~] = fileparts(mfilename('fullpath'));      
        p_in =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep] ;

        if which_fit == 1
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DTI_NLLS.m']};
        elseif which_fit == 2
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKI_NLLS.m']};
        elseif which_fit == 3
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKIax_NLLS.m']};
        elseif which_fit == 4
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKIax_NLLS_RBC.m']};
        end
        
        jobs = repmat(jobfile, 1, 1);        
        PP = [];
        
        Ptmp = spm_select('FPList', p_in, '^simulation_data_4D.nii');
        Ptmp = [Ptmp ',1'];
        PP   = cat(1,PP,Ptmp);
        clear Ptmp

        P = cell(size(PP,1),1);
        for inx = 1:size(PP,1)
            P(inx) = {deblank(PP(inx,:))};
        end
        
        % Fit Diffusion Tensor: DTI images - cfg_files
        inputs = cell(2,1);
        inputs{1,1} = P; 

        spm('defaults', 'FMRI');
        spm_jobman('run', jobs, inputs{:});
    end
end

[filepath,~,~] = fileparts(mfilename('fullpath'));
MSK =[26:150]';
MSK_check = [1:50,52:125]'; % Voxel number 51 showed a minute numerical difference that seems to be due to the operating system ( with identical ACID versions a difference occurs between the results obtained on a Windows 10 laptop and a linux system on the server, however, for the axial symmetric fit it occurs for all voxels).
% MSK_RBC_ON_AXIAL_FIT = [1:24,27:38,41:92,94:124]';% these voxels had a big difference (1e-4) due to different operating systems.

% tolerance = 1e-5;
pth_1 = [p_out filesep 'DTI-NLLS' filesep];
pth_2 = [p_out filesep 'DKI-NLLS' filesep];
pth_3 = [p_out filesep 'DKIax-NLLS' filesep];
pth_4 = [p_out filesep 'DKIax-NLLS-RBC' filesep];

if check_output
    
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
    if ismac
        load Ground_Truth_struct_Gauss_Newton_mac.mat
    elseif ispc
        load Ground_Truth_struct_Gauss_Newton.mat
    elseif isunix
        load Ground_Truth_struct_Gauss_Newton_unix.mat
    end
    
    DKI_Variables ={'AD','FA','AW','RW','MD','MK','RD','MW','RK','AK','S0'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(DKI_Variables,params_ground_truth_DKI_NLLS,MSK_check,MSK,pth_2,'DKI-NLLS-',0);

    eigenvectors_names = {'V1','V2','V3'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(eigenvectors_names,params_ground_truth_DKI_NLLS_eigenvecs,MSK_check,MSK,pth_2,'DKI-NLLS-',1);

    eigenvalue_names = {'L1','L2','L3'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(eigenvalue_names,params_ground_truth_DKI_NLLS_eigenvals,MSK_check,MSK,pth_2,'DKI-NLLS-',0);

    DTI_Variables ={'AD','FA','MD','RD'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(DTI_Variables,params_ground_truth_DTI_NLLS,MSK_check,MSK,pth_1,'DTI-NLLS-',0);
    Test_ACID_compare_estimated_parameters_to_ground_truth(eigenvectors_names,params_ground_truth_DTI_NLLS_eigenvecs,MSK_check,MSK,pth_1,'DTI-NLLS-',1);
    Test_ACID_compare_estimated_parameters_to_ground_truth(eigenvalue_names,params_ground_truth_DTI_NLLS_eigenvals,MSK_check,MSK,pth_1,'DTI-NLLS-',0);

    %DKIax_vars = {'aos-x','aos-y','aos-z','FA','AK','RK','MD','MW','AD','RD','AW','RW'};
    DKIax_vars = {'FA','AK','RK','MD','MW','AD','RD','AW','RW','S0'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(DKIax_vars,params_ground_truth_DKIax_NLLS,MSK_check,MSK,pth_3,'DKIax-NLLS-',0);
    Test_ACID_compare_estimated_parameters_to_ground_truth(DKIax_vars,params_ground_truth_DKIax_NLLS_RBC,MSK_check,MSK,pth_4,'DKIax-NLLS-RBC-',0);

    DKIax_vars = {'AOS'};
    Test_ACID_compare_estimated_parameters_to_ground_truth(DKIax_vars,params_ground_truth_DKIax_NLLS,MSK_check,MSK,pth_3,'DKIax-NLLS-',1);
    Test_ACID_compare_estimated_parameters_to_ground_truth(DKIax_vars,params_ground_truth_DKIax_NLLS_RBC,MSK_check,MSK,pth_4,'DKIax-NLLS-RBC-',1);

end

folder_signals = [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end

end