%-----------------------------------------------------------------------
% Job saved on 20-Jul-2022 08:59:30 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_sources = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_dummy_single_or_multi_target = false;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_target = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_bvals_type.ecmoco_bvals_exp = [0 0 500 1000 2000 500 1000 2000 500 1000 2000 0 500 1000 2000 500 1000 2000 500 1000 2000 0];

matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_vw_type.ecmoco_dof_vw_manual = [1 1 1 1 1 1 0 0 0 0 0 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_dummy_type.ecmoco_branch_vw_sw.ecmoco_dof_sw_type.ecmoco_dof_sw_manual = [0 1 0 0 1 0 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_dummy_init = true;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_mask = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_biasfield = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_excluded = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_zsmooth = 0;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_parallel_prog = 4;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.ecmoco_phdir = 2;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco.outdir = {''};
