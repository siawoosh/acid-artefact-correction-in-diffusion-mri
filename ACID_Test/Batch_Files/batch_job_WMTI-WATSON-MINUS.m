%-----------------------------------------------------------------------
% Job saved on 04-Apr-2023 14:28:10 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_all = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.dummy_convertW2K = 1;
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.mask = {''};
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.dummy_branch = 2;
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.npool = 1;
