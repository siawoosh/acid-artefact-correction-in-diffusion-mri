%-----------------------------------------------------------------------
% Job saved on 11-Jun-2021 09:54:13 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.source_up = {'/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,1'};
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.source_dw = {'/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,1'};
%%
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.others_up = {
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,2'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,3'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,4'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,5'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,6'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,7'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,8'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,9'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine_bu-1.nii,10'
                                                                            };
%%
%%
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.others_dw = {
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,2'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,3'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,4'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,5'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,6'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,7'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,8'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,9'
                                                                            '/Users/fricke/Documents/MATLAB/spm12/toolbox/ACID/ACID_Test/Fit_Results_Of_Test/HySCO/dwi_spine.nii,10'
                                                                            };
%%
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.perm_dim = 2;
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_fast = 1;
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_ecc = 1;
matlabbatch{1}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_3dor4d = 1;
