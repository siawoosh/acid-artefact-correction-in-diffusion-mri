
clear;




load bvalues.mat % these are the corresponding b values



[filepath,~,~] = fileparts(mfilename('fullpath'));

folder_signals = [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end


fit_data_DKI = 0;
check_output_DKI =0; % for DKI estimation

fit_data_DTI = 0;
check_output_DTI = 0;



if( fit_data_DKI == 1)

    [filepath,~,~] = fileparts(mfilename('fullpath'));
    
    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep] ;
    jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKI_OLS.m']};
    jobs = repmat(jobfile, 1, 1);
    
    PP=[];

    Ptmp = spm_select('FPList', p_in, '^simulation_data_4D.nii');
    Ptmp = [Ptmp ',1'];
    PP   = cat(1,PP,Ptmp);
    clear Ptmp
     
    P = cell(size(PP,1),1);
    for inx = 1:size(PP,1)
        P(inx) = {deblank(PP(inx,:))};
    end
    
    % Fit Diffusion Tensor: DTI images - cfg_files
    inputs = cell(2,1);
    inputs{1, 1} = P;
   

    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});



end

if( fit_data_DTI == 1)
    
    
    [filepath,~,~] = fileparts(mfilename('fullpath'));

    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep] ;
   
    jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DTI_OLS.m']};   
    jobs = repmat(jobfile, 1, 1);
    
    PP=[];
    Ptmp = spm_select('FPList', [p_in], ['^simulation_data_4D.nii']);
    Ptmp = [Ptmp ',1'];
    PP = cat(1,PP,Ptmp);
    clear Ptmp
    
    P = cell(size(PP,1),1);
    for inx = 1:size(PP,1)
        P(inx) = {deblank(PP(inx,:))};
    end
    
    % Fit Diffusion Tensor: DTI images - cfg_files
    inputs = cell(2,1);
    inputs{1, 1} = P;

    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});




end


MSK =[26:150]';





    if (check_output_DKI == 1)



           [filepath,~,~] = fileparts(mfilename('fullpath'));
        pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DKI-OLS' filesep] ;

         DKI_Variables ={'AD','FA','AK','RK','MD','MK','RD','MW','RW','AW','b0'};

        for inx_var = 1:numel(DKI_Variables)

                if strcmp(DKI_Variables{inx_var},'b0') 
                 Vol=(spm_vol([pth 'simulation_data_4D_desc-DKI-OLS-' DKI_Variables{inx_var} '.nii'])); 
                else
                 Vol=(spm_vol([pth 'simulation_data_4D_desc-DKI-OLS-' DKI_Variables{inx_var} '_map.nii'])); 
                end

            VG = Vol(1);
            parameter=acid_read_vols(Vol,VG,1);
           ground_truth_acid_DKI(inx_var,:) = parameter(MSK);
        end   

         dimension = {'V1','V2','V3'};

        for inx_eigen_var = 1:numel(dimension)
            Vol=(spm_vol([pth 'simulation_data_4D_desc-DKI-OLS-' dimension{inx_eigen_var} '_map.nii']));
            VG = Vol(1);

                   for ii = 1:3
                    eigenvector_tmp(:,:,:)=acid_read_vols(Vol(ii),VG,1);
                    eigenvector(:,ii) = eigenvector_tmp(MSK);
                   end


            eigenvector_ground_truth_acid_DKI(inx_eigen_var,:,:) = eigenvector;



        end   

            dimension ={'L1','L2','L3'};

       for inx_eigen_var = 1:numel(dimension)
              Vol = (spm_vol([pth 'simulation_data_4D_desc-DKI-OLS-' dimension{inx_eigen_var} '_map.nii']));
            VG = Vol(1);

            eigenvalue =acid_read_vols(Vol,VG,1);

            eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:) = eigenvalue(MSK);



        end 



    end







    if (check_output_DTI == 1)



        [filepath,~,~] = fileparts(mfilename('fullpath'));
        pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DTI-OLS' filesep] ;
            
         DTI_Variables ={'AD','FA','MD','RD','b0'};

        for inx_var = 1:numel(DTI_Variables)

                if strcmp(DTI_Variables{inx_var},'b0') 
                 Vol=(spm_vol([pth 'simulation_data_4D_desc-DTI-OLS-' DTI_Variables{inx_var} '.nii'])); 
                else
                 Vol=(spm_vol([pth 'simulation_data_4D_desc-DTI-OLS-' DTI_Variables{inx_var} '_map.nii'])); 
                end
            VG = Vol(1);

            parameter=acid_read_vols(Vol,VG,1);

            ground_truth_acid_DTI(inx_var,:) = parameter(MSK);



        end   

        dimension = {'V1','V2','V3'};

        for inx_eigen_var = 1:numel(dimension)
         Vol=(spm_vol([pth 'simulation_data_4D_desc-DTI-OLS-' dimension{inx_eigen_var} '_map.nii']));
            VG = Vol(1);

                   for ii = 1:3
                    eigenvector_tmp(:,:,:)=acid_read_vols(Vol(ii),VG,1);
                    eigenvector(:,ii) = eigenvector_tmp(MSK);
                   end

            eigenvector_ground_truth_acid_DTI(inx_eigen_var,:,:) = eigenvector;



        end   

                 dimension ={'L1','L2','L3'};

       for inx_eigen_var = 1:numel(dimension)
            Vol = (spm_vol([pth 'simulation_data_4D_desc-DTI-OLS-' dimension{inx_eigen_var} '_map.nii']));
            VG = Vol(1);

            eigenvalue =acid_read_vols(Vol,VG,1);

            eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:) = eigenvalue(MSK);


       end 

  


    end
   
    if ismac
        fname_out = 'Ground_Truth_ACID_mac.mat';
    elseif ispc
        fname_out = 'Ground_Truth_ACID.mat';
    elseif isunix
        fname_out = 'Ground_Truth_ACID_unix.mat';
    end
    
   
        cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);

        save(fname_out,'eigenvalue_ground_truth_acid_DTI','eigenvector_ground_truth_acid_DTI','ground_truth_acid_DTI','eigenvalue_ground_truth_acid_DKI','eigenvector_ground_truth_acid_DKI','ground_truth_acid_DKI')

   

        [filepath,~,~] = fileparts(mfilename('fullpath'));
        
        folder_signals = [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep];
        cd(folder_signals);
        
        try rmdir('derivatives','s')
        catch
        end


