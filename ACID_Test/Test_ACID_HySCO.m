function HySCO_result = Test_ACID_HySCO

[filepath,~,~] = fileparts(mfilename('fullpath'));
pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'HySCO' filesep];

jobfile = {[filepath filesep 'Batch_Files' filesep 'ACID_HySCO_Test_Batch_job.m']};
filepath_hysco = [filepath filesep 'Fit_Results_Of_Test' filesep 'HySCO'];% filesep 'derivatives'];
cd(filepath_hysco);

try rmdir('derivatives', 's');
catch
end

jobs = repmat(jobfile, 1, 1);

P=cell(size(PP,1),1);
for inx=1:size(PP,1)
    P(inx) = {deblank(PP(inx,:))};
end

inputs = cell(1,1);
inputs{1, 1} = P;

spm('defaults', 'FMRI');
spm_jobman('run', jobs); %, inputs{:});

PInd_GT = [filepath filesep 'Ground_Truth_Data' filesep 'HySCO' filesep 'GT_dwi_spine_desc-HySCO_dwi.nii,2']; 

Result = spm_vol(PInd_GT);
Result = acid_read_vols(Result, Result(1),1);

PInd_Result = [filepath filesep 'Fit_Results_Of_Test' filesep 'HySCO' filesep 'derivatives' filesep 'HySCO-Run' filesep 'dwi_spine_desc-HySCO_dwi.nii,2'];

GT = spm_vol(PInd_Result);
GT = acid_read_vols(GT, GT(1),1);

tolerance = 1e-5;

if(sum(Result - GT) < tolerance)
    cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'HySCO results are correct. \n');    
    HySCO_result = 0;
else
    cprintf('red', 'HySCO results are different. \n');   
    HySCO_result = 1;
end

end