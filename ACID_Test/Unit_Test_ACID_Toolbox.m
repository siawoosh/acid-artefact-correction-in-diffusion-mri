test_standard_ACID_DTI_DKI = 1;
test_Gauss_Newton = 1;
test_Biophysical_Parameters = 1;
fit_data = 1;
check_output = 1;
test_ECMOCO = 0;
test_POAS = 0;
test_HySCO = 0;

if test_standard_ACID_DTI_DKI
    Test_ACID_Branch(fit_data,check_output);
end

if test_Gauss_Newton
    Test_ACID_Gauss_Newton_Fit(fit_data,check_output)
end

if test_Biophysical_Parameters
    Test_ACID_Biophysical_Parameters(fit_data,check_output)
end

if test_ECMOCO
    ECMOCO_result = Test_ACID_ECMOCO;
end

if test_POAS
    POAS_result = Test_ACID_msPOAS;
end

if test_HySCO
    HySCO_result = Test_ACID_HySCO;
end

if exist('ECMOCO_result','var')
    if ECMOCO_result == 0
        cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'ECMOCO parameters are correct. \n');
    else
        cprintf('red', 'ECMOCO parameters are different. \n');
    end
end

if exist('POAS_result','var')
    if POAS_result == 0
        cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'POAS results are correct. \n');
    else
        cprintf('red', 'msPOAS results are different. \n');
    end
end

clear ECMOCO_result POAS_result;
disp('Unit test done.')