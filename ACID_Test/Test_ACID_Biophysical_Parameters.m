function Test_ACID_Biophysical_Parameters(fit_data,check_output)

profile_code = 0;

if profile_code == 1
    profile on
end

if fit_data == 1  
    for branch = 1:2
        
        [filepath,~,~] = fileparts(mfilename('fullpath'));    
        path_DKI_maps =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Biophysical_Parameters' filesep] ;
        
        if branch == 1
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_WMTI-WATSON-PLUS.m']};
        elseif branch == 2
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_WMTI-WATSON-MINUS.m']};
        end
           
        jobs = repmat(jobfile, 1, 1);
        
        D_para = spm_select('FPList', path_DKI_maps, '^simulation_DKI_NLLS_AD_map.nii');
        D_perp = spm_select('FPList', path_DKI_maps, '^simulation_DKI_NLLS_RD_map.nii');       
        W_mean = spm_select('FPList', path_DKI_maps, '^simulation_DKI_NLLS_MW_map.nii');
        K_para = spm_select('FPList', path_DKI_maps, '^simulation_DKI_NLLS_AK_map.nii'); 
        K_perp = spm_select('FPList', path_DKI_maps, '^simulation_DKI_NLLS_RK_map.nii');
                   
        % Estimation Of biophysical parameters: Perpendicular Diffusivity image - cfg_files
        inputs = cell(1,1);
        inputs{1,1} = {D_para; D_perp; W_mean; K_para; K_perp};
        spm('defaults', 'FMRI');
        spm_jobman('run', jobs, inputs{:});
    end
end

if check_output
    
    MSK =[26:150]';
    [filepath,~,~] = fileparts(mfilename('fullpath'));
    
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
    load Ground_Truth_BP.mat
        
    p_out =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Biophysical_Parameters' filesep] ;
    
    pth_1 =  [p_out 'derivatives' filesep 'WMTI-WATSON-PLUS'] ;
    pth_2 =  [p_out 'derivatives' filesep 'WMTI-WATSON-MINUS'] ;
    
    BP_Variables ={'DA','DE-PARA','DE-PERP','AWF','KAPPA'};
    for branch = 1:2
        for inx_var = 1:numel(BP_Variables)
            if branch == 1
                Vol=(spm_vol([pth_1 filesep 'simulation_DKI_NLLS_desc-WMTI-WATSON-PLUS-' BP_Variables{inx_var} '_map.nii']));
            elseif branch == 2
                Vol=(spm_vol([pth_2 filesep 'simulation_DKI_NLLS_desc-WMTI-WATSON-MINUS-' BP_Variables{inx_var} '_map.nii']));
            end
            
            VG = Vol(1);
            
            parameter=acid_read_vols(Vol,VG,1);      
            estimated_parameter_values(inx_var,branch,:) = parameter(MSK);
            
            if (isequaln(estimated_parameter_values(inx_var,branch,:), parameter_values_ground_truth_BP(inx_var,branch,:)))
                cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[BP_Variables{inx_var} ' Branch ' num2str(branch) ' Value is the same as the one estimated with the Sample Objective BP fit of 17/12/2020 \n']);
            else
                cprintf('*red',[BP_Variables{inx_var} ' Branch ' num2str(branch) ' Value is NOT the same as the one estimated with the Sample Objective BP fit of 17/12/2020 \n']);
                display([BP_Variables{inx_var} ' Mean should be (excluding NaNs):  ' num2str(nanmean(parameter_values_ground_truth_BP(inx_var,branch,:)),100)]);
                display([BP_Variables{inx_var} ' Mean is (excluding NaNs):         ' num2str(nanmean(estimated_parameter_values(inx_var,branch,:)),100)]);
                display([BP_Variables{inx_var} ' Mean Difference (excluding NaNs): ' num2str(nanmean(abs(estimated_parameter_values(inx_var,branch,:)-parameter_values_ground_truth_BP(inx_var,branch,:))),10)]);
            end 
        end 
    end
end

if profile_code == 1
    profile viewer
end

[filepath,~,~] = fileparts(mfilename('fullpath'));
p_out = [filepath filesep 'Fit_Results_Of_Test' filesep 'Biophysical_Parameters'];
cd(p_out);
rmdir([p_out filesep 'derivatives'],'s');

end