clear;

load bvalues.mat

fit_data = 1;
check_output = 1;


[filepath,~,~] = fileparts(mfilename('fullpath'));

folder_signals = [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end





if (fit_data == 1)

    for which_fit = 1:4

        p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep] ;

        if which_fit == 1
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DTI_NLLS.m']};
        elseif which_fit == 2
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKI_NLLS.m']};
        elseif which_fit == 3
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKIax_NLLS.m']};
        elseif which_fit == 4
            jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKIax_NLLS_RBC.m']};
        end

        jobs = repmat(jobfile, 1, 1);
        PP = [];

        Ptmp = spm_select('FPList', p_in, '^simulation_data_4D.nii');
        Ptmp = [Ptmp ',1'];
        PP   = cat(1,PP,Ptmp);
        clear Ptmp

        P = cell(size(PP,1),1);
        for inx = 1:size(PP,1)
            P(inx) = {deblank(PP(inx,:))};
        end

        % Fit Diffusion Tensor: DTI images - cfg_files
        inputs = cell(2,1);
        inputs{1,1} = P;

        spm('defaults', 'FMRI');
        spm_jobman('run', jobs, inputs{:});
    end



end


if (check_output == 1)

    MSK =[26:150]';

    [filepath,~,~] = fileparts(mfilename('fullpath'));
    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DTI-NLLS' filesep] ;

    % DTI_NLLS
    DTI_Variables ={'AD','FA','MD','RD','S0'};
    for inx_var = 1:numel(DTI_Variables)
        parameter_name = DTI_Variables{inx_var};

        if strcmp(parameter_name,'S0')
            Vol = spm_vol([p_in 'simulation_data_4D_desc-DTI-NLLS-' parameter_name '.nii']);
        else
            Vol = spm_vol([p_in 'simulation_data_4D_desc-DTI-NLLS-' parameter_name '_map.nii']);
        end
        VG  = Vol(1);
        parameter = acid_read_vols(Vol,VG,1);
        params_ground_truth_DTI_NLLS.(parameter_name) = parameter(MSK);
    end

    % DTI_NLLS_eigenvals
    dimension = {'L1','L2','L3'};
    for inx_eigen_var = 1:numel(dimension)
        parameter_name = dimension{inx_eigen_var};
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DTI-NLLS-' parameter_name '_map.nii']);
        VG  = Vol(1);
        eigenvalue = acid_read_vols(Vol,VG,1);
        params_ground_truth_DTI_NLLS_eigenvals.(parameter_name) = eigenvalue(MSK);
    end

    % DTI_NLLS_eigenvecs
    dimension = {'V1','V2','V3'};
    for inx_eigen_var = 1:numel(dimension)
        parameter_name = dimension{inx_eigen_var};
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DTI-NLLS-' parameter_name '_map.nii']);
        VG  = Vol(1);
        for ii = 1:3
            eigenvector_tmp(:,:,:)=acid_read_vols(Vol(ii),VG,1);
            eigenvector(:,ii) = eigenvector_tmp(MSK);
        end
        params_ground_truth_DTI_NLLS_eigenvecs.(parameter_name) = eigenvector;
    end


    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DKI-NLLS' filesep] ;

    % DKI_NLLS
    DKI_Variables ={'AD','FA','AW','RW','MD','MK','RD','MW','RK','AK','S0'};
    for inx_var = 1:numel(DKI_Variables)
        parameter_name = DKI_Variables{inx_var};
        % if strcmp(parameter_name,'S0')
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKI-NLLS-' parameter_name '.nii']);
        % else
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKI-NLLS-' parameter_name '_map.nii']);
        % end
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DKI-NLLS-' parameter_name '_map.nii']);

        VG  = Vol(1);
        parameter=acid_read_vols(Vol,VG,1);
        params_ground_truth_DKI_NLLS.(parameter_name) =  parameter(MSK);
    end

    % DKI_NLLS_eigenvals
    dimension ={'L1','L2','L3'};
    for inx_eigen_var = 1:numel(dimension)
        parameter_name = dimension{inx_eigen_var};
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DKI-NLLS-' parameter_name '_map.nii']);
        VG  = Vol(1);
        eigenvalue =acid_read_vols(Vol,VG,1);
        params_ground_truth_DKI_NLLS_eigenvals.(parameter_name) = eigenvalue(MSK);
    end

    % DKI_NLLS_eigenvecs
    dimension = {'V1','V2','V3'};
    for inx_eigen_var = 1:numel(dimension)
        parameter_name = dimension{inx_eigen_var};
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DKI-NLLS-' parameter_name '_map.nii']);
        VG  = Vol(1);
        for ii = 1:3
            eigenvector_tmp(:,:,:) = acid_read_vols(Vol(ii),VG,1);
            eigenvector(:,ii) = eigenvector_tmp(MSK);
        end
        params_ground_truth_DKI_NLLS_eigenvecs.(parameter_name) = eigenvector;
    end

    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DKIax-NLLS' filesep] ;

    % DKIax_NLLS
    Ax_sym_DKI_Variables ={'AOS','FA','AK','RK','MD','MW','AD','RD','AW','RW','S0'};
    for inx_var = 1:numel(Ax_sym_DKI_Variables)
        parameter_name = Ax_sym_DKI_Variables{inx_var};
        % if strcmp(parameter_name,'S0')
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-' parameter_name '.nii']);
        % else
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-' parameter_name '_map.nii']);
        % end
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-' parameter_name '_map.nii']);
        VG  = Vol(1);

        if strcmp(parameter_name,'AOS')
            for ii = 1:3
                aos_tmp(:,:,:)=acid_read_vols(Vol(ii),VG,1);
                aos(:,ii) = aos_tmp(MSK);
                params_ground_truth_DKIax_NLLS.(parameter_name) = aos;
            end
        else
            parameter=acid_read_vols(Vol,VG,1);
            params_ground_truth_DKIax_NLLS.(parameter_name) = parameter(MSK);

        end
    end

    clear aos aos_tmp

    p_in =  [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep 'derivatives' filesep 'DKIax-NLLS-RBC' filesep] ;

    % DKIax_NLLS_RBC
    Ax_sym_DKI_Variables ={'AOS','FA','AK','RK','MD','MW','AD','RD','AW','RW','S0'};
    for inx_var = 1:numel(Ax_sym_DKI_Variables)
        parameter_name = Ax_sym_DKI_Variables{inx_var};
        % if strcmp(parameter_name,'S0')
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-RBC-' parameter_name '.nii']);
        % else
        %     Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-RBC-' parameter_name '_map.nii']);
        % end
        Vol = spm_vol([p_in 'simulation_data_4D_desc-DKIax-NLLS-RBC-' parameter_name '_map.nii']);
        VG  = Vol(1);
        if strcmp(parameter_name,'AOS')
            for ii = 1:3
                aos_tmp(:,:,:)=acid_read_vols(Vol(ii),VG,1);
                aos(:,ii) = aos_tmp(MSK);
                params_ground_truth_DKIax_NLLS_RBC.(parameter_name) = aos;
            end
        else
            parameter = acid_read_vols(Vol,VG,1);
            params_ground_truth_DKIax_NLLS_RBC.(parameter_name) = parameter(MSK);
        end
    end
end


cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
if ismac
    fname_out = 'Ground_Truth_struct_Gauss_Newton_mac.mat';
elseif ispc
    fname_out = 'Ground_Truth_struct_Gauss_Newton.mat';
elseif isunix
    fname_out = 'Ground_Truth_struct_Gauss_Newton_unix.mat';
end

save(fname_out, ...
    'params_ground_truth_DTI_NLLS', ...
    'params_ground_truth_DTI_NLLS_eigenvals', ...
    'params_ground_truth_DTI_NLLS_eigenvecs', ...
    'params_ground_truth_DKI_NLLS', ...
    'params_ground_truth_DKI_NLLS_eigenvals', ...
    'params_ground_truth_DKI_NLLS_eigenvecs', ...
    'params_ground_truth_DKIax_NLLS', ...
    'params_ground_truth_DKIax_NLLS_RBC')

[filepath,~,~] = fileparts(mfilename('fullpath'));

folder_signals = [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end

