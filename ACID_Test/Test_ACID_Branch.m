function Test_ACID_Branch(fit_data,check_output)

% This script checks whether the ACID ordinary least squares DKI output is
% the same as the one computed with the ACID version of the master branch
% on 09/09/2020 with Matlab version 2019b

load bvalues.mat % these are the corresponding b values

[filepath,~,~] = fileparts(mfilename('fullpath'));

folder_signals = [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep];
cd(folder_signals);

try rmdir('derivatives','s')
catch
end

p_out = [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep 'derivatives'];



fit_data_DKI = fit_data;
check_output_DKI = check_output; % for DKI estimation

fit_data_DTI = fit_data;
check_output_DTI = check_output;

b = bvalues/1000; % to convert them from mm?/s to um?/ms

if fit_data_DKI == 1
       
    [filepath,~,~] = fileparts(mfilename('fullpath'));
    
    p_in =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep] ;
    jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DKI_OLS.m']};
    jobs = repmat(jobfile, 1, 1);
    
    PP=[];

    Ptmp = spm_select('FPList', p_in, '^simulation_data_4D.nii');
    Ptmp = [Ptmp ',1'];
    PP   = cat(1,PP,Ptmp);
    clear Ptmp
     
    P = cell(size(PP,1),1);
    for inx = 1:size(PP,1)
        P(inx) = {deblank(PP(inx,:))};
    end
    
    % Fit Diffusion Tensor: DTI images - cfg_files
    inputs = cell(2,1);
    inputs{1, 1} = P;
   

    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});
end

if fit_data_DTI == 1
    
    [filepath,~,~] = fileparts(mfilename('fullpath'));

    p_in =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Signals' filesep] ;
   
    jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_DTI_OLS.m']};   
    jobs = repmat(jobfile, 1, 1);
    
    PP=[];
    Ptmp = spm_select('FPList', p_in, '^simulation_data_4D.nii');
    Ptmp = [Ptmp ',1'];
    PP = cat(1,PP,Ptmp);
    clear Ptmp
    
    P = cell(size(PP,1),1);
    for inx = 1:size(PP,1)
        P(inx) = {deblank(PP(inx,:))};
    end
    
    % Fit Diffusion Tensor: DTI images - cfg_files
    inputs = cell(2,1);
    inputs{1, 1} = P;

    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});
end

MSK =[26:150]';
[filepath,~,~] = fileparts(mfilename('fullpath'));

if check_output_DKI == 1
    
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);

    if ismac
        load Ground_Truth_ACID_mac.mat
    elseif ispc
        load Ground_Truth_ACID.mat   
    elseif isunix
        load Ground_Truth_ACID_unix.mat
    end

    DKI_Variables ={'AD','FA','AK','RK','MD','MK','RD','MW','RW','AW'};
    
    for inx_var = 1:numel(DKI_Variables)
        
        Vol = spm_vol([p_out filesep 'DKI-OLS' filesep 'simulation_data_4D_desc-DKI-OLS-' DKI_Variables{inx_var} '_map.nii']);
        VG = Vol(1);
        
        parameter = acid_read_vols(Vol,VG,1);      
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,:) == ground_truth_acid_DKI(inx_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DKI_Variables{inx_var} ' DKI Value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DKI_Variables{inx_var} ' DKI Value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            display([DKI_Variables{inx_var} ' Mean should be:  ' num2str(mean(ground_truth_acid_DKI(inx_var,:),'omitnan'),100)]);
            display([DKI_Variables{inx_var} ' Mean is:         ' num2str(mean(estimated_parameter_values(inx_var,:),'omitnan'),100)]);
            display([DKI_Variables{inx_var} ' Mean Difference: ' num2str(mean(abs(estimated_parameter_values(inx_var,:)-ground_truth_acid_DKI(inx_var,:),'omitnan')),10)]);
        end    
    end
    
    dimension = {'V1','V2','V3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol = spm_vol([p_out filesep 'DKI-OLS' filesep 'simulation_data_4D_desc-DKI-OLS-' dimension{inx_eigen_var} '_map.nii']);
        VG = Vol(1);
        for inx_components = 1:3
            eigenvector=acid_read_vols(Vol(inx_components),VG,1);        
            estimated_eigenvector(1,:) = eigenvector(MSK);
            
            if (estimated_eigenvector(1,:) == eigenvector_ground_truth_acid_DKI(inx_eigen_var,:,inx_components))
                cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' DKI value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            else
                cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' DKI value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
                display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(mean(eigenvector_ground_truth_acid_DKI(inx_eigen_var,:,inx_components),'omitnan'),100)]);
                display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(mean(estimated_eigenvector(1,:),'omitnan'),100)]);
                display(['Mean Difference:                                      ' num2str(mean(abs(estimated_eigenvector(1,:)-eigenvector_ground_truth_acid_DKI(inx_eigen_var,:,inx_components)),'omitnan'),10)]);
            end
        end
    end
    
    dimension ={'L1','L2','L3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol = spm_vol([p_out filesep 'DKI-OLS' filesep 'simulation_data_4D_desc-DKI-OLS-' dimension{inx_eigen_var} '_map.nii']);
        VG = Vol(1);
        
        eigenvalue =acid_read_vols(Vol,VG,1);
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
             
        if (estimated_eigenvalue(inx_eigen_var,:) == eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DKI value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DKI value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(mean(eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:),'omitnan'),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(mean(estimated_eigenvalue(inx_eigen_var,:),'omitnan'),100)]);
            display(['Mean Difference:                       ' num2str(mean(abs(estimated_eigenvalue(inx_eigen_var,:)-eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:)),'omitnan'),10)]);
        end 
    end    
end

if check_output_DTI == 1
    
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);


    if ismac
        load Ground_Truth_ACID_mac.mat
    elseif ispc
        load Ground_Truth_ACID.mat   
    elseif isunix
        load Ground_Truth_ACID_unix.mat
    end
    
    DTI_Variables ={'AD','FA','MD','RD'};
    
    for inx_var = 1:numel(DTI_Variables)
        
        Vol = spm_vol([p_out filesep 'DTI-OLS' filesep 'simulation_data_4D_desc-DTI-OLS-' DTI_Variables{inx_var} '_map.nii']);
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);   
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,:) == ground_truth_acid_DTI(inx_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DTI_Variables{inx_var} ' DTI Value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DTI_Variables{inx_var} ' DTI Value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display([DTI_Variables{inx_var} ' Mean should be:  ' num2str(mean(ground_truth_acid_DTI(inx_var,:),'omitnan'),100)]);
            display([DTI_Variables{inx_var} ' Mean is:         ' num2str(mean(estimated_parameter_values(inx_var,:),'omitnan'),100)]);
            display([DTI_Variables{inx_var} ' Mean Difference: ' num2str(mean(abs(estimated_parameter_values(inx_var,:)-ground_truth_acid_DTI(inx_var,:)),'omitnan'),10)]);
        end       
    end
    
    dimension = {'V1','V2','V3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol=spm_vol([p_out filesep 'DTI-OLS' filesep 'simulation_data_4D_desc-DTI-OLS-' dimension{inx_eigen_var} '_map.nii']);
        VG = Vol(1);
        for inx_components = 1:3
            
            eigenvector=acid_read_vols(Vol(inx_components),VG,1);
            
            estimated_eigenvector(1,:) = eigenvector(MSK);
            
            if (estimated_eigenvector(1,:) == eigenvector_ground_truth_acid_DTI(inx_eigen_var,:,inx_components))
                cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' DTI value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            else
                cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' DTI value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
                display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(mean(eigenvector_ground_truth_acid_DTI(inx_eigen_var,:,inx_components),'omitnan'),100)]);
                display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(mean(estimated_eigenvector(1,:),'omitnan'),100)]);
                display(['Mean Difference:                                      ' num2str(mean(abs(estimated_eigenvector(1,:)-eigenvector_ground_truth_acid_DTI(inx_eigen_var,:,inx_components)),'omitnan'),10)]);
            end
        end
    end
    dimension ={'L1','L2','L3'};
    
    for inx_eigen_var = 1:numel(dimension)
        
        Vol = spm_vol([p_out filesep 'DTI-OLS' filesep 'simulation_data_4D_desc-DTI-OLS-' dimension{inx_eigen_var} '_map.nii']);
        VG = Vol(1);      
        eigenvalue = acid_read_vols(Vol,VG,1);      
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
              
        if (estimated_eigenvalue(inx_eigen_var,:) == eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DTI value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DTI value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(mean(eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:),'omitnan'),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(mean(estimated_eigenvalue(inx_eigen_var,:),'omitnan'),100)]);
            display(['Mean Difference:     '                                              num2str(mean(abs(estimated_eigenvalue(inx_eigen_var,:)-eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:)),'omitnan'),10)]);
        end      
    end
    %         Vol=spm_vol([pth 'meanDWI_simulation_001.nii']);
    %         VG = Vol(1);
    %
    %         meanDWI_param =  acid_read_vols(Vol,VG,1);
    %
    %
    %         meanDWI = meanDWI_param(1:150);
    %
    %         if (meanDWI == meanDWI_DTI_ground_truth_acid_DTI)
    %             cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['meanDWI DTI value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
    %         else
    %             cprintf('*red',['meanDWI value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
    %             display(['meanDWI Mean should be: ' num2str(mean(meanDWI_DTI_ground_truth_acid_DTI,'omitnan'),100)]);
    %             display(['meanDWI Mean is:        ' num2str(mean(meanDWI,'omitnan'),100)]);
    %             display(['Mean Difference:        ' num2str(mean(abs(meanDWI-meanDWI_DTI_ground_truth_acid_DTI),'omitnan'),10)]);
    %         end
    %
end

cd(folder_signals);

try rmdir('derivatives','s')
catch
end




end