function Test_ACID_compare_estimated_parameters_to_ground_truth(parameters_to_check,ground_truth_values,MSK_check,MSK,pth,algorithm_name,dummy_4d)



if dummy_4d == 0

    for inx_var = 1:numel(parameters_to_check)
        parameter_name = parameters_to_check{inx_var};
        Vol = spm_vol([pth 'simulation_data_4D_desc-' algorithm_name parameter_name '_map.nii']);            
        VG = Vol(1);

        parameter=acid_read_vols(Vol,VG,1);
        est_params.(parameter_name) = parameter(MSK);
        
        if (est_params.(parameter_name)(MSK_check) == ground_truth_values.(parameter_name)(MSK_check))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[algorithm_name 'algorithm, parameter:' parameter_name ' passed check. \n']);
        else
            cprintf('*red',[algorithm_name 'algorithm, parameter:' parameter_name ' FAILED check.  \n']);
            display([parameter_name ' Mean  should be: ' num2str(mean(ground_truth_values.(parameter_name),'omitnan'),100)]);
            display([parameter_name ' Mean is:         ' num2str(mean(est_params.(parameter_name),'omitnan'),100)]);
            display([parameter_name ' Mean Difference: ' num2str(mean(abs(est_params.(parameter_name) - ground_truth_values.(parameter_name)),'omitnan'),10)]);
        end
    end

elseif dummy_4d == 1

    for inx_var = 1:numel(parameters_to_check)
        parameter_name = parameters_to_check{inx_var};
        Vol= spm_vol([pth 'simulation_data_4D_desc-' algorithm_name parameter_name '_map.nii']);
        VG = Vol(1);
        
        for inx_components = 1:3
            parameter_component=acid_read_vols(Vol(inx_components),VG,1);
            
            est_params.(parameter_name)(1,:) = parameter_component(MSK);
            
            if (est_params.(parameter_name)(1,MSK_check) == ground_truth_values.(parameter_name)(MSK_check,inx_components)')
                cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[algorithm_name 'algorithm, parameter:' parameters_to_check{inx_var} ', component ' num2str(inx_components) ' passed check. \n']);
            else
                cprintf('*red',[algorithm_name 'algorithm, parameter:' parameter_name ', component ' num2str(inx_components) ' FAILED check.  \n']);
                display(['Eigenvector dimension ' parameter_name ' Mean should be: ' num2str(mean(ground_truth_values.(parameter_name)(:,inx_components),'omitnan'),100)]);
                display(['Eigenvector dimension ' parameter_name ' Mean is:        ' num2str(mean(est_params.(parameter_name)(1,:),'omitnan'),100)]);
                display(['Mean Difference:      '                                                  num2str(mean(abs(est_params.(parameter_name)(1,:) - ground_truth_values.(parameter_name)(:,inx_components)),'omitnan'),10)]);
            end
        end
    end
end
end