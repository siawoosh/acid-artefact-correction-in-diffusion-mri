function POAS_result = Test_ACID_msPOAS

[filepath,~,~] = fileparts(mfilename('fullpath'));

pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'POAS' filesep] ;
jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_msPOAS.m']};
filepath_poas = [filepath filesep 'Fit_Results_Of_Test' filesep 'POAS'];% filesep 'derivatives'];
cd(filepath_poas);

try rmdir('derivatives', 's');
catch
end

jobs = repmat(jobfile, 1, 1);
P = cell(size(PP,1),1);
for inx=1:size(PP,1)
    P(inx) = {deblank(PP(inx,:))};
end

inputs = cell(1,1);
inputs{1, 1} = P;
spm('defaults', 'FMRI');
spm_jobman('run', jobs); %, inputs{:});

PInd_Result = [filepath filesep 'Ground_Truth_Data' filesep 'POAS' filesep 'POAS_GT_rc_dwi_spine.nii,2'];
Result = spm_vol(PInd_Result);
Result = acid_read_vols(Result, Result(1),1);

PInd_GT = [filepath filesep 'Fit_Results_Of_Test' filesep 'POAS' filesep 'derivatives' filesep 'msPOAS-Run' filesep 'rc_dwi_spine_desc-msPOAS_dwi.nii,2'];
GT = spm_vol(PInd_GT);
GT = acid_read_vols(GT, GT(1),1);

tolerance = 1e-3;

if sum(Result - GT) < tolerance
    cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'msPOAS results are correct. \n');    
    POAS_result = 0;
else
    cprintf('red', 'msPOAS results are different. \n'); 
    POAS_result = 1;
end
end