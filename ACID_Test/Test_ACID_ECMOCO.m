function ECMOCO_result = Test_ACID_ECMOCO

[filepath,~,~] = fileparts(mfilename('fullpath'));
filepath_ecmoco = [filepath filesep 'Fit_Results_Of_Test' filesep 'ECMOCO'];% filesep 'derivatives'];
cd(filepath_ecmoco);

try rmdir('derivatives', 's');
catch
end

pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ECMOCO' filesep] ;
jobfile = {[filepath filesep 'Batch_Files' filesep 'batch_job_ECMOCO.m']};
jobs = repmat(jobfile, 1, 1);

PP=[];

 for inx = 1:22
    P_tmp = spm_select('FPList', [pth], ['c_dwi_spine.nii']);
    if inx<10
        P_tmp = [P_tmp ',' num2str(inx) ' '];
    else
        P_tmp = [P_tmp ',' num2str(inx)];
    end
    PP = cat(1,PP,P_tmp);
    clear Ptmp
 end

P = cell(size(PP,1),1);
for inx=1:size(PP,1)
    P(inx) = {deblank(PP(inx,:))};
end

inputs = cell(1,1);
inputs{1, 1} = P;
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});

cd([filepath filesep 'Ground_Truth_Data' filesep 'ECMOCO']);
load Ground_Truth_ECMOCO.mat

params_GT = params;

cd([filepath filesep 'Fit_Results_Of_Test' filesep 'ECMOCO' filesep 'derivatives' filesep 'ECMOCO-Run']);
load c_dwi_spine_desc-ECMOCO-params_dwi.mat

tolerance = 1;

diff = abs(params_GT - params);

max_diff = max(diff(:));

if max_diff < tolerance
    cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'ECMOCO parameters are correct. \n');
    ECMOCO_result = 0;
else
    cprintf('red', 'ECMOCO parameters are different. \n');
    ECMOCO_result = 1;
end
    
disp('ECMOCO test finished!')

end