clear;

fit_data = 1;
check_output = 1;
are_you_sure_you_want_to_do_this = 'No';
are_you_really_really_sure_you_want_to_do_this = 'Definitely Not';

if (fit_data == 1)
    
    for branch = 1:2    
        [filepath,~,~] = fileparts(mfilename('fullpath'));
        pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Biophysical_Parameters' filesep] ;
        
        jobfile = {[filepath filesep 'Batch_Files' filesep 'BP_Test_Batch_job.m']};  
        jobs = repmat(jobfile, 1, 1);
           
        per_D  = spm_select('FPList', pth, '^DKI_RBC_OFF_Radial_GN_simulation_001.nii');     
        para_D = spm_select('FPList', pth, '^DKI_RBC_OFF_Axial_GN_simulation_001.nii');
        per_K  = spm_select('FPList', pth, '^DKI_RBC_OFF_Kperp_GN_simulation_001.nii');
        para_K = spm_select('FPList', pth, '^DKI_RBC_OFF_Kparallel_GN_simulation_001.nii');
        mean_W = spm_select('FPList', pth, '^DKI_RBC_OFF_Wmean_GN_simulation_001.nii');
           
        inputs = cell(6,1);            
        inputs{1, 1} = {per_D};  % Estimation Of Biophysical Parameters: Perpendicular Diffusivity image - cfg_files
        inputs{2, 1} = {para_D}; % Estimation Of Biophysical Parameters: Parallel Diffusivitiy image - cfg_files
        inputs{3, 1} = {per_K};  % Estimation Of Biophysical Parameters: Perpedicular Kurtosis image - cfg_files
        inputs{4, 1} = {para_K}; % Estimation Of Biophysical Parameters: Parallel Kurtosis image - cfg_files
        inputs{5, 1} = {mean_W}; % Estimation Of Biophysical Parameters: Mean Kurtosis image - cfg_files
        inputs{6, 1} = branch;   % Estimation Of Biophysical Parameters: dummy_branch - cfg_entry
           
        spm('defaults', 'FMRI');
        spm_jobman('run', jobs, inputs{:});            
    end 
end

if (are_you_sure_you_want_to_do_this == 12)
    if (are_you_really_really_sure_you_want_to_do_this == 14)
        
        if (check_output == 1)
            
            MSK =[26:150]';
            
            [filepath,~,~] = fileparts(mfilename('fullpath'));
            pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Biophysical_Parameters' filesep] ;
                       
            BP_Variables ={'Da','De_par','De_perp','f','Kappa',};
            for branch = 1:2
                for inx_var = 1:numel(BP_Variables)
                    Vol=(spm_vol([pth 'BP_' BP_Variables{inx_var} '_Sample_Objective_Branch_' num2str(branch) '_DKI_RBC_OFF_Radial_GN_simulation_001.nii']));
                    VG = Vol(1);
                    
                    parameter=acid_read_vols(Vol,VG,1);                  
                    parameter_values_ground_truth_BP(inx_var,branch,:) = parameter(MSK);
                                      
                end 
            end          
        end
    end
    
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
    save('Ground_Truth_BP.mat','parameter_values_ground_truth_BP')   
end