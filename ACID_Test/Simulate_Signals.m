clear;
%
% ---> Just for documentation purposes <---
%
% Do not run this script (the random noise being added makes it impossible to recreate the primary data)

Dataset = 1;

load gradient_vectors.mat % these are the diffusion gradient vectors
load bvalues.mat % these are the corresponding b values

if Dataset == 1
    
    fintervall = linspace(0.73,0.73,1); %fintervall is an intervall for all the values for f that are used in the simulation
    Kintervall = linspace(8,8,1); %Kintervall is an intervall for all the values for K that are used in the simulation
    
    Da =2.00; % in um?/ms
    Deperp = 0.3 ;% in um?/ms
    Depara = 1.0;% in um?/ms
    
elseif Dataset == 2
    
    fintervall = linspace(0.25,0.25,1); %fintervall is an intervall for all the values for f that are used in the simulation
    Kintervall = linspace(50,50,1); %Kintervall is an intervall for all the values for K that are used in the simulation
    
    Da =2.37; % in um?/ms
    Deperp = 1.39 ;% in um?/ms
    Depara = 1.3;% in um?/ms
    
end

simulate_data = 1;
fit_data_DKI = 1;
check_output_DKI = 1; % for DKI estimation
fit_data_DTI = 1;
check_output_DTI = 1;
b = bvalues/1000;% to convert them from mm2/s to um2/ms

bMSK1000=find(b==1);
bMSK2000=find(b==2);
bMSK2500=find(b==2.5);

g1 = gradient_vectors(1,:) ;% dimensionless
g2 = gradient_vectors(2,:) ;% dimensionless
g3 = gradient_vectors(3,:) ;% dimensionless

c1 = 1; % c1, c2 and c3 are the components of the axis of symmetry (= main fiber direction)
c2 = 0;
c3 = 0;

q1 = 5;
q2 = 5;
q3 = 5;

number_of_noise_realizations = q1*q2*q3;

% simulate the biophysical signal model based upon signal equation (1) in Jespersen et al. 2018 "Diffusion time dependence of microstructural parameters in fixed spinal cord".
contaminate_signal = 1 ;

%these are the upper and lower boundaries for the integral of the signal simulation
thetalower = 0;
thetaupper = pi;
philower = 0;
phiupper = 2*pi;

simulation = ones(size(fintervall,2)*size(Kintervall,2),size(g1,2));

K = Kintervall(1);
n = @(theta, phi)  sin(theta) .* ( ( exp (K .* ( c1.* sin(theta) .* cos(phi) + c2 .* sin(theta) .* sin(phi) + c3 .* cos(theta) ) .^2 ) ) );
N = integral2( n , thetalower , thetaupper , philower , phiupper,'AbsTol',1e-20)  ; % this is the normalisation for the Watson distribution
watson_factor = 1;% ( 4 .* pi .* hypergeom( (1/2) , (3/2) , K ) )^-1; % this is an alternative way to express the normalisation of the Watson distribution

f = fintervall(1);

for m = 1: number_of_noise_realizations
    for i=1:size(g1,2)
        
        % I have split up equation (1) from Jespersen et al. 2018 into
        % two parts Sa and Sb. N is the factor with which to norm the
        % Watson distribution. The f and (1-f) factors of the signal
        % equation are included when adding the two integrals.
        % The integral is in spherical coordinates (r=1)
        % x = sin(theta)*cos(phi), y = sin(theta)*sin(phi),
        % z = cos(theta)
        
        gnorm=norm([g1(i),g2(i),g3(i)]); % this is being done because the gradient vectors from the measurement were not 100% normed
        if gnorm>0
            g1norm(i)= g1(i)/gnorm;
            g2norm(i)= g2(i)/gnorm;
            g3norm(i)= g3(i)/gnorm;
        else
            g1norm(i)= g1(i);
            g2norm(i)= g2(i);
            g3norm(i)= g3(i);
        end     
            
        Sa = @(theta, phi) sin(theta) .* ( ( watson_factor .*  exp (K .* ( c1.* sin(theta) .* cos(phi) + c2 .* sin(theta) .* sin(phi) + c3 .* cos(theta) ) .^2 ) ) .* exp ( -b(i) .* Da .* ( g1norm(i) .* sin(theta) .* cos(phi) + g2norm(i) .* sin(theta) .* sin(phi) + g3norm(i) .* cos(theta) ) .^2 ) )  ;   
        Sb = @(theta, phi) sin(theta) .* ( ( watson_factor .*  exp (K .* ( c1.* sin(theta) .* cos(phi) + c2 .* sin(theta) .* sin(phi) + c3 .* cos(theta) ) .^2 ) ) .* exp ( -b(i) .* Deperp - b(i) .* ( Depara - Deperp ) .* (g1norm(i) .* sin(theta) .* cos(phi) + g2norm(i) .* sin(theta) .* sin(phi) + g3norm(i) .* cos(theta) ) .^2 ) ) ;
        
        qa = integral2( Sa , thetalower , thetaupper , philower , phiupper,'AbsTol',1e-20);     
        qb = integral2( Sb , thetalower , thetaupper , philower , phiupper,'AbsTol',1e-20);
         
        signal(i) = (f/N) * qa + ( ( 1 - f ) / N ) * qb ;
    end
    
    if (contaminate_signal == 1)
        for noise_index = 1:size(signal,2)
            
            mean = 0 ;
            sigma = 1/100; % the zero weighted signal intensity S0 is one, so that, for example, a sigma of 1/5 would yield an SNR of S0/sigma = 1  / (1/5) = 5
            
            real = random ( 'Normal', mean , sigma );
            
            imaginary =  random ( 'Normal', mean , sigma );
            
            contaminated_signal = abs ( ( signal(noise_index) ) + complex ( real , imaginary ) ) ;
            
            signal(noise_index) = contaminated_signal;
        end
    end
     
    signalS(:,m) = signal' * 1000; % The image intensities in the nii data have to be between 0 and 1000 for the ACID toolbox
    
end

% The steps from hereon onwards are for writing the data into a file to be
% able to process them further

[filepath,~,~] = fileparts(mfilename('fullpath'));
pth_signal = [filepath filesep 'Ground_Truth_Data' filesep 'Signals' filesep];
P = [filepath filesep 'image_template.nii'];

V = spm_vol(P) ;
[p,f,e] = spm_fileparts(P) ;

fname ='simulation';

V.fname =  [pth_signal fname '.mat']  ;

V.dim = [q1 q2 q3+1];

A = zeros ( [ V.dim , size(b,2) ] ) ;
signal_count = 1;

for i=1:q1
    for j=1:q2
        for slice = 2:q3+1
            
            temporary_signal = reshape ( signalS(:,signal_count) , [1,1,numel(b)] );
                 
            A( i , j , slice, 1:numel(g1)) = temporary_signal ;
            signal_count=signal_count+1;
        end
    end
end

for inx = 1:numel(g1)
    
    if inx < 10
        num = ['_00' num2str(inx)];
    elseif inx < 100 
        num = ['_0' num2str(inx)];
    elseif inx < 1000
        num = ['_' num2str(inx)];
    end
    
    my_write_vol_nii( A(:,:,:,inx) ,V,'' ,num) ;
    
end