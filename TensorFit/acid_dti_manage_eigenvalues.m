function [FA, EVAL, EVEC, MSKo2_idx, tmp] = acid_dti_manage_eigenvalues(DT2d, MSKo_idx, dummy_cond_strict, dummy_dki, dummy_algo)

% =========================================================================
% The function calculates the eigenvalues and eigenvectors of the tensor
% received in the input. Strong assumption on tensor: trace is assumed to be positive.
% It is a wrapper around the core functions 'DTtoFA_nachHBM2010' and 'DTI2EVEW'.
%
% Inputs:
%   DT2d        - diffusion tensor (ols) for each voxel (within mask), 2D array [num_of_voxels x 7]
%   MSKo_idx    - mask storing the linear indices of voxels to be included, 2d array [num_of_voxels_within_mask x 1]
%
% Outputs:
%   FA        - fractional anisotropy values, of size [num_voxels, 1]
%   EVAL      - eigenvalues of the tensor, in decreasing order, of size [num_voxels, 3]
%   EVEC      - eigenvectors of the tensor, same order as EVAL, of size [num_voxels, 3, 3]
%   MSKo2_idx - output mask
%   tmp       - 
%
% Created by S.Mohammadi 11/05/2015
% =========================================================================

if dummy_cond_strict
    % necessary (but not sufficient) conditions for the matrix to be positive definite
    % MSKidx contains the voxels fullfilling the below criteria
    %(1) Dxx+Dyy+Dxx < Inf
    %(2) |Dxy|+|Dxz|+|Dyz| < Inf
    %(3) log(S0) <  Inf
    %(4) Dxx > eps
    %(5) Dyy > eps
    %(6) Dzz > eps
    %(7) Dxx + Dyy > 2*Dxy
    %(8) Dxx + Dyx > 2*Dxz
    %(9) Dyy + Dzz > 2*Dyz
    MSKidx = find(sum(DT2d(:,1:3),2)<Inf & ...
            sum(abs(DT2d(:,4:6)),2)<Inf & ...
            DT2d(:,end)<Inf & ...
            DT2d(:,1)>eps & ...
            DT2d(:,2)>eps & ...
            DT2d(:,3)>eps &  ...
            (DT2d(:,1)+DT2d(:,2)) > 2*abs(DT2d(:,4)) & ...
            (DT2d(:,1)+DT2d(:,3)) > 2*abs(DT2d(:,5)) & ...
            (DT2d(:,2)+DT2d(:,3)) > 2*abs(DT2d(:,6)));
        
    MSKo2_idx = intersect(MSKo_idx,MSKidx);
else
    MSKo2_idx = MSKo_idx;
end

if ~dummy_dki && (dummy_algo < 3) 
    DT2d = DT2d(MSKo2_idx,:);
end

% calculate FA and eigenvalues/vectors
[FA, EVEC(:,:,1), EVEC(:,:,2), EVEC(:,:,3), EVAL, tmp] = acid_c_dti_to_ev_ew(DT2d(:,1:6)); 

end