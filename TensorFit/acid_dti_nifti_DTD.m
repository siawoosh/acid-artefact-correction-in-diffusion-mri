function acid_dti_nifti_DTD(evals_names, evecs_names_4D, b0_name, pthfname)
% Created by S.Mohammadi 09/10/2012
% Adapted by B.Fricke 30/06/2022

eval_struct = spm_vol(evals_names);
evec_struct = spm_vol(evecs_names_4D);
b0_struct   = spm_vol(b0_name);

for i=3:-1:1
    eval_val(:,:,:,i) = acid_read_vols(eval_struct(i,:),eval_struct(1),-7);
end

for i=9:-1:1
    evec_val_tmp(:,:,:,i) = acid_read_vols(evec_struct(i,:),evec_struct(1),-7);
    
    for j=1:size(evec_val_tmp,3)
        evec_val(:,:,j,i) = evec_val_tmp(:,:,j,i).';
    end
    
end

b0_val = acid_read_vols(b0_struct(1,:),b0_struct(1),-7);
b0_val = permute(b0_val,[2 1 3]);

counter = 1;
for i=1:3
    for j=1:3
        evecs_names_3D(counter,:) = [evecs_names_4D(i,:) ',' num2str(j)];
        counter = counter + 1;
    end
end

% bring evecs in right shape
sz = size(evec_val);
evec_val = reshape(evec_val,[sz(1:3) 3 3]);

eval_val = permute(eval_val,[2 1 3 4]);

dimension_size = size(eval_val,1):-1:1;
dimension_size_evec = size(evec_val,1):-1:1;

eval_val = eval_val([dimension_size],:,:,:);
evec_val = evec_val([dimension_size_evec],:,:,:,:);
evec_val = evec_val(:,:,:,[2 1 3],:);

eval_new.dataAy = eval_val;
evec_new.dataAy = evec_val;
b0_new.dataAy   = b0_val;

b0_new.memoryType = '';
b0_new.dim1  = 'size_x';
b0_new.dim2  = 'size_y';
b0_new.dim3  = 'size_z';
b0_new.dim4  = 'unused';
b0_new.dim5  = 'unused';
b0_new.dim6  = 'unused';
b0_new.dim7  = 'unused';
b0_new.dim8  = 'unused';
b0_new.dim9  = 'unused';
b0_new.dim10 = 'unused';
b0_new.dim11 = 'unused';
b0_new.vox  = '';
b0_new.edges= '';
b0_new.orient  = '';
b0_new.method= '';
b0_new.te  = [];
b0_new.tr= [];
b0_new.ti= [];
b0_new.patient= '';
b0_new.user= [];

eval_new.memoryType = '';
eval_new.dim1  = 'size_x';
eval_new.dim2  = 'size_y';
eval_new.dim3  = 'size_z';
eval_new.dim4  = 'size_t';
eval_new.dim5  = 'unused';
eval_new.dim6  = 'unused';
eval_new.dim7  = 'unused';
eval_new.dim8  = 'unused';
eval_new.dim9  = 'unused';
eval_new.dim10 = 'unused';
eval_new.dim11 = 'unused';
eval_new.vox  = '';
eval_new.edges= '';
eval_new.orient  = '';
eval_new.method= '';
eval_new.te  = [];
eval_new.tr= [];
eval_new.ti= [];
eval_new.patient= '';
eval_new.user= [];

evec_new.memoryType = '';
evec_new.dim1  = 'size_x';
evec_new.dim2  = 'size_y';
evec_new.dim3  = 'size_z';
evec_new.dim4  = 'size_t';
evec_new.dim5  = 'size_t';
evec_new.dim6  = 'unused';
evec_new.dim7  = 'unused';
evec_new.dim8  = 'unused';
evec_new.dim9  = 'unused';
evec_new.dim10 = 'unused';
evec_new.dim11 = 'unused';

evec_new.vox  = '';
evec_new.edges= '';
evec_new.orient  = '';
evec_new.method= '';
evec_new.te  = [];
evec_new.tr= [];
evec_new.ti= [];
evec_new.patient= '';
evec_new.user= [];


% create dtdstruct
[dtd, err] = dtdstruct_init('DTD',evec_new,eval_new,b0_new,'b0_image_struc');
% err
% save
dtdstruct_write(dtd,[pthfname '_DTD.mat']);

return
end