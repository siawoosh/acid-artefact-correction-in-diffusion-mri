function [DT_sl,w,w1,w2,w3,F1] = acid_dti_robust_estimate_Zwiers2010(DT0_sl, logS_sl, MSK_sl, MSK_sl_idx, MSKo_sl, B, bvals, V_ref, A1, A2, lambda, sigma, kmax, z, F1)

% ========================================================================
% The script is the core of the robust fitting algorithm, which computes
% robust estimates of the diffusion tensor by solving the logarithmic
% signal equation by means of an iteratively-reweighted least squares (IRLS)
% algorithm.
%
% Input:
%   DT0_sl  - initial DT parameters of voxels in slice z, of size (n of voxels in slice z, 7)
%   logS_sl - logarithmic signal of voxels in slice z, os fize [n of voxels in slice z]
%   MSK_sl  - 3D binary mask for computing weights, restricted to slice z
%   MSK_idx - holds the liner indices of MSK_sl
%   MSKo_sl - 3D binary mask applied on the output, restricted to slice z
%   B       - B matrix of the dMRI dataset
%   bvals   - b values of the dMRI dataset
%   V_ref   - reference header
%   A1      - scaling parameter for w1
%   A2      - scaling parameter for w2
%   lambda  - Tikhonov regularization parameter
%   sigma   - noise estimate for w3
%   kmax    - parameter of the smoothing kernel applied on C1
%   z       - selected slice
%   F1      - figure handle for plotting weights
%
% Output:
%   DT_sl   - DT parameters at the end of the iterations, of size (n of voxels in slice z, 7)
%   w       - weight product (w = w1*w2*w3)
%   w1      - first weight component
%   w2      - second weight component
%   w3      - third weight component
%   F1      - figure handle for plotting weights
% ========================================================================

% def
thr_w = 0;

% get dimension
dm = V_ref(1).dim;

% get b value related variables
b0 = min(bvals);
idx_dw = find(bvals > b0);
[bC,~,~] = unique(bvals(1,bvals>=b0));
 
if numel(logS_sl)==0 % if the slice is empty
    DT_sl = zeros(size(DT0_sl));
    w  = ones(size(logS_sl));
    w1 = ones(size(logS_sl));
    w2 = ones(size(logS_sl));
    w3 = ones(size(logS_sl));
else
    % fitted log(S) signal
    logS_sl_fit = (B*DT0_sl')';

    % model-fit error: fitted log(S) - actual log(S)
    res = logS_sl_fit - logS_sl; 

    % compute C1 for w1 (scaled version of MAD(error))
    C1 = 1.4826 * median(abs(res),2);

    % convert C1 into 2D format
    %   C1    - [num_of_voxels_slice_mask x 1]
    %   C1_2d - [dim(1) x dim(2)]
    C1_2d = zeros(dm(1:2));
    C1_2d(MSKo_sl>0) = C1;   
   
    % smooth C1 in the plane and convert it back to 1D format
    C1_2d_sm = acid_dti_smoothing2D_prod(C1_2d,kmax);
    C1_2d_sm = C1_2d_sm(MSKo_sl>0);
     
    % compute C2 for w2 (scaled version of MAD(slice-averaged error))
    if(isempty(MSK_sl_idx) || sum(MSK_sl_idx)==0)
        res_plane = zeros(1,size(res,2));
        C2 = 1;
    else
        res_plane = sum(abs(res),1,"omitnan")/sqrt(numel(MSK_sl_idx));
        C2 = 1.4826 * median(abs(res_plane));
    end
    
    % w1 - regional weight
    tmp = ones(size(logS_sl));
    w1  = exp(-2*(bsxfun(@rdivide,A1*res,C1_2d_sm).^2));

    % w2 - slice-wise weight
    if(C2~=0 && abs(C2)<Inf)
        w2 = exp(-2*(bsxfun(@rdivide,A2*res_plane(C2>0,:),C2(C2>0))).^2);
        w2 = bsxfun(@times,w2,tmp);
    end
    
    % w3 - global weight (correcting for the distortion from taking the logarithm)
    if ~exist('dummy_DKI_robust','var')
        if size(logS_sl,2)~=numel(idx_dw)
            w3 = (bsxfun(@rdivide,exp(logS_sl_fit),sigma)).^2;
        else
            w3 = (bsxfun(@rdivide,exp(logS_sl_fit),sigma(idx_dw))).^2;  
        end
    elseif dummy_DKI_robust==1
        MSK_sl          = find(MSK_sl>0);
        nMSK_sl         = find(MSK_sl==0);
        [lSWtmp,~]      = callogDW_vstruct_slicew(MSK_sl,V_ref,b0,bvals,z);
        [lSWtmp1,~]     = callogDW_vstruct_slicew(nMSK_sl,V_ref,b0,bvals,z);
        sigma00         = mean(max(lSWtmp1,1),1); 
        sigma00(sigma00<=1 | sigma00>=Inf) = 4; % hacked
        w3             = bsxfun(@rdivide,exp(lSWtmp),sigma00).^2;    
    end
   
    % w - resulting weight (w = w1*w2*w3)
    if ~exist('dummy_DKI_robust','var')
        w     = w1.*w2.*w3;  % weight to estimate the tensor
        witer = w1.*w2;      % weight to iterate
    elseif dummy_DKI_robust == 0
        bMSK1 = find(bvals(idx_dw)==max(bvals(idx_dw)));
        bMSK2 = find(bvals(idx_dw)<max(bvals(idx_dw)) & bvals(idx_dw)>b0);
        wa          = ones(size(tmp));
        wa(:,bMSK1) = 1;
        wa(:,bMSK2) = 1/3;
        w = tmp.*tmp1.*tmp2.*wa;
    elseif dummy_DKI_robust == 1
        bMSK1 = find(bvals(idx_dw)==max(bvals(idx_dw)));
        bMSK2 = find(bvals(idx_dw)<max(bvals(idx_dw)) & bvals(idx_dw)>b0);
        wa          = ones(size(tmp));
        wa(:,bMSK1) = 1/3;
        wa(:,bMSK2) = 1;
        w = tmp.*tmp1.*tmp2.*wa;       
    end
      
    % compute normalized w
    % w1 and w2 are normalized between 0 and 1, but w3 (and correspondingly) w are not
    % w is normalized by dividing w by the maximum w across the dw volumes in that voxel
    for i=1:numel(bC)
        MSK_b_tmp = find(bvals==bC(i));
        tmp_b1 = max(w(:,MSK_b_tmp),[],2);
        tmp_b2 = max(witer(:,MSK_b_tmp),[],2);
        w(:,MSK_b_tmp) = bsxfun(@rdivide, w(:,MSK_b_tmp),tmp_b1);       
        witer(:,MSK_b_tmp) = bsxfun(@rdivide, witer(:,MSK_b_tmp),tmp_b2);  
    end
    
    % plot weights
    if ~isempty(F1)
        mtmp = mean(w3,1);
        subplot(3,1,1);plot(mtmp/max(mtmp),'x-'); ylim([0 1]);title(['Weights at slice: ' num2str(z)]);set(gca,'fontsize',16)
        mtmp1 = mean(w1,1);
        subplot(3,1,2);plot(mtmp1/max(mtmp1),'x-'); ylim([0 1]);title(['Outliers (per voxels) in slice: ' num2str(z)]);set(gca,'fontsize',16)
        mtmp2 = mean(w2,1);
        subplot(3,1,3);plot(mtmp2/max(mtmp2),'x-'); ylim([0 1]);title(['Whole-slice outliers in slice: ' num2str(z)]);set(gca,'fontsize',16)
        drawnow
    end
    
    % solving the equation: DT = [B'WB + lambda*B'B]^-1  B'W y
    %                       DT = [ A   + lambda* C ]^-1   D  y
    %
    % Note that each voxel has a separate B, W, y, A, C, and D a matrix, so
    % the above equation is to be solved separately for each voxel.
    % However, here, these matrices are defined in an extended diagonal
    % form, so that only one equation has to be solved (making it much faster).
    % It means that instead of matrix B and W, we have           
    % B_ext = [ B 0 0
    %           0 B 0
    %           0 . 0
    %           0 0 B]
    % W_ext = [ W 0 0
    %           0 W 0
    %           0 . 0
    %           0 0 W]

    % W
    % diagonal matrix, where entries of w_i are in the diagonal   
    ww = w';
    ww = ww(:);
    MSK_nz  = find(ww>thr_w);
    W = sparse(1:numel(ww(MSK_nz)),1:numel(ww(MSK_nz)),ww(MSK_nz)); 
    
    % B'
    % B matrices in the diagonal
    B2 = kron(speye(size(logS_sl,1)),sparse(B'));

    % A = B'WB
    A = B2(:,MSK_nz)*W*B2(:,MSK_nz)';
    
    % C = B'B
    C = B2(:,MSK_nz)*B2(:,MSK_nz)';
    
    % D*y = B'W*y
    y = logS_sl';
    y = y(:);
    Dy = B2(:,MSK_nz)*W*y(MSK_nz);
    
    % solve equation
    %  DT   - updated diffusion tensor in the iteration
    DT_sl = (A + lambda*C)\Dy;
    DT_sl = reshape(DT_sl(:,1),size(B,2),size(logS_sl,1));
    DT_sl = DT_sl';
end
end