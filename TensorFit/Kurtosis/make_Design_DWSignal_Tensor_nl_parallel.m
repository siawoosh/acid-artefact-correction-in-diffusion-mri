function [dummy_dki, Asym, X4, lS, lS0] = make_Design_DWSignal_Tensor_nl_parallel(dummy_dki, bvals, bvecs, binfo, V, VG, MSK, dki_thrcond, dummy_opt, n_workers)
% S.Mohammadi 14/10/2013

if ~exist('npool','var')
    n_workers = 1;
end

X4 = create_DesigneM_4Kurtosis_new(bvals,bvecs);
if cond(X4)> dki_thrcond
    warning('The experiment is badly conditioned for DKI, switching to DTI...');
    dummy_dki = 0;
else
    % estimating Kurtosis as suggested by Tabesh et al. 2011
    % notation as in Tabesh et al. 2011, e.g. D1 = ADC(b1) and D2 = ADC(b2)
    sz = VG.dim;
    Asym_olsq = zeros([prod(sz) 6]);
    Asym_X4   = zeros([prod(sz) 15]);
    n_workers = acid_multicore(n_workers);

    % if n_workers>1
    %     if exist('parpool')>0
    %         poolobj = parpool('local',n_workers); 
    %         dummy_matlabpool = false; 
    %     elseif exist('matlab')>0
    %         matlabpool('open',n_workers); 
    %         dummy_matlabpool = true; 
    %     else
    %         n_workers = 1;
    %         warning('No parallel processing possible on this matlab version! Proceed without parallel processing.')            
    %     end
    % end

    for p = 1:sz(3)
        disp(['slice:' num2str(p)]);
        disp('');
        
        MSKvol        = zeros(sz);
        MSKvol(:,:,p) = ones(sz(1:2));
        if ~isempty(find(MSK(:,:,p)>0))

            [lS,lS0] = callogDWS0_vstruct2D_DKI_test(V,binfo.b0,bvals,p);
            lS       = bsxfun(@minus,lS,lS0); %original
            sz       = VG.dim;

            if n_workers>1
                cdiff = OptimizationProblem_parfor(bvecs,bvals(1,:),lS,MSK(:,:,p),dummy_opt);
            else
                cdiff = OptimizationProblem_single(bvecs,bvals(1,:),lS,MSK(:,:,p),dummy_opt);
            end
            Asym_olsq(MSKvol>0,:) = cdiff(1:6,:)';
            Asym_X4(MSKvol>0,:)   = cdiff(7:end,:)';
        end
    end
    
    % if n_workers>1  
    %     if dummy_matlabpool
    %         matlabpool close;
    %     else
    %         delete(poolobj);  
    %     end
    % end

%     DM = struct('DM0',X2,'X4',X4);
%     lSW = struct('lSWvol',Dconstr,'lSW_X4',Kconstr);
    Asym     = struct('Asym_olsq',Asym_olsq(MSK>0,:)','Asym_X4',Asym_X4(MSK>0,:)');
    [lS,lS0] = callogDW_vstruct_vol(find(MSK>0),V,binfo.b0,bvals);
end
end