function [X, b_min] = create_DesigneM_4Kurtosis_new(bvals,bvecs)
% S. Mohammadi 21/10/2010
% using the definition as in Tabesh et al., 2011

if size(bvecs,1) ~= 3
    error('Diffusion gradient vectors in wrong format: 3xN (N = number of diffusion gradients)!')
end

X2(1,:)  = bvecs(1,:).^2;
X2(2,:)  = bvecs(2,:).^2;
X2(3,:)  = bvecs(3,:).^2;

X2(4,:)  = 2*bvecs(1,:).*bvecs(2,:);
X2(5,:)  = 2*bvecs(1,:).*bvecs(3,:);
X2(6,:)  = 2*bvecs(2,:).*bvecs(3,:);

X4(1,:)  = bvecs(1,:).^4; % V_1111
X4(2,:)  = bvecs(2,:).^4; % V_2222
X4(3,:)  = bvecs(3,:).^4; % V_3333

X4(4,:)  = 4*bvecs(1,:).^3.*bvecs(2,:); % V_1112
X4(5,:)  = 4*bvecs(1,:).^3.*bvecs(3,:); % V_1113
X4(6,:)  = 4*bvecs(2,:).^3.*bvecs(1,:); % V_2221
X4(7,:)  = 4*bvecs(2,:).^3.*bvecs(3,:); % V_2223
X4(8,:)  = 4*bvecs(3,:).^3.*bvecs(1,:); % V_3331
X4(9,:)  = 4*bvecs(3,:).^3.*bvecs(2,:); % V_3332

X4(10,:) = 6*bvecs(1,:).^2.*bvecs(2,:).^2; % V_1122
X4(11,:) = 6*bvecs(1,:).^2.*bvecs(3,:).^2; % V_1133
X4(12,:) = 6*bvecs(2,:).^2.*bvecs(3,:).^2; % V_2233

X4(13,:) = 12*bvecs(1,:).^2.*bvecs(2,:).*bvecs(3,:); % V_1123 = V_permut(1132)
X4(14,:) = 12*bvecs(2,:).^2.*bvecs(1,:).*bvecs(3,:); % V_2213
X4(15,:) = 12*bvecs(3,:).^2.*bvecs(1,:).*bvecs(2,:); % V_3312

szb = size(bvals); 
if(szb(1)==1)
%     nbvalue = bvalueTE/max(bvalueTE); % rescale the b-value
    nbvalue = bvals;
    b_min   = min(nbvalue);
    bMSK = find(nbvalue>b_min);
    X2   = bsxfun(@times,X2, nbvalue);
    X4   = bsxfun(@times,X4, (nbvalue.^2)/6);
    X    = cat(1,-X2,X4);
elseif(szb(1)==2)
    nbvalue = bvals(1,:)/max(bvals(1,:)); % rescale the b-values
    b_min   = min(nbvalue(1,:));
    bMSK = find(nbvalue(1,:)>b_min);
    nTE  = bvals(2,:)/max(bvals(2,:));
    X2   = bsxfun(@times,X2, nbvalue);
    X4   = bsxfun(@times,X4, (nbvalue.^2)/6);
    X    = cat(1,-X2,X4,-nTE);
end

% mask for b>min(b)
X = X(:,bMSK);

end


