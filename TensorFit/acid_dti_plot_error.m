function acid_dti_plot_error(Error_4d, MSK_idx, p_out, VG, keyword)

% =========================================================================
% This script is for creating diagnostic plots on the distribution of
% model-fit error. 
%
% The model-fit error can be defined in two different ways:
%   1) abs(log(S_mes)-log(S_fit))
%   2) abs(S_meas-S_fit) 
% In the current implementation, we pass the logarithmic model-fit error to
% this script.
%
% Inputs:
%   resError4d - model-fit error  for each voxel, 4D array
%   MSKidx     - mask containing linear indices
%   p_out      - output folder
%
% =========================================================================

% Get dimensions of 4D dMRI dataset
dm = size(Error_4d);

% Turning the 2D mask into a 4D binary mask
mask = zeros(dm(1:3));
mask(MSK_idx) = 1;

% Initialization
Error_vw = zeros(dm(4),1);     % Mean absolute model-fit error per volume
Error_sw = zeros(dm(4),dm(3)); % Mean absolute model-fit error per volume and slice
SEMError_vw = zeros(dm(4),1);  % Standard error of the mean of absolute model-fit error across volumes 

% Compute absolute model-fit error
Error_4d = abs(Error_4d);

% Looping through volumes
for k = 1:size(Error_4d,4) 
    
    tmp = Error_4d(:,:,:,k).*mask;
    tmp = tmp(:); tmp(tmp==0)=[]; tmp(isnan(tmp))=[];
    Error_vw(k) = mean(tmp);
    SEMError_vw(k) = std(tmp)/sqrt(numel(tmp));
    
    for i = 1:size(Error_4d,3)
        temp2 = Error_4d(:,:,i,k).*mask(:,:,i);
        temp2 = temp2(:); 
        temp2(temp2==0) = []; 
        temp2(isnan(temp2))=[];
        Error_sw(k,i) = mean(temp2);  
    end
end

% Plot model-fit error for dw volumes
figure
errorbar(Error_vw, SEMError_vw, 'r*')
xlabel('Index of volume')
ylabel('Model-fit error')
title('Diagnostic plot for model-fit errors')
fname_plot = acid_bids_filename(VG,[keyword '-Error-Vw'],'','.fig');
fname_plot = [p_out filesep fname_plot];
saveas(gcf, fname_plot, 'fig');

% Plot slice-wise model-fit error for volumes
figure
imagesc(Error_sw')
xlabel('Index of volume')
ylabel('Slice')
title('Diagnostic plot for model-fit errors')
colorbar
fname_plot = acid_bids_filename(VG,[keyword '-Error-Sw'],'','.fig');
fname_plot = [p_out filesep fname_plot];
saveas(gcf, fname_plot, 'fig');

end