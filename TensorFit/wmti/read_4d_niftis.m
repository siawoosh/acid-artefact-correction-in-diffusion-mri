function [data] = read_4d_niftis(path)
%% prepare if first volume of a 4D file was selected


    struct4D = nifti(path);
    dim4D = struct4D.dat.dim;
    n = dim4D(4);
    if n == 1
        error('A single 3D source image was selected. Choose a single 4D volume or select all 3D volumes manually!');
    end
    nifti_files_4d = strcat(repmat(path(1:end), n, 1), ',', num2str([1:n]'));



    volume = spm_vol(nifti_files_4d(1,:));
    data = zeros([volume.dim,n]);
    
    for inx = 1:n

         vol = spm_vol(nifti_files_4d(inx,:));
         data(:,:,:,inx) = acid_read_vols(vol,vol,1); 

    end

    % [Signal, Sigma] = MPdenoising(data, [], [], 'full')

end