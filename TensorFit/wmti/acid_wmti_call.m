function wmti_call = acid_wmti_call()


% diffusion tensor file
wmti_DT         = cfg_files;
wmti_DT.tag     = 'wmti_DT';
wmti_DT.name    = 'Diffusion Tensor';
wmti_DT.help    = {'Select diffusion tensor (DT) file (4D NIfTI).'};
wmti_DT.filter  = 'image';
wmti_DT.ufilter = '.*';
wmti_DT.num     = [0 1];

% diffusion kurtosis tensor file
wmti_KT         = cfg_files;
wmti_KT.tag     = 'wmti_KT';
wmti_KT.name    = 'Kurtosis Tensor';
wmti_KT.help    = {'Select kurtosis tensor (KT) file (4D NIfTI).'};
wmti_KT.filter  = 'image';
wmti_KT.ufilter = '.*';
wmti_KT.num     = [0 1];


% mask
wmti_mask         = cfg_files;
wmti_mask.tag     = 'wmti_mask';
wmti_mask.name    = 'Mask (binary)';
wmti_mask.help    = {'Select binary mask.'};
wmti_mask.filter  = 'image';
wmti_mask.ufilter = '.*';
wmti_mask.num     = [0 1];
wmti_mask.help    = {'Mandatory, please select a mask.'};



%% startup
wmti_call         = cfg_exbranch;
wmti_call.tag     = 'wmti_call';
wmti_call.name    = 'wmti';
wmti_call.val     = {wmti_DT, wmti_KT, wmti_mask};
wmti_call.help    = { '' };
wmti_call.prog = @local_wmti;
wmti_call.help = {'Calls wmti implementation ("wmti_parameters.m") from git repository: https://github.com/NYU-DiffusionMRI/DESIGNER,' ...
    'when using this module, PLEASE CITE: Fieremans, Els, Jens H. Jensen, and Joseph A. Helpern. White matter characterization with diffusional kurtosis imaging, Neuroimage 58.1 (2011): 177-188.'};
% wmti_call.vout = @vout_wmti;


end


function out = local_wmti(job)

    DT_path = job.wmti_DT{1};
    ADT = read_4d_niftis(DT_path(1:end-2));

    KT_path = job.wmti_KT{1};
    AKT = read_4d_niftis(KT_path(1:end-2));



    volume = spm_vol(job.wmti_mask{1});
    mask = acid_read_vols(volume,volume,1); 
    mask = logical(mask);


    dt_v(:,:,:,1)  = ADT(:,:,:,1);
    dt_v(:,:,:,2)  = ADT(:,:,:,4);
    dt_v(:,:,:,3)  = ADT(:,:,:,5);
    dt_v(:,:,:,4)  = ADT(:,:,:,2);
    dt_v(:,:,:,5)  = ADT(:,:,:,6);
    dt_v(:,:,:,6)  = ADT(:,:,:,3);
    dt_v(:,:,:,7)  = AKT(:,:,:,1);
    dt_v(:,:,:,8)  = AKT(:,:,:,4);
    dt_v(:,:,:,9)  = AKT(:,:,:,5);
    dt_v(:,:,:,10) = AKT(:,:,:,10);
    dt_v(:,:,:,11) = AKT(:,:,:,13);
    dt_v(:,:,:,12) = AKT(:,:,:,11);
    dt_v(:,:,:,13) = AKT(:,:,:,6); 
    dt_v(:,:,:,14) = AKT(:,:,:,14); 
    dt_v(:,:,:,15) = AKT(:,:,:,15); 
    dt_v(:,:,:,16) = AKT(:,:,:,7); 
    dt_v(:,:,:,17) = AKT(:,:,:,2); 
    dt_v(:,:,:,18) = AKT(:,:,:,8); 
    dt_v(:,:,:,19) = AKT(:,:,:,12); 
    dt_v(:,:,:,20) = AKT(:,:,:,9); 
    dt_v(:,:,:,21) = AKT(:,:,:,3);


    [awf, eas, ias] = wmti_parameters(dt_v,mask);
    
    disp('--->When using this wmti fit, PLEASE CITE: Fieremans, Els, Jens H. Jensen, and Joseph A. Helpern. White matter characterization with diffusional kurtosis imaging, Neuroimage 58.1 (2011): 177-188.<---')

    my_write_vol_nii(awf,spm_vol(DT_path),'awf_GK_');
    my_write_vol_nii(eas.de_perp,spm_vol(DT_path),'eas_perp_GK_');
    my_write_vol_nii(eas.de1,spm_vol(DT_path),'eas_parallel_GK_');
    my_write_vol_nii(ias.Da,spm_vol(DT_path),'ias_Da_GK_');

    average_of_cosine_of_theta_squared = ias.da1 ./ ias.Da; %Jelescu et al. 2015, doi: 10.1016/j.neuroimage.2014.12.009
    p2 = (3 .* average_of_cosine_of_theta_squared -1)./2;

    my_write_vol_nii(average_of_cosine_of_theta_squared,spm_vol(DT_path),'fiber_orientation_dispersion_GK_');
    my_write_vol_nii(p2,spm_vol(DT_path),'p2_GK_');


end
