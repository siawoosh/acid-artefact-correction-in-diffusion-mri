function [logS_sl, logS0_sl, S_sl] = acid_dti_get_logS_2D(V, VG, bvals, p, res)

% =========================================================================
% This function outputs for the slice p the b0 image as an array (logS0_sl) and
% the whole DTI dataset (including b0 images) (logS_sl) as an 2D array.
% It cleans entries with S0 <= 0 to avoid lSDW = infinity -> zero.
%
% Inputs:
%   V       - headers of the input images
%   VG      - reference volume
%   bvals   - b values of all N dMRI volumes, of size (1,N)
%   p       - index of the investigated slice
%   res     - interpolation kernel
%
% Outputs:
%   logS_sl  - logarithmic signal for voxels in slice p, for all images (num_of_voxels in slice, num_of_volumes)
%   logS0_sl - logarithmic signal for voxels in slie p, for b0 images (num_of_voxels in slice, num_of_volumes)
%   S_sl     - signal for voxels in slice p, for all images (num_of_voxels in slice, num_of_volumes)
%
% Created by S.Mohammadi 20/12/2013
% Adapted by G.David
% =========================================================================

% type of interpolation at resampling
if ~exist('res','var')
    res = -7;
end

dm = VG.dim;

% Get indices
b0 = min(bvals);
idx_b0 = find(bvals==b0);

% Initialization
S_sl  = zeros(dm(1)*dm(2), size(V,1));
S0_sl = zeros(dm(1)*dm(2), numel(idx_b0));

% Calculate log(S) and set voxels with S<1 to 0
for i = 1:size(V,1)
    S_sl_tmp = acid_read_vols(V(i),VG,res,p);
    idx = (S_sl_tmp>=1);
    S_sl(idx,i) = S_sl_tmp(idx);
end
logS_sl = log(S_sl);
logS_sl(logS_sl==-Inf) = 0;

% Calculate log(S0) and set voxels with S0<1 to 0
for i = 1:numel(idx_b0)
    S0_sl_tmp = acid_read_vols(V(idx_b0(i)),VG,res,p);
    idx = (S0_sl_tmp>=1);
    S0_sl(idx,i) = S0_sl_tmp(idx);
end
logS0_sl = log(S0_sl);
logS0_sl(logS0_sl==-Inf) = NaN;

end