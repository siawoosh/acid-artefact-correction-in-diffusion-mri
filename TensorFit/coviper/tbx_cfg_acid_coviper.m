function diff_coviper = tbx_cfg_acid_coviper(dummy_Tfreiburg, RMatrix, Dthr)

% Created at 2020-10-12

%% COVIPER correction
% ---------------------------------------------------------------------
% percentage coverage of brain mask
% ---------------------------------------------------------------------
perc_coviper         = cfg_entry;
perc_coviper.tag     = 'perc_coviper';
perc_coviper.name    = 'brain mask parameter';
perc_coviper.help    = {'Factor that depends on ratio between brain coverage and field-of-view. Less brain coverage of field-of-view means lower perc-value.'};
perc_coviper.strtype = 'e';
perc_coviper.num     = [1 1];
perc_coviper.val     = {0.8};

% ---------------------------------------------------------------------
% dummy for study - if it is on, all kind of DTI data are written
% ---------------------------------------------------------------------
dummy_study_coviper   = cfg_menu;
dummy_study_coviper.tag     = 'dummy_study_coviper';
dummy_study_coviper.name    = 'Write all DTI data.';
dummy_study_coviper.help    = {'This option enables to write out all DTI data (e.g. FA, tensor-error-fit, etc) for blip-up and down data separately, as well as for the arithmetic mean and the COVIPER combination.'};
dummy_study_coviper.labels =    {
                                 'No'
                                 'Yes'
                                }';
dummy_study_coviper.values = {0 1};
dummy_study_coviper.val    = {0};

% ---------------------------------------------------------------------
% b values
% ---------------------------------------------------------------------
b_vals_coviper         = cfg_entry;
b_vals_coviper.tag     = 'b_vals_coviper';
b_vals_coviper.name    = 'b-values';
b_vals_coviper.help    = {'Provide a 1 x N  - matrix with b-values, b-values should appear in the same order as the low- and high-diffusion weighted images were entered. b-values is expected in units of s/mm^2.' 
                          'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'
                            };
b_vals_coviper.strtype = 'e';
b_vals_coviper.num     = [1 Inf];
b_vals_coviper.val     = {[5 1000 1000 2000]};

% ---------------------------------------------------------------------
% diffusion directions
% ---------------------------------------------------------------------
diff_dirs_coviper         = cfg_entry;
diff_dirs_coviper.tag     = 'diff_dirs_coviper';
diff_dirs_coviper.name    = 'b-vectors';
diff_dirs_coviper.help    = {'Provide a 3 x N  - matrix with b-vectors, b-vectors should appear in the same order as the low- and high-diffusion weighted images were entered. The b-vectors are dimensionless.' 
                             'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'
                             };
diff_dirs_coviper.strtype = 'e';
diff_dirs_coviper.num     = [3 Inf];
diff_dirs_coviper.val     = {[1 0 0; 0 1 0; 0 0 1; 0 1/sqrt(2) 1/sqrt(2)]'};

% ---------------------------------------------------------------------
% in_vols diffusion tensor
% ---------------------------------------------------------------------
in_vols_dw_coviper         = cfg_files;
in_vols_dw_coviper.tag     = 'in_vols_dw_coviper';
in_vols_dw_coviper.name    = 'Blip-down DTI images';
in_vols_dw_coviper.help    = {'Select high- and low-b-value images of the diffusion data set that has to be correction for vibration artefacts.'
                              'Note that DTI datasets should be preprocessed, i.e. eddy current distortion correction, unwarping of susceptibility effects, and registeration of blip-up and blip-down images.'
                                };
in_vols_dw_coviper.filter = 'image';
in_vols_dw_coviper.ufilter = '.*';
in_vols_dw_coviper.num     = [0 Inf];
% ---------------------------------------------------------------------
% in_vols diffusion tensor
% ---------------------------------------------------------------------
in_vols_up_coviper         = cfg_files;
in_vols_up_coviper.tag     = 'in_vols_up_coviper';
in_vols_up_coviper.name    = 'Blip-up DTI images';
in_vols_up_coviper.help    = {'Select high- and low-b-value images of the diffusion data set that has to be correction for vibration artefacts.'
                              'Note that DTI datasets should be preprocessed, i.e. eddy current distortion correction, unwarping of susceptibility effects, and registeration of blip-up and blip-down images.'
                                };
in_vols_up_coviper.filter = 'image';
in_vols_up_coviper.ufilter = '.*';
in_vols_up_coviper.num     = [0 Inf];

%% defining COVIPER correction 

diff_coviper         = cfg_exbranch;
diff_coviper.tag     = 'diff_coviper';
diff_coviper.name    = 'COVIPER combination of Diffusion Tensor';
diff_coviper.val     = {in_vols_up_coviper in_vols_dw_coviper b_vals_coviper diff_dirs_coviper dummy_study_coviper perc_coviper RMatrix};
diff_coviper.help    = {'Least square tensor estimation of diffusion tensor using weighted sum to correct for vibration artefacts. Blip-up and Blipd-down DTI dataset needed. ' 
                    'Note that DTI datasets should be preprocessed, i.e. eddy current distortion correction, unwarping of susceptibility effects, and registeration of blip-up and blip-down images.'
                    'The COVIPER procedure is described in Mohammadi et al., MRM 2011 (doi: 10.1002/mrm.23308).'
                    'Please cite Mohammadi et al., MRM 2011 (doi: 10.1002/mrm.23308) when using this code.'};
diff_coviper.prog = @local_diff_coviper;
diff_coviper.vout = @vout_diff_coviper;




%----COVIPER-------------------
function out = local_diff_coviper(job)



Dthr = 1e-5;



VG = COVIPER_PROD(char(job.in_vols_up_coviper),char(job.in_vols_dw_coviper),job.diff_dirs_coviper,job.b_vals_coviper,job.dummy_study_coviper,job.perc_coviper,0,job.RMatrix,Dthr);
% add more dependencies
% out.files_up       = my_spm_file(job.in_vols_up_coviper);
%  out.files_up       = acid_my_select(out.files_up(:));
% out.files_dw       = my_spm_file(job.in_vols_dw_coviper);
%  out.files_dw       = acid_my_select(out.files_dw(:));

out.FAfiles  = VG(1).fname;

[a,b,c] = fileparts(VG(1).fname);

ending = 'MD';

fname = [b(1:end-6) ending '_dwi'];

out.MDfiles = {[a filesep fname c ',1']};


ending = 'AD';

fname = [b(1:end-6) ending '_dwi'];

out.ADfiles = {[a filesep fname c ',1']};

ending = 'RD';

fname = [b(1:end-6) ending '_dwi'];

out.RDfiles = {[a filesep fname c ',1']};






% out.FAfiles     = my_spm_file(job.in_vols_up_coviper{1},'prefix','FA_ols_COVIPER_');
% out.MDfiles     = my_spm_file(job.in_vols_up_coviper{1},'prefix','MD_ols_COVIPER_'); 
% out.ADfiles     = my_spm_file(job.in_vols_up_coviper{1},'prefix','Axial_ols_COVIPER_'); 
% out.RDfiles     = my_spm_file(job.in_vols_up_coviper{1},'prefix','Radial_ols_COVIPER_'); 
% out.RESfiles    = my_spm_file(job.in_vols_up_coviper{1},'prefix','RES_ols_COVIPER_'); 
% out.b0files     = my_spm_file(job.in_vols_up_coviper{1},'prefix','b0meas_ols_COVIPER_');  
% out.filesCOVIPER       = my_spm_file(char(job.in_vols_up_coviper),'prefix','rawDWI_ols_COVIPER_');
% out.b0COVIPER     = my_spm_file(job.in_vols_up_coviper{1},'prefix','rawb0_ols_COVIPER_');  


%------------------------
function dep = vout_diff_coviper(job) %to be adjusted
kk = 1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'input data up';
% dep(kk).src_output = substruct('.','files_up'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk+1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'input data down';
% dep(kk).src_output = substruct('.','files_dw'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk +1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'FA map COVIPER';
dep(kk).src_output = substruct('.','FAfiles'); 
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
kk = kk +1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'MD map COVIPER';
dep(kk).src_output = substruct('.','MDfiles'); 
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
kk = kk +1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'AD map COVIPER';
dep(kk).src_output = substruct('.','ADfiles'); 
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk +1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'RD map COVIPER';
dep(kk).src_output = substruct('.','RDfiles'); 
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'b0 map COVIPER';
% dep(kk).src_output = substruct('.','b0files'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk +1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'RES map COVIPER';
% dep(kk).src_output = substruct('.','RESfiles'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk +1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'meanDWI map COVIPER';
% dep(kk).src_output = substruct('.','DWIfiles'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk +1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'input data COVIPER';
% dep(kk).src_output = substruct('.','filesCOVIPER'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); 
% kk = kk +1;
% dep(kk)            = cfg_dep;
% dep(kk).sname      = 'b0 map COVIPER';
% dep(kk).src_output = substruct('.','b0COVIPER'); 
% dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
