function [IDM, DM] = acid_dti_get_bmatrix_varTE_varb(V, bvals)

% =========================================================================
% Inputs:
%   V       - headers of the dMRI dataset 
%   bvals   - b-values (1xN array)
%
% Outputs:
%   DM      - matrix of the diffusion tensor
%   IDM     - inverse of DM

% Created by S. Mohammadi 05/08/2012
% =========================================================================
    sz=size(V);

    if sz(1) ~= 3
        disp(sz(1))
        error('Invalid dimensions of gradient vectors!');
    end

    if size(bvals,1) ~= 2
        disp(size(bvals,2))
        error('Invalid dimensions of gradient vectors!');
    end

    DD = inline('bsxfun(@times,[-x.*x; -y.*y; -z.*z; -2.*x.*y; -2.*x.*z; -2.*y.*z], bvalue)','x','y','z','bvalue');
    MSK = find((V(1,:).*V(1,:)+V(2,:).*V(2,:)+V(3,:).*V(3,:))>0);
    if numel(MSK) ~= sz(2)
        nbvalue = bvals(1,:)/max(bvals(MSK));
    else
        nbvalue = bvals(1,:)/max(bvals(1,:));
        nTE     = bvals(2,:)/max(bvals(2,:));
    end

    DM = [DD(V(1,:),V(2,:),V(3,:),nbvalue);nTE;ones(1,numel(nbvalue))];
    DM  = DM';
    IDM = inv((DM')*DM)*DM';
return
end