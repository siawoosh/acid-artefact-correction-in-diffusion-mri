function [logS, logS0] = acid_dti_get_logS(V, VG, bvals, MSKidx)

% =========================================================================
% This function takes the logarithm of the measured signal. It outputs for
% the slice p the b0 image as an array (lS0) and the whole DWI dataset
% (including b0 images) as an 2D array (lSDW).
%
% NOTE: all of these calculations are performed only in the given slice.
%
% Inputs:
%   V      - headers of the dMRI dataset 
%   VG     - reference header
%   bvals  - b values of the dMRI dataset
%   MSKidx - mask containing linear indices
%
% Outputs:
%   logS  - log(S) for all voxels within mask, for all volumes, 2D array (num_of_voxels x num_of_volumes)
%   logS0 - log(S0) for voxels in within mask, for b0 volumes, 2D array (num_of_voxels x num_of_volumes)
%   
% Created by S.Mohammadi (20/12/2013)
% Adapted by G.David
% =========================================================================

% type of interpolation at resampling
if ~exist('res','var')
    res = -7;
end

% get indices
b0 = min(bvals);
idx_b0 = find(bvals==b0);

% initialize
logS  = zeros(prod(VG.dim),size(V,1));
logS0 = zeros(prod(VG.dim),length(idx_b0));
S0    = zeros([VG.dim numel(idx_b0)]);

% interpolate all b0 images to the first b0 volume
for i=1:numel(idx_b0)
    S0(:,:,:,i) = acid_read_vols(V(idx_b0(i)),VG,res);
end

% calculate log(S0) and set voxels with S0=0 to 0
for i = 1:size(idx_b0,2)
    S0_tmp = S0(:,:,:,i);
    logS0(S0_tmp>0,i) = log(S0_tmp(S0_tmp>0));  
end

% calculate log(S) and set voxels with S0=0 to 0
for i = 1:size(V,1)
    S = acid_read_vols(V(i),VG,res);
    logS(S>0,i) = log(S(S>0));  
end

% mask output
logS = logS(MSKidx,:);
logS0 = logS0(MSKidx,:);

end