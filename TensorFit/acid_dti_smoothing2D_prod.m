function [sA] = acid_dti_smoothing2D_prod(A,kmax)
% ========================================================================
% The script smoothes the input matrix in the plane by convolving it with a
% kernel. The peak of the convolution kernel is specified in the input.
%
% Input:
%   A       - matrix to be smoothed
%   kmax    - peak of the convolution kernel
%
% Output:
%   sA      - smoothed matrix
%
% S.Mohammadi 08/02/2011
% ========================================================================

% initial kernel
k0 = [0 1 0; 1 kmax 1; 0 1 0]; % default k0  = [0 1 0; 1 32 1; 0 1 0];

% create a kernel by repetadly convolving the initial kernel with itself
% and normalizing it
k = k0;
for i = 1:5
    k = conv2(k,k0);
end
k = k/sum(k(:));

% smooth A in the plane
sA = zeros(size(A));
for z = 1:size(A,3)
    sA(:,:,z) = conv2(A(:,:,z),k','same');
end

end