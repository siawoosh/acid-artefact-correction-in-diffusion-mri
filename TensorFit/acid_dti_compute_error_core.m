function [ResError, ResErrorLog, Sfit, SfitLog] = acid_dti_compute_error_core(DT2d, V, VG, bvals, B, dm, p, dummy_type)

% =========================================================================
% This function calculates the residuals from the tensor.
% Two options are available for model fit-error estimation:
% dummy_type == 0 	Estimating model-fit error from the signal:
%                   i.e. error = log(S_meas) - log(S_fitted))
%                   This option is more sensitive to instrumental effects,
%                   because it directly depends on the measured signal.
% dummy_type == 1   Estimating model-fit error from the ADCs: 
%                   i.e. error = ADCmeas - ADCmodel, where
%                   ADCmeas(bvalue) = 1/b x (log(Smeas(bvalue))-log(Smeas(min(bvalue)))). 
%                   This option is more sensitive to perturbations that 
%                   will have a direct influence on your model estimates, 
%                   because the baseline signal (log(Smeas(min(bvalue)))) 
%                   is substracted taken from the measured signal.
%
% Input(s):
%   DT2d        - elements of the diffusion tensor for each voxel, 2D array [num_of_voxels x 7]
%   V           - headers of the dMRI volumes
%   VG          - header of the reference volume
%   bvals       - b values of the dMRI volumes
%   B           - B matrix (design matrix for dMRI experiment)
%   dm          - matrix size
%   p           - slice index
%   dummy_type  - see above
%   dummy_log   - option for writing out log or not log RMSE
%
% Output(s):
%   resError    - 2D array of non-logarithmic model-fit errors
%   resErrorLog - 2D array of logarithmic model-fit errors
% 
% Created by S.Mohammadi (2013)
% Adapted by G.David
% Adapted by B.Fricke
% =========================================================================

% extracting a single slice from DT
%   DT2d:  [num_of_voxels, 7]
%   DT_sl: [num_of_voxels(slice), 7]
DT_sl = zeros([dm(1:2) size(DT2d,2)]);
for i = 1:size(B,2)
    AMSKvol      = zeros(dm);
    AMSKvol(:)   = DT2d(:,i);
    DT_sl(:,:,i) = AMSKvol(:,:,p);
end
DT_sl = reshape(DT_sl,[],size(DT_sl,3));

% Extracting the measured signal from all volumes for a specific slice
[SmeasLog, S0measLog, Smeas] = acid_dti_get_logS_2D(V,VG,bvals,p);

% Initialization
inx = 1;
ResError    = zeros(size(Smeas));
ResErrorLog = zeros(size(SmeasLog));

% Calculating model-fit error
if dummy_type == 0
    
    % Estimating model-fit error as follows:
    %   error = log(Smeas) - log(Sfit), where
    %   log(Sfit) = B*Tensor;

    % Sfit_log: log(Sfit) for all voxels and volumes in given slice, [num_of_voxels(slice), num_of_volumes]
    SfitLog = (B*DT_sl')';
    Sfit    = exp(SfitLog);

    % Computing model-fit error (e)
    for j = 1:numel(V)
        ResErrorLog(:,j) = SmeasLog(:,j) - SfitLog(:,j);
        ResError(:,j) = Smeas(:,j) - Sfit(:,j);
    end

elseif dummy_type == 1 
    
    % Calculating lS0-lSWi from data
    SmeasLog = bsxfun(@minus,S0measLog,SmeasLog);

    % Calculating ADC from lS0-lSWi
    SmeasLog = bsxfun(@rdivide,SmeasLog,bvals);

    % Calculating lS0-lSWi from model
    lS0model = mean(B(bvals==min(bvals),:)*DT_sl',1);
    lS0model = permute(lS0model,[2 1]);
    lSmodel  = (B*DT_sl');
    lSmodel  = permute(lSmodel,[2 1]);
    SfitLog  = bsxfun(@minus,lS0model,lSmodel);
    
    % Calculating ADC from lS0-lSWi
    SfitLog = bsxfun(@rdivide,SfitLog,bvals);
    for j = 1:numel(V)
        ResErrorLog(:,inx) = SmeasLog(:,j) - SfitLog(:,j);
        inx = inx+1;
    end
end
end