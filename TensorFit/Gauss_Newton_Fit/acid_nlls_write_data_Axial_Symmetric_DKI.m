function   VOut = acid_nlls_write_data_Axial_Symmetric_DKI(DT2d, dummy_RBC, VG, MSK, dummy_dki_extra, ... 
    dummy_calculate_uncertainty, Asym_olsq_rice_uncertainty, p_out, dummy_unit_test)

MSK = logical(MSK);
dummy_float = 1;

if dummy_RBC
    keyword = 'DKIax-NLLS-RBC';
else
    keyword = 'DKIax-NLLS';
end

% Compute and write out DTI and DKI metrics
L1 = DT2d(:,1); % the first eigenvalue in the axial symmetric case is the first estimated parameter (D_parallel)
L2 = DT2d(:,2); % the second eigenvalue in the axial symmetric case is the second estimated parameter (D_perp), the third eigenvalue is equal to the second
MD = (L1+2.* L2) ./3;
FA = (3/2)^0.5 .* sqrt( ( (L1-MD) .^ 2 + 2 .* (L2-MD) .^ 2 ) ./ (L1.^2 + 2 .* L2 .^ 2 ) );
%         FA2 = Compute_FA_Axial_Symmetry(first_ev,second_ev,Asym_olsq_rice)';
%         FA =((0.5)^0.5.* sqrt((first_ev-second_ev).^2+(second_ev-first_ev).^2))./ sqrt(first_ev+2.*second_ev);

VOut = acid_write_vol(FA, VG, p_out, [keyword '-FA'], '_map', MSK, '', dummy_float);
acid_write_vol(MD, VG, p_out, [keyword '-MD'], '_map', MSK, '', dummy_float);
acid_write_vol(L1, VG, p_out, [keyword '-AD'], '_map', MSK, '', dummy_float);
acid_write_vol(L2, VG, p_out, [keyword '-RD'], '_map', MSK, '', dummy_float);
acid_write_vol(DT2d(:,6), VG, p_out, [keyword '-AW'], '_map', MSK, '', dummy_float);
acid_write_vol(DT2d(:,7), VG, p_out, [keyword '-RW'], '_map', MSK, '', dummy_float);
acid_write_vol(DT2d(:,8), VG, p_out, [keyword '-MW'], '_map', MSK, '', dummy_float);
acid_write_vol(exp(DT2d(:,3)), VG, p_out, [keyword '-S0'], '_map', MSK, '', dummy_float);

% Compute and write out FA outliers
MSK_FA_outliers = zeros(size(FA));
MSK_FA_outliers = logical(MSK_FA_outliers);
MSK_FA_outliers(FA>1) = true;
MSK_FA_outliers(FA<0) = true;
acid_write_vol(MSK_FA_outliers, VG, p_out, [keyword '-FA-outliers'], '', MSK, '', dummy_float);

% Compute and write out axial and radial kurtosis
if (dummy_dki_extra == 1) || dummy_unit_test
    Kpara = DT2d(:,6) .* (MD.^2) ./ L1.^2;
    Kperp = DT2d(:,7) .* (MD.^2) ./ L2.^2;
    acid_write_vol(Kpara, VG, p_out, [keyword '-AK'], '_map', MSK, '', dummy_float);
    acid_write_vol(Kperp, VG, p_out, [keyword '-RK'], '_map', MSK, '', dummy_float);
end

% Compute and write out axes of symmetries
theta_estimated = DT2d(:,4);
phi_estimated   = DT2d(:,5);
axis_of_symmetry_x = sin(theta_estimated) .* cos(phi_estimated); 
axis_of_symmetry_y = sin(theta_estimated).* sin(phi_estimated);
axis_of_symmetry_z = cos(theta_estimated);
aos = horzcat(axis_of_symmetry_x, axis_of_symmetry_y, axis_of_symmetry_z);

AOS  = zeros([VG.dim, 3]);
tmp1 = zeros(VG.dim);
tmp2 = zeros(VG.dim);
tmp3 = zeros(VG.dim);
tmp1(MSK) = aos(:,1);
tmp2(MSK) = aos(:,2);
tmp3(MSK) = aos(:,3);
AOS(:,:,:,1) = tmp1;
AOS(:,:,:,2) = tmp2;
AOS(:,:,:,3) = tmp3;

acid_write_vol(squeeze(AOS), VG, p_out, [keyword '-AOS'], '_map', '', '', dummy_float);

% Write out uncertainties
if dummy_calculate_uncertainty
    acid_write_vol(Asym_olsq_rice_uncertainty(:,1), VG, p_out, [keyword '-AD-unc'], '_map', '', '', dummy_float);
    acid_write_vol(Asym_olsq_rice_uncertainty(:,2), VG, p_out, [keyword '-RD-unc'], '_map', '', '', dummy_float);
    acid_write_vol(Asym_olsq_rice_uncertainty(:,6), VG, p_out, [keyword '-AW-unc'], '_map', '', '', dummy_float);
    acid_write_vol(Asym_olsq_rice_uncertainty(:,7), VG, p_out, [keyword '-RW-unc'], '_map', '', '', dummy_float);
    acid_write_vol(Asym_olsq_rice_uncertainty(:,8), VG, p_out, [keyword '-MW-unc'], '_map', '', '', dummy_float);
end
end