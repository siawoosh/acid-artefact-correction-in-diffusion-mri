function [nParameters] = acid_nlls_number_of_fit_parameters(dummy_model)

    if(dummy_model == 2)
        nParameters = 22;                                              %The DKI model has 22 parameters to be estimated
    elseif(dummy_model == 1 || dummy_model == 6)
        nParameters = 7;                                               %The DTI model has 7 parameters to be estimated
    elseif(dummy_model == 3)
        nParameters = 8;                                               % The axial symmetric DKI model has 8 parameters to be estimated
    elseif(dummy_model == 4)
        nParameters = 8;                                               % The biophysical model has 8 parameters to be estimated (7 biophysical and S0)
    elseif(dummy_model == 5)
        nParameters = 2;
    end

end

