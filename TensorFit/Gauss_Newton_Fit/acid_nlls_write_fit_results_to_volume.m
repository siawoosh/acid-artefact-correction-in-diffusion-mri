function [Asym_olsq_rice,Asym_olsq0,Asym_olsq00,mDWI_GN,Asym_olsq_rice_uncertainty] = acid_nlls_write_fit_results_to_volume(Asym_olsq_rice,Asym_olsq0,Asym_olsq00,sz,MSK_b,AMSK,dummy_model,volume_of_fit_results,volume_of_lS,volume_of_fit_uncertainties)

n_parameters = acid_nlls_number_of_fit_parameters(dummy_model);
Asym_olsq00_uncertainty = Asym_olsq00;

for inx_vol = 1:numel(volume_of_fit_results)
    if ~isempty(volume_of_fit_results{inx_vol})

        MSKvol          = zeros(sz);
        MSKvol(:,:,inx_vol)  = ones(sz(1:2));
        AMSKslice       = AMSK(:,:,inx_vol);

        tmp3 = volume_of_fit_results{inx_vol};
        lS   = volume_of_lS{inx_vol};
        tmp_uncertainty = volume_of_fit_uncertainties{inx_vol};

        for i = 1:n_parameters
            tmp4              = Asym_olsq00(:,:,inx_vol,i);
            tmp4(AMSKslice>0) = tmp3(i,:);
            Asym_olsq00(:,:,inx_vol,i) = tmp4;

            tmp_uncertainty_2 = Asym_olsq00_uncertainty(:,:,inx_vol,i);
            tmp_uncertainty_2(AMSKslice>0) = tmp_uncertainty(i,:);
            Asym_olsq00_uncertainty(:,:,inx_vol,i) = tmp_uncertainty_2;
        end

        tmp3 = zeros(sz);
        tmp3(MSKvol>0) = mean(exp(lS(:,MSK_b)),2);
        mDWI_GN(:,:,inx_vol) = tmp3(:,:,inx_vol);

        for i=1:n_parameters
            tmp3  = Asym_olsq00(:,:,:,i);
            Asym_olsq_rice(:,i) = tmp3(AMSK>0);

            tmp_uncertainty = Asym_olsq00_uncertainty(:,:,:,i);
            Asym_olsq_rice_uncertainty(:,i) = tmp_uncertainty(AMSK>0);
        end

    elseif isempty(volume_of_fit_results{inx_vol})

        AMSKslice = AMSK(:,:,inx_vol);
        mDWI_GN(:,:,inx_vol) = NaN(size(AMSKslice));

    end
end
end