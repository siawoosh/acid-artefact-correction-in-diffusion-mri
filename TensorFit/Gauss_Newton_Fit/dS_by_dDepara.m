function [derivative] = dS_by_dDepara(S0,K,f,Depara,Deperp,watson_factor,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param)
    derivative = S0 .* watson_factor(1,:) .* ( (1 -f(1,:)) .* ( ( exp (K(1,:) .* ( sinus_theta_param .* cosinus_phi_param .* sinus_theta_leb .* cosinus_phi_leb + sinus_theta_param .* sinus_phi_param .* sinus_theta_leb .* sinus_phi_leb +cosinus_theta_param   .* cosinus_theta_leb ) .^2 ) ) .* exp ( -b .* Deperp(1,:) - b .* ( Depara(1,:) - Deperp(1,:) ) .* g_dot_u_squared )    .*(-b.* g_dot_u_squared) ));
end

