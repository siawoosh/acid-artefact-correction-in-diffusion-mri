function [parameters] = acid_nlls_num_param(parameters,dummy_model)

    if (dummy_model == 2)
        parameters = reshape(parameters,22,[]); % number in the middle is 7 for DTI and 22 for DKI
    elseif(dummy_model == 1 || dummy_model == 6)
        parameters = reshape(parameters,7,[]); % number in the middle is 7 for DTI and 22 for DKI
    elseif(dummy_model == 3)
        parameters = reshape(parameters,8,[]); % number in the middle is 6 (26.08.2019: I rather think its 8(=number of parameters)) for axially symmetric DKI
    elseif(dummy_model == 4)
        parameters = reshape(parameters,8,[]); %
    elseif(dummy_model == 5)
        parameters = reshape(parameters,2,[]); %
    end

end