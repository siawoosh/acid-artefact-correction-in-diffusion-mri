function [pred]=signal_prediction_DKI_lin(parameters,design_kurtosis)

     traceterm_1 = ((parameters(1,:)+parameters(2,:)+parameters(3,:))/3); % for the trace term in the DKI signal equation
     traceterm = traceterm_1.^2;
%      traceterm = 1;
     pred = ( design_kurtosis(:,1:6) * parameters(1:6,:) +  traceterm.* (design_kurtosis(:,8:22) * parameters(8:22,:) ) ) + parameters(7,:) ; % DKI-MODEL 

     

end