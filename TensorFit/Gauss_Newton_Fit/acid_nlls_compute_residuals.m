function [RMSE, RMSE_shell_vol_out, Error_4d, Sfit_4d, MAE, MeAE] = acid_nlls_compute_residuals(parameters,design_kurtosis,DM0,dummy_model,measured_signal,bvalues,bvecs,dm,MSKslice,datascale,dummy_error_per_shell)

RMSE = zeros(dm(1),dm(2));
MAE = zeros(dm(1),dm(2));
MeAE = zeros(dm(1),dm(2));
Error_4d = zeros([dm(1), dm(2), 1, size(design_kurtosis,1)]);
Sfit_4d  = zeros([dm(1), dm(2), 1, size(design_kurtosis,1)]);

[bvals_unique,~,~] = unique(bvalues','rows');
RMSE_shell_vol_out = zeros(dm(1),dm(2),numel(bvals_unique));

if(or(dummy_model ==1, dummy_model ==2))
    parameters(7,:) = exp(parameters(7,:));  % The data writing algorithms expects the log of the measured signal ln(S0), so for the non linear case where S0 is estimated the found values need to be converted.
end

if(dummy_model == 3)
    parameters(3,:) = exp(parameters(3,:));
end

if(dummy_model == 2)
    pred = acid_nlls_signal_prediction_DKI_non_lin(parameters,design_kurtosis);
elseif(dummy_model == 1)
    pred = acid_nlls_signal_prediction_DTI_non_lin(parameters,DM0);
elseif(dummy_model == 3)
    exp_D = acid_nlls_exponent_D(bvalues,bvecs,parameters)';                   % Hansen et al., formula 21
    exp_W = acid_nlls_exponent_W(bvalues,bvecs,parameters)';                   % Hansen et al., formula 20
    % This is the signal prediction generation adapted from the script from Jespersen et al.
    pred  = exp (-1.* exp_D + exp_W) .* parameters(3,:);
end

residuals = (datascale .* measured_signal(MSKslice>0,:)' - pred);


RMSE_all = sqrt(mean(residuals.^2,1));
RMSE(logical(MSKslice)) = RMSE_all;

MAE_all = mean(abs(residuals),1);
MAE(logical(MSKslice)) = MAE_all;

MeAE_all = median(abs(residuals),1);
MeAE(logical(MSKslice)) = MeAE_all;

if dummy_error_per_shell == 1

    for n = 1:numel(bvals_unique)
        b_msk = bvalues==bvals_unique(n);
        RMSE_shell = sqrt(mean(residuals(b_msk,:).^2)); 
        RMSE_shell_vol = zeros(dm(1),dm(2));
        RMSE_shell_vol(logical(MSKslice)) = RMSE_shell;
        RMSE_shell_vol_out(:,:,n) = RMSE_shell_vol;
    end
end


for inxGradient = 1:size(residuals,1)
    Error_slice = zeros(dm(1),dm(2));
    Error_slice(logical(MSKslice)) = residuals(inxGradient,:);
    Error_4d(:,:,1,inxGradient) = Error_slice;

    Sfit_slice = zeros(dm(1),dm(2));
    Sfit_slice(logical(MSKslice)) = pred(inxGradient,:);
    Sfit_4d(:,:,1,inxGradient) = Sfit_slice;
end

end