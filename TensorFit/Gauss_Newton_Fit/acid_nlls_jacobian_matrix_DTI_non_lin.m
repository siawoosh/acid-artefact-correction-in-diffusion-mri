function [jacobi_i]=acid_nlls_jacobian_matrix_DTI_non_lin(parameters,DM0,pred,S0)
%%%%%%%%%%%%%%%%%%%%%%%%%---> NON LINEAR <---%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DKI-Jacobian-Matrix-Composition
jacobi_i=cell(1,size(parameters,2));
for i=1:size(parameters,2) 
    jacobi_i{i}=sparse(pred(:,i).*DM0); % das hier ist die Jacobi-Matrix(für alle 135 Messungen) für den i-ten Pixel
    jacobi_i{i}(:,7)=jacobi_i{i}(:,7).*(1/S0(i));
end

end