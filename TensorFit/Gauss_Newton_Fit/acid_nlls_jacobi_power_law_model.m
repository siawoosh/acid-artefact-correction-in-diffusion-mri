function [jacobi_matrix] = acid_nlls_jacobi_power_law_model(bvalues, bvalue_labels, parameters, measured_signal)

alpha = 1/2;
MSK_bvalues = sort_b_values(bvalues,bvalue_labels);
S0 = mean(measured_signal(MSK_bvalues{1}));

for k=1:size(parameters,2)
    for i = 1:size(bvalue_labels,2)-1
        jacobi(i,1) = S0 .* (bvalue_labels(i+1).^(-alpha))  ; % first index i in jacobi(i,j) indexes the number of b-values (here specified explicitly)
        jacobi(i,2) = S0 .* 1 ;
    end
    jacobi_matrix{k}=sparse(jacobi);
end
end