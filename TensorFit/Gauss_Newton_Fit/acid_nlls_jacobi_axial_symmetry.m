function [jacobi_matrix] = acid_nlls_jacobi_axial_symmetry( exp_W, pred, bvalues, DiffVecORIG,parameters)

jacobi_matrix=cell(1,size(parameters,2));

Dpara=parameters(1,:);
Dperp=parameters(2,:);
S0=parameters(3,:);
theta=parameters(4,:);
phi=parameters(5,:);
Wpara=parameters(6,:);
Wperp=parameters(7,:);
Wmean=parameters(8,:);

jacobi_matrix_temp=zeros(size(bvalues,2),size(parameters,1));

for k = 1:size(parameters,2)
    u=[ sin(theta(k)).*cos(phi(k)), sin(theta(k)).*sin(phi(k)),cos(theta(k))]';

    traceterm_1 = ((Dpara(k)+ 2 * Dperp(k))/3); %for the trace term in the DKI signal equation
    traceterm = traceterm_1.^2;

    for i=1:numel(bvalues)

        b_matrices= bvalues(i).* [DiffVecORIG(1,i).* DiffVecORIG(1,i),DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(1,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(3,i),DiffVecORIG(2,i).*DiffVecORIG(3,i),DiffVecORIG(3,i).*DiffVecORIG(3,i)];

        tr_bmatrices = trace(b_matrices);
        mat_exp_W    = exp_W(i,k);

        %see "b_matrix_tensor_product.m" for how the derivatives are computed

        u_b_u_theta=acid_nlls_ubu_theta(DiffVecORIG(:,i), theta(k),phi(k)); %derivative of u'bu by theta
        u_bb_u_theta=acid_nlls_ubbu_theta(DiffVecORIG(:,i), theta(k),phi(k)); %derivative of u'bbu by theta
        u_b_u_phi=acid_nlls_ubu_phi(DiffVecORIG(:,i), theta(k),phi(k)); %derivative of u'bu by phi
        u_bb_u_phi=acid_nlls_ubbu_phi(DiffVecORIG(:,i), theta(k),phi(k)); %derivative of u'bbu by phi

        jacobi_matrix_temp(i,1)= pred(i,k) .* (-(u'*b_matrices*u) + (2/3).* (1/traceterm_1) .* mat_exp_W); % its 1/tracterm_1 in the second summand because Dmean/Dmean^2=1/Dmean=1/traceterm_1
        jacobi_matrix_temp(i,2)= pred(i,k) .* ( -tr_bmatrices + (u'*b_matrices*u) + (4/3) .* (1/traceterm_1) .* mat_exp_W );
        jacobi_matrix_temp(i,3)= pred(i,k) ./ S0(k);
        jacobi_matrix_temp(i,4)= pred(i,k) .* ( -(Dpara(k)-Dperp(k))*(u_b_u_theta)+ (1/6)*traceterm*(0.5*(10*Wperp(k)+5*Wpara(k)-15*Wmean(k))*(2*(u'*b_matrices*u)*(u_b_u_theta))+0.5*(5*Wmean(k)-Wpara(k)-4*Wperp(k))*(u_b_u_theta*tr_bmatrices+2*u_bb_u_theta)));
        jacobi_matrix_temp(i,5)= pred(i,k) .* ( -(Dpara(k)-Dperp(k))*(u_b_u_phi)+ (1/6)*traceterm*(0.5*(10*Wperp(k)+5*Wpara(k)-15*Wmean(k))*(2*(u'*b_matrices*u)*(u_b_u_phi))+0.5*(5*Wmean(k)-Wpara(k)-4*Wperp(k))*(u_b_u_phi*tr_bmatrices+2*u_bb_u_phi)));
        jacobi_matrix_temp(i,6)= pred(i,k) .* (1/6)*traceterm*(0.5* ((u'*b_matrices*u)^2)*5-0.5*(u'*b_matrices*u*tr_bmatrices+2*(u'*b_matrices*b_matrices*u)));
        jacobi_matrix_temp(i,7)= pred(i,k) .* (1/6)*traceterm*(0.5* ((u'*b_matrices*u)^2)*10-2*(u'*b_matrices*u*tr_bmatrices+2*(u'*b_matrices*b_matrices*u))+(1/3)*(tr_bmatrices^2+2*trace_b_b(b_matrices))); %trace_b_b ist eine Funktion, die die Spur des Rang 4 Tensors (b-Matrix Tensorprodukt b-Matrix) ausrechnet.
        jacobi_matrix_temp(i,8)= pred(i,k) .* (1/6)*traceterm*(0.5* ((u'*b_matrices*u)^2)*(-15)+(5/2)*(u'*b_matrices*u*tr_bmatrices+2*(u'*b_matrices*b_matrices*u)));
    end
    jacobi_matrix{k}=sparse(jacobi_matrix_temp);
end

end