function [pred,exp_D,exp_W] = acid_nlls_signal_prediction(dummy_model,parameters,design_kurtosis,DM0,bvalues,bvalue_labels,DiffVecORIG,lebedev_quadrature_list,watson_factor,measured_signal,beta_map_slice,credit)

exp_D = cell(size(design_kurtosis,1),size(parameters,2));
exp_W = cell(size(design_kurtosis,1),size(parameters,2));

if(dummy_model == 2)
    pred = acid_nlls_signal_prediction_DKI_non_lin(parameters,design_kurtosis);
elseif(dummy_model == 1)

    pred = acid_nlls_signal_prediction_DTI_non_lin(parameters,DM0);

elseif (dummy_model == 3)

    exp_D = acid_nlls_exponent_D(bvalues,DiffVecORIG,parameters)';                   %Hansen et al. Formel 21
    exp_W = acid_nlls_exponent_W(bvalues,DiffVecORIG,parameters)';                   %Hansen et al. Formel 20
    %      pred  = total_signal(bvalues,DiffVecORIG,parameters);                %this is the signal prediction generation adapted from the script from jespersen
    pred  = exp (-1.* exp_D + exp_W) .* parameters(3,:);   %hier wird das Signal entsprechend Hansen et al. "Fast imaging of mean, axial and radial diffusion kurtosis" Formel 20 und 21 berechnet.

elseif(dummy_model == 4)
    %parameters(1,:) = S0
    %parameters(2,:) = f
    %parameters(3,:) = Da
    %parameters(4,:) = kappa
    %parameters(5,:) = De_parallel
    %parameters(6,:) = De_perpendicular
    %parameters(7,:) = Theta
    %parameters(8,:) = Phi

    pred  = acid_nlls_integrate_numerically_lebedev(DiffVecORIG', bvalues', parameters,lebedev_quadrature_list,watson_factor);

elseif(dummy_model == 5)
    %parameters(1,:) = beta
    %parameters(2,:) = gamma
    pred  = acid_nlls_power_law_dMRI_signal(bvalues, bvalue_labels, parameters,measured_signal,credit);
elseif(dummy_model == 6)
    %          S0 = parameters(1,:);
    %          f = parameters(2,:);
    %          K = parameters(3,:);
    %          Depara = parameters(4,:);
    %          Deperp = parameters(5,:);
    %          theta_param = parameters(6,:); % this is the theta in the axis of symmetry
    %          phi_param = parameters(7,:);  % this is the phi in the axis of symmetry
    pred  = integrate_numerically_lebedev_constrained_biophysical_model(DiffVecORIG', bvalues', parameters,lebedev_quadrature_list,watson_factor,beta_map_slice);
end
end