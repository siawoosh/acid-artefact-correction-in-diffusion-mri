function [jacobi_matrix] = acid_nlls_jacobi_biophysical_model(pred, bvalues, diffusion_gradients,parameters,lebedev_quadrature_list,kummer_derivative,watson_factor)

jacobi_matrix=cell(1,size(parameters,2));

S0 = parameters(1,:);
f = parameters(2,:);
Da = parameters(3,:);
K = parameters(4,:);
Depara = parameters(5,:);
Deperp = parameters(6,:);
theta_param = parameters(7,:); % this is the theta in the axis of symmetry
phi_param = parameters(8,:); % this is the phi in the axis of symmetry

phi_leb = lebedev_quadrature_list(:,1);  %Grid for integration of the sphere

theta_leb = lebedev_quadrature_list(:,2); %Grid for integration of the sphere

weight = lebedev_quadrature_list(:,3);

watson_factor_signal = watson_factor_numeric(K,watson_factor);
kummer_derive = kummer_numerical_derivative(K,kummer_derivative);% numerical derivative of Kummer's Function M(1/2),3/2,Kappa) by Kappa

g1 = diffusion_gradients(:,1);
g2 = diffusion_gradients(:,2);
g3 = diffusion_gradients(:,3);

b = bvalues ;

summand_2 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_3 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_4 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_5 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_6 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_7 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));
summand_8 = zeros(size(g1,1), size(parameters,2),numel (theta_leb(:,1)));

sinus_theta_param = sind(theta_param(1,:));
cosinus_phi_param = cosd(phi_param(1,:));
cosinus_theta_param = cosd(theta_param(1,:));
sinus_phi_param = sind(phi_param(1,:));

for  inx = 1: numel (theta_leb(:,1))

    sinus_theta_leb = sind(theta_leb(inx));
    cosinus_phi_leb = cosd(phi_leb(inx));
    sinus_phi_leb = sind(phi_leb(inx));
    cosinus_theta_leb = cosd(theta_leb(inx));

    g_dot_u_squared = (g1 .* sinus_theta_leb .* cosinus_phi_leb + g2 .* sinus_theta_leb .* sinus_phi_leb + g3 .* cosinus_theta_leb ) .^2;

    summand_2(:, :, inx) = weight(inx)*dS_by_df(S0,Da,K,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_3(:, :, inx) = weight(inx)*dS_by_dDa(g1,g2,g3,S0,Da,K,f,Depara,Deperp,watson_factor_signal,b,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_6(:, :, inx) = weight(inx)*dS_by_dDeperp(S0,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_5(:, :, inx) = weight(inx)*dS_by_dDepara(S0,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_7(:, :, inx) = weight(inx)*dS_by_dTheta_c(S0,Da,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_8(:, :, inx) = weight(inx)*dS_by_dPhi_c(S0,Da,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param);
    summand_4(:, :, inx) = weight(inx)*dS_by_dKappa(S0,Da,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param,kummer_derive);

end

lebedev_sum_solution_2 = 4 .* pi .* sum (summand_2,3);
lebedev_sum_solution_3 = 4 .* pi .* sum (summand_3,3);
lebedev_sum_solution_6 = 4 .* pi .* sum (summand_6,3);
lebedev_sum_solution_5 = 4 .* pi .* sum (summand_5,3);
lebedev_sum_solution_7 = 4 .* pi .* sum (summand_7,3);
lebedev_sum_solution_8 = 4 .* pi .* sum (summand_8,3);
lebedev_sum_solution_4 = 4 .* pi .* sum (summand_4,3);

jacobi_matrix_temp=zeros(size(bvalues,1),size(parameters,1));

for k = 1:size(parameters,2)

    jacobi_matrix_temp(:,1)= pred(:,k)./S0(1,k);
    jacobi_matrix_temp(:,2)= lebedev_sum_solution_2(:,k);
    jacobi_matrix_temp(:,3)= lebedev_sum_solution_3(:,k);
    jacobi_matrix_temp(:,4)= lebedev_sum_solution_4(:,k);
    jacobi_matrix_temp(:,5)= lebedev_sum_solution_5(:,k);
    jacobi_matrix_temp(:,6)= lebedev_sum_solution_6(:,k);
    jacobi_matrix_temp(:,7)= lebedev_sum_solution_7(:,k);
    jacobi_matrix_temp(:,8)= lebedev_sum_solution_8(:,k);

    jacobi_matrix{k}=sparse(jacobi_matrix_temp);
end
end