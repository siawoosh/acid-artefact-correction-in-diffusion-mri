function   write_data_power_law(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK,dummy_bids,path_nlls)

%parameters(1,:) = beta

ending = '';


if dummy_bids == 1
    if(dummy_RBC == 0)
        midfix = '';
        
        
        prefix = ['Power-law-RBC-OFF-beta-GN_' midfix];
        my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending,dummy_bids,path_nlls);
        
        prefix = ['Power-law-RBC-OFF-gamma-GN_' midfix];
        my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending,dummy_bids,path_nlls);
        
    elseif(dummy_RBC == 1)
        
        midfix = '';
        
        prefix = ['Power-law-RBC-ON-beta-GN_' midfix];
        my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending,dummy_bids,path_nlls);
        
        prefix = ['Power-law-RBC-ON-gamma-GN_' midfix];
        my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending,dummy_bids,path_nlls);
        
    end
else
    if(dummy_RBC == 0)
        midfix = '';
        
        
        prefix = ['Power_law_RBC_OFF_beta_GN_' midfix];
        my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
        
        prefix = ['Power_law_RBC_OFF_gamma_GN_' midfix];
        my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending);
        
    elseif(dummy_RBC == 1)
        
        midfix = '';
        
        prefix = ['Power_law_RBC_ON_beta_GN_' midfix];
        my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
        
        prefix = ['Power_law_RBC_ON_gamma_GN_' midfix];
        my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending);
    end
end



end

