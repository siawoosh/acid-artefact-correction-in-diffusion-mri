function [Signal] = acid_nlls_BP_Constrained_Signal_Equation(S0,K,f,Depara,Deperp,watson_factor,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param,beta_map_slice)

    Da = ((pi/4).*f.^2)./(beta_map_slice.^2);
    Signal = S0 .* watson_factor(1,:) .* ( exp (K(1,:) .* ( sinus_theta_param .* cosinus_phi_param .* sinus_theta_leb .* cosinus_phi_leb + sinus_theta_param .* sinus_phi_param  .* sinus_theta_leb .* sinus_phi_leb + cosinus_theta_param   .* cosinus_theta_leb ) .^2 ) ) .* (f(1,:).* (  exp ( -b.* Da(1,:) .* g_dot_u_squared ) ) + (1 -f(1,:)) .* (  exp ( -b .* Deperp(1,:) - b .* ( Depara(1,:) - Deperp(1,:) ) .* g_dot_u_squared ) ));

end