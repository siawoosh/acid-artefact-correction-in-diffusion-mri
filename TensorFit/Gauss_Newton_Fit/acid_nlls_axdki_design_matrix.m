function [design_kurtosis_axial_symmetry]  = acid_nlls_axdki_design_matrix(V,bvalue)

    % design matrix diffusion tensor
    X2(1,:)  = V(1,:).^2;
    X2(2,:)  = V(2,:).^2 + V(3,:).^2;
    X2(3,:)  = 1;
    X2(4,:)  = 2.*V(1,:).*V(2,:) + 2.* V(3,:).*V(1,:);
    % design matrix kurtosis tensor
    % X4(1,:)  = V(1,:).^4 - 1.5 .* V(3,:).^2 .* V(1,:).^2 - 1.5 .* V(1,:).^2 .* V(2,:).^2;
    % X4(2,:)  = V(3,:).^4 + V(2,:) .^4 + 2 .* V(3,:).^2 .* V(2,:).^2 - 4 .* V(3,:).^2 .* V(1,:).^2 - 4 .* V(1,:).^2 .* V(2,:).^2 ;
    % X4(3,:)  = 7.5 .* V(3,:).^2 .* V(1,:).^2 + 7.5 .* V(1,:).^2 .* V(2,:).^2;

    szb = size(bvalue);
    if(szb(1)==1)
        XX2     = bsxfun(@times,X2, bvalue);
        XX2(3,:) = -1; % this entry is corresponding to the ln(S0) part in the parameter vector
        XX      = cat(1,-XX2);%,XX4);
    else
        error('This version cannot handle b-values other than of the form: 1xN')
    end

    design_kurtosis_axial_symmetry = XX';

end