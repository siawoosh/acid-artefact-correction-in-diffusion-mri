function [pred]=acid_nlls_signal_prediction_DTI_non_lin(parameters,DM0)

pred =( exp( DM0(:,1:6) * parameters(1:6,:) ) ) .* parameters(7,:) ; % DTI-MODEL

end