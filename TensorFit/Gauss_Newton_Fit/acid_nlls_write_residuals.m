function acid_nlls_write_residuals(Sfit, Error_4d, RMSE, MAE, MeAE, RMSE_shell_vol_out, VG, p_out, keyword, bvals, ...
    dummy_write_fitted, dummy_error_per_shell, dummy_write_error, dummy_plot_error, MSKidx, V)

% Writing out RMS model-fit error
acid_write_vol(RMSE, VG, p_out, [keyword '-RMSE'], '_map', '', '', 1);

% Optional: Writing out MAE and MeAE maps, and 4D model-fit error map
if dummy_write_error
    acid_write_vol(MAE, VG, p_out, [keyword '-MAE'], '_map', '', '', 1);
    acid_write_vol(MeAE, VG, p_out, [keyword '-MeAE'], '_map', '', '', 1);
    acid_write_vol(Error_4d, VG, p_out, [keyword '-Err'], '_map', '', '', 1);
end

% Optional: Writing out fitted signal
if dummy_write_fitted
    acid_write_vol(Sfit, VG, p_out, [keyword '-SFit'], '_map', '', '', 1);
end

% Optional: Writing out model-fit error per shell
if dummy_error_per_shell == 1
        [bvals_unique,~,~] = unique(bvals','rows');
        RMSE_shell_vol_out = permute(RMSE_shell_vol_out,[1,2,4,3]);
    for n = 1:numel(bvals_unique)
        acid_write_vol(RMSE_shell_vol_out(:,:,:,n), VG, p_out, [keyword '-b' num2str(bvals_unique(n)) '-RMSE'], '_map', '', '', 1);
    end
end

% Optional: Creating diagnostic plots on model-fit error
if dummy_plot_error
    acid_dti_plot_error(Error_4d, MSKidx, p_out, V, [keyword '-diagnostic_plot'])
end

end