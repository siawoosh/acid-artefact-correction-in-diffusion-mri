function [Dc,para,dD,H,Hstar] = acid_nlls_ObjectiveFunction(parameters,measured_signal,DM0,credit,datascale,design_kurtosis,sigma,dummy_model,dummy_RBC,DiffVecORIG,bvals,bvalue_labels,n_coils,use_noise_map,sigma_map_slice,beta_map_slice,biasParams)
%
% Input:
%
% parameters - this is an array which contains in its columns the model's
% parameters which are to be found (here, for example, it would be the 6 diffusion
% tensor's parameters and b=0 weighted signal strength for DTI)
%
% measured_signal - this is a "number of active voxels" x "number of
% acquired diffusion weighted images" array containting the
% measured_signals
%
% credit - specifies indices of the voxels that are being worked on actively
%
% biasParams=[sigma,L] these are the parameters being used for the Rician
% noise correction, sigma is the noise level (= standard deviation) L is
% the number of receiver coil systems (2 coils in each system) used in the signal acquisition

if (dummy_model == 4 || dummy_model == 5 || dummy_model == 6)
    if ~exist('lebedev_quadrature_list','var')
        load('lebedev_quadrature_list.txt'); % values taken from http://people.sc.fsu.edu/~jburkardt/datasets/sphere_lebedev_rule/lebedev_059.txt
        load('kummer_derivative.mat') %contains the numerical derivate of Kummer's function
        load('watson_factor.mat') %contains the pre calculated values for ( 4 .* pi .* hypergeom( (1/2) , (3/2) , K(inx) ) )^-1 where hypergeom is Kummer's function
    end
end

if  isempty(credit)
    credit = 1:size(measured_signal,1);
end

persistent mux;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if dummy_RBC==1
    if use_noise_map == 0
        biasParams = [sigma/datascale n_coils];                            %only set this variable if Rician bias correction is being done
    elseif use_noise_map == 1
        sigma_map_slice  = repmat(sigma_map_slice,1,size(measured_signal,2));
        biasParams = [sigma_map_slice/datascale];
        biasParams = biasParams(credit,:);
    end
end

% ndata      = size(measured_signal,2);                   % this is the number of data points for every voxel
parameters = acid_nlls_num_param(parameters,dummy_model); % the variable "parameters" is adapted to either the DKI case (22 parameters), the DTI case (7 parameters) or the axial symmetric DKI case (8 parameters)
parameters = parameters(:,credit);

nvoxel = size(parameters,2);                                            %this is the number of active voxels
data   = acid_nlls_data_to_be_fitted(credit,measured_signal,dummy_model, bvals, bvalue_labels); %"data" is a variable that contains the (measured) data to be fitted by the algorith, credit specifies those datapoints/voxels that are actively being worked on

%"data" has the form : "number-of-gradient-directions"x"number-of-voxels"
%and "parameters" has the form: "number-of-parameters"x"number-of-voxles"

if(dummy_model ==3)
    S0 = (parameters(3,:)');
elseif(dummy_model == 4 || dummy_model ==6)
    S0 = (parameters(1,:)');
elseif(dummy_model == 5)
    %%Do Nothing
else
    S0 = (parameters(7,:)');                                                %this is the non diffusion weighted signal, its the same for both DTI and DKI
end

doDerivative = (nargout>2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here the measured data are being modeled, "pred" is a prediciton based
% upon the model's parameters. The model's parameters are iteratively being
% changed in order to make the model fit the measured signals better and
% better. "pred" must have the same size and structure as "data"(=that which is being modeled).
if (dummy_model == 1 || dummy_model == 2 || dummy_model == 3)
    [pred,~,exp_W] = acid_nlls_signal_prediction(dummy_model,parameters,design_kurtosis,DM0,bvals,bvalue_labels,DiffVecORIG,[],[],measured_signal,beta_map_slice,credit);
elseif(dummy_model == 4 || dummy_model == 5 || dummy_model == 6)
    [pred,~,exp_W] = acid_nlls_signal_prediction(dummy_model,parameters,design_kurtosis,DM0,bvals,bvalue_labels,DiffVecORIG,lebedev_quadrature_list,watson_factor,measured_signal,beta_map_slice,credit);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Here the Rician noise correction is being done and the prediction "pred" is being refined as "corrpred"
if dummy_RBC == 1

    if isempty(mux)
        try
            load('muFunct','-mat','mu');                                       %muFunct.mat is a data set, containing L (1.column), Zeta (2.column) and the corresponding Rician expectation value (3.column)
            mux = reshape(mu(:,3),5001,8);
        catch
            muf=load('muFunct');
            mux = reshape(muf(:,3),5001,8);
        end
    end

    if use_noise_map == 0
        [corrpred,dcorrpred] = acid_nlls_linearInter1D(mux(:,biasParams(2)),[0.0,50],pred./biasParams(1),'matrixFree',1);
    elseif use_noise_map == 1
        [corrpred,dcorrpred] = acid_nlls_linearInter1D(mux(:,n_coils),[0.0,50],pred./biasParams','matrixFree',1);
    end

    corrpred = reshape(corrpred,size(pred)).*biasParams(1);
    pred     = corrpred;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
res  = -1*(data-pred);                                                      %these are the residuals, the -1 here is important for the search direction (=dD)
Dcs  = sum(res.^2,1);                                                       %first sum of residuals
Dc   = 0.5*sum(Dcs);                                                        %this is the least squares sum which is to be minimized

para = struct('omega',[],'m',[],'nvoxel',nvoxel,'Dc',Dc,'Dcs',Dcs,'Rc',0.0,'res',res);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not(doDerivative)
    dD = [];
    H  = [];
else

    if or(dummy_model == 1, dummy_model == 2)
        jacobi_i      = acid_nlls_jacobi_func(dummy_model,parameters,design_kurtosis,pred,S0,DM0); % here the Jacobians for each individual voxel are being computed for the different cases (DTI/DKI, non-linear/linear)
        jacobi_matrix = blkdiag(jacobi_i{1,:});                             %here the Jacobian for the whol image slice (each individual voxel) is being constructed by putting
    elseif(dummy_model == 3)
        jacobi        = acid_nlls_jacobi_axial_symmetry(exp_W, pred,bvals,DiffVecORIG,parameters); % hier wird die Jacobi-Matrix f??r das axialsymmetrische Model berechnet.
        jacobi_matrix = blkdiag(jacobi{1,:});
    elseif(dummy_model == 4)
        jacobi        = acid_nlls_jacobi_biophysical_model(pred, bvals', DiffVecORIG',parameters,lebedev_quadrature_list,kummer_derivative,watson_factor);
        jacobi_matrix = blkdiag(jacobi{1,:});
    elseif(dummy_model == 5)
        jacobi        = acid_nlls_jacobi_power_law_model(bvals, bvalue_labels, parameters, measured_signal);
        jacobi_matrix = blkdiag(jacobi{1,:});
    elseif(dummy_model == 6)
        jacobi        = acid_nlls_jacobi_constrained_biophysical_model(pred, bvals', DiffVecORIG',parameters,lebedev_quadrature_list,kummer_derivative,watson_factor,beta_map_slice);
        jacobi_matrix = blkdiag(jacobi{1,:});
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if(or(dummy_model == 1, dummy_model == 2 ))
        if(dummy_RBC == 1)
            dD                      = jacobi_matrix'*(res(:).*dcorrpred);       %With Rician correction
        elseif(dummy_RBC == 0)
            dD                      = jacobi_matrix'*(res(:));
        end
    end

    if( dummy_model == 3 || dummy_model == 4 || dummy_model == 5 || dummy_model == 6)
        if(dummy_RBC == 0)
            dD                      = jacobi_matrix'*(res(:));
            para.dDs                = reshape(dD,size(parameters,1),[]);
        elseif(dummy_RBC == 1)
            dD                      = jacobi_matrix'*(res(:).*dcorrpred);
            para.dDs                = reshape(dD,size(parameters,1),[]);
        end
    end

    if(dummy_model == 1)
        para.dDs = reshape(dD,size(DM0,2),[]);               %DTI-Model
    elseif(dummy_model == 2)
        para.dDs = reshape(dD,size(design_kurtosis,2),[]);   %DKI-Model
    end

    % This is the Hessian-Matrix which is used for finding the search direction (= in what direction in the parameter space should the algorithm vary the parameters
    % in order to minimize the residuals?. Here the approximation J'J for the Hessian-Matrix is used.
    Hstar = jacobi_matrix'*jacobi_matrix;

    if(dummy_model == 2)
        H  = Hstar + 1e-4*speye(size(parameters,2)*size(design_kurtosis,2));   %DKI-Model
    elseif(dummy_model == 1)
        H  = Hstar + 1e-4*speye(size(parameters,2)*size(DM0,2));               %DTI-Model
    elseif(dummy_model == 3 || dummy_model == 4 || dummy_model == 5 || dummy_model == 6)
        H  = Hstar + 1e-5*speye(size(parameters,2)*size(parameters,1));        %Axial Symmetric DKI-Model and biophysical models
    end
end
end