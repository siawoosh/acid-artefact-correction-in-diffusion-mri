function [K_para, K_perp, MK, D_hat, K_hat, W_mean, MSK2] = acid_nlls_make_kurtosis_27_02_19(Asym, Dthr, bvals, bvecs, dummy_algo)
% S.Mohammadi 21.10.2012
% Note that this code uses the elliptic integrals written by Thomas Hoffend
% (see https://de.mathworks.com/matlabcentral/fileexchange/3705-elliptic-integrals-zip/content/Elliptic_Integrals/rd.m)

Dav = mean(Asym(:,1:3),2);
MSK2 = find(Dav>Dthr);
DK = define_Kurtosistensor(Asym,MSK2);

if dummy_algo == 3 || dummy_algo == 4
    DK = permute(DK,[5 1 2 3 4]);
else
    DK = bsxfun(@ldivide,(Dav(MSK2)).^2,permute(DK,[5 1 2 3 4]));
    % this needs to be done if the MD^2*W_ijkl are estimated, but I estimate W_ijkl.
end

[FA,EVEC,EVAL,~] = acid_dti_manage_eigenvalues_GN(Asym(MSK2,:),MSK2);

W_hat = make_W_hat(EVEC,DK);
D_hat = write_D_hat(W_hat);

% % solution 1
% MSK1 = find(EVAL(:,1)<=Dthr);EVAL(MSK1,1)=Dthr;
% MSK22 = find(EVAL(:,1)<=Dthr | EVAL(:,2)<=Dthr);EVAL(MSK22,2)=Dthr*0.7;
% MSK3 = find(EVAL(:,1)<=Dthr | EVAL(:,2)<=Dthr | EVAL(:,3)<=Dthr);EVAL(MSK3,3)=Dthr*0.3;

% define
errtol = 1e-2;
[F11,F21] = make_F1F2(EVAL,errtol);
[F12,F22] = make_F1F2(cat(2,EVAL(:,2),EVAL(:,1),EVAL(:,3)),errtol);
[F13,F23] = make_F1F2(cat(2,EVAL(:,3),EVAL(:,2),EVAL(:,1)),errtol);

MSK = find(abs(F11)<Inf & abs(F12)<Inf & abs(F13)<Inf & abs(F21)<Inf & abs(F22)<Inf & abs(F23)<Inf & EVAL(:,1)>0 & EVAL(:,2)>0 & EVAL(:,3)>0);
MK = zeros(size(FA));
MK(MSK) = F11(MSK).*W_hat(MSK,1,1,1,1) + F21(MSK).*W_hat(MSK,2,2,3,3)...
    + F12(MSK).*W_hat(MSK,2,2,2,2) + F22(MSK).*W_hat(MSK,1,1,3,3)...
    + F13(MSK).*W_hat(MSK,3,3,3,3) + F23(MSK).*W_hat(MSK,1,1,2,2);

% W_mean
% Hansens's definition in Hansen et al. 2013 MRM 
W_mean = zeros(size(FA));
MSK11 = find(EVAL(:,1)>=Dthr);
W_mean(MSK11) = 1/5.*(W_hat(MSK11,1,1,1,1) + 2*W_hat(MSK11,2,2,3,3)...
    + W_hat(MSK11,2,2,2,2) + 2*W_hat(MSK11,1,1,3,3)...
    + W_hat(MSK11,3,3,3,3) + 2*W_hat(MSK11,1,1,2,2));

% K_para
% Hui's definition in Hui et al. 2008 
% Kparallel(MSK11)   = (EVAL(MSK11,1)+EVAL(MSK11,2)+EVAL(MSK11,3)).^2./(9*EVAL(MSK11,1).^2).*What(MSK11,3,3,3,3);
% Hansens's definition in Hansen et al. 2017 Neuroimage, except for using
% 1st EV instead of 3rd
K_para   = zeros(size(EVAL(:,1)));
K_para(MSK11) = (EVAL(MSK11,1)+EVAL(MSK11,2)+EVAL(MSK11,3)).^2./(9*EVAL(MSK11,1).^2).*W_hat(MSK11,1,1,1,1);

% K_perp
% Hui's definition in Hui et al. 2008 
% [G1,G2]     = make_G1G2(EVAL(MSK33,:));
% [G10,G20]   = make_G1G2(cat(2,EVAL(MSK33,1),EVAL(MSK33,3),EVAL(MSK33,2)));
% Kperp(MSK33)       = G1.*What(MSK33,2,2,2,2)+G10.*What(MSK33,1,1,1,1)+G2.*What(MSK33,2,2,1,1);
% Hansens's definition in Hansen et al. 2017 Neuroimage, except for using
% 2nd and 3rd EVs instead of 1st and 2nd
K_perp   = zeros(size(EVAL(:,1)));
MSK33 = find((EVAL(MSK11,2)+EVAL(MSK11,3))/2>=Dthr);
K_perp(MSK33) = (EVAL(MSK33,1)+EVAL(MSK33,2)+EVAL(MSK33,3)).^2./((9/4)*(EVAL(MSK33,2)+EVAL(MSK33,3)).^2).*(3/8).*(W_hat(MSK33,2,2,2,2)+W_hat(MSK33,3,3,3,3)+2.*W_hat(MSK33,3,3,2,2));
% estimate Kurtosis value along each diffusion direction
K_hat = make_K_hat(bvals,bvecs,DK,MSK2,Asym);

function [G1,G2] = make_G1G2(EVAL)
    G1 = (EVAL(:,1)+EVAL(:,2)+EVAL(:,3)).^2./(18*(EVAL(:,2)-EVAL(:,3)).^2).*(2*EVAL(:,2)+(EVAL(:,3).^2-3*EVAL(:,2).*EVAL(:,3))./sqrt(EVAL(:,2).*EVAL(:,3)));
    G2 = (EVAL(:,1)+EVAL(:,2)+EVAL(:,3)).^2./(3*(EVAL(:,2)-EVAL(:,3)).^2).*((EVAL(:,2)+EVAL(:,3))./sqrt(EVAL(:,2).*EVAL(:,3))-2);
end

function [F1,F2] = make_F1F2(EVAL,errtol)
    % F1 and F2 are copied from Tabesh et al. 2011
    % S. Mohammadi 22.10.2012
    F1 = (EVAL(:,1)+EVAL(:,2)+EVAL(:,3)).^2./(18*(EVAL(:,1)-EVAL(:,2)).*(EVAL(:,1)-EVAL(:,3))).*((sqrt(EVAL(:,2).*EVAL(:,3))./EVAL(:,1)).*rf(EVAL(:,1)'./EVAL(:,2)',EVAL(:,1)'./EVAL(:,3)',ones(size(EVAL(:,3)')),errtol)' ...
        + (3*EVAL(:,1).^2-EVAL(:,1).*EVAL(:,2)-EVAL(:,1).*EVAL(:,3)-EVAL(:,2).*EVAL(:,3))./(3*EVAL(:,1).*sqrt(EVAL(:,2).*EVAL(:,3))).*rd(EVAL(:,1)'./EVAL(:,2)',EVAL(:,1)'./EVAL(:,3)',ones(size(EVAL(:,3)')),errtol)'-1);
    F2 = (EVAL(:,1)+EVAL(:,2)+EVAL(:,3)).^2./(3*(EVAL(:,2)-EVAL(:,3)).^2).*(((EVAL(:,2)+EVAL(:,3))./sqrt(EVAL(:,2).*EVAL(:,3))).*rf(EVAL(:,1)'./EVAL(:,2)',EVAL(:,1)'./EVAL(:,3)',ones(size(EVAL(:,3)')),errtol)'...
        + (2*EVAL(:,1)-EVAL(:,2)-EVAL(:,3))./(3.*sqrt(EVAL(:,2).*EVAL(:,3))).*rd(EVAL(:,1)'./EVAL(:,2)',EVAL(:,1)'./EVAL(:,3)',ones(size(EVAL(:,3)')),errtol)'-2);
end


function What = make_W_hat(EVEC,DK)
    % make What 
    % S.Mohammadi 22.10.2012
    What = zeros(size(DK));
    for ii = 1:size(EVEC,3)
        for jj = 1:size(EVEC,3)
            for kk = 1:size(EVEC,3)
                for ll = 1:size(EVEC,3)
                    What(:,ii,jj,kk,ll) = zeros(size(DK,1),1);
                    for i = 1:size(EVEC,3)
                        for j = 1:size(EVEC,3)
                            for k = 1:size(EVEC,3)
                                for l = 1:size(EVEC,3)
    %                                 What(:,ii,jj,kk,ll) = EVEC(:,i,ii).*EVEC(:,j,jj).*EVEC(:,k,kk).*EVEC(:,l,ll).*DK(:,i,j,k,l)+What(:,ii,jj,kk,ll); 
                                   What(:,ii,jj,kk,ll) = EVEC(:,ii,i).*EVEC(:,jj,j).*EVEC(:,kk,k).*EVEC(:,ll,l).*DK(:,i,j,k,l)+What(:,ii,jj,kk,ll);
    %                                 % how about this?
    %                                  What(:,ii,jj,kk,ll) = EVEC(:,i,ii).*EVEC(:,j,jj).*EVEC(:,kk,k).*EVEC(:,ll,l).*DK(:,i,j,k,l)+What(:,ii,jj,kk,ll);
    %                                 
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

function D_hat = write_D_hat(What)
    % S.Mohammadi 24.10.2012
    D_hat(:,1) = What(:,1,1,1,1);
    D_hat(:,2) = What(:,2,2,2,2);
    D_hat(:,3) = What(:,3,3,3,3);

    D_hat(:,4) = What(:,1,1,1,2);
    D_hat(:,5) = What(:,1,1,1,3);
    D_hat(:,6) = What(:,2,2,2,1);
    D_hat(:,7) = What(:,2,2,2,3);
    D_hat(:,8) = What(:,3,3,3,1);
    D_hat(:,9) = What(:,3,3,3,2);

    D_hat(:,10) = What(:,1,1,2,2);
    D_hat(:,11) = What(:,1,1,3,3);
    D_hat(:,12) = What(:,2,2,3,3);

    D_hat(:,13) = What(:,1,1,2,3);
    D_hat(:,14) = What(:,2,2,1,3);
    D_hat(:,15) = What(:,3,3,1,2);
end


function Khat = make_K_hat(bvals, bvecs, DK, MSK, Asym)
    DT  = zeros(size(Asym,1),3,3);
    Dav = mean(Asym(:,1:3),2);
    DT(:,1,1)  = Asym(:,1);
    DT(:,2,2)  = Asym(:,2);
    DT(:,3,3)  = Asym(:,3);
    DT(:,2,1)  = Asym(:,4);
    DT(:,1,2)  = Asym(:,4);
    DT(:,3,1)  = Asym(:,5);
    DT(:,1,3)  = Asym(:,5);
    DT(:,3,2)  = Asym(:,6);
    DT(:,2,3)  = Asym(:,6);

    Khat = zeros(size(Asym,1),size(bvecs,2));
    DTn = zeros(size(Asym,1),size(bvecs,2));
    bshell = unique(bvals);
    bMSK = find(bvals>bshell(1));
    for ii = bMSK
        for i = 1:size(bvecs,1)
            for j = 1:size(bvecs,1)
                DTn(MSK2,ii) = (bvecs(i,ii).*bvecs(j,ii).*DT(MSK2,i,j)).^2+DTn(MSK2,ii);
                for k = 1:size(bvecs,1)
                    for l = 1:size(bvecs,1)
                        Khat(MSK2,ii) = bvecs(i,ii).*bvecs(j,ii).*bvecs(k,ii).*bvecs(l,ii).*DK(:,i,j,k,l) + Khat(MSK2,ii);
                    end
                end
            end
        end
        Khat(MSK2,ii) = Khat(MSK2,ii).*Dav(MSK2).^2./DTn(MSK2,ii);
    end
end
end