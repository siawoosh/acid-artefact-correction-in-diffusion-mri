function [m0big,lower,upper] = acid_nlls_def_starting_guess_and_bounds(dummy_model,nParameters,measured_signal,lS0slice,datascale,AMSKslice,bvecs,bvals,sz)

bvals = bvals/1000;

% "m0big" is the starting guess

m0big = 0.0001*ones(nParameters,size(measured_signal,1)); % hier steht 0.0001 als Startwert, weil 0 einen Fehler f??r die Fit-Option dummy_quasi_linear_no_rice = 1 produziert (jedenfalls aktuell: 07:02.2019 16:15 Uhr)
m0big(7,:) = exp(lS0slice')/datascale;

lower = -Inf*ones(size(m0big)); %lower is the lower bound for the parameters to be found by the algorithm
lower = lower(:,AMSKslice>0);
upper = Inf*ones(size(m0big));  %upper is the upper bound for the parameters to be found by the algorithm
upper = upper(:,AMSKslice>0);

if(dummy_model == 2)

    %1 through 6 : diffusion parameters
    %7 : S0
    %8 through 22: kurtosis parameters

    lower(1:6,:)=-10;
    upper(1:6,:)= 10;
    lower(8:22,:)=-10;
    upper(8:22,:) = 10;
    m0big(1:6,AMSKslice>0)=1;

end

if(dummy_model == 3)
    % Dpara=parameters(1,:);
    % Dperp=parameters(2,:);
    % S0=parameters(3,:);
    % theta=parameters(4,:);
    % phi=parameters(5,:);
    % Wpara=parameters(6,:);
    % Wperp=parameters(7,:);
    % Wmean=parameters(8,:);

    mask=AMSKslice>0;

    % The code for initial_guess_axial.m is based on code from the
    % repository of Sune N??rh??j Jespersen : https://github.com/sunenj/Fast-diffusion-kurtosis-imaging-DKI
    [theta_guess,phi_guess,Dpar_guess,Dper_guess] = acid_nlls_initial_guess_axial(bvecs,bvals,reshape(measured_signal,sz(1),sz(2),numel(bvals)),mask);

    m0big = (1/1000)*ones(nParameters,size(measured_signal,1)); % hier stand 0.0001 als Startwert, weil 0 einen Fehler f??r die Fit-Option dummy_quasi_linear_no_rice = 1 produziert (jedenfalls aktuell: 07.02.2019 16:15 Uhr)
    m0big(1,AMSKslice>0) = 1;%Dpar_guess;
    m0big(2,AMSKslice>0) = 1; %Dper_guess;
    m0big(3,:)=exp(lS0slice')/datascale;
    m0big(4,AMSKslice>0)=theta_guess;
    m0big(5,AMSKslice>0)=phi_guess;
    m0big(6:8,AMSKslice>0)=1;

    lower(4,:)=-2*pi;%0;
    upper(4,:)=2*pi;%pi;%as of right now (05.11.2019) precise bounds for theta and phi (=parameters defining the axis of symmetry) are central for the axial symmetric DKI implementation to find the correct parameters, SOLVED (10.01.2020)
    lower(5,:)=-2*pi;%0;
    upper(5,:)=2*pi;
    lower(1:2,:)=1e-4; %so that traceterm does not become 0
    upper(1:2,:)=10;
    lower(6:8,:)=-10;
    upper(6:8,:)=10;

end

if(dummy_model == 4)
    
    mask=AMSKslice>0;
    [theta_guess,phi_guess,Dpar_guess,Dper_guess] = acid_nlls_initial_guess_axial(bvecs,bvals,reshape(measured_signal,sz(1),sz(2),numel(bvals)),mask);
    m0big = (1)*ones(nParameters,size(measured_signal,1));
    m0big(1,:)=exp(lS0slice')/datascale;
    m0big(7,AMSKslice>0)=rad2deg(theta_guess);
    m0big(8,AMSKslice>0)=rad2deg(phi_guess);
    m0big(2,AMSKslice>0)=0.5;

    lower(7:8,:)=-360;
    upper(7:8,:)=360;
    lower(1:6,:)=0;
    upper(4,:) = 100;

end

if dummy_model == 5

    m0big = -0.01*ones(nParameters,size(measured_signal,1));
    m0big(1,AMSKslice>0)= 0;

    lower = -Inf.*ones(size(m0big));
    upper = Inf.*ones(size(m0big));

    lower=lower(:,AMSKslice>0);
    upper=upper(:,AMSKslice>0);
    lower(1,:)=0;
end

if dummy_model == 6

    %
    %             S0 = parameters(1,:);
    %             f = parameters(2,:);
    %             K = parameters(3,:);
    %             Depara = parameters(4,:);
    %             Deperp = parameters(5,:);
    %             theta_param = parameters(6,:); % this is the theta in the axis of symmetry
    %             phi_param = parameters(7,:);  % this is the phi in the axis of symmetry

    mask=AMSKslice>0;
    [theta_guess,phi_guess,Dpar_guess,Dper_guess] = acid_nlls_initial_guess_axial(bvecs,bvals,reshape(measured_signal,sz(1),sz(2),numel(bvals)),mask);
    m0big = (1)*ones(nParameters,size(measured_signal,1));
    m0big(1,:)=exp(lS0slice')/datascale;
    m0big(6,AMSKslice>0)=rad2deg(theta_guess);
    m0big(7,AMSKslice>0)=rad2deg(phi_guess);
    m0big(2,AMSKslice>0)=0.5;

    lower(6:7,:)=-360;%0;
    upper(6:7,:)=360;%pi;%as of right now (05.11.2019) precise bounds for theta and phi (=parameters defining the axis of symmetry) are central for the axial symmetric DKI implementation to find the correct parameters, SOLVED (10.01.2020)
    lower(1:5,:)=0;
    upper(3,:) = 100;
    upper(2,:) = 1;
end
end