function [jacobi_i] = acid_nlls_jacobi_func(dummy_model,parameters,design_kurtosis,pred,S0,DM0)

switch dummy_model
    case 1
        jacobi_i = acid_nlls_jacobian_matrix_DTI_non_lin(parameters,DM0,pred,S0);
    case 2
        jacobi_i = acid_nlls_jacobian_matrix_DKI_non_lin(parameters,design_kurtosis,pred,S0);
end

end