function dout=watson_factor_numeric(k,watson_factor)


xvec = linspace(0,100,10^5);


tmp = abs(k'-xvec);
[kinx,inx]=min(tmp,[],2);

dout = watson_factor(inx);

end