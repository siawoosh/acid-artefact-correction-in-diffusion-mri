function [EVEC_, EVAL_] = acid_dti_compute_eigenvalues(DT2d)

% initialization
D     = zeros(size(DT2d,1),3,3);
EVEC_ = zeros(size(DT2d,1),3,3);
EVAL_ = zeros(size(DT2d,1),3);

% fill up tensor from input
D(:,1,1) = DT2d(:,1);
D(:,2,2) = DT2d(:,2);
D(:,3,3) = DT2d(:,3);
D(:,1,2) = DT2d(:,4);
D(:,2,1) = DT2d(:,4);
D(:,1,3) = DT2d(:,5);
D(:,3,1) = DT2d(:,5);
D(:,2,3) = DT2d(:,6);
D(:,3,2) = DT2d(:,6);

for i = 1:size(D,1)
    [EVEC_tmp,EVAL_tmp]  = eig(squeeze(D(i,:,:)));
    EVEC_(i,:,1) = EVEC_tmp(:,1);
    EVEC_(i,:,2) = EVEC_tmp(:,2);
    EVEC_(i,:,3) = EVEC_tmp(:,3);
    EVAL_(i,1) = EVAL_tmp(1,1);
    EVAL_(i,2) = EVAL_tmp(2,2);
    EVAL_(i,3) = EVAL_tmp(3,3);
end
end