function [exp_D] = acid_nlls_exponent_D(bvals,bvecs,parameters)
% Dpara=parameters(1,:);
% Dperp=parameters(2,:);
% S0=parameters(3,:);
% theta=parameters(4,:);
% phi=parameters(5,:);
% Wpara=parameters(6,:);
% Wperp=parameters(7,:);
% Wmean=parameters(8,:);

for i=1:numel(bvals)

    b_matrices= bvals(i).* [bvecs(1,i).* bvecs(1,i),bvecs(1,i).*bvecs(2,i),bvecs(1,i).*bvecs(3,i);bvecs(1,i).*bvecs(2,i),bvecs(2,i).*bvecs(2,i),bvecs(2,i).*bvecs(3,i);bvecs(1,i).*bvecs(3,i),bvecs(2,i).*bvecs(3,i),bvecs(3,i).*bvecs(3,i)];
    tr_bmatrices = trace(b_matrices);

    for j=1: size(parameters,2)
        u=[ sin(parameters(4,j)).*cos(parameters(5,j)), sin(parameters(4,j)).*sin(parameters(5,j)),cos(parameters(4,j))]';
        exp_D(j,i)=( tr_bmatrices*parameters(2,j) + (parameters(1,j)-parameters(2,j))* (u'*b_matrices*u) );
    end
end
end