function [MSK_bvalues] = acid_nlls_sort_b_values(bvalues,bvalue_labels)

    for i = 1:size(bvalue_labels,2)
        MSK_bvalues{i} = find((bvalues(1,:))==bvalue_labels(1,i));
    end
    
end

