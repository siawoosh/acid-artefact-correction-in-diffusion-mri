function [EVEC_2,EVEC_3] = acid_dti_compute_eigenvalues_GN(Asym0,MSK,EVEC)

    D = zeros(size(EVEC,1),3,3);

    D(MSK,1,1)=Asym0(:,1);
    D(MSK,2,2)=Asym0(:,2);
    D(MSK,3,3)=Asym0(:,3);
    D(MSK,1,2)=Asym0(:,4);
    D(MSK,2,1)=Asym0(:,4);
    D(MSK,1,3)=Asym0(:,5);
    D(MSK,3,1)=Asym0(:,5);
    D(MSK,2,3)=Asym0(:,6);
    D(MSK,3,2)=Asym0(:,6);

    EVEC_1 = zeros(size(EVEC,1),1,3);
    EVEC_2 = zeros(size(EVEC,1),1,3);
    EVEC_3 = zeros(size(EVEC,1),1,3);

 for inx_mask = 1: size(MSK,1)
    [EVEC_tmp,~] = eigs(squeeze(D(MSK(inx_mask),:,:)));           
    EVEC_1(MSK(inx_mask), 1 , :)  = EVEC_tmp(:,1);
    EVEC_2(MSK(inx_mask), 1 , :)  = EVEC_tmp(:,2);
    EVEC_3(MSK(inx_mask), 1 , :)  = EVEC_tmp(:,3);    
 end