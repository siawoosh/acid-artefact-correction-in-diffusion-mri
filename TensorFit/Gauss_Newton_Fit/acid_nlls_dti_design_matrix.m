function [DM,design] = acid_nlls_dti_design_matrix(bvecs,bvals)
% S. Mohammadi 08/01/2012

sz=size(bvecs);

if sz(1) ~= 3
    disp(sz(1))
    error('Invalid dimensions of gradient vectors!');
end

if size(bvals,1) ~=1
    disp(size(bvals,1))
    error('Invalid dimensions of gradient vectors!');
end


designmatrix= @(x,y,z,bv) bsxfun(@times,[-x.*x;-y.*y;-z.*z;-2.*x.*y;-2.*x.*z;-2.*y.*z],bv); % this function computes the designmatrix which is needed for finding the diffusion tensor, the input arguments are the b-vectors ("DiffVecORIG") and b-values

DM  = [designmatrix(bvecs(1,:),bvecs(2,:),bvecs(3,:),bvals); ones(1,numel(bvals))];
DM  = DM';

design= [designmatrix(bvecs(1,:),bvecs(2,:),bvecs(3,:),bvals)];
design = design';
%IDM = inv((DM')*DM)*DM';

end