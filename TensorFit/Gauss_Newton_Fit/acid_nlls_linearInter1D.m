function [Tc,dT] = acid_nlls_linearInter1D(T,omega,x,varargin)

% flag for computing the derivative
doDerivative = (nargout>1);
matrixFree   = 0;
for k=1:2:length(varargin) % overwrite default parameter
    eval([varargin{k},'=varargin{',int2str(k+1),'};']);
end

% get data size m, cell size h, dimension d, and number n of interpolation points
dim = length(omega)/2;
if not(dim==1), error('this function is for 1D interpolation only'); end
m   = numel(T);
h   = (omega(2:2:end)-omega(1:2:end))./m;
n   = numel(x);
x   = x(:);

% map x from [h/2,omega-h/2] -> [1,m],
xOrig = x;
for i=1:dim, x(:,i) = (x(:,i)-omega(2*i-1))/h(i) + 0.5; end

Tc = zeros(n,1); dT = [];                     % initialize output
if doDerivative, dT = zeros(n,1);  end       % allocate memory in column format
% valid =  (0<x & x<m+1);                     % determine indices of valid points, 30.06.2021: valid points reduced by 1 to avoid divergence of dT, TP(5003) = 0 which causes the divergence
valid =  (0<x & x<m);                         % determine indices of valid points

% asymptotically we go to 1
Tc(~valid) = xOrig(~valid);
dT(~valid) = h(1);

pad = 1; TP = zeros(m+2*pad);                 % pad data to reduce cases

P = floor(x); x = x-P;                        % split x into integer/remainder
p = @(j) P(valid,j); xi = @(j) x(valid,j);

% increments for linearized ordering
i1 = 1; i2 = size(T,1)+2*pad; i3 = (size(T,1)+2*pad)*(size(T,2)+2*pad);

TP(pad+(1:m)) = reshape(T,m,1);
clear T;
p = pad + p(1);
Tc(valid) = TP(p).* (1-xi(1)) + TP(p+1).*xi(1);   % compute weighted sum

if ~doDerivative, return; end;
% compute and format the derivative
dT(valid) = TP(p+1)-TP(p);

if doDerivative
    for i=1:dim, dT(:,i) = dT(:,i)/h(i); end
    if not(matrixFree)
        dT = spdiags(dT,n*(0:(dim-1)),n,dim*n);
    end
end

end
