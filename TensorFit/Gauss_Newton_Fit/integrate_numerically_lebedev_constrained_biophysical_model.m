function [lebedev_sum_solution] = integrate_numerically_lebedev_constrained_biophysical_model(diffusion_gradients, b, parameters,lebedev_quadrature_list,watson_factor,beta_map_slice)

S0 = parameters(1,:);
f = parameters(2,:);
K = parameters(3,:);
Depara = parameters(4,:);
Deperp = parameters(5,:);
theta_param = parameters(6,:); % this is the theta in the axis of symmetry
phi_param = parameters(7,:);  % this is the phi in the axis of symmetry


phi_leb = lebedev_quadrature_list(:,1);  %Grid for integration of the sphere , -180 degrees =< phi =< 180 degrees

theta_leb = lebedev_quadrature_list(:,2); %Grid for integration of the sphere, 0 degrees =< theta =< 180 degrees

weight = lebedev_quadrature_list(:,3);

watson_factor_signal = watson_factor_numeric(K,watson_factor);

g1 = diffusion_gradients(:,1);
g2 = diffusion_gradients(:,2);
g3 = diffusion_gradients(:,3);

sinus_theta_param = sind(theta_param(1,:));
cosinus_phi_param = cosd(phi_param(1,:));

cosinus_theta_param = cosd(theta_param(1,:));
sinus_phi_param = sind(phi_param(1,:));

summand = zeros(size(b,1), size(parameters,2), size(theta_leb,1));
for  inx = 1: numel (theta_leb(:,1))

    sinus_theta_leb = sind(theta_leb(inx));
    cosinus_phi_leb = cosd(phi_leb(inx));
    sinus_phi_leb = sind(phi_leb(inx));
    cosinus_theta_leb = cosd(theta_leb(inx));

    g_dot_u_squared = (g1 .* sinus_theta_leb .* cosinus_phi_leb + g2 .* sinus_theta_leb .* sinus_phi_leb + g3 .* cosinus_theta_leb ) .^2;

    summand(:, :, inx) = weight(inx) *  acid_nlls_BP_Constrained_Signal_Equation(S0,K,f,Depara,Deperp,watson_factor_signal,b,g_dot_u_squared,sinus_theta_param,cosinus_phi_param,sinus_theta_leb,cosinus_phi_leb,sinus_phi_leb,cosinus_theta_leb,cosinus_theta_param,sinus_phi_param,beta_map_slice);

end

lebedev_sum_solution = 4 .* pi .* sum (summand,3);

end


            
            
            
            