function [data] = acid_nlls_data_to_be_fitted(credit, measured_signal, dummy_model, bvals, bvals_labels)

    if dummy_model==5

        MSK_bvalues = sort_b_values(bvals,bvals_labels);
        data =zeros(size(bvals_labels,2)-1,size(measured_signal,1));

        for i = 1:size(bvals_labels,2)-1
            data(i,:) = mean(measured_signal(:,MSK_bvalues{i+1}),2)';
        end
    else
        data = measured_signal';
    end

    data  = data(:,credit);

end