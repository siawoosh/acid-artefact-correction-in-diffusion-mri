% function [uc,His] = ProjGaussNewton(fctn,uc,varargin)
%
% Inputs:
%   fctn - objective function
%   uc   - starting guess
%
% Ouputs:
%   uc  - estimated parameters
%   his - struct with info about the optimization
%           his.his iteration history (obj function, gradient, ...
%           his.iter  contains the number of iterations for each voxel
%           his.conv  boolean flag for each voxel (1-> converged, 0-> not
%                     converged to desired accuracy)
%
function [uc,His,uncertainty_on_parameter_estimates] = acid_nlls_GaussNewton(fctn,uc,uncertainty_on_parameter_estimates,dummy_calculate_uncertainty,varargin)

if nargin==0
    help(mfilename);
    return;
end

maxIter      = 30;
uStop        = [];
Jstop        = [];
paraStop     = [];
tolJ         = 1e-5;            % for stopping, objective function
tolU         = 1e-4;            %   - " -     , current value
tolG         = 1e-4;            %   - " -     , norm of gradient
LSMaxIter    = 15;              % maximum number of line search iterations
LSreduction  = 0;               % minimal reduction in line search
Plots        = @(task,para) [];
verbose      = false;
upper        = Inf*ones(numel(uc),1);
lower        = -Inf*ones(numel(uc),1);
maxStep      = 10;

for k=1:2:length(varargin)   % overwrites default parameter
    eval([varargin{k},'=varargin{',int2str(k+1),'};']);
end
if verbose
    fprintf('%s %s %s\n',ones(1,30)*char('='),mfilename,ones(1,40)*char('='));
end

% evaluate objective function for stopping criteria

if isempty(uStop), uStop = uc; end
if isempty(Jstop) || isempty(paraStop)
    [J0,paraStop] = fctn(uStop,[]); J0 = abs(J0) + (J0 == 0);
    Jstop = paraStop.Dcs;
    Plots('stop',paraStop);
end
nvoxel = paraStop.nvoxel;
uc    = reshape(uc,[],nvoxel);
lower = reshape(lower,[],nvoxel);
upper = reshape(upper,[],nvoxel);


nParamModel = size(uc,1); % @CdA extract the number of parameters (3 for model without MTw, 4 otherwise)

his = zeros(maxIter+2,9);
his(1,1:6) = [-1,J0,0,0,paraStop.Dc,paraStop.Rc];
% evaluate objective function for starting values and plots
[Jc,para,dJ,H,Hstar] = fctn(uc,[]);
his(2,1:6) = [0,Jc,J0-Jc,norm(dJ),para.Dc,para.Rc];

if verbose
    fprintf('[ maxIter=%s / tolJ=%s / tolU=%s / tolG=%s / length(yc)=%d ]\n',...
        num2str(maxIter),num2str(tolJ),num2str(tolU),num2str(tolG),size(uc,2));
    fprintf('%4s %9s %9s %9s %9s %9s %2s %9s %9s\n','iter','Jc','Jold-Jc','norm(dJ)','Dc','Rc','LS','Active','perc.conv');
    fprintf('%4d %9.2e %9.2e %9.2e %9.2e %9.2e\n',his(1,1:6))
end

Plots('start',para);
iter = 0; Iter = zeros(nvoxel,1); uOld = 0*uc; Jold = para.Dcs; u0 = uc; LSiter = 0;

% initialize
nvoxel    = para.nvoxel;
STOP      = zeros(5,nvoxel);
credit    = true(1,nvoxel);
normU     = @(u) sqrt(sum(u).^2);

Dcs    = para.Dcs;
while 1
    Plots(iter,para);

    %hier werden f??r jeden Voxel die Abbruchbedingungen ??berpr??ft und
    %dementsprechend credit neu definiert, so dass nur noch mit den Voxeln
    %gearbeitet wird, f??r die die Objective-Function noch nicht minimiert
    %worden ist.
    % check stopping rules for all voxels of previous iteration and new
    % credit set
    STOP(1,credit) = (iter>0) & (abs(Jold(credit)-para.Dcs)   <= tolJ*(1+abs(Jstop(credit))));
    STOP(2,:)      = (iter>0) & (normU(uc-uOld)               <= tolU*(1+normU(u0)));
    STOP(3,credit) = (normU(para.dDs)                         <= tolG*(1+abs(Jstop(credit))));
    STOP(4,credit) = (norm(para.dDs)                          <= 1e6*eps);
    STOP(5,:) = (iter >= maxIter);
    creditNew  = ( ~STOP(1,:) | ~STOP(2,:) | ~STOP(3,:)) & ~STOP(4,:);
    if isempty(creditNew)
        break;
    end
    Dcs(credit) = para.Dcs;

    % remove rows from gradient that are associated with voxels that have
    % converged
    creditDiff = creditNew(credit); % voxels in credit who have converged in last iteration
    idCredit   = reshape(repmat(creditDiff,nParamModel,1),[],1);

    dJ = dJ(idCredit);
    H  = H(idCredit,idCredit);
    Hstar = Hstar(idCredit,idCredit);
    %%%%%%%%%%%%% added to compute uncertainty on parameter estimates
    if (iter > 0 && dummy_calculate_uncertainty == 1)
        nMeasurements = size(para.res,1);
        [uncertainty_on_parameter_estimates] = acid_nlls_parameter_uncertainty(uncertainty_on_parameter_estimates,creditNew, Hstar, nParamModel, Dcs, nMeasurements);
    end
    %%%%%%%%%%%%%%%%%%
    % update
    Active =  (uc(:,creditNew)<=lower(:,creditNew)) | (uc(:,creditNew)>=upper(:,creditNew));

    % some output
    nActive = nnz((uc<=lower) | (uc>=upper));
    his(iter+2,:) = [iter,Jc,sum(.5*(Jold-Dcs)),norm(dJ),para.Dc,para.Rc,LSiter,nActive,100*nnz(not(credit))/nvoxel];
    if verbose
        fprintf('%4d %9.2e %9.2e %9.2e %9.2e %9.2e %2d\t%2d\t%1.1f%%\n',his(iter+2,:));
    end

    if isempty(credit) || iter >=maxIter, break;  end;

    iter = iter + 1;
    % solve the Gauss-Newton System on inactive set
    Hr  = H(not(Active),not(Active));
    Jr  = dJ(not(Active));
    dur = -(Hr\Jr);
    if max(abs(dur))>maxStep
        dur = maxStep*dur/max(abs(dur));
    end
    du = 0*uc(:,creditNew);
    du(not(Active)) = dur;

    % take gradient step on inactive set
    gl = max(0,-dJ(uc(:,creditNew)==lower(:,creditNew)));
    if not(isempty(gl)),
        if max(abs(gl))>max(abs(dur)),
            gl = max(abs(dur))*gl./max(abs(gl));
        end
        gl = max(gl,0);
        du(uc(:,creditNew)==lower(:,creditNew))=gl;
    end

    gu = min(0,-dJ(uc(:,creditNew)==upper(:,creditNew)));
    if not(isempty(gu)),
        if max(abs(gu))>max(abs(dur)),
            gu = max(abs(dur))*gu./max(abs(gu));
        end
        gu = min(gu,0);
        du(uc(:,creditNew)==upper(:,creditNew)) = gu;
    end

    if all(normU(du)<=1e-10),
        %fprintf('\n %s - found stationary point.\n\n',mfilename)
        break;
    end

    % perform Armijo linesearch
    t = 1;
    LSiter = 1; creditLS = creditNew; idx = 1:nnz(creditLS);
    while 1
        ut(:,creditLS) = uc(:,creditLS) + t*du(:,idx);
        ut(ut<lower) = lower(ut<lower);
        ut(ut>upper) = upper(ut>upper);

        [~,parat] = fctn(ut,creditLS);
        % Armijo condition: fctn(uc+t*du) <= Jc + t*LSreduction*(dJ*du)
        LS = parat.Dcs < (para.Dcs(idx)); % compare
        if all(LS) || LSiter>=LSMaxIter, break; end
        creditLS(creditLS) = ~LS;
        idx(LS) = [];
        t = t/2;
        LSiter = LSiter+1;
    end
    if not(LS)
        ut(:,creditLS) = uc(:,creditLS);
        %         warning('Linesearch failed in %1.2f%% of voxels!',100*numel(find(not(LS)))/numel(creditLS));
    end
    Iter(credit) = iter;
    Jold(credit) = para.Dcs;
    credit = creditNew;
    uOld = uc;  uc = ut;
    [Jc,para,dJ,H,Hstar] = fctn(uc,credit); % evalute objective function
end
% report history
His = struct('his',his,'iter',Iter,'conv', STOP(1,:)& STOP(2,:)&STOP(3,:));
if verbose
    fprintf('%s\nSTOPPING:\n',ones(1,87)*char('-'));
    fprintf('%1.1f%%\t[ %-10s=%16.8e <= %-25s=%16.8e]\n',100*nnz(STOP(1,:))/nvoxel,...
        '(Jold-Jc)',max(Jold-Jc),'tolJ*(1+|Jstop|)'   ,tolJ*max(1+abs(Jstop)));
    fprintf('%1.1f%%\t[ %-10s=%16.8e <= %-25s=%16.8e]\n',100*nnz(STOP(2,:))/nvoxel,...
        '|yc-yOld|',norm(uc-uOld),'tolU*(1+norm(yc)) ',tolU*(1+norm(uc)));
    fprintf('%1.1f%%\t[ %-10s=%16.8e <= %-25s=%16.8e]\n',100*nnz(STOP(3,:))/nvoxel,...
        '|dJ|',norm(dJ),'tolG*(1+abs(Jstop))',min(tolG*(1+abs(Jstop))));
    fprintf('%1.1f%%\t[ %-10s=%16.8e <= %-25s=%16.8e]\n',100*nnz(STOP(4,:))/nvoxel,...
        'norm(dJ)',norm(dJ),'eps',1e3*eps);
    fprintf('%d\t[ %-10s=  %-14d >= %-25s=  %-14d]\n',any(STOP(5,:)),...
        'iter',iter,'maxIter',maxIter);

    fprintf('%s %s : done ! %s\n',ones(1,30)*char('='),mfilename,ones(1,30)*char('='));
end

if not(LS)
    %fprintf('Projected Gauss Newton: Linesearch failed in final iteration for %d out of %d voxels\n',find(not(LS)),nvoxel);
end

end