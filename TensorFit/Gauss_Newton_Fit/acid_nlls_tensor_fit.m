function [DT, RMSE, RMSE_shell_vol_out, Error_4d, Sfit_4d, MAE, MeAE] = acid_nlls_tensor_fit(V, bvals, bvecs, dummy_nlls_model, dummy_RBC, sigma, MSK, n_coils, noise_map, npool, maxIter, bvalue_labels, constraints_map, MSK_idx, dummy_error_per_shell)

% =========================================================================
% Gauss Newton Fit and Rician Bias Correction

% V             - Headers of the dMRI dataset (i.e. low and high b-value
%                 images). The order of the images must be the same as in bvals
%                 and bvecs.
% bvals         - b-value for each image, i.e. b-value for each diffusion
%                 gradients; it should be a matrix of size 1 x number of diffusion
%                 directions
% bvecs         - b-vectors, i.e. vector of diffusion gradients;
%                 it should be a matrix of size 3 x number of diffusion
%                 dections
% dummy_nlls_model   - Chooses signal model fitted to the data
%                   1:'Diffusion Tensor Imaging (DTI)'
%                   2:'Diffusion Kurtosis Imaging (DKI)'
%                   3:'Axial Symmetric DKI'
%                   4:'Biophysical Model'
%                   5:'Power-law fitting to obtain beta'
%                   6:'Constrained biophysical model using beta'
% dummy_RBC     - Activates/Deactivates Rician bias corrected model
%                 parameter estimation (1: ON, 0:OFF)
% sigma         - Estimated noise standard deviation
% MSK           - Optional region of interest, in which the tensor estimates are
%                 calculated. Could also be a brain mask.
% n_coils       - Number of receiver coils
% =========================================================================

use_noise_map = 0;

dummy_calculate_uncertainty = 0;
%     bvals = bvalues0';
%     bvals = round(bvals/100)*100;
%     bvalues0 = bvals';

dm = V(1).dim;
b0 = min(bvals);
idx_dw = find(bvals > b0);
res = -4; % interpolation kernel

if max(bvals)<= 200
    fprintf(2,'Important: Please provide b-values in s/mm^2 (typically in the order of 1000).\n');
    error('Fit aborted because b-values were not given in s/mm^2 which might cause numerical problems.');
end

bvals = bvals/1000;
b0 = b0 / 1000;

%%
[DM0,~] = acid_nlls_dti_design_matrix(bvecs,bvals(1,:)); % DM0 is the DTI-design-matrix of the measurement. It is built using the b-vectors (gx,gy,gz) and the b-values for each
% individual measurement. DM0 is a #measurements x 7 Matrix containing 1's in the last column which represent the
% logarithm of the signal acquired for b=0 (=lS0). (see for example Koay et al. doi: 10.1016/j.jmr.2006.06.020)

[design_kurtosis] = acid_nlls_dki_design_matrix(bvecs,bvals(1,:));
% [design_kurtosis_axial_symmetry] = acid_nlls_axdki_design_matrix(bvec,bvals(1,:));

Asym_olsq0  = zeros([dm size(DM0,2)]);
Asym_olsq00 = zeros([dm size(design_kurtosis,2)]);

nParameters = acid_nlls_number_of_fit_parameters(dummy_nlls_model);   % model dependent number of fit parameters
DT = zeros(numel(MSK_idx),nParameters);

if dummy_nlls_model == 3
    Asym_olsq00 = zeros([dm 8]);
end

if ~isempty(noise_map)
    use_noise_map = 1;
    % noise_map = spm_vol(noise_map);
    % sigma_map = acid_read_vols(noise_map,noise_map(1),1);
    sigma_map = noise_map;
else
    sigma_map = zeros(dm(1:3));
end

if ~isempty(constraints_map)
    constraints_map = spm_vol(constraints_map);
    beta_map = acid_read_vols(constraints_map,constraints_map(1),1);
else
    beta_map = zeros(dm(1:3));
end

%% Gauss-Newton fit

% if exist('parpool','file') && (npool>1)
%     if isempty(gcp('nocreate'))
%         parpool('local',npool);
%         M = npool;
%     else
%         poolobj = gcp('nocreate');
%         M = min(poolobj.NumWorkers,npool);
%     end
% else
%     M = 0;
% end

npool = acid_multicore(npool);
if npool == 1
    M = 0;
else
    M = npool;
end

volume_of_lS = cell(1,dm(3));
volume_of_fit_results = cell(1,dm(3));

D = parallel.pool.DataQueue;
afterEach(D, @nUpdateWaitbar);
spm_progress_bar('Init', dm(3) , 'Parameter estimation', 'Slice');
slice = 1;

[bvals_unique,~,~] = unique(bvals','rows');

RMSE = zeros(dm);
Error_4d = zeros([dm,size(bvecs,2)]);
RMSE_shell_vol_out = zeros(dm(1),dm(2),numel(bvals_unique),dm(3));

parfor (p = 1:dm(3), M)
    send(D, slice);

    MSKvol        = zeros(dm);
    MSKvol(:,:,p) = ones(dm(1:2));
    MSKslice      = MSK(:,:,p);

    sigma_map_slice = sigma_map(:,:,p);
    beta_map_slice  = beta_map(:,:,p);

    if(~isempty(find(MSKvol>0, 1)) && ~isempty(find(MSKslice>0, 1)))

        [lS,lS0slice]   = acid_dti_get_logS_2D_GN(V,b0,bvals,p,res);
        volume_of_lS{p} = lS;

        datascale       = 100;
        measured_signal = exp(lS)/datascale;

        parameters = 0.0001*ones(nParameters,size(measured_signal,1)); % initialization of model parameters

        if dummy_nlls_model == 3
            parameters(3,:) = exp(lS0slice')/datascale;
        elseif dummy_nlls_model == 4 || dummy_nlls_model == 6
            parameters(1,:) = exp(lS0slice')/datascale;      % 1 Arbitrarily chosen
        else
            parameters(7,:) = exp(lS0slice')/datascale;
        end

        parameters_MSK = parameters(:,MSKslice>0);

        fctn = @(parameters_MSK,credit) acid_nlls_ObjectiveFunction(parameters_MSK,measured_signal(MSKslice>0,:),DM0,credit,datascale,design_kurtosis,sigma,dummy_nlls_model,dummy_RBC,bvecs,bvals,bvalue_labels,n_coils,use_noise_map,sigma_map_slice(MSKslice>0), beta_map_slice(MSKslice>0)');
        [m0big,lower,upper] = acid_nlls_def_starting_guess_and_bounds(dummy_nlls_model,nParameters,measured_signal,lS0slice,datascale,MSKslice,bvecs,bvals,dm); %define the starting guess and the lower/upper bounds for the algorithm

        tolJ = 1e-5;
        tolG = 1e-4;
        tolU = 1e-4;

        uncertainty_on_parameter_estimates = ones(size(parameters_MSK)) * NaN;
        [estimated_parameters,~,uncertainty_on_parameter_estimates] = acid_nlls_GaussNewton(fctn,m0big(:,MSKslice>0),uncertainty_on_parameter_estimates,dummy_calculate_uncertainty,'verbose',0,'tolJ',tolJ,'tolG',tolG,'tolU',tolU,'lower',lower,'upper',upper,'maxIter',maxIter,'maxStep',10); % estimated parameters within ACIDGaussNewton = uc

        if(or(dummy_nlls_model ==1, dummy_nlls_model ==2))
            estimated_parameters(7,:) = log(estimated_parameters(7,:)*datascale);      %The data writing algorithms expects the log of the measured signal ln(S0), so for the non linear case where S0 is estimated the found values need to be converted.
        end

        if dummy_nlls_model == 3
            estimated_parameters(3,:) = log(estimated_parameters(3,:)*datascale);
        end

        estimated_parameters = estimated_parameters(:);
        uncertainty_on_parameter_estimates = uncertainty_on_parameter_estimates(:);

        tmp3 = reshape(estimated_parameters,nParameters,[]);
        volume_of_fit_results{p} = tmp3;

        tmp_uncertainty = reshape(uncertainty_on_parameter_estimates,nParameters,[]);
        volume_of_fit_uncertainties{p} = tmp_uncertainty;
        [RMSE(:,:,p), RMSE_shell_vol_out(:,:,:,p), Error_4d(:,:,p,:), Sfit_4d(:,:,p,:), MAE(:,:,p), MeAE(:,:,p)] = acid_nlls_compute_residuals(volume_of_fit_results{p}, design_kurtosis, DM0, dummy_nlls_model, measured_signal, bvals, bvecs, dm, MSKslice, datascale, dummy_error_per_shell);

    end
end
% if M > 0
%     poolobj = gcp('nocreate');
%     delete(poolobj);
% end

%% Write results to volume
if nnz(MSK) > 0
    [DT,~,~,~,~] = acid_nlls_write_fit_results_to_volume(DT,Asym_olsq0,Asym_olsq00,dm,idx_dw,MSK,dummy_nlls_model,volume_of_fit_results,volume_of_lS,volume_of_fit_uncertainties);

    if ismember(dummy_nlls_model,[1,2])
        DT(:,1:6) = DT(:,1:6) /1000;
    elseif dummy_nlls_model == 3
        DT(:,1:2) = DT(:,1:2) /1000;
    end
end

    function nUpdateWaitbar(~)
        spm_progress_bar('Set',slice);
        slice = slice + 1;
    end

end