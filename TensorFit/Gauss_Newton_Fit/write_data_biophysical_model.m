function   write_data_biophysical_model(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK,dummy_calculate_uncertainty,Asym_olsq_rice_uncertainty)
     
            %parameters(1,:) = S0
            %parameters(2,:) = f
            %parameters(3,:) = Da
            %parameters(4,:) = kappa
            %parameters(5,:) = De_parallel
            %parameters(6,:) = De_perpendicular
            %parameters(7,:) = Theta
            %parameters(8,:) = Phi 

              ending = '';   

        if(dummy_RBC == 0)
            midfix = '';
            
            prefix = ['NLLS_Biophysical_model_RBC_OFF_S0_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_f_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Da_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,3),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Kappa_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,4),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_De_parallel_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,5),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_De_perp_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Theta_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,7),VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Phi_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,8),VS0,prefix,AS0,MSK,ending);    
            
           if(dummy_calculate_uncertainty == 1)  
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_S0_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_f_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,2),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_Da_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,3),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_Kappa_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,4),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_De_parallel_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,5),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_De_perp_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_Theta_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,7),VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_Biophysical_model_RBC_OFF_Uncertainty_Phi_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,8),VS0,prefix,AS0,MSK,ending);    
           
           end
           
        elseif(dummy_RBC == 1)
            
            midfix = '';
            
            prefix = ['NLLS_Biophysical_model_RBC_ON_S0_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_f_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Da_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,3),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Kappa_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,4),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_De_parallel_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,5),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_De_perp_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Theta_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,7),VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_Biophysical_model_RBC_ON_Phi_GN_' midfix];
            my_write_data(Asym_olsq_rice(:,8),VS0,prefix,AS0,MSK,ending); 
            
           if(dummy_calculate_uncertainty == 1)  
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_S0_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_f_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,2),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_Da_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,3),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_Kappa_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,4),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_De_parallel_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,5),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_De_perp_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_Theta_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,7),VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_Biophysical_model_RBC_ON_Uncertainty_Phi_GN_' midfix];
            my_write_data(Asym_olsq_rice_uncertainty(:,8),VS0,prefix,AS0,MSK,ending);    
           end           
end

