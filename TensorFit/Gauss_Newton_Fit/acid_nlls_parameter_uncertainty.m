function [uncertainty_on_parameter_estimates] = acid_nlls_parameter_uncertainty(uncertainty_on_parameter_estimates,creditNew, Hessian, nParamModel, Dcs, nMeasurements)

 k = 0;

 variance = (Dcs(creditNew)./(nMeasurements - nParamModel));
        for i = 1: nnz(creditNew)
                 
       
                J_transpose_J = full(Hessian( 1+k*nParamModel: nParamModel+k*nParamModel,1+k*nParamModel: nParamModel+k*nParamModel)); 
                k = k + 1;

                covariance_matrix = inv(J_transpose_J) * variance(i); 
        
                uncertainty(:,i) =  sqrt(diag(covariance_matrix));
        end
   

    if isempty(Hessian)
     %Do Nothing
    else
        uncertainty_on_parameter_estimates(:,creditNew) = uncertainty; % update uncertainty estimates
    end
    
    
end

