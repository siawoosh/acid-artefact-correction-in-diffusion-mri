function [jacobi_i]=acid_nlls_jacobian_matrix_DKI_non_lin(parameters,design_kurtosis,pred,S0)
%%%%%%%%%%%%%%%%%%%%%%%%%---> NON LINEAR <---%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DKI-Jacobian-Matrix-Composition
jacobi_i=cell(1,size(parameters,2));
for i=1:size(parameters,2)
    
   %lambdaX are helpful terms for building the Jacobian
 lambda  = design_kurtosis(:,8:22)*parameters(8:22,i) * (2/9);
 lambda1 = (parameters(1,i) + parameters(2,i) + parameters(3,i)); 
 lambda2 = (lambda1/3)^2; 
 lambda3 = pred(:,i).* lambda * lambda1;
 
 jacobi_i{i}=sparse(pred(:,i).*design_kurtosis); 
 jacobi_i{i}(:,1) = jacobi_i{i}(:,1) + lambda3;
 jacobi_i{i}(:,2) = jacobi_i{i}(:,2) + lambda3;
 jacobi_i{i}(:,3) = jacobi_i{i}(:,3) + lambda3;
 jacobi_i{i}(:,7) = jacobi_i{i}(:,7).*(1/S0(i));
 jacobi_i{i}(:,8:22) = jacobi_i{i}(:,8:22) * lambda2;        
end     
end