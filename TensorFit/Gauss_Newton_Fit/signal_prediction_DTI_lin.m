function [pred]=signal_prediction_DTI_lin(parameters,DM0)
    pred = ( DM0(:,1:6) * parameters(1:6,:) ) + parameters(7,:) ; % DTI-MODEL
end