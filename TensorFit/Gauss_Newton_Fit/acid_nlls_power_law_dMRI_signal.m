function [signal] = acid_nlls_power_law_dMRI_signal(bvalues, bvalue_labels, parameters, measured_signal,credit)
%parameters(1,:) = beta

MSK_bvalues = acid_nlls_sort_b_values(bvalues,bvalue_labels);
S0 = mean(measured_signal(MSK_bvalues{1}));

alpha = 1/2;

gamma = parameters(2,:);

signal =zeros(size(bvalue_labels,2)-1,size(measured_signal(credit),2));
for i = 1:size(bvalue_labels,2)-1
    signal(i,:) =  (parameters(1,:) .* bvalue_labels(i+1).^(-alpha) + gamma) .* S0 ;
end

end
