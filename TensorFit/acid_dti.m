function [Vweights, VOut] = acid_dti(P, bvals, bvecs, dummy_algo, Pmask, RM, dummy_mask, dummy_write_fitted, dummy_write_error, dummy_write_extra, dummy_TFreiburg, ...
    dummy_plot_error, dummy_write_weights_WLS, dummy_plot_weights, A1, kmax, A2, sigma, lambda, max_iter, dummy_dki, dki_thrcond, ...
    dummy_RBC, n_coils, n_workers, map_constraints, max_iter_GN, dummy_cond_strict, dummy_eigensys, dummy_write_tensors, dummy_write_weights_ROBUST, ...
    dummy_calculate_uncertainty, dummy_unit_test, dummy_error_per_shell)

% =========================================================================
% Tensor estimation algorithm uses either ordinary least squares or robust fitting.
% This robust fitting model has been used in Mohammadi et al., MRM, 2012 and is based on the method presented in 
% Zwiers et al., Neuroimage 2010 and Mangin et al., Med Image Anal 2002
%
% Please cite Mohammadi et al., MRM 2012 (doi: 10.1002/mrm.24467) when using this algorithm.
%
% Inputs:
%   P                   - filenames of DTI images (i.e. low and high b-value images).
%                         The order of the images must sample the order of the b-vector and b-values (see below).
%   bvals               - b-value for each image, i.e. b-value for each diffusion gradients;
%                         it should be a matrix of size 1 x number of diffusion direction
%   bvecs               - b vectors, i.e. vector of diffusion gradients;
%                         it should be a matrix of size 3 x number of diffusion directions
%   dummy_algo          - options for model fitting method
%                         0 - ordinaly least squares
%                         1 - weighted least square
%                         2 - robust fitting
%                         3 - non-linear least squares
%                         4 - non-linear least squares for axisymmetric DKI
%   PMask               - File name of Region of interest (ROI), in which the tensor estimates are calculated
%   RM                  - reorientation matrix that can be applied on the b vectors
%   dummy_mask          - mask applied when computing weights (if dummy_mask is ON, the same mask is applied on the output)
%   dummy_write_fitted  - option for writing out fitted signal (Smodel) [1: yes, 0: no]
%   dummy_write_error   - option for writing out 4d model-fit error [1: yes, 0: no]
%   dummy_write_eigen   - option for writing out all eigenvectors [0: only first eigenvector is written out, 1: all eigenvectors written out]
%   dummy_TFreiburg     - option for writing out Freiburg Tractography [1: yes, 0: no]
%   dummy_write_weights - option for writing out weights [1: yes, 0: no]
%   dummy_plot_weights  - option for plotting weights [1: yes, 0: no]
%   dummy_plot_res      - option for plotting model-fit errors [1: yes, 0: no]
%   dummy_plot_error    - option for plotting distribution of model-fit error across volumes and slices [1: yes, 0: no]
%   A1                  - scaling factor for regional weight component
%                        (smaller A1 means less outliers will be rejected, see Zwiers et al., 2010); default: A1=0.3
%   kmax                - smoothing factor for the residuals; smaller kmax results in more smoothing
%   A2                  - scaling factor for slice-wise weight component
%                         (smaller A2 means less outliers will be rejected, see Zwiers et al., 2010); default: A2=0.1
%   sigma               - normalization factor
%   lambda              - Tikhonov regularization factor
%   Niter               - maximum number of iteration in the IRLS process
%   dummy_dki           - option for kurtosis fitting [1: yes, 0: no]
%   n_workers           - number of CPU cores for parallel programming
%
% Outputs:
%   Vweights - 
%   V_out    -
%
% Created by S. Mohammadi (11/10/2012)
% Adapted by G. David, B. Fricke, M. Oeschger
% =========================================================================

%% CHECKING INPUT
% start timer
tic

% Predefine Vweights
Vweights = [];

% Loading in 4D image
[V,P_uncomp,dummy_compress] = acid_prepare_and_load_4Dimage(P);
VG = V(1);

% Checking bvals/bvecs input
if isempty(bvals)
    error('No b-values have been provided. Check for correct input.')
elseif size(bvals,2)~=size(V,1)
    bvals = bvals';
    if size(bvals,2)~=size(V,1)
        error('The number of source images and b-value entries has to be the same.');
    end
end

if isempty(bvecs)
    error('No b-vectors have been provided. Check for correct input.')
elseif size(bvecs,2)~=size(V,1)
    bvecs = bvecs';
    if size(bvecs,2)~=size(V,1)
        error('The number of source images and b-vector entries has to be the same');
    end
end

% bvals_round = round(bvals/rounding_tol)*rounding_tol;
% [bvals_unique,~,~] = unique(bvals,'rows');

% Reorienting diffusion directions
bvecs = RM * bvecs;

% Creating B matrix
% B matrix, 2D array (number of volumes x 7)
if(size(bvals,1)==1)
    [B,~] = acid_dti_get_bmatrix(bvecs,bvals(1,:));
     elseif(size(bvals,1)==2)
        [B,~] = acid_dti_get_bmatrix_varTE_varb(bvecs,bvals);
end

% Indices of the volumes of the different shells
b = sort(unique(bvals));
nb = numel(b);

idx = cell(1,4);

for k = 1:nb
    idx{k} = find(bvals == b(k));
end
idx_b0 = idx{1}; % indices of the b0 shells
idx_dw = find(bvals > b(1)); % indices of the non-b0 shells

% If DKI, check for 2 shells
if dummy_dki
    if isempty(b(3))
        error('Not enough b-values! Change to tensor estimation.');
    end
end

if ~isempty(Pmask)
    V1_val = acid_read_vols(spm_vol(Pmask),VG,-4);
    if max(V1_val(:)) == 0
        error('Mask contains only zeros!')
    end
else
    if dummy_algo==2
        warning('It is recommended to specify a mask when using robust tensor fitting.')
    end
end

% Processing sigma
if isa(sigma,'char')
    sigma_struct = spm_vol(sigma);
    map_noise = acid_read_vols(sigma_struct(1), VG, -7);
elseif length(sigma) > 1
    if length(sigma)~=size(Pmask,1)
        error('The number of the source images and the noise estimate vector has to be the same!')
    end
elseif isscalar(sigma)
    sigma = sigma*ones(1,size(Pmask,1));
    map_noise = '';
else
    sigma = 10*ones(1,size(Pmask,1));
    map_noise = '';
end

% Setting parameters for fitting methods
switch dummy_algo
    case 0
        if dummy_dki == 0
            keyword = 'DTI-OLS';
        else
            keyword = 'DKI-OLS';
        end
    case 1
        % for wls, w = w3; meaning that w1 and w2 are set to 0
        % for this, we set A1 and A2 to 0, and lambda to eps
        if dummy_dki == 0
            keyword = 'DTI-WLS';
        else
            keyword = 'DKI-WLS';
        end
        A1 = 0;
        A2 = 0;
        lambda = eps;
        max_iter = 3;
    case 2
        if dummy_dki == 0
            keyword = 'DTI-ROB';
        else
            keyword = 'DKI-ROB';
        end
    case {3}
        if dummy_dki == 0
            if dummy_RBC == 0
                keyword = 'DTI-NLLS';
            elseif dummy_RBC == 1
                keyword = 'DTI-NLLS-RBC';
            end
        else
            if dummy_RBC == 0
                keyword = 'DKI-NLLS';
            elseif dummy_RBC == 1
                keyword = 'DKI-NLLS-RBC';
            end
        end
    case {4}
        if dummy_RBC == 0
            keyword = 'DKIax-NLLS';
        else
            keyword = 'DKIax-NLLS-RBC';
        end
end

if ismember(dummy_algo,[3,4])
    if dummy_algo == 3
        if ~dummy_dki
            dummy_nlls_model = 1;
        else
            dummy_nlls_model = 2;
        end
    end
    if dummy_algo == 4
        dummy_nlls_model = 3;
    end
    
    % if ~isempty(map_noise)
    %     map_noise = map_noise{1};
    % end

    bvalue_labels = [];
end

% Defining output directory
[path,fname,~] = spm_fileparts(VG.fname);

p_out = acid_bids(path,fname,keyword,1);
diary([p_out filesep 'logfile_' keyword '.txt'])

% Miscalleneous settings
interp = -4;    % interpolation kernel
dm = VG.dim;    % image matrix size

%% Calculating b0 (mean measured non-diffusion-weighted signal)

% Resample all volumes on the first volume
S0_tmp = zeros([dm numel(idx_b0)]);
kk = 1;
for j = idx_b0
    for p = 1:dm(3)
        M = V(j).mat*spm_matrix([0 0 p]);
        S0_tmp(:,:,p,kk) = spm_slice_vol(V(j),V(idx_b0(1)).mat\M,dm(1:2),interp);
    end
    kk = kk+1;
end

if numel(size(S0_tmp))==4
    b0 = mean(S0_tmp,4);
elseif numel(size(S0_tmp))==3
    b0 = S0_tmp;
else
    error('The dimensionality of your images is wrong.');
end
clear S0tmp;

%% Creating masks

% MSK: 3D mask, excluding voxels with avg(S0)<0 and, if additional mask is provided, voxels outside binary Pmask
% MSK_idx: vector containing the linear indices where MSK is 1
% MSKo: 3D mask, used for output
% MSKo_idx: vector containing the linear indices where oMSK is 1

% MSK
% - computing average model-fit error for QC purposes
% - computing weights for robust fitting
%
% MSKo (output mask)
% - masking output files

% create a binary mask (MSK) that excludes
% (i) voxels with avg(S0)<=0
% (ii) voxels outside binary Pmask
if ~isempty(Pmask)
    MSK = zeros(size(b0));
    MSKtmp = acid_read_vols(spm_vol(Pmask),VG,interp);
    MSK_idx = find(MSKtmp>10^-3 & b0>0);
    MSK(MSK_idx) = 1;
else
    MSK = zeros(size(b0));
    MSK_idx = find(b0>0);
    MSK(MSK_idx) = 1;
end

% create mask for output
if dummy_mask == 1
    MSKo     = MSK;
    MSKo_idx = MSK_idx;
else
    MSKo = zeros(size(b0));
    MSKo_idx = find(b0>0);
    MSKo(MSKo_idx) = 1;
end

%% Calculating the tensor and kurtosis elements

switch dummy_algo

    %% OLS, WLS, robust fitting
    case {0,1,2}

        % Diffusion Tensor Imaging (DTI)
        if ~dummy_dki
            fprintf('\n');
            disp('Obtaining the OLS solution: ');
            fprintf('\n');

            % Computing OLS solution. This is the starting point for WLS
            % and robust fitting as well.

            DT_ols  = zeros([dm size(B,2)]);

            for p = 1:dm(3)
                disp(['slice: ' num2str(p)]);

                % Tagging the slice under investigation
                % MSKsl (ones in the investigated slice and zeros elsewhere)
                MSKsl        = zeros(dm);
                MSKsl(:,:,p) = ones(dm(1:2));

                if ~isempty(find(MSKsl>0,1))
                    % Taking the logarithm of the signal in slice p
                    %   logS_sl      - log(S) in the given slice for all images, 2D array (number of voxels in slice x number of volumes)
                    [logS_sl,~]= acid_dti_get_logS_2D(V,VG,bvals,p,interp);

                    % Solving for the diffusion coefficients
                    %   B         - B matrix, 2D array (number of volumes, 7)
                    %   logS_sl  - log(S) in the given slice, 2D array (number of voxels in slice, number of volumes)
                    %   DT_ols_sl - diffusion coefficients, 2D array (7, number of voxels in slice)

                    DT_ols_sl = B\logS_sl';

                    % Storing the solution as a 4d volume [size_x, size_y, size_z, 7]
                    DT_ols(:,:,p,:) = reshape(permute(DT_ols_sl,[2 1]),[dm(1) dm(2) 7]);
                end
            end

            % Reshaping the DT from 4d to 2d, where only voxels within the mask (MSK) are considered
            % DT2d_ols: [num_of_voxels, 7]
            DT2d_ols  = zeros(dm(1)*dm(2)*dm(3),size(B,2));
            for i = 1:size(B,2)
                tmp = DT_ols(:,:,:,i);
                DT2d_ols(:,i) = tmp(:);
            end

            % Computing WLS (dummy_algo=1) or robust fitting (dummy_algo=2) solutions
            if dummy_algo == 1 || dummy_algo == 2
                % Calculate the log signal
                % logS_msk: log(S) within the mask for all volumes, 2D array [num_of_voxels_within_mask, num_of_vols]
                [logS_MSKo,~] = acid_dti_get_logS(V,VG,bvals,MSKo_idx);

                % Computing tensor
                [DT2d,~] = acid_dti_robust_compute_tensor_IRLS(DT2d_ols, V, B, bvals, logS_MSKo, MSK, MSKo, A1, kmax, A2, sigma, lambda, max_iter, ...
                    dummy_plot_weights, dummy_write_weights_WLS, dummy_write_weights_ROBUST, dummy_algo, dummy_mask, p_out, keyword, dummy_unit_test);
            end

            disp('Done.');
        
        % Diffusion Kurtosis Imaging (DKI)
        else

            % Extract info from the b values
            [bC,bia,bic] = unique(bvals(1,bvals>=b(3))); % b values from the third shell upwards
            binfo = struct('b0',b(1),'bMSK0',idx{1},'b1',b(2),'bMSK1',idx{2},'b2',bC,'bMSK2',idx{3},'bMSK',idx_dw,'bpos',bia,'binx',bic);
            
            % Compute kurtosis parameters
            [dummy_dki, dki_struct, ~, ~, ~] = make_Design_DWSignal_Tensor_nl_parallel(dummy_dki, bvals, bvecs, binfo, V, VG, MSKo, dki_thrcond, 0, n_workers);
            DT2d = struct('Asym_olsq',dki_struct.Asym_olsq','Asym_X4',dki_struct.Asym_X4');
            disp('Done.');

        end

    %% NLLS, both for DTI and DKI
    case {3,4}
        % Compute tensor parameters, model-fit errors, and fitted signal
        [DT2d, RMSE_nlls, RMSE_shell_vol_out, Error_nlls_4D, Sfit_nlls_4d, MAE_nlls, MeAE_nlls] = acid_nlls_tensor_fit(V, bvals, bvecs, dummy_nlls_model, dummy_RBC, sigma, MSK, n_coils, map_noise, n_workers, max_iter_GN, bvalue_labels, map_constraints, MSK_idx, dummy_error_per_shell);
        
        % Write out model-fit errros and fitted signal
        acid_nlls_write_residuals(Sfit_nlls_4d, Error_nlls_4D, RMSE_nlls , MAE_nlls, MeAE_nlls, RMSE_shell_vol_out, VG, p_out, keyword, bvals, ...
            dummy_write_fitted, dummy_error_per_shell, dummy_write_error, dummy_plot_error, MSK_idx, V);
end

%% Computing eigenvectors, eigenvalues, and FA from diffusion tensor and kurtosis
if ~dummy_dki
    switch dummy_algo
        case 0
            [FA,EVAL,EVEC,MSKo2_idx] = acid_dti_manage_eigenvalues(DT2d_ols, MSKo_idx, dummy_cond_strict, dummy_dki, dummy_algo);
        case {1,2,3}
            [FA,EVAL,EVEC,MSKo2_idx] = acid_dti_manage_eigenvalues(DT2d,     MSKo_idx, dummy_cond_strict, dummy_dki, dummy_algo);
    end
else
    switch dummy_algo
        case 0
            [FA,EVAL,EVEC,MSKo2_idx] = acid_dti_manage_eigenvalues(dki_struct.Asym_olsq',MSKo_idx,dummy_cond_strict,dummy_dki,dummy_algo);
        case {3,4}
            [FA,EVAL,EVEC,MSKo2_idx] = acid_dti_manage_eigenvalues(DT2d,MSKo_idx,dummy_cond_strict,dummy_dki,dummy_algo);
    end
end

%% Computing and writing out DTI metrics (including eigenvectors and -values)
if ~dummy_dki
    if dummy_algo == 0 || dummy_algo == 1 || dummy_algo == 2
        DT2d_MSK = DT2d_ols(MSKo2_idx,:);
    else
        DT2d_MSK = DT2d;
    end

    tmp = zeros(dm);
    tmp(MSKo_idx)=1;
    b0 = b0.*tmp;

    [VOut,VLout,VVout] = acid_dti_compute_maps(FA, EVAL, EVEC, DT2d_MSK, MSKo2_idx, bvals, bvecs, VG, '', keyword, ...
        dummy_write_extra, p_out, dummy_algo, dummy_eigensys, dummy_write_tensors, dummy_unit_test, dummy_dki);

elseif dummy_algo == 4
    Asym_olsq_rice_uncertainty = 1;
    VOut = acid_nlls_write_data_Axial_Symmetric_DKI(DT2d, dummy_RBC, VG, MSK, dummy_write_extra, dummy_calculate_uncertainty, ...
        Asym_olsq_rice_uncertainty, p_out, dummy_unit_test);
else
    Dthr = 1e-5;
    [VOut,VLout,VVout] = acid_dti_compute_maps(FA, EVAL, EVEC, DT2d, MSKo2_idx, bvals, bvecs, VG, Dthr, keyword, ...
        dummy_write_extra, p_out, dummy_algo, dummy_eigensys, dummy_write_tensors, dummy_unit_test, dummy_dki);
end


%% Computing fitted signal and model-fit error
% Only for DTI using linearized methods (OLS, WLS, robust fitting). For non-linear
% methods (NLLS), this calculation is directly after the model fitting.
if dummy_algo == 0
    if ~dummy_dki
        acid_dti_compute_error(DT2d_ols, MSK_idx, MSKo, V, VG, bvals, B, idx, dummy_plot_error, dummy_write_error, dummy_write_fitted, p_out, keyword);
    end
elseif ismember(dummy_algo, [1, 2])
    acid_dti_compute_error(DT2d, MSK_idx, MSKo, V, VG, bvals, B, idx, dummy_plot_error, dummy_write_error, dummy_write_fitted, p_out, keyword);
end

%% Writing out the tensor in the Freiburg Tractography Format
if dummy_TFreiburg && dummy_algo ~= 4
    acid_dti_write_freiburgFT(VOut, EVEC, keyword, bvecs, bvals, P(1,:), RM, VLout, VVout);
end

if ~exist('Vweights','var')
    Vweights = [];
end

if dummy_compress == 1
    to_delete = convertCharsToStrings(P_uncomp);
    delete(to_delete)
end

T = toc/60;
T = duration(minutes(T),'format','hh:mm:ss');
disp(['The total time for ' keyword ' was: ' char(T) '.']);
diary off

end