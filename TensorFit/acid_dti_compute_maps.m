function [VOut, VLout, VVout] = acid_dti_compute_maps(FA, EVAL, EVEC, DT, MSK_idx, bvals, bvecs, ...
    VG, Dthr, keyword, dummy_dki_extra, p_out, dummy_algo, dummy_eigen, dummy_write_tensors, dummy_unit_test, dummy_dki)

% =========================================================================
% This function takes the FA values, eigenvectors, and eigenvalues of a
% dMRI dataset and calculates other DTI and DKI indices such as MD, AD, RD, AD-RD,
% b0, and model-fit error. These calculated maps are also saved as nii
% files.
%
% Inputs:
%   FA          - FA map
%   EVAL        - eigenvalues
%   EVEC        - eigenvectors
%   DT          - diffusion tensor
%   b0          - the minimum of bvals
%   MSK_idx     - mask storing the linear indices of voxels to be included, 2D array [num_of_voxels_within_mask x 1]
%   bvals       - b values
%   bvecs       - b vectors
%   VG          - header of the reference volume
%   Dthr        - 
%   keyword     - tag for the 'desc' field in the BIDS-compliant output filename
%   dummy_dki_extra - option for w maps 
%   p_out       - output folder
%   dummy_algo  - type of fitting algorithm
%   dummy_eigen - option for writing out all eigenvectors (0: only first eigenvector is written out, 1: all eigenvectors written out)
%   dummy_write_tensors - option for writing out the tensor elements
%   dummy_unit_test   - option for including the function in the unit test
%   dummy_dki   - option for DKI (0: DTI, 1: DKI)
%
% Outputs:
%   VOut  - 
%   VLout -
%   VVout - 
%
% Created by S.Mohammadi (2011)
% Adapted by G.David
% =========================================================================

% check variables
if ~exist('dummy_w_maps','var')
    dummy_dki_extra = 1;
end

dummy_float = 1;

% Get image dimensions
dm = VG.dim;

%% Compute and write DTI metrics

% FA (the received value in the input has to be divided by 1000)
FA = FA/1000;
tmp = zeros(VG.dim);
tmp(MSK_idx) = FA;
FA = tmp;
VOut = acid_write_vol(FA, VG, p_out, [keyword '-FA'], '_map', '', '', 1);

% FA outliers
MSK_FA_outliers = zeros(size(FA));
MSK_FA_outliers = logical(MSK_FA_outliers);
MSK_FA_outliers(FA>1) = true;
MSK_FA_outliers(FA<0) = true;
acid_write_vol(MSK_FA_outliers, VG, p_out, [keyword '-FA-outliers'], '_map', '', '', 1);

% MD
MD = mean(EVAL,2);
tmp = zeros(VG.dim);
tmp(MSK_idx) = MD;
MD = tmp;
acid_write_vol(MD, VG, p_out, [keyword '-MD'], '_map', '', '', 1);

% AD
AD = EVAL(:,1);
tmp = zeros(VG.dim);
tmp(MSK_idx) = AD;
AD = tmp;
acid_write_vol(AD, VG, p_out, [keyword '-AD'], '_map', '', '', 1);

% RD
RD = (EVAL(:,2)+EVAL(:,3))/2;
tmp = zeros(VG.dim);
tmp(MSK_idx) = RD;
RD = tmp;
acid_write_vol(RD, VG, p_out, [keyword '-RD'], '_map', '', '', 1);

% S0
if ~dummy_dki
    S0_vol = exp(DT(:,7));
    tmp = zeros(VG.dim);
    tmp(MSK_idx) = S0_vol;
    S0_vol = tmp;
    acid_write_vol(S0_vol, VG, p_out, [keyword '-S0'], '', '', '', dummy_float);
end

% Tensor elements for DTI
if (dummy_write_tensors == 1 || dummy_unit_test) && ~isstruct(DT)

        VG_DT = VG;
        VG_DT.dim = [VG.dim 6];
        vol_DT = zeros(VG_DT.dim);
        
        for i = 1:6
            vol_tmp = zeros(VG.dim);
            vol_tmp(MSK_idx) = DT(:,i);
            vol_DT(:,:,:,i) = vol_tmp;
        end
        
        acid_write_vol(vol_DT, VG, p_out, [keyword '-DT'], '_map', '', '', dummy_float);
end

% Eigenvalues and eigenvectors
tmp1 = zeros(dm);
tmp2 = zeros(dm);
tmp3 = zeros(dm);

if dummy_eigen || dummy_unit_test
    
    for i = 1:size(EVAL,2)
        VL = acid_write_vol(EVAL(:,i), VG, p_out, [keyword '-L' num2str(i)], '_map', MSK_idx, '', dummy_float);
        VLout{i} = VL.fname;
    end
    
    EVEC5d  = zeros([dm size(EVEC,2) size(EVEC,3)]);
    for i = 1:size(EVEC,2)
        tmp1(MSK_idx) = EVEC(:,i,1);
        tmp2(MSK_idx) = EVEC(:,i,2);
        tmp3(MSK_idx) = EVEC(:,i,3);
        EVEC5d(:,:,:,i,1) = tmp1;
        EVEC5d(:,:,:,i,2) = tmp2;
        EVEC5d(:,:,:,i,3) = tmp3;    
        VV = acid_write_vol(squeeze(EVEC5d(:,:,:,i,:)), VG, p_out, [keyword '-V' num2str(i)], '_map', '', '', dummy_float);
        VVout{i} = VV.fname;
    end
else
    VLout = '';
    VVout = '';
end

%% Compute and write out DKI metrics
if dummy_dki || isstruct(DT)
    
    if isfield(DT,'Asym_olsq')
        KT = cat(2,DT.Asym_olsq,DT.Asym_X4);
    elseif isfield(DT,'Asym_ad')
        KT = cat(2,DT.Asym_ad,DT.Asym_X4_ad);
    elseif isfield(DT,'Asym_robust')
        KT = cat(2,DT.Asym_robust,DT.AsymX4_robust);
    else
        KT = cat(2,DT(:,1:6),DT(:,8:end));
    end
    
    % This is the updated version(07/08/2020) where the trace term is not multiplied with DK because its already in the signal prediction model 
    [K_para, K_perp, MK, ~, ~, W_mean, MSK2] = acid_nlls_make_kurtosis_27_02_19(KT, Dthr, bvals, bvecs, dummy_algo);
    
    % MK
    MSK1 = (MK > -100 & MK < 100);
    acid_write_vol(MK(MSK1), VG, p_out, [keyword '-MK'], '_map', MSK_idx(MSK2(MSK1)), '', dummy_float);
    
    % AK
    MSK1 = find((abs(K_para))<1e+3);
    acid_write_vol(K_para(MSK1), VG, p_out, [keyword '-AK'], '_map', MSK_idx(MSK2(MSK1)), '', dummy_float);
    
    % RK
    MSK1 = find((abs(K_perp))<1e+3);
    acid_write_vol(K_perp(MSK1), VG, p_out, [keyword '-RK'], '_map', MSK_idx(MSK2(MSK1)), '', dummy_float);
    
    clear ending MSK1;
    
    % "W" maps
    if dummy_dki_extra || dummy_unit_test

        acid_write_vol(W_mean, VG, p_out, [keyword '-MW'], '_map', MSK_idx(MSK2), '', dummy_float);
        
        % Hansen et al, 2017
        Wperp = ( K_perp .* RD(MSK_idx(MSK2)).^2 )./ MD(MSK_idx(MSK2)).^2;
        Wparallel = ( K_para .* AD(MSK_idx(MSK2)).^2 )./ MD(MSK_idx(MSK2)).^2;
        acid_write_vol(Wperp,     VG, p_out, [keyword '-RW'], '_map', MSK_idx(MSK2), '', dummy_float);
        acid_write_vol(Wparallel, VG, p_out, [keyword '-AW'], '_map', MSK_idx(MSK2), '', dummy_float);
    end

    % S0
    if dummy_algo ~= 0
        S0_vol = exp(DT(:,7));
        tmp = zeros(VG.dim);
        tmp(MSK_idx) = S0_vol;
        S0_vol = tmp;
        acid_write_vol(S0_vol, VG, p_out, [keyword '-S0'], '_map', '', '', dummy_float);
    end
    
    if dummy_write_tensors || dummy_unit_test
        
        % Get diffusion tensor
        vol_DT = zeros([VG.dim 6]);       
        for i = 1:6
            vol_tmp = zeros(VG.dim);
            vol_tmp(MSK_idx) = KT(:,i);
            vol_DT(:,:,:,i) = vol_tmp;
        end
        acid_write_vol(vol_DT, VG, p_out, [keyword '-DT'], '_map', '', '', dummy_float);  
        
        % Get kurtosis tensor
        vol_KT = zeros([VG.dim 15]);   
        for i = 7:21
            vol_tmp = zeros(VG.dim);
            vol_tmp(MSK_idx) = KT(:,i);
            vol_KT(:,:,:,i-6) = vol_tmp;
        end
        
        vol_tmp = zeros(VG.dim);
        vol_tmp(MSK_idx) = KT(:,13);
        vol_KT(:,:,:,8) = vol_tmp;
        
        vol_tmp = zeros(VG.dim);
        vol_tmp(MSK_idx) = KT(:,14);
        vol_KT(:,:,:,7) = vol_tmp;
        acid_write_vol(vol_KT, VG, p_out, [keyword '-KT'], '_map', '', '', dummy_float);
    end 
end
end