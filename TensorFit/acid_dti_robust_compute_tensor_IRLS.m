function [DT2d,sigma] = acid_dti_robust_compute_tensor_IRLS(DT2d_ols, V, B, bvals, logS, MSK, MSKo, A1, kmax, A2, sigma, lambda, max_iter, ...
    dummy_plot_weights, dummy_write_weights_WLS, dummy_write_weights_ROB, dummy_algo, dummy_mask, p_out, keyword, dummy_unit_test)

% =========================================================================
% This script obtains the diffusion tensor using a robust tensor fitting
% algorithm. The solution is obtained by an iteratively reweighted least
% squares (IRLS) algorithm. In each iteration, the diffusion tensor is updated
% and fed into the next iteration. The first iteration starts with the
% OLS solution (received in the argument).The iteration is terminated when
% the number of iterations reaches N_iter or if the rms difference between
% the previous and actual tensor elements falls below lambda.
% 
% Inputs:
%   DT2d_ols - diffusion tensor obtained by OLS, used as initial parameters for the IRLS algorithm
%   V        - reference header
%   B        - B matrix (design matrix for the dMRI experiment)
%   bvals    - b values of all N dMRI volumes, [1 x N] array
%   logS     - logarithmic signal within the MSK mask
%   MSK      - 3D binary mask for calculating the weights
%   MSKo     - 3D binary mask mask applied on the output
%   A1       - scaling factor for w1
%   kmax     -
%   A2       - scaling factor for w2
%   sigma    - noise estimate for w3
%   lambda   - Tikhonov regularization parameter
%   max_iter - maximum number of iterations in the IRLS algorithm
%   dummy_plot_weights      - options for plotting the weights
%   dummy_plot_weights_WLS  - options for plotting the weights
%   dummy_plot_weights_ROB  - options for plotting the weights
%   dummy_algo - options for fitting algorithm
%   p_out      - output folder
%   keyword    - tag for the 'desc' field in the BIDS-compliant output filename
%   dummy_unit_test - options for including the function in the unit test
%
% Outputs:
%   DT2d       - diffusion tensor obtained by robust fitting, 2d array [x 7]
%   sigma      - 
%   
% =========================================================================

% DEFINE
thr_DT   = 0;
rms_dDT0 = 1000;

% Getting dimension
dm  = size(MSK); % 3d
dm1 = [dm size(V,1)]; % 4d

% Displaying message
fprintf('\n');
if dummy_algo == 1
    disp('Obtaining the WLS solution: ');
elseif dummy_algo == 2
    disp('Obtaining the robust fitting solution: ');
end

% Creating a struct array, where each struct stores the elements of the DT
% within the given slice (after masking)
DT_sl_robust = [];
for z = 1:dm(3)
    DT_sl_robust = setfield(DT_sl_robust,{z},'slice',zeros([numel(find(MSKo(:,:,z)>0)) size(DT2d_ols,2)]));
end

% Creating a nii object for writing weights
[~,f,~] = spm_fileparts(V(1).fname);
dt      = [spm_type('int16'),spm_platform('bigend')];
     
if (dummy_write_weights_WLS || dummy_write_weights_ROB) && dummy_unit_test
    if (dummy_algo == 1 || dummy_write_weights_WLS == 1) && dummy_unit_test
        Ni = nifti; Ni.mat  = V(1).mat; Ni.mat0 = V(1).mat;
        Ni.descrip = '4d array of weights';
        prefix = [keyword 'WEIGHT'];
        V4D = spm_file_merge(V(1),[prefix f '_4d'],spm_type('int32'));
        V4D.fname = [p_out filesep ];
        Ni.dat = file_array(V4D.fname,dm1,dt,0,1/255,0);
        create(Ni);
        NI = {Ni}; 
    % only for robust fitting
    elseif (dummy_algo == 2 || dummy_write_weights_ROB == 1) && dummy_unit_test   
        Ni = nifti; Ni.mat  = V(1).mat; Ni.mat0 = V(1).mat;
        Ni.descrip = '4d array of weights';
        prefix  = 'weight_';
        V4D     = spm_file_merge(V(1),[prefix f '_4d'],spm_type('int32'));
        Ni.dat  = file_array(V4D.fname,dm1,dt,0,1/255,0);        
        create(Ni);
        
        Ni1 = nifti; Ni1.mat  = V(1).mat; Ni1.mat0 = V(1).mat;
        Ni1.descrip = '4d array of weights';
        prefix  = 'weight1_';
        V4D     = spm_file_merge(V(1),[prefix f '_4d'],spm_type('int16'));
        Ni1.dat = file_array(V4D.fname,dm1,dt,0,1/255,0);        
        create(Ni1);

        Ni2 = nifti; Ni2.mat  = V(1).mat; Ni2.mat0 = V(1).mat;
        Ni2.descrip = '4d array of weights';
        prefix  = 'weight2_';
        V4D     = spm_file_merge(V(1),[prefix f '_4d'],spm_type('int16'));
        Ni2.dat = file_array(V4D.fname,dm1,dt,0,1/255,0);        
        create(Ni2);
        
        Ni3 = nifti; Ni3.mat  = V(1).mat; Ni3.mat0 = V(1).mat;
        Ni3.descrip = '4d array of weights';
        prefix  = 'weight3_';
        V4D     = spm_file_merge(V(1),[prefix f '_4d'],spm_type('int32'));
        Ni3.dat  = file_array(V4D.fname,dm1,dt,0,1/255,0);        
        create(Ni3);
        
        NI = {Ni;Ni1;Ni2;Ni3};
    end   
end

% Initializing figure for plotting
if dummy_plot_weights
    F1 = figure;     
else
    F1 = [];
end

counter = 1;

for z = 1:dm(3) % go through each slice
    
    fprintf('slice: %d',z);
    fprintf('\n');
    
    % 2D mask and its linear indices in the given slice
    MSK_sl      = MSK(:,:,z);
    MSK_sl_idx  = find(MSK_sl>0);   
    MSKo_sl     = MSKo(:,:,z);
    MSKo_sl_idx = find(MSKo_sl>0);
    
    % log(S) and DT in the given slice
    logS_sl = logS(counter:(counter+sum(sum(MSKo_sl))-1),:);
    DT_ols_sl = DT2d_ols(counter:(counter+sum(sum(MSKo_sl))-1),:);
    
    counter = counter+sum(sum(MSKo_sl));
    
    iter = 1;        
    rms_dDT = rms_dDT0;    
    
    while (rms_dDT > 1E-6 && iter < max_iter+1)

        % initial tensor in the iteration. At the first iteration, it is
        % the ols solution (received in the input). Later, it is the actual
        % tensor (returned by the previous iteration step).
        if iter==1
            DT_sl_init = DT_ols_sl;
        else
            DT_sl_init = DT_sl;
        end
        
        % iterating of the diffusion tensor, IRLS
        %   DT_tmp      - actual iteration of the tensor
        %   w           - combined weight (to iterate)
        %   w1          - weight 1
        %   w2          - weight 2
        %   w3          - weight 3
        [DT_sl,w,w1,w2,w3,F1] = acid_dti_robust_estimate_Zwiers2010(DT_sl_init, logS_sl, MSK_sl, MSK_sl_idx, MSKo_sl, B, bvals, V, A1, A2, lambda, sigma, kmax, z, F1);
            
        DAsym0 = zeros([dm(1)*dm(2) size(DT_ols_sl,2)]);
        DAsym1 = zeros([dm(1)*dm(2) size(DT_ols_sl,2)]);
        
        if dummy_mask
            DAsym0(MSK_sl>0,:) = DT_ols_sl - DT_sl; % difference between ols and actual solution
            DAsym1(MSK_sl>0,:) = DT_sl_init - DT_sl; % difference between previous and actual solution
        else
            DAsym0(MSK_sl>0,:) = DT_ols_sl(MSK_sl>0,:) - DT_sl(MSK_sl>0,:); % difference between ols and actual solution
            DAsym1(MSK_sl>0,:) = DT_sl_init(MSK_sl>0,:) - DT_sl(MSK_sl>0,:); % difference between previous and actual solution
        end
        
        % find those voxels where the rms difference between ols and actual solution 
        % (mean across the seven tensor elements) is higher than thr_DT
        MSK2 = find(sqrt(mean(DAsym0(MSKo_sl>0,:).^2,2))>thr_DT);

        % rms difference between previous and actual solution 
        % (mean across the seven tensor elements and all voxels within MSK)
        if ~isempty(MSK2)
            rms_dDT = sqrt(mean(mean(DAsym1(MSKo_sl_idx(MSK2),1:6).^2,2)));
        else
            rms_dDT = 0;
        end

        if (dummy_algo==2 && numel(logS_sl)~=0)
            disp([num2str(iter) '.iteration:    ' 'difference = ' num2str(rms_dDT)]);
        end
        
        iter = iter+1;
        
    end
    
    if(dummy_algo==2 && (iter-1 == max_iter))
        warning('The iteratively reweighted least squares method has not converged!')
    end
    
    % Storing final tensor
    DT_sl_robust = setfield(DT_sl_robust,{z},'slice',DT_sl);
    
    if (dummy_write_weights_WLS || dummy_write_weights_ROB) && dummy_unit_test
        if (dummy_algo == 1 || dummy_write_weights_WLS == 1) && dummy_unit_test
            W = {w};
            tmp = zeros(dm1(1)*dm1(2),dm1(4));
            tmp((MSKo_sl>0),:) = reshape(W{1},size(logS_sl));
            tmp = reshape(tmp,dm1([1:2 4]));
            volW = zeros([dm1(1:2) 1 dm1(4)]);
            volW(:,:,1,:) = tmp;
            
            spm_progress_bar('Init',dm(3),NI{1}.descrip,'volumes completed');
            NI{1}.dat(:,:,z,:) = single(volW);
            spm_progress_bar('Set',z);
            clear tmp volW;
            
        % for robust fitting
        elseif (dummy_algo == 2 || dummy_write_weights_ROB == 1) && dummy_unit_test 
            W = {w;w1;w2;w3};
            for n = 1:4
                tmp = zeros(dm1(1)*dm1(2),dm1(4));
                tmp(MSKo_sl>0,:) = reshape(W{n},size(logS_sl));
                tmp = reshape(tmp,dm1([1:2 4]));
                volW = zeros([dm1(1:2) 1 dm1(4)]);
                volW(:,:,1,:) = tmp;
                
                spm_progress_bar('Init',dm(3),NI{n}.descrip,'volumes completed');
                NI{n}.dat(:,:,z,:) = single(volW);
                spm_progress_bar('Set',z);
                clear tmp volW;
            end
        end
        
        spm_progress_bar('Clear');
        clear tmp volW;
    end
end

if dummy_plot_weights
    close(F1)
end            

% Linearization of DT_robust, [number of voxels within MSK x 7]
DT2d = zeros(size(DT2d_ols));
for i = 1:size(DT2d_ols,2)
    tmp = zeros(dm);
    for z = 1:dm(3)
        tmp1 = zeros(dm(1:2));
        tmp1((MSKo(:,:,z)>0)) = DT_sl_robust(z).slice(:,i);
        tmp(:,:,z) = tmp1;
    end
    DT2d(:,i) = tmp(:);
end

fprintf('\n');

end