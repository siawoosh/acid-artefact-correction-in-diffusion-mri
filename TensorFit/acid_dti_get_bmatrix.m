function [B,Binv] = acid_dti_get_bmatrix(bvecs,bvals)

% ========================================================================
% This function creates the B matrix from the gradient directions and
% corresponding b-values. The B-matrix is a kind of design matrix
% (determined by experimental factors) in the DTI experiment and is used
% to solve the equation:
% ln(S)=B*D+e
%   ln(S): logarithm of the measured signal, Nx1
%   B: design matrix, Nx7
%   D: diffusion coefficients, 7x1
%
% Input:
%   - bvecs: gradient directions (3xN array)
%   - bvals: b-values (1xN array)
%
% Output:
%   - B: B-matrix
%   - Binv: inverse of the B-matrix
%
% Created by S. Mohammadi 08/01/2012
% ========================================================================

% check arguments
dm = size(bvecs);
if dm(1)~=3
    disp(dm(1))
    error('Invalid dimensions of gradient vectors!');
end
if size(bvals,1) ~=1
    disp(size(bvals,1))
    error('Invalid dimensions of gradient vectors!');
end

% define function that creates B matrix
DD = @(x,y,z,bvalue) (bsxfun(@times,[-x.*x; -y.*y; -z.*z; -2.*x.*y; -2.*x.*z; -2.*y.*z],bvalue)) ;
% MSK = find((bvecs(1,:).*bvecs(1,:)+bvecs(2,:).*bvecs(2,:)+bvecs(3,:).*bvecs(3,:))>0);

% if(numel(MSK)~=dm)
%     nbvalue = bvals; %/max(bvalue(MSK));
% else
%     nbvalue = bvals; %/max(bvalue);
% end

% create B matrix and its inverse
B  = [DD(bvecs(1,:),bvecs(2,:),bvecs(3,:),bvals); ones(1,numel(bvals))];
B  = B';
Binv = inv((B')*B)*B';

return
end
