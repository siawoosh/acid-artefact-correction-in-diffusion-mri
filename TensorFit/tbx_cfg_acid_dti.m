function [dti_fit,dki_fit] = tbx_cfg_acid_dti(dummy_dki, mask, bvals_bvecs, in_vols)

% dummy_DKI
dummy_DKI      = cfg_menu;
dummy_DKI.tag  = 'dummy_DKI';
dummy_DKI.name = 'DTI/DKI';
dummy_DKI.help = {'If the DKI option is on, the output artguments will additionally include:'
    '- mean  (MK), perpendicular (Kper), parralel (Kpar) kurtosis, and axonal water fraction (AWF)'
    'The constrained ols Kurtosis fitting model has been used in Mohammadi et al., Front. Neurosci., 2015 and is based on the method presented in Tabesh et al., MRM, 2011.'
    'Please cite Mohammadi et al., Front. Neurosci., 2015 and Tabesh et al., MRM, 2011 when using the Kurtosis code.'};
if dummy_dki
    dummy_DKI.labels = {'DKI'}';
    dummy_DKI.values = {1};
    dummy_DKI.val = {1};
else
    dummy_DKI.labels = {'DTI'}';
    dummy_DKI.values = {0};
    dummy_DKI.val = {0};
end

% dummy for writing out W maps
dummy_write_W        = cfg_menu;
dummy_write_W.tag    = 'dummy_write_W';
dummy_write_W.name   = 'Write W maps';
dummy_write_W.help   = {'This option allows you to additionally write out the "W" kurtosis maps based on the kurtosis tensor.'};
dummy_write_W.labels = {
    'NO'
    'YES'
    }';
dummy_write_W.values = {0 1};
dummy_write_W.val    = {0};

% dummy for writing out K maps
dummy_write_K        = cfg_menu;
dummy_write_K.tag    = 'dummy_write_K';
dummy_write_K.name   = 'Write K maps';
dummy_write_K.help   = {'This option allows you to write out the "K" kurtosis maps, also referred to as the "apparent" kurtosis parameters which are based on the measured, apparent kurtosis along a combination of diffusion gradient vectors.'};
dummy_write_K.labels = {
    'NO'
    'YES'
    }';
dummy_write_K.values = {0 1};
dummy_write_K.val    = {0};

% sigma
sigma_file         = cfg_files;
sigma_file.tag     = 'sigma_file';
sigma_file.name    = 'File (*.txt)';
sigma_file.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_file.filter  = 'any';
sigma_file.ufilter = '.*txt';
sigma_file.num     = [0 1];

sigma_exp         = cfg_entry;
sigma_exp.tag     = 'sigma_exp';
sigma_exp.name    = 'Expression/Dependency';
sigma_exp.help    = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_exp.strtype = 'e';
sigma_exp.num     = [Inf Inf];

% noise map
sigma_map         = cfg_files;
sigma_map.tag     = 'sigma_map';
sigma_map.name    = 'Noise map';
sigma_map.help    = {'Select a map containing a noise estimate (sigma) for every image voxel.'};
sigma_map.filter  = 'image';
sigma_map.ufilter = '.*';
sigma_map.val     = {{''}};
sigma_map.num     = [0 1];

sigma_type        = cfg_choice;
sigma_type.tag    = 'sigma_type';
sigma_type.name   = 'Noise estimate';
sigma_type.help   = {'Needs to be provided by background estimator or similar. Standard deviation of noise distribution.'};
sigma_type.values = {sigma_exp, sigma_file, sigma_map};
sigma_type.val    = {sigma_exp};

% % effective number of coils
% n_coils         = cfg_entry;
% n_coils.tag     = 'n_coils';
% n_coils.name    = 'Effective number of coils';
% n_coils.help    = {'Effective number of receiver coils.'};
% n_coils.strtype = 'e';
% n_coils.num     = [1 1];
% n_coils.val     = {1};



% dummy_RBC = OFF
dummy_RBC_OFF        = cfg_menu;
dummy_RBC_OFF.tag    = 'dummy_RBC_OFF';
dummy_RBC_OFF.name   = 'Rician bias correction';
dummy_RBC_OFF.help   = {''};
dummy_RBC_OFF.labels = {'OFF'}';
dummy_RBC_OFF.values = {1};
dummy_RBC_OFF.val    = {1};

RBC_OFF      = cfg_branch;
RBC_OFF.tag  = 'RBC_OFF';
RBC_OFF.name = 'OFF';
RBC_OFF.help = {''};
RBC_OFF.val  = {dummy_RBC_OFF};

% dummy_RBC = ON
dummy_RBC_ON        = cfg_menu;
dummy_RBC_ON.tag    = 'dummy_RBC_ON';
dummy_RBC_ON.name   = 'Rician bias correction';
dummy_RBC_ON.help   = {''};
dummy_RBC_ON.labels = {'ON'}';
dummy_RBC_ON.values = {1};
dummy_RBC_ON.val    = {1};

RBC_ON      = cfg_branch;
RBC_ON.tag  = 'RBC_ON';
RBC_ON.name = 'ON';
RBC_ON.help = {'Settings for Rician bias correction'};
RBC_ON.val  = {dummy_RBC_ON, sigma_type};

%% DEFAULTS FOR OLS

% fitting method
dummy_ols        = cfg_menu;
dummy_ols.tag    = 'dummy_ols';
dummy_ols.name   = 'Fitting method';
dummy_ols.help   = {''};
dummy_ols.labels = {'OLS'}';
dummy_ols.values = {1};
dummy_ols.val    = {1};

ols      = cfg_branch;
ols.tag  = 'ols';
ols.name = 'OLS';
ols.help = {'PLS'};
if ~dummy_dki
    ols.val  = {dummy_ols};
else
    ols.val  = {dummy_ols, dummy_write_W};
end

%% DEFAULTS FOR WLS

% fitting method
dummy_wls        = cfg_menu;
dummy_wls.tag    = 'dummy_wls';
dummy_wls.name   = 'Fitting method';
dummy_wls.help   = {'If the DKI option is on, the output artguments will additionally include:'};
dummy_wls.labels = {'WLS'}';
dummy_wls.values = {1};
dummy_wls.val    = {1};

wls      = cfg_branch;
wls.tag  = 'wls';
wls.name = 'WLS';
wls.help = {'WLS'};
wls.val  = {dummy_wls, sigma_type};

%% DEFAULTS FOR ROBUST FITTING

% fitting method
dummy_rob        = cfg_menu;
dummy_rob.tag    = 'dummy_rob';
dummy_rob.name   = 'Fitting method';
dummy_rob.help   = {'If the DKI option is on, the output artguments will additionally include:'};
dummy_rob.labels = {'Robust fitting'}';
dummy_rob.values = {1};
dummy_rob.val    = {1};

rob      = cfg_branch;
rob.tag  = 'rob';
rob.name = 'Robust Fitting';
rob.help = {'Robust Fitting'};
rob.val  = {dummy_rob, sigma_type};

%% DEFAULTS FOR NLLS

% fitting method
dummy_nlls        = cfg_menu;
dummy_nlls.tag    = 'dummy_nlls';
dummy_nlls.name   = 'Fitting method';
dummy_nlls.help   = {'If the DKI option is on, the output artguments will additionally include:'};
dummy_nlls.labels = {'NLLS'}';
dummy_nlls.values = {1};
dummy_nlls.val    = {1};

% dummy for Rician bias correction
dummy_RBC        = cfg_choice;
dummy_RBC.tag    = 'dummy_RBC';
dummy_RBC.name   = 'Rician bias correction';
dummy_RBC.help   = {'This option allows you to activate/deactivate Rician bias correction on model parameter estimation.'};
dummy_RBC.values = {RBC_OFF, RBC_ON};
dummy_RBC.val    = {RBC_OFF};

% number of workers for parallel programming
n_workers         = cfg_entry;
n_workers.tag     = 'n_workers';
n_workers.name    = 'Number of workers (parallel programming)';
n_workers.help    = {'Specify number of workers for parallel computation. A value of 1 indicates that no parallel computing is used. A value above 1 requires the Parallel Computing Toolbox.'};
n_workers.strtype = 'e';
n_workers.num     = [1 1];
n_workers.val     = {1};

% defauls for non-linear least squares (Gauss Newton)
nlls      = cfg_branch;
nlls.tag  = 'nlls';
nlls.name = 'NLLS';
nlls.help = {'NLLS'};
if ~dummy_dki
    nlls.val  = {dummy_nlls, dummy_RBC, n_workers};
else
    nlls.val  = {dummy_nlls, dummy_RBC, dummy_write_W, n_workers};
end

% defauls for non-linear least squares (Gauss Newton)
nlls_axDKI      = cfg_branch;
nlls_axDKI.tag  = 'nlls_axDKI';
nlls_axDKI.name = 'NLLS - Axisymmetric DKI';
nlls_axDKI.help = {'Axisymmetric DKI'};
nlls_axDKI.val  = {dummy_nlls, dummy_RBC, dummy_write_K, n_workers};

dummy_algo      = cfg_choice;
dummy_algo.tag  = 'dummy_algo';
dummy_algo.name = 'Fitting method';
dummy_algo.help = {'Choose tensor fitting method.'
    'Note that robust fitting has long computing times. Therefore, we recommend using a ROI mask to speed up the process (for an example batch see wiki page: https://bitbucket.org/siawoosh/acid-artefact-correction-in-diffusion-mri/wiki/TensorFitting%20Wiki).'
    'At least 10 GB memory is required for high-resolution and/or HARDI data.'};
if ~dummy_dki
    dummy_algo.values = {ols, wls, rob, nlls};
else
    dummy_algo.values = {ols, nlls, nlls_axDKI};
end
dummy_algo.val = {ols};

%% Exbranch: DTI
dti_fit         = cfg_exbranch;
dti_fit.tag     = 'dti_fit';
dti_fit.name    = 'DTI';
dti_fit.val     = {dummy_DKI, in_vols, bvals_bvecs, dummy_algo, mask};
dti_fit.help    = { 'Tensor estimation algorithm uses either ordinary least squares or robust tensor fitting.'
    'If the DKI option is of, the output artguments are:'
    '- various diffusion tensor indices: axial diffusivity (Axial_), radial diffusivity (Radial_), mean diffusitivty (MD_), axial - radial diffusivity (AD_RD_), fractional anisotropy (FA_)'
    '- other diffusion tensor estimates: root-mean square of tensor-fit error (RES_), estimate of b=0 image (b0_), mean of diffusion weigted images (meanDWI_), eigenvalues (EVAL_), eigenvectors (EVEC_)'
    'The robust fitting model has been used in Mohammadi et al., MRM, 2012 and is a modified version of the method presented in  Zwiers et al., Neuroimage 2010 and Mangin et al., Med Image Anal 2002.'
    'Please cite Mohammadi et al., MRM 2012 (doi: 10.1002/mrm.24467) and Zwiers et al., Neuroimage 2010 when using this code.'
    'If the DKI option is on, the output artguments will additionally include:'
    '- mean  (MK), perpendicular (Kper), parralel (Kpar) kurtosis, and axonal water fraction (AWF)'
    'The Kurtosis fitting model has been used in Mohammadi et al., Front. Neurosci., 2015 and is based on the method presented in  Tabesh et al., MRM, 2011.'
    'Please cite Mohammadi et al., Front. Neurosci., 2015 and Tabesh et al., MRM, 2011 when using the Kurtosis code.'
    };
dti_fit.prog = @local_dtifit;
dti_fit.vout = @vout_dtifit;

%% Exbranch: DKI
dki_fit         = cfg_exbranch;
dki_fit.tag     = 'dki_fit';
dki_fit.name    = 'DKI';
dki_fit.val     = {dummy_DKI, in_vols, bvals_bvecs, dummy_algo, mask};
dki_fit.help    = { 'Tensor estimation algorithm uses either ordinary least squares or robust tensor fitting.'
    'If the DKI option is of, the output artguments are:'
    '- various diffusion tensor indices: axial diffusivity (Axial_), radial diffusivity (Radial_), mean diffusitivty (MD_), axial - radial diffusivity (AD_RD_), fractional anisotropy (FA_)'
    '- other diffusion tensor estimates: root-mean square of tensor-fit error (RES_), estimate of b=0 image (b0_), mean of diffusion weigted images (meanDWI_), eigenvalues (EVAL_), eigenvectors (EVEC_)'
    'The robust fitting model has been used in Mohammadi et al., MRM, 2012 and is a modified version of the method presented in  Zwiers et al., Neuroimage 2010 and Mangin et al., Med Image Anal 2002.'
    'Please cite Mohammadi et al., MRM 2012 (doi: 10.1002/mrm.24467) and Zwiers et al., Neuroimage 2010 when using this code.'
    'If the DKI option is on, the output artguments will additionally include:'
    '- mean  (MK), perpendicular (Kper), parralel (Kpar) kurtosis, and axonal water fraction (AWF)'
    'The Kurtosis fitting model has been used in Mohammadi et al., Front. Neurosci., 2015 and is based on the method presented in  Tabesh et al., MRM, 2011.'
    'Please cite Mohammadi et al., Front. Neurosci., 2015 and Tabesh et al., MRM, 2011 when using the Kurtosis code.'
    };
dki_fit.prog = @local_dtifit;
dki_fit.vout = @vout_dtifit;

end

function out = local_dtifit(job)

    max_iter_GN                 = acid_get_defaults('diffusion.max_iter_GN');
    A1                          = acid_get_defaults('diffusion.A1');
    A2                          = acid_get_defaults('diffusion.A2');
    rmatrix                     = acid_get_defaults('diffusion.rmatrix');
    dummy_plot_error            = acid_get_defaults('diffusion.dummy_plot_error');
    dummy_write_error           = acid_get_defaults('diffusion.dummy_write_error');
    dummy_error_per_shell       = acid_get_defaults('diffusion.dummy_error_per_shell');
    dummy_write_fitted          = acid_get_defaults('diffusion.dummy_write_fitted');
    dummy_mask                  = acid_get_defaults('diffusion.dummy_mask');
    dummy_cond_strict           = acid_get_defaults('diffusion.dummy_cond_strict');
    dummy_eigensys              = acid_get_defaults('diffusion.dummy_eigensys');
    dummy_write_tensors         = acid_get_defaults('diffusion.dummy_write_tensors');
    dummy_write_Weights_ROBUST  = acid_get_defaults('diffusion.dummy_weights_ROBUST');
    dummy_write_Weights_WLS     = acid_get_defaults('diffusion.dummy_weights_WLS');
    dummy_calculate_uncertainty = acid_get_defaults('diffusion.dummy_calculate_uncertainty');
    dummy_plot_weights          = acid_get_defaults('diffusion.dummy_plot');
    thr_cond                    = acid_get_defaults('diffusion.thr_cond');
    lambda                      = acid_get_defaults('diffusion.thr_DTvar');
    kmax                        = acid_get_defaults('diffusion.kmax');
    dummy_Tfreiburg             = acid_get_defaults('diffusion.dummy_write_TFreiburg');
    max_iter                    = acid_get_defaults('diffusion.Niter');
    map_constraints             = [];

    % read in bvals and bvecs
    if isfield(job.bvals_bvecs,'bvals_bvecs_file')

        job.files1 = job.bvals_bvecs.bvals_bvecs_file{1,1};
        job.files2 = job.bvals_bvecs.bvals_bvecs_file{2,1};
        [~,~,e] = fileparts(job.files1);

        if ~isempty(job.files1)
            switch lower(e)
                case '.mat'
                    if size(cell2mat(struct2cell(load(job.files1))),1) == 1 && size(cell2mat(struct2cell(load(job.files2))),1) == 3
                        job.bvals = cell2mat(struct2cell(load(job.files1)));
                        job.bvecs = cell2mat(struct2cell(load(job.files2)));
                    elseif size(cell2mat(struct2cell(load(job.files1))),1) == 3 && size(cell2mat(struct2cell(load(job.files2))),1) == 1
                        job.bvecs = cell2mat(struct2cell(load(job.files1)));
                        job.bvals = cell2mat(struct2cell(load(job.files2)));
                    else
                        error('Unexpected file extension!')
                    end

                case '.nii'
                    error('Unexpected file extension!')
                otherwise
                    if size(dlmread(job.files1),1) == 1 && size(dlmread(job.files2),1) == 3
                        job.bvals = dlmread(job.files1);
                        job.bvecs = dlmread(job.files2);
                    elseif size(dlmread(job.files1),1) == 3 && size(dlmread(job.files2),1) == 1
                        job.bvecs = dlmread(job.files1);
                        job.bvals = dlmread(job.files2);
                    else
                        error('Unexpected file extension!')
                    end
            end
        else
            job.bvals = '';
            job.bvecs = '';
        end

    elseif isfield(job.bvals_bvecs,'bvals_bvecs_exp_type')

        if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvals_exp')
            if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp)
                job.bvals = job.bvals_bvecs.bvals_bvecs_exp_type.bvals_exp;
            else
                job.bvals = '';
            end
        end

        if isfield(job.bvals_bvecs.bvals_bvecs_exp_type,'bvecs_exp')
            if ~isempty(job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp)
                job.bvecs = job.bvals_bvecs.bvals_bvecs_exp_type.bvecs_exp;
            else
                job.bvecs = '';
            end
        end
    end

    % read in sigma (noise estimate)
    if isfield(job.dummy_algo, 'wls') && isfield(job.dummy_algo.wls, 'sigma_type')
        if isfield(job.dummy_algo.wls.sigma_type, 'sigma_exp')
            sigma = job.dummy_algo.wls.sigma_type.sigma_exp;
        elseif isfield(job.dummy_algo.wls.sigma_type, 'sigma_file')
            if ~isempty(job.dummy_algo.wls.sigma_type.sigma_file{1})
                sigma = job.dummy_algo.wls.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        end
    elseif isfield(job.dummy_algo, 'rob') && isfield(job.dummy_algo.rob, 'sigma_type')
        if isfield(job.dummy_algo.rob.sigma_type, 'sigma_exp')
            sigma = job.dummy_algo.rob.sigma_type.sigma_exp;
        elseif isfield(job.dummy_algo.rob.sigma_type, 'sigma_file')
            if ~isempty(job.dummy_algo.rob.sigma_type.sigma_file{1})
                sigma = job.dummy_algo.rob.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);   
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        end
    elseif isfield(job.dummy_algo, 'nlls') && isfield(job.dummy_algo.nlls.dummy_RBC, 'RBC_ON') && isfield(job.dummy_algo.nlls.dummy_RBC.RBC_ON, 'sigma_type')
        if isfield(job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type, 'sigma_exp')
            sigma = job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type.sigma_exp;
        elseif isfield(job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type, 'sigma_file')
            if ~isempty(job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type.sigma_file{1})
                sigma = job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);   
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        elseif isfield(job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type, 'sigma_map')
            sigma = job.dummy_algo.nlls.dummy_RBC.RBC_ON.sigma_type.sigma_map{1};
        end
    elseif isfield(job.dummy_algo, 'nlls_axDKI') && isfield(job.dummy_algo.nlls_axDKI.dummy_RBC, 'RBC_ON') && isfield(job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON, 'sigma_type')
        if isfield(job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type, 'sigma_exp')
            sigma = job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type.sigma_exp;
        elseif isfield(job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type, 'sigma_file')
            if ~isempty(job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type.sigma_file{1})
                sigma = job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type.sigma_file{1};
                [~,~,e] = fileparts(sigma);   
                switch lower(e)
                    case '.mat'
                        sigma = cell2mat(struct2cell(load(sigma)));
                    case '.nii'
                        error('Unexpected file extension!')
                    otherwise
                        sigma = dlmread(sigma);
                end
            else
                sigma = '';
            end
        elseif isfield(job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type, 'sigma_map')
            sigma = job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.sigma_type.sigma_map{1};
        end
    else
        sigma = [];
    end
    
    % dummy_algo
    if isfield(job.dummy_algo, 'ols')
        dummy_algo = 0;
    elseif isfield(job.dummy_algo, 'wls')
        dummy_algo = 1;
    elseif isfield(job.dummy_algo, 'rob')
        dummy_algo = 2;
    elseif isfield(job.dummy_algo, 'nlls')
        dummy_algo = 3;
    elseif isfield(job.dummy_algo, 'nlls_axDKI')
        dummy_algo = 4;
    end

    % dummy_write_extra
    if job.dummy_DKI
        if isfield(job.dummy_algo, 'ols')
            dummy_write_extra = job.dummy_algo.ols.dummy_write_W;
        elseif isfield(job.dummy_algo, 'nlls')
            dummy_write_extra = job.dummy_algo.nlls.dummy_write_W;
        elseif isfield(job.dummy_algo, 'nlls_axDKI')
            dummy_write_extra = job.dummy_algo.nlls_axDKI.dummy_write_K;
        end
    else
        dummy_write_extra = 0;
    end

    if iscell(job.in_vols)
        if contains(char(job.in_vols{1}),'ACID_Test')
            dummy_unit_test = 1;
        else
            dummy_unit_test = 0;
        end
    elseif contains(char(job.in_vols),'ACID_Test')
        dummy_unit_test = 1;
    else
        dummy_unit_test = 0;
    end
    
    % read out parameters from NLLS or NLLS_axDKI
    if isfield(job.dummy_algo, 'nlls')
        
        n_workers = job.dummy_algo.nlls.n_workers;
        
        if isfield(job.dummy_algo.nlls.dummy_RBC, 'RBC_ON')
            dummy_RBC = 1;
        else
            dummy_RBC = 0;
        end
        
        if isfield(job.dummy_algo.nlls.dummy_RBC, 'RBC_ON')
            % map_noise = job.dummy_algo.nlls.dummy_RBC.RBC_ON.map_noise;      
            % n_coils   = job.dummy_algo.nlls.dummy_RBC.RBC_ON.n_coils;
              n_coils   = 1;
        else
            % map_noise = [];     
             n_coils = [];
        end
        
    elseif isfield(job.dummy_algo, 'nlls_axDKI')
        
        n_workers = job.dummy_algo.nlls_axDKI.n_workers;
        
        if isfield(job.dummy_algo.nlls_axDKI.dummy_RBC, 'RBC_ON')
            dummy_RBC = 1;
        else
            dummy_RBC = 0;
        end
        
        if isfield(job.dummy_algo.nlls_axDKI.dummy_RBC, 'RBC_ON')
            % map_noise = job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.map_noise; 
            % n_coils   = job.dummy_algo.nlls_axDKI.dummy_RBC.RBC_ON.n_coils;
            n_coils   = 1;
        else
            % map_noise = [];     
            n_coils = [];
        end
        
    else
        n_workers = 1;
        dummy_RBC = 0;
        % map_noise = [];     
        n_coils = [];
    end
    

    % run DTI/DKI
    [Vweights, VG] = acid_dti(char(job.in_vols), job.bvals, job.bvecs, dummy_algo, char(job.mask), ...
        rmatrix, dummy_mask, dummy_write_fitted, ...
        dummy_write_error, dummy_write_extra, dummy_Tfreiburg, dummy_plot_error, ...
        dummy_write_Weights_WLS, dummy_plot_weights, A1, kmax, A2, ...
        sigma, lambda, max_iter, job.dummy_DKI, thr_cond, ...
        dummy_RBC, n_coils, n_workers, map_constraints, ...
        max_iter_GN, dummy_cond_strict, dummy_eigensys, dummy_write_tensors, ...
        dummy_write_Weights_ROBUST, dummy_calculate_uncertainty, dummy_unit_test, dummy_error_per_shell);

    out.files = job.in_vols(:);
   
    [p,f,~] = fileparts(VG(1).fname);
    out.FA = {[p filesep f(1:end-6) 'FA_map.nii,1']};
    out.MD = {[p filesep f(1:end-6) 'MD_map.nii,1']};
    out.AD = {[p filesep f(1:end-6) 'AD_map.nii,1']};
    out.RD = {[p filesep f(1:end-6) 'RD_map.nii,1']};
    
    if dummy_algo ~= 5
        out.L1 = {[p filesep f(1:end-6) 'L1_map.nii,1']};
        out.L2 = {[p filesep f(1:end-6) 'L2_map.nii,1']};
        out.L3 = {[p filesep f(1:end-6) 'L3_map.nii,1']};
    elseif dummy_algo == 5
        out.L1 = {[p filesep f(1:end-6) 'AD_map.nii,1']};
        out.L2 = {[p filesep f(1:end-6) 'RD_map.nii,1']};
        out.L3 = {[p filesep f(1:end-6) 'RD_map.nii,1']};
    end    
    
    if job.dummy_DKI == 1
        
        if isfield(job.dummy_algo, 'ols') || isfield(job.dummy_algo, 'nlls')

            out.MK = {[p filesep f(1:end-6) 'MK_map.nii,1']};
            out.AK = {[p filesep f(1:end-6) 'AK_map.nii,1']};
            out.RK = {[p filesep f(1:end-6) 'RK_map.nii,1']};

            if dummy_write_extra
                out.MW = {[p filesep f(1:end-6) 'MW_map.nii,1']};
                out.AW = {[p filesep f(1:end-6) 'AW_map.nii,1']};
                out.RW = {[p filesep f(1:end-6) 'RW_map.nii,1']};
            end

            if dummy_write_tensors
                out.DT = {[p filesep f(1:end-6) 'DT_map.nii,1']};
                out.KT = {[p filesep f(1:end-6) 'KT_map.nii,1']};
            end
        end
        
        if isfield(job.dummy_algo, 'nlls_axDKI')
            
            out.MW = {[p filesep f(1:end-6) 'MW_map.nii,1']};
            out.AW = {[p filesep f(1:end-6) 'AW_map.nii,1']};
            out.RW = {[p filesep f(1:end-6) 'RW_map.nii,1']};

            if dummy_write_extra
                out.MK = {[p filesep f(1:end-6) 'MK_map.nii,1']};
                out.AK = {[p filesep f(1:end-6) 'AK_map.nii,1']};
                out.RK = {[p filesep f(1:end-6) 'RK_map.nii,1']};
            end  
        end
        
    end

    if ~isempty(Vweights)
        out.wfiles = {Vweights(1).fname};
        out.wfiles = acid_select(out.wfiles(:));
        out.wfiles = out.wfiles(:);
    else
        out.wfiles = [];
    end

end

function dep = vout_dtifit(job)

        dummy_write_tensors = acid_get_defaults('diffusion.dummy_write_tensors');

    if ~isfield(job,'dummy_write_W')
        job.dummy_write_W = 0;
    end
    
    kk = 1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Input images';
    dep(kk).src_output = substruct('.','files');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'FA map';
    dep(kk).src_output = substruct('.','FA');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'MD map';
    dep(kk).src_output = substruct('.','MD');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'AD map';
    dep(kk).src_output = substruct('.','AD');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'RD map';
    dep(kk).src_output = substruct('.','RD');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'First eigenvalue map';
    dep(kk).src_output = substruct('.','L1');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Second eigenvalue map';
    dep(kk).src_output = substruct('.','L2');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    kk = kk+1;
    dep(kk)            = cfg_dep;
    dep(kk).sname      = 'Third eigenvalue map';
    dep(kk).src_output = substruct('.','L3');
    dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

    % dep(6)            = cfg_dep;
    % dep(6).sname      = 'b0 map';
    % dep(6).src_output = substruct('.','b0files');
    % dep(6).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    % dep(7)            = cfg_dep;
    % dep(7).sname      = 'RES map';
    % dep(7).src_output = substruct('.','RESfiles');
    % dep(7).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    % dep(8)            = cfg_dep;
    % dep(8).sname      = 'DWI map';
    % dep(8).src_output = substruct('.','DWIfiles');
    % dep(8).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    % dep(9)            = cfg_dep;
    % dep(9).sname      = 'Weight images';
    % dep(9).src_output = substruct('.','wfiles');
    % dep(9).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    % dep(10)            = cfg_dep;
    % dep(10).sname      = 'HARDI-mat file from Freiburg Fibertools';
    % dep(10).src_output = substruct('.','HARDIfiles');
    % dep(10).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});

    if job.dummy_DKI == 1

        if isfield(job.dummy_algo, 'ols') || isfield(job.dummy_algo, 'nlls')

            if isfield(job.dummy_algo, 'nlls')
                dummy_write_extra = job.dummy_algo.nlls.dummy_write_W;
            elseif isfield(job.dummy_algo, 'ols')
                dummy_write_extra = job.dummy_algo.ols.dummy_write_W;
            end

                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'MK map';
                dep(kk).src_output = substruct('.','MK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'AK map';
                dep(kk).src_output = substruct('.','AK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'RK map';
                dep(kk).src_output = substruct('.','RK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

            if dummy_write_extra
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'MW map';
                dep(kk).src_output = substruct('.','MW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'AW map';
                dep(kk).src_output = substruct('.','AW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'RW map';
                dep(kk).src_output = substruct('.','RW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
            end

            if dummy_write_tensors
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'DT map';
                dep(kk).src_output = substruct('.','DT');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'KT map';
                dep(kk).src_output = substruct('.','KT');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
            end
        end
        
        if isfield(job.dummy_algo, 'nlls_axDKI')

                dummy_write_extra = job.dummy_algo.nlls_axDKI.dummy_write_K;

                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'MW map';
                dep(kk).src_output = substruct('.','MW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'AW map';
                dep(kk).src_output = substruct('.','AW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'RW map';
                dep(kk).src_output = substruct('.','RW');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

            if dummy_write_extra

                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'MK map';
                dep(kk).src_output = substruct('.','MK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'AK map';
                dep(kk).src_output = substruct('.','AK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
                kk = kk+1;
                dep(kk)            = cfg_dep;
                dep(kk).sname      = 'RK map';
                dep(kk).src_output = substruct('.','RK');
                dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
            end  
        end


  

    end
end