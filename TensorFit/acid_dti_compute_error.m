function acid_dti_compute_error(DT2d, MSK_idx, MSKo, V, VG, bvals, B, idx, ...
    dummy_plot_error, dummy_write_error, dummy_write_fitted, p_out, keyword)

% =========================================================================
% The script computes and writes out the model-fit error, defined as the
% difference between the measured and fitted signal. Model-fit error
% quantifies the quality of fitting, and high model-fit error might
% indicate an artifactual data point.
%
% IMPORTANT: In the current implementation, the errors derived from the ols
% fit are written out (even if robust fitting was used).
%
% Inputs:
%   DT2d        - diffusion tensor for each voxel (within mask), 2D array [num_of_voxels x 7]
%   MSK_idx     - mask in which model-fit error is plotted for QC purposes
%   MSKo        - mask applied for the output
%   V           - headers of the dMRI dataset
%   VG          - header of the reference volume
%   bvals       - b values of the dMRI dataset, 2D array [1 x num_of_volumes]
%   B           - B matrix (design matrix for dMRI experiment)
%   idx         - binary array indicating the volume indices for each shell
%   dummy_plot_error   - option for plotting model-fit error
%   dummy_write_error  - option for writing out model-fit error
%   dummy_write_fitted - option for writing out fitted signal
%   dummy_log          - o ption for writing out log or not log RMSE
%   path_out           - output folder
%   keyword            - tag for the 'desc' field in BIDS-compliant output
%   dummy_unit_test    - option for including this function in the unit test
%
% Created by G.David
% Adapted by B.Fricke
% =========================================================================

% Getting image dimensions
dm = VG.dim;
idx_dw = [];
for k = 2:numel(idx)
    idx_dw = cat(2,idx_dw,idx{k});
end

% Initialization
Error_4d    = zeros([dm numel(V)]);
Error_Log_4d = zeros([dm numel(V)]);
Sfit_4d        = zeros([dm numel(V)]);
SfitLog_4d     = zeros([dm numel(V)]);
RMSE = zeros(dm);
MAE  = zeros(dm);
MeAE = zeros(dm);
RMSE_Log  = zeros(dm);
MAE_Log  = zeros(dm);
MeAE_Log = zeros(dm);

for p = 1:dm(3)      
    % Calculating model-fit error
    % Note that the model-fit error can be defined in two different ways: 
    % 1) Error = S_meas - S_fit
    % 2) ErrorLog = log(S_meas) - log(S_fit)
    % Accordingly, two arrays are defined, of size [num_of_voxels_within_slice, num_of_dw_vols]
    % resError    - model-fit error of the signal in a given slice for all volumes, abs(S_meas - S_fit),
    % resErrorlog - model-fit error of the logarithmic signal in a given slice for all volumes, abs(log(S_meas)-log(S_fit))
    % Both variables are 2D arrays 
    [Error, Error_Log, Sfit, SfitLog] = acid_dti_compute_error_core(DT2d,V,VG,bvals,B,dm,p,0);
        
    % Storing model-fit errors and fitted signals in 4D files
    Error_4d(:,:,p,:)     = reshape(Error,[dm(1:2) numel(V)]);
    Error_Log_4d(:,:,p,:) = reshape(Error_Log,[dm(1:2) numel(V)]);
    Sfit_4d(:,:,p,:)      = reshape(Sfit,[dm(1:2) numel(V)]);
    SfitLog_4d(:,:,p,:)   = reshape(SfitLog,[dm(1:2) numel(V)]);
        
    % Computing root-mean-square of the model-fit error per voxel
    RMSE(:,:,p)     = reshape(sqrt(mean(min(Error.*Error,1e10),2)),dm(1:2));
    RMSE_Log(:,:,p) = reshape(sqrt(mean(min(Error_Log.*Error_Log,1e10),2)),dm(1:2));

    % Computing mean absolute displacement (MAE) of the model-fit error per voxel
    MAE(:,:,p)     = reshape(mean(abs(Error),2),dm(1:2));
    MAE_Log(:,:,p) = reshape(mean(abs(Error_Log),2),dm(1:2));

    % Computing median (MeAE) of the absolute model-fit error per voxel
    MeAE(:,:,p)     = reshape(median(abs(Error),2),dm(1:2));
    MeAE_Log(:,:,p) = reshape(median(abs(Error_Log),2),dm(1:2));
end

% Normalised max-norm of tensor fit error
% mresDT0 = max(abs(resDT0),[],2)./sqrt(mean(resDT0.^2,2));

%% Writing out RMS model-fit error

keyword_RMS      = '-RMSE';
keyword_RMSE_Log = '-RMSE-Log';

% Specifying image dimensions
VG.dim = dm(1:3);
VG.n   = [1,1];
VG.dt  = [64, 0];

% Masking
RMSE = RMSE.*MSKo;
RMSE_Log = RMSE_Log.*MSKo;

% Writing
acid_write_vol(RMSE,     VG, p_out, [keyword keyword_RMS],      '_map', '', '', 1);
acid_write_vol(RMSE_Log, VG, p_out, [keyword keyword_RMSE_Log], '_map', '', '', 1);

%% Optional: Writing out MAE and MeAE maps
if dummy_write_error

    keyword_MAE      = '-MAE';
    keyword_MeAE     = '-MeAE';
    keyword_MAE_Log  = '-MAE-Log';
    keyword_MeAE_Log = '-MeAE-Log';
    
    MAE  = MAE.*MSKo;
    MeAE = MeAE.*MSKo;
    MAE_Log  = MAE_Log.*MSKo;
    MeAE_Log = MeAE_Log.*MSKo;
    
    acid_write_vol(MAE,      VG, p_out, [keyword keyword_MAE],      '_map', '', '', 1);
    acid_write_vol(MeAE,     VG, p_out, [keyword keyword_MeAE],     '_map', '', '', 1);
    acid_write_vol(MAE_Log,  VG, p_out, [keyword keyword_MAE_Log],  '_map', '', '', 1);
    acid_write_vol(MeAE_Log, VG, p_out, [keyword keyword_MeAE_Log], '_map', '', '', 1);

end

%% Optional: Writing out 4D model-fit error
if dummy_write_error

    keyword_error     = '-Err';
    keyword_error_log = '-Err-Log';

    % Specifying image dimensions
    VG.dim = [dm, numel(V)];
    VG.n   = [numel(V), 1];
    VG.dt  = [64, 0];

    % Masking
    Error_4d    = Error_4d.*repmat(MSKo,[1,1,1,numel(V)]);
    Error_Log_4d = Error_Log_4d.*repmat(MSKo,[1,1,1,numel(V)]);

    % Writing
    acid_write_vol(Error_4d, VG, p_out, [keyword keyword_error], '_map', '', '', 1);
    acid_write_vol(Error_Log_4d, VG, p_out, [keyword keyword_error_log], '_map', '', '', 1);

end

%% Optional: Writing out fitted signal
if dummy_write_fitted

    keyword_Sfit    = '-SFit';
    keyword_SfitLog = '-SFit-Log';

    % Getting dimensions
    VG.dim = [dm, numel(V)];
    VG.n   = [numel(V), 1];
    VG.dt  = [64, 0];

    % Masking
    Sfit_4d    = Sfit_4d.*repmat(MSKo,[1,1,1,numel(V)]);
    SfitLog_4d = SfitLog_4d.*repmat(MSKo,[1,1,1,numel(V)]);

    % Writing
    acid_write_vol(Sfit_4d, VG, p_out, [keyword keyword_Sfit], '_map', '', '', 1);
    acid_write_vol(SfitLog_4d, VG, p_out, [keyword keyword_SfitLog], '_map', '', '', 1);
end

%% Optional: Creating diagnostic plots on model-fit error
if dummy_plot_error
    acid_dti_plot_error(Error_Log_4d, MSK_idx, p_out, VG, keyword);
end

end