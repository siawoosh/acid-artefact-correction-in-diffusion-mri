The provided parameter maps (each map contains 5 identical voxels) were computed from synthetic MRI data and based upon a ground truth dataset taken from Coelho et al. (DOI: 10.1002/mrm.27714). 
The provided diffusivity maps are in um²/ms, the biophysical output parameter maps should be: Kappa=8.00, f=0.73, Da=2.00 um²/ms, De_perp=0.30 um²/ms and De_par=1.00 um²/ms.
