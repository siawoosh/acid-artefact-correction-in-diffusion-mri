function [bp_1, bp_2, bp_3, bp_4, bp_5, mask_good_vals, mask] = acid_wmti_watson_fit_parameters_and_compute_results(...
D_para, D_perp, W_mean, W_para, W_perp, npool, dummy_convertW2K, mask, branch, array_p2, array_p4)

% =========================================================================
%
% Inputs:
%   D_para -
%   D_perp -
%   W_mean -
%   W_para -
%   W_perp -
%   npool  -
%   dummy_convertW2K -
%   mask     - 
%   brach    -
%   array_p2 -
%   array_p4 -
%
% Outputs:
%   bp_1 - Kappa
%   bp_2 - De,perpendicular
%   bp_3 - Da
%   bp_4 - De,parallel
%   bp_5 - f
% =========================================================================

D_perp = D_perp*1e3;
D_para = D_para*1e3;

if isempty(mask)
    mask = find(abs(sum(cat(4, D_perp, D_para, W_mean, W_perp, W_para),4,'omitnan'))>1e-6);
end

D_perp = D_perp(mask);
D_para = D_para(mask);
D0     = 1/3.*(2.*D_perp+D_para);

W_mean = W_mean(mask);
W_perp  = W_perp(mask);
W_para = W_para(mask);

if dummy_convertW2K
    W_perp = W_perp.*( (D_perp.^2)./ (D0.^2) );
    W_para = W_para.*( (D_para.^2)./ (D0.^2) );
end

D2 = 2/3.*(D_para- D_perp);
W0 = W_mean;
W2 = 1/7.*(3.*W_para+5.*W_mean-8.*W_perp);
W4 = 4/7.*(W_para-3*W_mean+2*W_perp);

c1 = 3.*D0;
c2 = (3/2).*D2;
c3 = D2.^2+5.*D0.^2.*(1+W0./3);
c4 = 1/2.*D2.*(D2+7.*D0)+(7/12*W2.*D0.^2);
c5 = 9/4*D2.^2+35/24*W4.*D0.^2.;

sz = numel(D0);

%  plot_objective_function_to_explore(D_perp,D_para,W_mean,W_para,W_perp,branch)

%% start parallel computing

npool = acid_multicore(npool);
if npool == 1
    M = 0;
else
    M = npool;
end


% M = npool;

% if (exist('parpool','file'))&&(npool>1)
%     if isempty(gcp('nocreate'))
%         parpool('local',npool);
%         M = npool;
%     else
%         poolobj = gcp('nocreate');
%         M = min(poolobj.NumWorkers,npool);
%     end
% else
%     M = 0;
% end

D = parallel.pool.DataQueue;
afterEach(D, @nUpdateWaitbar);
spm_progress_bar('Init', 100, 'Biophysical Parameter Estimation', 'Progress [%]');
number_of_voxels_to_fit = numel(c1);
p = 1;

parfor (inx = 1:numel(c1), M)
    if mod(inx,5000) == 0
        send(D, inx);
    end

    exactness = 1000;
    [cX{inx}] = acid_wmti_watson_minimize_objective_function(array_p2,array_p4,c1(inx),c2(inx),c3(inx),c4(inx),c5(inx),0,50,exactness,branch);
end

% if (M > 0)
%     poolobj = gcp('nocreate');
%     delete(poolobj);
% end

MSK_good_f_vals = false(1,nnz(mask));
X = zeros(5,sz);

% the following has to be done because there are sometimes multiple kappa values
% if the objective function is sampled (dummy_Fit_Method == 3)
for o = 1:numel(cX)
    if isempty(cX{o})
        cX{o} = NaN;
    else
        tmp=cX{o};
        tmp=tmp(1);
        cX{o}=tmp;
    end
end

for inx = 1:length(cX)

    if or( isempty(cX{inx}) , (isnan(cX{inx})))
        X(1,inx) = NaN;
        X(2,inx) = NaN;  % D_perp
        X(3,inx) = NaN ; % D_a
        X(4,inx) = NaN;  % D_para
        X(5,inx) = NaN;
    else
        X(1,inx) = cX{inx};

        p4in(inx) = p4_numeric(X(1,inx),array_p4,0,70,100000);%(1/(32.*((X(1,inx)).^2))*(105+(12.*(X(1,inx))).*(5+(X(1,inx)))+(5.*sqrt((X(1,inx))).*(2.*(X(1,inx))-21))./(dawson(sqrt((X(1,inx)))))));
        p2in(inx) = p2_numeric(X(1,inx),array_p2,0,70,100000);%(0.25.*((3./(sqrt((X(1,inx))).*dawson(sqrt((X(1,inx))))))-2-3./(X(1,inx))));

        M1(inx) = c1(inx);
        M2(inx) = c2(inx)./p2in(inx);
        M3(inx) = c3(inx);
        M4(inx) = c4(inx)./p2in(inx);

        a(inx) = ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) )^2- ( (7/3) + 2*M2(inx)./ ( (1/3).*(M1(inx)-M2(inx)) )) .* ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) + M4(inx) ./ ((1/9).* (M1(inx)-M2(inx))^2);
        c(inx)=   ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) - 5 - M2(inx) ./ ( (1/3) .* (M1(inx)- M2(inx)) ) ) .^2;
        f1(inx) = ( (-40 ./ 3) + a(inx) +c(inx)- ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));
        f2(inx) = ( (-40 ./ 3) + a(inx) +c(inx)+ ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));

        if branch == 1
            f(inx)=f2(inx);
        else
            f(inx)=f1(inx);
        end

        D_bar(inx) =  ( (1/3) * (M1(inx)-M2(inx)) ) ;

        X(2,inx) = D_bar(inx) ./ (1-f(inx)); % D_perpe
        X(3,inx) = ( ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ).* (1-f(inx))-5- ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) ) ./ (-f(inx)) ) .* D_bar(inx) ; % D_a
        X(4,inx) = ( ( ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) - f(inx).* ( X(3,inx)./D_bar(inx) ) ) ./ (1-f(inx)) ) .* D_bar(inx) + X(2,inx); % D_parae
        X(5,inx) = f(inx);

        if branch ==1 % these conditions are to make sure 0 =< f =< 1
            if(a(inx)<(40/3)|| 0>(1/3)*(3*a(inx)-4* sqrt(30) * sqrt(a(inx)) +40 ) -c(inx)  )
                MSK_good_f_vals(inx)= false;
            else
                MSK_good_f_vals(inx) = true;
            end
        elseif branch == 2
            if(   a(inx)<(40/3)|| c(inx) <= 0 || 0>(1/3)*(3*a(inx)-4* sqrt(30) * sqrt(a(inx)) +40 ) -c(inx) )

                MSK_good_f_vals(inx)= false;
            else
                MSK_good_f_vals(inx) = true;
            end
        end
    end
end

bp_1 = X(1,:);
bp_2 = X(2,:) .* 1e-3;
bp_3 = X(3,:) .* 1e-3;
bp_4 = X(4,:) .* 1e-3;
bp_5 = X(5,:);

MSK_tmp = find(imag(bp_1) == 0);
MSK_good_kappa_vals = zeros(size(bp_1));
MSK_good_kappa_vals(MSK_tmp) = 1;
MSK_good_kappa_vals = logical(MSK_good_kappa_vals);
mask_good_vals       = MSK_good_kappa_vals;

for inx_msk_good_vals = 1: size(MSK_good_kappa_vals,2)
    if MSK_good_kappa_vals(inx_msk_good_vals) == true && MSK_good_f_vals(inx_msk_good_vals) == true
        mask_good_vals(inx_msk_good_vals)  = true;
    else
        mask_good_vals(inx_msk_good_vals)  = false;
    end
end

function nUpdateWaitbar(~)
    spm_progress_bar('Set', 100*(p*5000)/number_of_voxels_to_fit);
    p = p + 1;
end

end