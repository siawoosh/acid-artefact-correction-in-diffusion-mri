function [cX] = acid_wmti_watson_minimize_objective_function(array_p2,array_p4,c1,c2,c3,c4,c5,lb,ub,n,branch)

kappa = linspace(lb,ub,n);
i = 1;

for j = 1:n

    p4in = p4_numeric(kappa(j),array_p4,0,70,100000); % (1/(32.*((kappa(j)).^2))*(105+(12.*(kappa(j))).*(5+(kappa(j)))+(5.*sqrt((kappa(j))).*(2.*(kappa(j))-21))./(dawson_behnam(sqrt((kappa(j)))))));
    p2in = p2_numeric(kappa(j),array_p2,0,70,100000); %(0.25.*((3./(sqrt((kappa(j))).*dawson_behnam(sqrt((kappa(j))))))-2-3./(kappa(j)))); %

    M1_= c1;
    M2_= c2./p2in;
    M3_= c3;
    M4_= c4./p2in;

    M1 = M1_(i,1);
    M2 = M2_(i,1);
    M3 = M3_(i,1);
    M4 = M4_(i,1);

    a(i,1) = ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) )^2- ( (7/3) + 2*M2./ ( (1/3).*(M1-M2) )) .* ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ) + M4 ./ ((1/9).* (M1-M2)^2);
    c(i,1) = ( ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ) - 5 - M2 ./ ( (1/3) .* (M1- M2) ) ) ^2;

    % assuming Da> De_parallel /corresponds to eta = +1 in jespersen et al 2018 (diffusion time dependence...) and ceta = +1 in novikov et al 2018 (Rotationally invariant...)
    if branch == 1
        f = ( (-40 / 3) + a(i,1) +c(i,1) + ( -4.*a(i,1).*c(i,1)+( (40/3)-a(i,1)-c(i,1) )^2)^0.5) ./ (2.*a(i,1));
    elseif branch == 2
        f = ( (-40 / 3) + a(i,1) +c(i,1) - ( -4.*a(i,1).*c(i,1)+( (40/3)-a(i,1)-c(i,1) )^2)^0.5) ./ (2.*a(i,1));
    end

    D_bar =  ( (1/3) * (M1-M2) ) ;
    per_De = D_bar ./ (1-f);
    d_a = ( ( ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ).* (1-f)-5- ( M2 ./ ( (1/3).*(M1-M2))) ) ./ (-f) ) ;
    D_a = d_a .* D_bar ;
    para_De = ( ( ( M2 ./ ( (1/3).*(M1-M2))) - f.* d_a ) ./ (1-f) ) .* D_bar + per_De;

    F(j) =  (p4in.*(f.*D_a.^2+(1-f).*(para_De-per_De).^2))-c5 ;

    if imag(F(j))==0
        % these conditions are to make sure 0 =< f =< 1
        if branch ==1
            if(  a(i,1)<(40/3)|| 0>(1/3)*(3*a(i,1)-4* sqrt(30) * sqrt(a(i,1)) +40 ) -c(i,1)  )
                Objective(j)=NaN;
            else
                Objective(j) = F(j);
            end
        elseif branch == 2
            if(  a(i,1)<(40/3)|| c(i,1) <= 0 || 0>(1/3)*(3*a(i,1)-4* sqrt(30) * sqrt(a(i,1)) +40 ) -c(i,1) )
                Objective(j)=NaN;
            else
                Objective(j) = F(j);
            end
        end
    else
        Objective(j)=NaN;
    end
end

Objective_2 = Objective .* Objective ;
index_minimum = find(Objective_2 == min(Objective_2));
cX = kappa(index_minimum);

if isempty(cX)
    cX = NaN;
end

end