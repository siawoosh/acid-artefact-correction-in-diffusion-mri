function acid_biophysical_params = tbx_cfg_acid_wmti_watson

%% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%% Input
in_all         = cfg_files;
in_all.tag     = 'in_all';
in_all.name    = 'Maps (AD, RD, MW/MK, AW/AK, RW/RK) (order is mandatory)';
in_all.help    = {'Select the AD, RD, MW/MK, AW/AK, RW/RK maps (order is mandatory).'};
in_all.filter  = 'image';
in_all.ufilter = '.*';
in_all.num     = [5 5];

% Region of interest
mask         = cfg_files;
mask.tag     = 'mask';
mask.name    = 'Region of interest';
mask.help    = {'Recommended to provide a white matter mask as the model has been developed for the white matter.'};
mask.filter  = 'image';
mask.ufilter = '.*';
mask.num     = [0 1];
mask.val     = {{''}};

% number of workers for parallel programming (npools)
npool         = cfg_entry;
npool.tag     = 'npool';
npool.name    = 'Number of workers (parallel programming)';
npool.help    = {'Specify number of workers for parallel computation. A value of 1 indicates that no parallel computing is used. A value above 1 requires the Parallel Computing Toolbox.'};
npool.strtype = 'e';
npool.num     = [1 1];
npool.val     = {1};

% conversion of K to W
dummy_convertW2K        = cfg_menu;
dummy_convertW2K.tag    = 'dummy_convertW2K';
dummy_convertW2K.name   = 'Type of input maps';
dummy_convertW2K.help   = {'The kurtosis maps you have loaded above have to be the ones starting with "W" (kurtosis tensor variables). This is a switch to internally change "AK" and "RK" (="apparent kurtosis" variables) to "AW" and "RW" (="kurtosis tensor" variables). If your input for the axial and radial kurtosis maps above was "AK" and "RK" choose "YES", if your input was "AW" and "RW" choose "NO". (Check doi:10.1016/j.neuroimage.2016.08.022 for more details on W and K).'};
dummy_convertW2K.labels = {
    'AW/RW'
    'AK/RK'
    }';
dummy_convertW2K.values = {0 1};
dummy_convertW2K.val    = {0};

% Branch
dummy_branch        = cfg_menu;
dummy_branch.tag    = 'dummy_branch';
dummy_branch.name   = 'Biophysical branch';
dummy_branch.help   = {'Choose the biophysical parameter branch (plus or minus) you would like to estimate.'};
dummy_branch.labels = {
    'PLUS'
    'MINUS'
    }';
dummy_branch.values = {1 2};
dummy_branch.val    = {1};

%% noddi dti
acid_biophysical_params      = cfg_exbranch;
acid_biophysical_params.tag  = 'acid_biophysical_parameters';
acid_biophysical_params.name = 'WMTI-WATSON';
acid_biophysical_params.val  = {in_all dummy_convertW2K mask dummy_branch npool};
acid_biophysical_params.help = {
                    'This function computes the biophysical parameters KAPPA (axon alignment), AWF (axonal water fraction), DA (intra-axonal diffusivity), DE-PER (perpendicular extra-axonal diffusivity) and DE-PARA (parallel extra-axonal diffusivity) based on the axisymmetric DKI tensor metrics.'
                    'The axisymmetric DKI tensor metrics can be obtained by both the axisymmetric DKI fit, as well as the standard DKI fit where they are calculated as aggregate, for example, the perpendicular diffusivity can be calculated as (lambda2+lambda3)/2, where lambda are the diffusion tensor?s eigenvalues. Additionally to the biophysical parameters, an outlier (OUTLIERS) map will be computed that encodes voxels with 0 if the condition 0=<AWF=<1 is not met or if KAPPA has a non-zero imaginary part.'
                    'The estimation is based upon the work of Jespersen et al. (2018)(doi: http://dx.doi.org/10.1016/j.neuroimage.2017.08.039) and Novikov et al. (2018) (https://doi.org/10.1016/j.neuroimage.2018.03.006).'
};
acid_biophysical_params.prog = @local_acid_biophysical_params;
acid_biophysical_params.vout = @vout_acid_biophysical_params;

end

function out = local_acid_biophysical_params(job)
    [Vkappa,VAWF,Vodi] = acid_wmti_watson_main(char(job.in_all), job.npool, job.dummy_convertW2K, char(job.mask), job.dummy_branch);
    out.kappa = {Vkappa.fname};
    out.AWF  = {VAWF.fname};
    out.odi  = {Vodi.fname};
end

function dep = vout_acid_biophysical_params(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Kappa image';
    dep(1).src_output = substruct('.','kappa');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'ODI image';
    dep(2).src_output = substruct('.','odi');
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'AWF image';
    dep(3).src_output = substruct('.','AWF');
    dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end