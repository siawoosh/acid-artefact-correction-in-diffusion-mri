function [kappa_image,AWF_image,odi_image] = acid_wmti_watson_main(images_struct, npool, dummy_convertW2K, mask, branch)

% =========================================================================

% Inputs:
%   images_struct    -
%   npool            -
%   dummy_convertW2K -
%   mas              -
%   branch           -
%   p_out            -
%
% Output:
%   out - 
%
% =========================================================================
tic
load('array_p2.mat');
load('array_p4.mat');

% load in headers
V1 = spm_vol(images_struct(1,:));
V2 = spm_vol(images_struct(2,:));
V3 = spm_vol(images_struct(3,:));
V4 = spm_vol(images_struct(4,:));
V5 = spm_vol(images_struct(5,:));

% load in images
VG = V1(1);
D_para = acid_read_vols(V1,VG,1);
D_perp = acid_read_vols(V2,VG,1);
W_mean = acid_read_vols(V3,VG,1);
W_para = acid_read_vols(V4,VG,1);
W_perp = acid_read_vols(V5,VG,1);

% define output folder
if branch == 1
    keyword = 'WMTI-WATSON-PLUS';
elseif branch == 2
    keyword = 'WMTI-WATSON-MINUS';
end
[path,fname,~] = spm_fileparts(V1(1).fname);

p_out = acid_bids(path,fname,keyword,1);

diary([p_out filesep 'logfile_' keyword '.txt'])

% load in mask
if ~isempty(mask)
    MSK_Vol = spm_vol(mask);
    mask = acid_read_vols(MSK_Vol,VG,1);
    mask = logical(mask);
end

% compute biophysical parameters
[kappa, De_perp, Da, De_par, f, mask_good_vals, mask] = acid_wmti_watson_fit_parameters_and_compute_results(D_para, D_perp, W_mean,  W_para, W_perp, ...
    npool, dummy_convertW2K, mask, branch, array_p2, array_p4);

% write out maps
dummy_float = 1;
Atmp = zeros(VG.dim);
Atmp(mask) = De_perp;
VG.fname = [VG.fname(1:end-11) '_map.nii'];
acid_write_vol(Atmp, VG, p_out, [keyword '-DE-PERP'], '_map', '', '', dummy_float);

Atmp(mask) = Da;
acid_write_vol(Atmp, VG, p_out, [keyword '-DA'], '_map', '', '', dummy_float);

Atmp(mask) = kappa;
kappa_image = acid_write_vol(Atmp, VG, p_out, [keyword '-KAPPA'], '_map', '', '', dummy_float);

% Atmp(mask) = kappa;
Atmp = acid_kappa_to_odi(kappa,mask,VG);
odi_image = acid_write_vol(Atmp, VG, p_out, [keyword '-ODI'], '_map', '', '', dummy_float);

Atmp(mask) = De_par;
acid_write_vol(Atmp, VG, p_out, [keyword '-DE-PARA'], '_map', '', '', dummy_float);

Atmp(mask) = f;
AWF_image = acid_write_vol(Atmp, VG, p_out, [keyword '-AWF'], '_map', '', '', dummy_float);

Atmp(mask) = mask_good_vals;
acid_write_vol(Atmp, VG, p_out, [keyword '-outliers'], '_map', '', '', dummy_float);

T = toc/60;

T = duration(minutes(T),'format','hh:mm:ss');
disp(['The total time for ' keyword ' was: ' char(T) '.']);
diary off
end