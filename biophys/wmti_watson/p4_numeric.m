function dout = p4_numeric(k, array, min_xvec, max_xvec, length_array)
    % 
    % min_xvec = 0; % min of xvec
    % max_xvec = 70; %max of xvec
    % length_array = 100000; %length of array

    if k == 35
        inx = 50000;
    elseif k == 0
        inx = 2;
    else
        inx = 1+round ((length_array-1) * (( k -min_xvec) /(max_xvec-min_xvec)));
    end

    dout = array(inx);
end