function odi_map = acid_kappa_to_odi(kappa_map,mask,VG)
% Convert to κ to ODI using Equation (1) in Mollink et al., 2017

% written: B.Fricke (19/04/2024)
    odi_map = zeros(VG.dim);
    odi_map(mask) = 2/pi .* atan(1./kappa_map);

end