function tau = acid_noddidti_MDFA2tau(MD, FA, iso, dummy_invivo,D_a,D_iso)

% =========================================================================
% Maps DTI MD and FA values to tau, a measure of dispersion,
% using a relationship discovered via cumulant expansion of
% the NODDI signal model.
%
% luke.edwards@ucl.ac.uk
% add option to have ex vivo diffusivities
% S.Mohammadi 25/05/2020
% =========================================================================

    [d,diso] = acid_noddidti_fixedParams(dummy_invivo,D_a,D_iso);
    icvf = acid_noddidti_MD2icvf(MD,iso,[],[],[],[],[],dummy_invivo,D_a,D_iso);

    % from Mathematica
    if all(iso(:)==0)
        % N.B. eps in denominator prevents division by zero
        tau = 1/3*(1+4*FA.*(MD./abs(d-MD))./sqrt(3-2*FA.^2));

    else
        tau = (1/3).*(1+2.*d.^(-1).*FA.*((3+(-2).*FA.^2) ...
            .*((-2)+icvf).^2.*icvf.^2).^(-1/2).*((-1)+iso).^(-1)...
            .*(d.*(3+2.*((-2)+icvf).*icvf).*((-1)+iso)+(-3).*diso.*iso));
    end

    errtau = (real(tau)<1/3)|(real(tau)>1)|(abs(imag(tau))>1e-10)|isnan(icvf)|isnan(tau);
    tau(errtau) = -3;
    errFA = (FA<0)|(FA>1);
    tau(errFA) = -2;

    errMD=(MD<0);
    tau(errMD)=-1;

end