function a = acid_noddidti_dawson(x)

% =========================================================================
% Computes Dawson's function.
%
% luke.edwards@ucl.ac.uk
% =========================================================================
    
    a = exp(-x.^2).*erfi(x)*sqrt(pi)/2;

end
