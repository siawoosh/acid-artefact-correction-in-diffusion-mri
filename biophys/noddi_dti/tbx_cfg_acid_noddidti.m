function acid_noddidti = tbx_cfg_acid_noddidti


% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

% FA 
FA         = cfg_files;
FA.tag     = 'FA';
FA.name    = 'FA map';
FA.help    = {'Select FA map.'};
FA.filter  = 'image';
FA.ufilter = '.*';
FA.num     = [0 1];

% MD
MD         = cfg_files;
MD.tag     = 'MD';
MD.name    = 'MD map';
MD.help    = {'Select MD map. Note that a bias in MD will also bias the icvf value. Details can be found in Edwards et al., 2017.'};
MD.filter  = 'image';
MD.ufilter = '.*';
MD.num     = [0 1];

% EVALS
EVALS         = cfg_files;
EVALS.tag     = 'EVALS';
EVALS.name    = 'Eigenvalues (3 maps)';
EVALS.help    = {'The eigenvalues are used for the heuristic bias correction of the MD map (details see Edwards et al., 2017).'};
EVALS.filter  = 'image';
EVALS.ufilter = '.*';
EVALS.num     = [0 3];

% Region of interest
mask         = cfg_files;
mask.tag     = 'mask';
mask.name    = 'Region of interest';
mask.help    = {'Recommended to provide a white matter mask as the model has been developed for the white matter.'};
mask.filter  = 'image';
mask.ufilter = '.*';
mask.num     = [0 1];
mask.val     = {{''}};

% dummy_invivo
dummy_invivo        = cfg_menu;
dummy_invivo.tag    = 'dummy_invivo';
dummy_invivo.name   = 'Define fixed diffusivity D_a';
dummy_invivo.help   = {'The fixed diffusivities from NODDI are used also in NODDI-DTI. This flag defines whether the in vivo or ex vivo diffusivities are used (in vivo: D_a = 1.7e-3 mm^2/s; ex vivo: D_a = 0.6e-3 mm^2/s). '};
dummy_invivo.labels = {
               'In vivo'
               'Ex vivo'
               'Manual definition'
}';
dummy_invivo.values = {1 0 2};
dummy_invivo.val    = {1};









dummy_invivo_exp         = cfg_menu;
dummy_invivo_exp.tag     = 'dummy_invivo_exp';
dummy_invivo_exp.name    = 'Choose predefined option';
dummy_invivo_exp.help    = {
    'The values of the predefined sets are (in vivo: D_a = 1.7e-3 mm^2/s, D_iso = 3.0e-3 mm^2/s; ex vivo: D_a = 0.6e-3 mm^2/s, D_iso = 2.0e-3 mm^2/s'};
dummy_invivo_exp.labels = {'Ex vivo', 'In vivo'};
dummy_invivo_exp.values  = {0 1};
dummy_invivo_exp.val     = {1};

dummy_invivo_manual_exp         = cfg_entry;
dummy_invivo_manual_exp.tag     = 'dummy_invivo_manual_exp';
dummy_invivo_manual_exp.name    = 'Manual expression (left D_a, right D_iso)';
dummy_invivo_manual_exp.help    = {'Define D_a (left) and D_iso (right) values (in mm^2/s) manually. The values of the predefined sets are (in vivo: D_a = 1.7e-3 mm^2/s, D_iso = 3.0e-3 mm^2/s; ex vivo: D_a = 0.6e-3 mm^2/s, D_iso = 2.0e-3 mm^2/s'};
dummy_invivo_manual_exp.strtype = 'e';
dummy_invivo_manual_exp.num     = [1 2];
dummy_invivo_manual_exp.val     = {[1e-3 1e-3]};

dummy_invivo_type        = cfg_choice;
dummy_invivo_type.tag    = 'dummy_invivo_type';
dummy_invivo_type.name   = 'Define fixed diffusivity D_a and D_iso';
dummy_invivo_type.help   = {'The fixed diffusivities from NODDI are used also in NODDI-DTI. This flag defines whether the in vivo or ex vivo diffusivities are used (in vivo: D_a = 1.7e-3 mm^2/s, D_iso = 3.0e-3 mm^2/s; ex vivo: D_a = 0.6e-3 mm^2/s, D_iso = 2.0e-3 mm^2/s). '};
dummy_invivo_type.values = {dummy_invivo_exp, dummy_invivo_manual_exp};
% dummy_invivo_type.val    = {dummy_invivo_exp};
















% b-values
bvals         = cfg_entry;
bvals.tag     = 'bvals';
bvals.name    = 'b-values';
bvals.help    = {'Provide a b-value for the heuristic kurtosis bias correction of the MD map. If a set of b-values is provided (e.g., via dependency) the highest b-value is selected.'};
bvals.strtype = 'e';
bvals.num     = [0 Inf];
bvals.val     = {''};


% b-values
bvals_dki         = cfg_entry;
bvals_dki.tag     = 'bvals_dki';
bvals_dki.name    = 'No b-values have to be set!';
bvals_dki.help    = {''};
bvals_dki.strtype = 'e';
bvals_dki.num     = [0 0];
bvals_dki.val     = {''};

dummy_dti         = cfg_branch;
dummy_dti.tag     = 'dummy_dti';
dummy_dti.name    = 'DTI';
% bvals_bvecs_exp_type.help    = {'Specify the b-values (bval) and b-vectors of the N dMRI volumes.'
%     'Note: The expression should contain a 4 x N vector, where b-values should appear in the same order as the source images were entered (b-values: 1 x N, b-vectors: (2-4) x N). b-values are expected in units of s/mm^2.'};
dummy_dti.val  = {bvals};

dummy_dki         = cfg_branch;
dummy_dki.tag     = 'dummy_dki';
dummy_dki.name    = 'DKI';
% bvals_bvecs_exp_type.help    = {'Specify the b-values (bval) and b-vectors of the N dMRI volumes.'
%     'Note: The expression should contain a 4 x N vector, where b-values should appear in the same order as the source images were entered (b-values: 1 x N, b-vectors: (2-4) x N). b-values are expected in units of s/mm^2.'};
dummy_dki.val  = {bvals_dki};

% registration type
dummy_dti_or_dki         = cfg_choice;
dummy_dti_or_dki.tag     = 'dummy_dti_or_dki';
dummy_dti_or_dki.name    = 'Select if model fit is performed via DTI or DKI';
dummy_dti_or_dki.help    = {''};
% ecmoco_dummy_type.labels = {'Volume-wise (3D)','Slice-wise (2D)', 'Volume-wise (3D) + Slice-wise (2D)'};
dummy_dti_or_dki.values = {dummy_dti, dummy_dki};
% dummy_dti_or_dki.val    = {''};

% noddi dti
acid_noddidti      = cfg_exbranch;
acid_noddidti.tag  = 'acid_noddidti';
acid_noddidti.name = 'NODDI-DTI';
acid_noddidti.val  = {FA MD EVALS mask dummy_dti_or_dki dummy_invivo_type};
acid_noddidti.help = {
                    'This function extracts biophysical tissue properties from DTI data, using the NODDI-DTI model.'
                    'Note that NODDI-DTI is defined only in white matter, and should be applied only to healthy white matter.' 
                    'Reference:'
                    ''
};
acid_noddidti.prog = @local_acid_noddidti;
acid_noddidti.vout = @vout_acid_noddidti;

end

function out = local_acid_noddidti(job)

if isfield(job.dummy_dti_or_dki,'dummy_dti')
    bvals =  job.dummy_dti_or_dki.dummy_dti.bvals;
else
    bvals = [];
end



if isfield(job.dummy_invivo_type,'dummy_invivo_exp')
    job.dummy_invivo =  job.dummy_invivo_type.dummy_invivo_exp;
    D_a = '';
    D_iso = '';
else
    D_a__D_iso =  job.dummy_invivo_type.dummy_invivo_manual_exp;
    D_a = D_a__D_iso(1);
    D_iso = D_a__D_iso(2);
    job.dummy_invivo = 50;
end






    [Vkappa,Vicvf,VODI] = acid_noddidti(bvals,char(job.FA),char(job.MD),char(job.EVALS),char(job.mask),job.dummy_invivo,D_a,D_iso);
    out.kappa = {Vkappa.fname};
    out.icvf  = {Vicvf.fname};
    out.odi  = {VODI.fname};
end

function dep = vout_acid_noddidti(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Kappa image';
    dep(1).src_output = substruct('.','kappa');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'ODI image';
    dep(2).src_output = substruct('.','odi');
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'AWF image';
    dep(3).src_output = substruct('.','icvf');
    dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end