function MD = acid_noddidti_icvf2MD(ficvf,fiso)

% Forward relationship between ficvf and fiso output from NODDI and MD 
% output from a diffusion tensor fit using a relationship discovered via 
% cumulant expansion of the NODDI signal model.
%
% luke.edwards@ucl.ac.uk

    [d,diso] = acid_noddidti_fixedParams();
    MD = (1/3).*((-1).*d.*(3+2.*((-2)+ficvf).*ficvf).*((-1)+fiso)+3.*diso.*fiso);

end