function kappa = acid_noddidti_tau2kappa(tauk)

    kappa = 0*tauk;

    % Avoid for loop for voxels that would fail a priori
    failTau = (tauk<(1/3))|(tauk>1);
    kappa(failTau) = -1;
    otherTau = tauk(~failTau);
    otherKappa = 0*otherTau;

    for n = 1:length(otherTau)
        try
            otherKappa(n) = fzero(@(kappa) acid_noddidti_tau(kappa)-otherTau(n),[1/3,1e2]);
        catch
            otherKappa(n) = NaN;
        end
    end

    kappa(~failTau) = otherKappa;
    kappa = reshape(kappa,size(tauk));

end