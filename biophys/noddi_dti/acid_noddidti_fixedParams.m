function [d, d_iso] = acid_noddidti_fixedParams(dummy_invivo,D_a,D_iso)

% =========================================================================
% Definitions of in vivo fixed parameters used by NODDI.
%
% luke.edwards@ucl.ac.uk & S.Mohammadi
% 25/05/2020
% =========================================================================

    if ~exist('dummy_invivo','var')
        dummy_invivo = 1;
    end

    if dummy_invivo == 1
        d     = 1.7e-3;
        d_iso = 3.0e-3; % CSF compartment
    elseif dummy_invivo == 0 % ex vivo parameters as provided in the noddi toolbox
        d     = 0.6e-3;
        d_iso = 2.0e-3;
    else
        d     = D_a;
        d_iso = D_iso;
    end
end