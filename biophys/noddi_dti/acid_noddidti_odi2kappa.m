function kappa = odi2kappa(odi)
    kappa = cot(odi*pi/2);
end