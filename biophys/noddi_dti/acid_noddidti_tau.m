function t = acid_noddidti_tau(kappa)

% =========================================================================
% Relationship between the two dispersion parameters kappa and tau.
%
% luke.edwards@ucl.ac.uk
% =========================================================================

    % bound determined through trial and error
    low_kappa = (abs(kappa)<1e-12);

    t(~low_kappa)=0.5*(-1./kappa(~low_kappa)...
        +1./(sqrt(kappa(~low_kappa)).*acid_noddidti_dawson(sqrt(kappa(~low_kappa)))));

    % analytical limit
    t(low_kappa)=1/3;

end