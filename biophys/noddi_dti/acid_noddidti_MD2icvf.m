function ficvf = acid_noddidti_MD2icvf(MD, fiso, correctionFlag, bvals, L1,L2,L3,dummy_invivo)

% =========================================================================
% Inversion of the relationship between ficvf and fiso output from NODDI 
% and MD output from a diffusion tensor fit using a relationship discovered
% via cumulant expansion of the NODDI signal model.
%
% luke.edwards@ucl.ac.uk 
% S.Mohammadi added the option to switch between in ivvo and ex vivo
% diffusivities.
% 25/05/2020
% =========================================================================

    [d,diso] = acid_noddidti_fixedParams(dummy_invivo,D_a,D_iso);

    if exist('correctionFlag','var') && ~isempty(bvals)
        if ~isempty(correctionFlag)
            MD = acid_noddidti_correctMD(bvals, L1, L2, L3);
        end
    end

    % from Mathematica
    if all(fiso==0)
        ficvf = 1-sqrt(0.5*(3.*MD./d-1));
    else
        ficvf = 1+(-1).*2.^(-1/2).*(d.*((-1)+fiso).*(d+(-3)*MD+(-1).*d.*fiso+3.*diso.*fiso).^(-1)).^(-1/2);
        ficvf = ficvf./(1-fiso);
    end

    % Check icvf reasonable
    erricvf = (real(ficvf)<0)|(real(ficvf)>1)|(abs(imag(ficvf))>1e-12)|isnan(ficvf);

    % Error codes
    ficvf(erricvf)=-1;

    errMD=(MD>d)|(MD<d/3);
    ficvf(errMD)=-0.5;

    ficvf=real(ficvf);

end