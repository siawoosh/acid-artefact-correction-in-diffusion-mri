function Vo = acid_noddidti_kappa2tau(file_prefix)

% =========================================================================
% Converts the dispersion measure kappa output by NODDI to the dispersion 
% measure tau.
%
% luke.edwards@ucl.ac.uk
% =========================================================================

    disp(file_prefix)
    FNkappa = [file_prefix, '_kappa.nii'];

    Vtau = spm_vol(FNkappa);
    Vtau.fname = [file_prefix,'_tau.nii'];

    Vo = spm_imcalc(FNkappa,Vtau,'tau(10*i1)'); % 10 because NODDI returns scaled kappa

end