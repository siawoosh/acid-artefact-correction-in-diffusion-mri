function MD = acid_noddidti_correctMD(bvals,L1,L2,L3,MK)

    % Heuristic correction of MD for the effects of the mean kurtosis.
    % 
    % L1, L2, and L3 are the eigenvalues of the diffusion tensor calculated
    % using data with a b-value of b. MK is an estimate of the mean kurtosis; 
    % if left unspecified, MK is assumed to be unity.
    %
    % luke.edwards@ucl.ac.uk
    
    b = max(bvals);

    MD0 = (L1+L2+L3)/3;
    MD2 = acid_noddidti_MDsquared(L1,L2,L3);

    if ~exist('MK','var')
        MK = 1;
    end

    correction = b/6.* MD2.* MK;
    MD = MD0 + correction;

end