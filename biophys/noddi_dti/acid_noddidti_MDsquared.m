function MD2 = acid_noddidti_MDsquared(L1, L2, L3)
    MD2 = (L1.^2 + L2.^2 + L3.^2)/5 + 2*(L1.*L2 + L1.*L3 + L2.*L3)./15;
end