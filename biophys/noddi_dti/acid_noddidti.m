function [VKAPPA, Vicvf, VODI] = acid_noddidti(bvals, P_FA, P_MD, P_EVALS, P_mask, dummy_invivo,D_a,D_iso)

% =========================================================================
% This is a wrapper for the NODDI-DTI tools written by Luke Edwards, as
% described in Edwards et al., 2017. The NODDI-DTI wrapper estimates a kappa
% and icvf map from DTI data.
%
% Input:
%   bvals        - b values of the dMRI data
%   P_FA         - filename of the FA map
%   P_MD         - filename of the MD map
%   P_EVALS      - filenames of the eigenvalue maps
%   P_mask       - filename of the mask
%   dummy_invivo - whether in vivo or ex vivo diffusivities are used (false or true)
%   p_out        - output directory
%
% Output: 
%   VKAPPA - structure file corresponding to kappa image
%   Vicvf  - structure file corresponding to icvf image
%
% L. Edwards and S. Mohammadi (12.2017)
% S. Mohammadi added the option to have diffusivities for ex vivo (05.2020)
% =========================================================================

% resampling method (7th order polynomial)
res = -7;

% set flag
if (~isempty(bvals) && max(bvals)>0)
    correctionFlag = true;
else
    correctionFlag = false;
end

% no isotropic compartment
fiso = 0;
 
% read in files by taking the FA image as spatial reference
VG = spm_vol(P_FA);

% define output directory
keyword = 'NODDI-DTI';
[path,fname,~] = spm_fileparts(VG.fname);

p_out = acid_bids(path,fname,keyword,1);
diary([p_out filesep 'logfile_' keyword '.txt'])

% load in mask
if ~isempty(P_mask)
    mask = acid_read_vols(spm_vol(P_mask),VG,res);
else
    mask = ones(VG.dim);
end
mask_idx = find(mask>0);

% load in and mask maps
FA = mask.*acid_read_vols(spm_vol(P_FA),VG,res);
MD = mask.*acid_read_vols(spm_vol(P_MD),VG,res);
EVALS = spm_vol(P_EVALS);
L1 = mask.*acid_read_vols(EVALS(1),VG,res);
L2 = mask.*acid_read_vols(EVALS(2),VG,res);
L3 = mask.*acid_read_vols(EVALS(3),VG,res);
    
Atau      = acid_noddidti_MDFA2tau(MD,FA,zeros(size(FA)),dummy_invivo,D_a,D_iso);
taulook   = 1/3:0.01:1;
kappaLook = acid_noddidti_tau2kappa(taulook);
ytau      = Atau(mask_idx);
ykappa    = zeros(numel(ytau),1);
Aficvf    = acid_noddidti_MD2icvf(MD, fiso, correctionFlag, bvals, L1, L2, L3, dummy_invivo,D_a,D_iso);

for ii = 1:numel(ytau)
   [~,xval]   = min(abs(ytau(ii)-taulook));
   ykappa(ii) = kappaLook(xval);
end

Akappa = zeros(size(Atau));
Akappa(mask_idx) = ykappa;
AODI = acid_kappa_to_odi(Akappa(mask_idx),mask_idx,VG);
% write out kappa and icvf maps
dummy_float = 1;    
VG.fname = [VG.fname(1:end-11) '_map.nii'];


VKAPPA = acid_write_vol(Akappa, VG, p_out, [keyword '-KAPPA'], '_map', '', '', dummy_float);
VODI   = acid_write_vol(AODI, VG, p_out, [keyword '-ODI'], '_map', '', '', dummy_float);
Vicvf  = acid_write_vol(Aficvf, VG, p_out, [keyword '-AWF'], '_map', '', '', dummy_float);
diary off    
end