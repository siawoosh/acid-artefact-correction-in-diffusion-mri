function acid = tbx_cfg_acid_redirect
% Diversion config file for the ACID toolbox
%
% PURPOSE
% The present ACID config file redirects towards the full implementation of
% the toolbox (acid_cfg_main) only if present in the Matlab path. The
% toolbox can therefore be stored in a directory independent from the SPM
% implementation and synch'd with the main ACID-Toolbox repository whenever
% needed. If acid_cfg_main is not found in the Matlab path, the ACID Tools
% are listed in the SPM Batch GUI but not available. A brief help section
% provides the user with instructions for ACID Tools installation.
%
% USAGE
% Copy this file into a directory in the SPM toolbox directory (e.g.
% <path-to-SPM>/toolbox/ACID-Redirect). Add the ACID-Toolbox directory
% (containing the full implementation of the toolbox) to the Matlab path.
% Restart SPM and the Batch GUI. The ACID Tools will be available in the
% SPM>Tools menu.
%
% Warning and disclaimer: This software is for research use only.
% Do not use it for clinical or diagnostic purposes.
%
%__________________________________________________________________________
% Cyclotron Research Centre - University of Liège
% Evelyne Balteau - April 2017
% Modified for ACID Björn Fricke - May 2022
%==========================================================================

if ~isdeployed, addpath(fileparts(mfilename('fullpath'))); end

try
    % ACID is available
    acid = tbx_cfg_acid;
catch %#ok<CTCH>
    % No ACID toolbox found
    acid         = cfg_exbranch;
    acid.tag     = 'ACID';
    acid.name    = 'ACID Tools - not available!';
    acid.help    = {
        ['The ACID toolbox does not seem to be available on this computer. ',...
        'The directory containing the toolbox implementation should be ',...
        'in the Matlab path to be used in SPM. See installation ',...
        'instructions on the ACID-Toolbox Wiki.']
        }';
    acid.val  = {};
end

end
